*** Settings ***
Documentation    Test Cases to validate all Study Event Types Controller endpoints authorization for Support role

Library     ../../TestCaseMethods/StudyEventTypesControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py
Library  ../../TestCaseMethods/_CommonStudiesTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***
${super_admin_token}
${test_support_token}
${study_event_type_id}
${test_study_event_type_id}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${support}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    set common studies token  ${super_admin_token}
    change selected facility   ${mmt_fac_id}
    create test user  ${support}
    ${study_event_type_id}=  create test study event type
    set suite variable  ${study_event_type_id}


Multiple Teardown Methods
    delete test user    ${support}
    RUN KEYWORD IF  $test_study_event_type_id is not None    delete study event type   ${test_study_event_type_id}
    RUN KEYWORD IF  $study_event_type_id is not None    delete study event type        ${study_event_type_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Study Event Types Controller for Support
    initialize access for study event type controller   ${support}

Login with Support
    ${test_support_token}=  test user login by role  ${support}  ${None}
    set suite variable  ${test_support_token}
    set token study event types   ${test_support_token}

# STUDY EVENT TYPES CONTROLLER
TC 007: Support's access to Post StudyEventTypes
    [Tags]      Support StudyEventTypesController   Post
    ${test_study_event_type_id}=  post study event types tc          ${None}     ${None}     TC 007
    set suite variable   ${test_study_event_type_id}
    Log   ${test_study_event_type_id}

TC 008: Support's access to Get StudyEventTypes
    [Tags]      Support StudyEventTypesController   Get
    get study event types   TC 008

TC 009: Support's access to Get StudyEventTypes with ID
    [Tags]      Support StudyEventTypesController   Get?id=
    get study event types by id   ${study_event_type_id}    TC 009

TC 010: Support's access to Patch StudyEventTypes
    [Tags]      Support StudyEventTypesController   Patch
    patch study event types   ${study_event_type_id}    TC 010

TC 011: Support's access to Put StudyEventTypes
    [Tags]      Support StudyEventTypesController   Put
    put study event types    ${study_event_type_id}     TC 011

TC 012: Support's access to Delete StudyEventTypes
    [Tags]      Support StudyEventTypesController   Delete
    delete study event types    ${study_event_type_id}      TC 012


