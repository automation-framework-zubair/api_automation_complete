*** Settings ***
Documentation    Test Cases to validate all Study Event Types Controller endpoints authorization for Office Personnel role

Library     ../../TestCaseMethods/StudyEventTypesControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py
Library  ../../TestCaseMethods/_CommonStudiesTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***
${super_admin_token}
${test_office_personnel_token}
${study_event_type_id}
${test_study_event_type_id}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${office_personnel}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    set common studies token  ${super_admin_token}
    change selected facility   ${mmt_fac_id}
    create test user  ${office_personnel}
    ${study_event_type_id}=  create test study event type
    set suite variable  ${study_event_type_id}


Multiple Teardown Methods
    delete test user    ${office_personnel}
    RUN KEYWORD IF  $test_study_event_type_id is not None    delete study event type   ${test_study_event_type_id}
    RUN KEYWORD IF  $study_event_type_id is not None    delete study event type        ${study_event_type_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Study Event Types Controller for Office Personnel
    initialize access for study event type controller   ${office_personnel}

Login with Office Personnel
    ${test_office_personnel_token}=  test user login by role  ${office_personnel}  ${None}
    set suite variable  ${test_office_personnel_token}
    set token study event types   ${test_office_personnel_token}

# STUDY EVENT TYPES CONTROLLER
TC 043: Office Personnel's access to Post StudyEventTypes
    [Tags]      OfficePersonnel StudyEventTypesController   Post
    ${test_study_event_type_id}=  post study event types tc     ${None}     ${None}     TC 043
    set suite variable   ${test_study_event_type_id}
    Log   ${test_study_event_type_id}

TC 044: Office Personnel's access to Get StudyEventTypes
    [Tags]      OfficePersonnel StudyEventTypesController   Get
    get study event types   TC 044

TC 045: Office Personnel's access to Get StudyEventTypes with ID
    [Tags]      OfficePersonnel StudyEventTypesController   Get?id=
    get study event types by id   ${study_event_type_id}    TC 045

TC 046: Office Personnel's access to Patch StudyEventTypes
    [Tags]      OfficePersonnel StudyEventTypesController   Patch
    patch study event types   ${study_event_type_id}    TC 046

TC 047: Office Personnel's access to Put StudyEventTypes
    [Tags]      OfficePersonnel StudyEventTypesController   Put
    put study event types    ${study_event_type_id}     TC 047

TC 048: Office Personnel's access to Delete StudyEventTypes
    [Tags]      OfficePersonnel StudyEventTypesController   Delete
    delete study event types    ${study_event_type_id}      TC 048


