*** Settings ***
Documentation    Test Cases to validate all Study Event Types Controller endpoints authorization for Production role

Library     ../../TestCaseMethods/StudyEventTypesControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py
Library  ../../TestCaseMethods/_CommonStudiesTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***
${super_admin_token}
${test_production_token}
${study_event_type_id}
${test_study_event_type_id}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${production}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    set common studies token  ${super_admin_token}
    change selected facility   ${mmt_fac_id}
    create test user  ${production}
    ${study_event_type_id}=  create test study event type
    set suite variable  ${study_event_type_id}


Multiple Teardown Methods
    delete test user    ${production}
    RUN KEYWORD IF  $test_study_event_type_id is not None    delete study event type   ${test_study_event_type_id}
    RUN KEYWORD IF  $study_event_type_id is not None    delete study event type        ${study_event_type_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Study Event Types Controller for Production
    initialize access for study event type controller   ${production}

Login with Production
    ${test_production_token}=  test user login by role  ${production}  ${None}
    set suite variable  ${test_production_token}
    set token study event types   ${test_production_token}

# STUDY EVENT TYPES CONTROLLER
TC 013: Production's access to Post StudyEventTypes
    [Tags]      Production StudyEventTypesController   Post
    ${test_study_event_type_id}=  post study event types tc     ${None}     ${None}     TC 013
    set suite variable   ${test_study_event_type_id}
    Log   ${test_study_event_type_id}

TC 014: Production's access to Get StudyEventTypes
    [Tags]      Production StudyEventTypesController   Get
    get study event types   TC 014

TC 015: Production's access to Get StudyEventTypes with ID
    [Tags]      Production StudyEventTypesController   Get?id=
    get study event types by id   ${study_event_type_id}    TC 015

TC 016: Production's access to Patch StudyEventTypes
    [Tags]      Production StudyEventTypesController   Patch
    patch study event types   ${study_event_type_id}    TC 016

TC 017: Production's access to Put StudyEventTypes
    [Tags]      Production StudyEventTypesController   Put
    put study event types    ${study_event_type_id}     TC 017

TC 018: Production's access to Delete StudyEventTypes
    [Tags]      Production StudyEventTypesController   Delete
    delete study event types    ${study_event_type_id}      TC 018


