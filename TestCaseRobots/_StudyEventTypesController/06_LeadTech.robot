*** Settings ***
Documentation    Test Cases to validate all Study Event Types Controller endpoints authorization for Lead Tech role

Library     ../../TestCaseMethods/StudyEventTypesControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py
Library  ../../TestCaseMethods/_CommonStudiesTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***
${super_admin_token}
${test_lead_tech_token}
${study_event_type_id}
${test_study_event_type_id}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${lead_tech}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    set common studies token  ${super_admin_token}
    change selected facility   ${mmt_fac_id}
    create test user  ${lead_tech}
    ${study_event_type_id}=  create test study event type
    set suite variable  ${study_event_type_id}


Multiple Teardown Methods
    delete test user    ${lead_tech}
    RUN KEYWORD IF  $test_study_event_type_id is not None    delete study event type   ${test_study_event_type_id}
    RUN KEYWORD IF  $study_event_type_id is not None    delete study event type        ${study_event_type_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Study Event Types Controller for Lead Tech
    initialize access for study event type controller   ${lead_tech}

Login with Lead Tech
    ${test_lead_tech_token}=  test user login by role  ${lead_tech}  ${None}
    set suite variable  ${test_lead_tech_token}
    set token study event types   ${test_lead_tech_token}

# STUDY EVENT TYPES CONTROLLER
TC 031: Lead Tech's access to Post StudyEventTypes
    [Tags]      LeadTech StudyEventTypesController   Post
    ${test_study_event_type_id}=  post study event types tc     ${None}     ${None}     TC 031
    set suite variable   ${test_study_event_type_id}
    Log   ${test_study_event_type_id}

TC 032: Lead Tech's access to Get StudyEventTypes
    [Tags]      LeadTech StudyEventTypesController   Get
    get study event types   TC 032

TC 033: Lead Tech's access to Get StudyEventTypes with ID
    [Tags]      LeadTech StudyEventTypesController   Get?id=
    get study event types by id   ${study_event_type_id}    TC 033

TC 034: Lead Tech's access to Patch StudyEventTypes
    [Tags]      LeadTech StudyEventTypesController   Patch
    patch study event types   ${study_event_type_id}    TC 034

TC 035: Lead Tech's access to Put StudyEventTypes
    [Tags]      LeadTech StudyEventTypesController   Put
    put study event types    ${study_event_type_id}     TC 035

TC 036: Lead Tech's access to Delete StudyEventTypes
    [Tags]      LeadTech StudyEventTypesController   Delete
    delete study event types    ${study_event_type_id}      TC 036
