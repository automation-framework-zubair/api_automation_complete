*** Settings ***
Documentation    Test Cases to validate all Study Event Types Controller endpoints authorization for Review Doctor role

Library     ../../TestCaseMethods/StudyEventTypesControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py
Library  ../../TestCaseMethods/_CommonStudiesTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***
${super_admin_token}
${test_review_doctor_token}
${study_event_type_id}
${test_study_event_type_id}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${review_doctor}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    set common studies token  ${super_admin_token}
    change selected facility   ${mmt_fac_id}
    create test user  ${review_doctor}
    ${study_event_type_id}=  create test study event type
    set suite variable  ${study_event_type_id}


Multiple Teardown Methods
    delete test user    ${review_doctor}
    RUN KEYWORD IF  $test_study_event_type_id is not None    delete study event type   ${test_study_event_type_id}
    RUN KEYWORD IF  $study_event_type_id is not None    delete study event type        ${study_event_type_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Study Event Types Controller for Review Doctor
    initialize access for study event type controller   ${review_doctor}

Login with Review Doctor
    ${test_review_doctor_token}=  test user login by role  ${review_doctor}  ${None}
    set suite variable  ${test_review_doctor_token}
    set token study event types   ${test_review_doctor_token}

# STUDY EVENT TYPES CONTROLLER
TC 025: Review Doctor's access to Post StudyEventTypes
    [Tags]      ReviewDoctor StudyEventTypesController   Post
    ${test_study_event_type_id}=  post study event types tc     ${None}     ${None}     TC 025
    set suite variable   ${test_study_event_type_id}
    Log   ${test_study_event_type_id}

TC 026: Review Doctor's access to Get StudyEventTypes
    [Tags]      ReviewDoctor StudyEventTypesController   Get
    get study event types   TC 026

TC 027: Review Doctor's access to Get StudyEventTypes with ID
    [Tags]      ReviewDoctor StudyEventTypesController   Get?id=
    get study event types by id   ${study_event_type_id}    TC 027

TC 028: Review Doctor's access to Patch StudyEventTypes
    [Tags]      ReviewDoctor StudyEventTypesController   Patch
    patch study event types   ${study_event_type_id}    TC 028

TC 029: Review Doctor's access to Put StudyEventTypes
    [Tags]      ReviewDoctor StudyEventTypesController   Put
    put study event types    ${study_event_type_id}     TC 029

TC 030: Review Doctor's access to Delete StudyEventTypes
    [Tags]      ReviewDoctor StudyEventTypesController   Delete
    delete study event types    ${study_event_type_id}      TC 030


