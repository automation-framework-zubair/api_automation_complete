*** Settings ***
Documentation    Test Cases to validate all Study Event Types Controller endpoints authorization for Facility Admin role

Library     ../../TestCaseMethods/StudyEventTypesControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py
Library  ../../TestCaseMethods/_CommonStudiesTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***
${super_admin_token}
${test_support_token}
${study_event_type_id}
${test_study_event_type_id}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${facility_admin}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    set common studies token  ${super_admin_token}
    change selected facility   ${mmt_fac_id}
    create test user  ${facility_admin}
    ${study_event_type_id}=  create test study event type
    set suite variable  ${study_event_type_id}


Multiple Teardown Methods
    delete test user    ${facility_admin}
    RUN KEYWORD IF  $test_study_event_type_id is not None    delete study event type   ${test_study_event_type_id}
    RUN KEYWORD IF  $study_event_type_id is not None    delete study event type        ${study_event_type_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Study Event Types Controller for Facility Admin
    initialize access for study event type controller   ${facility_admin}

Login with Facility Admin
    ${test_support_token}=  test user login by role  ${facility_admin}  ${None}
    set suite variable  ${test_support_token}
    set token study event types   ${test_support_token}

# STUDY EVENT TYPES CONTROLLER
TC 019: Facility Admin's access to Post StudyEventTypes
    [Tags]      FacilityAdmin StudyEventTypesController   Post
    ${test_study_event_type_id}=  post study event types tc     ${None}     ${None}     TC 019
    set suite variable   ${test_study_event_type_id}
    Log   ${test_study_event_type_id}

TC 020: Facility Admin's access to Get StudyEventTypes
    [Tags]      FacilityAdmin StudyEventTypesController   Get
    get study event types   TC 020

TC 021: Facility Admin's access to Get StudyEventTypes with ID
    [Tags]      FacilityAdmin StudyEventTypesController   Get?id=
    get study event types by id   ${study_event_type_id}    TC 021

TC 022: Facility Admin's access to Patch StudyEventTypes
    [Tags]      FacilityAdmin StudyEventTypesController   Patch
    patch study event types   ${study_event_type_id}    TC 022

TC 023: Facility Admin's access to Put StudyEventTypes
    [Tags]      FacilityAdmin StudyEventTypesController   Put
    put study event types    ${study_event_type_id}     TC 023

TC 024: Facility Admin's access to Delete StudyEventTypes
    [Tags]      FacilityAdmin StudyEventTypesController   Delete
    delete study event types    ${study_event_type_id}      TC 024
