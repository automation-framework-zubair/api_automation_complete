*** Settings ***
Documentation    Test Cases to validate all Study Event Types Controller endpoints authorization for Super Admin role

Library     ../../TestCaseMethods/StudyEventTypesControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py
Library  ../../TestCaseMethods/_CommonStudiesTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***
${super_admin_token}
${test_super_admin_token}
${study_event_type_id}
${test_study_event_type_id}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${super_admin}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    get_version_number  ${super_admin_token}
    set common studies token  ${super_admin_token}
    change selected facility   ${mmt_fac_id}
    create test user  ${super_admin}
    ${study_event_type_id}=  create test study event type
    set suite variable  ${study_event_type_id}


Multiple Teardown Methods
    delete test user    ${super_admin}
    RUN KEYWORD IF  $test_study_event_type_id is not None    delete study event type   ${test_study_event_type_id}
    RUN KEYWORD IF  $study_event_type_id is not None    delete study event type        ${study_event_type_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Study Event Types Controller for Super Admin
    initialize access for study event type controller   ${super_admin}

Login with SuperAdmin
    ${test_super_admin_token}=  test user login by role  ${super_admin}  ${None}
    set suite variable  ${test_super_admin_token}
    set token study event types   ${test_super_admin_token}

# STUDY EVENT TYPES CONTROLLER
TC 001: SuperAdmin's access to Post StudyEventTypes
    [Tags]      SuperAdmin StudyEventTypesController   Post
    ${test_study_event_type_id}=  post study event types tc     ${None}     ${None}     TC 001
    set suite variable   ${test_study_event_type_id}

TC 002: SuperAdmin's access to Get StudyEventTypes
    [Tags]      SuperAdmin StudyEventTypesController   Get
    get study event types   TC 002

TC 003: SuperAdmin's access to Get StudyEventTypes with ID
    [Tags]      SuperAdmin StudyEventTypesController   Get?id=
    get study event types by id   ${study_event_type_id}    TC 003

TC 004: SuperAdmin's access to Patch StudyEventTypes
    [Tags]      SuperAdmin StudyEventTypesController   Patch
    patch study event types   ${study_event_type_id}    TC 004

TC 005: SuperAdmin's access to Put StudyEventTypes
    [Tags]      SuperAdmin StudyEventTypesController   Put
    put study event types    ${study_event_type_id}     TC 005

TC 006: SuperAdmin's access to Delete StudyEventTypes
    [Tags]      SuperAdmin StudyEventTypesController   Delete
    delete study event types    ${study_event_type_id}      TC 006


