*** Settings ***
Documentation    Test Cases to validate all Study Event Types Controller endpoints authorization for Field Tech role

Library     ../../TestCaseMethods/StudyEventTypesControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py
Library  ../../TestCaseMethods/_CommonStudiesTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***
${super_admin_token}
${test_field_tech_token}
${study_event_type_id}
${test_study_event_type_id}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${field_tech}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    set common studies token  ${super_admin_token}
    change selected facility   ${mmt_fac_id}
    create test user  ${field_tech}
    ${study_event_type_id}=  create test study event type
    set suite variable  ${study_event_type_id}


Multiple Teardown Methods
    delete test user    ${field_tech}
    RUN KEYWORD IF  $test_study_event_type_id is not None    delete study event type   ${test_study_event_type_id}
    RUN KEYWORD IF  $study_event_type_id is not None    delete study event type        ${study_event_type_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Study Event Types Controller for Field Tech
    initialize access for study event type controller   ${field_tech}

Login with Field Tech
    ${test_field_tech_token}=  test user login by role  ${field_tech}  ${None}
    set suite variable  ${test_field_tech_token}
    set token study event types   ${test_field_tech_token}

# STUDY EVENT TYPES CONTROLLER
TC 037: Field Tech's access to Post StudyEventTypes
    [Tags]      FieldTech StudyEventTypesController   Post
    ${test_study_event_type_id}=  post study event types tc     ${None}     ${None}     TC 037
    set suite variable   ${test_study_event_type_id}
    Log   ${test_study_event_type_id}

TC 038: Field Tech's access to Get StudyEventTypes
    [Tags]      FieldTech StudyEventTypesController   Get
    get study event types       TC 038

TC 039: Field Tech's access to Get StudyEventTypes with ID
    [Tags]      FieldTech StudyEventTypesController   Get?id=
    get study event types by id   ${study_event_type_id}    TC 039

TC 040: Field Tech's access to Patch StudyEventTypes
    [Tags]      FieldTech StudyEventTypesController   Patch
    patch study event types   ${study_event_type_id}    TC 040

TC 041: Field Tech's access to Put StudyEventTypes
    [Tags]      FieldTech StudyEventTypesController   Put
    put study event types    ${study_event_type_id}     TC 041

TC 042: Field Tech's access to Delete StudyEventTypes
    [Tags]      FieldTech StudyEventTypesController   Delete
    delete study event types    ${study_event_type_id}      TC 042


