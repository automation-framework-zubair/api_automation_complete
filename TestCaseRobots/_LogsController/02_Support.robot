*** Settings ***
Documentation    Test Cases to validate all Logs Controller endpoints authorization for Support role

Library  ../../TestCaseMethods/LogsControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***

${super_admin_token}
${test_support_token}
${test_device_id}
${created_device_log_id}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${support}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    change selected facility  ${MMT_FAC_ID}
    ${test_device_id}=  create test device  ${mmt_fac_id}
    set suite variable  ${test_device_id}
    ${created_device_log_id}=  create a device log  ${test_device_id}  ${mmt_fac_id}
    set suite variable  ${created_device_log_id}
    create test user  ${support}


Multiple Teardown Methods
    delete test user    ${support}
    delete test device    ${test_device_id}
#    RUN KEYWORD IF  $created_user_id is not None     delete user by id    ${created_user_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Logs Controller for Support
    initialize access for logs controller  ${support}

Login with Support
    ${test_support_token}=   test user login by role     ${support}    ${None}
    set suite variable  ${test_support_token}
    set token logs  ${test_support_token}

# LOGS CONTROLLER
TC 010: Support's access to Add Device Log Item
    [Tags]    Support LogsController      AddDeviceLogItem
    post add device log item  ${test_device_id}  ${mmt_fac_id}      TC 010

TC 011: Support's access to Get Facility Logs by Facility ID
    [Tags]    Support LogsController      GetFacilityLogs
    get facility logs  ${MMT_FAC_ID}                TC 011

TC 012: Support's access to Get Device Logs by Device ID
    [Tags]    Support LogsController      GetDeviceLogs
    get device logs  ${test_device_id}  100             TC 012

TC 013: Support's access to Get Log by Log ID
    [Tags]    Support LogsController      GetLogById
    get log by id  ${created_device_log_id}             TC 013

TC 014: Support's access to Get Logs
    [Tags]    Support LogsController      GetLogs
    get logs                            TC 014

TC 015: Support's access to Post Log
    [Tags]    Support LogsController      Post
    post log  ${test_device_id}  ${mmt_fac_id}              TC 015

TC 016: Support's access to Patch Log
    [Tags]    Support LogsController      Patch
    patch log  ${created_device_log_id}  ${test_device_id}  ${mmt_fac_id}           TC 016

TC 017: Support's access to Put Log
    [Tags]    Support LogsController      Put
    put log  ${created_device_log_id}  ${test_device_id}  ${mmt_fac_id}         TC 017

TC 018: Support's access to Delete Log
    [Tags]    Support LogsController      Delete
    delete log  ${created_device_log_id}            TC 018
