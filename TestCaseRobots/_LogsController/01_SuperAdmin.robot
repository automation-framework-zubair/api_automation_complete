*** Settings ***
Documentation    Test Cases to validate all Logs Controller endpoints authorization for Super Admin role

Library  ../../TestCaseMethods/LogsControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***

${super_admin_token}
${test_super_admin_token}
${test_device_id}
${created_device_log_id}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${super_admin}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    get_version_number  ${super_admin_token}
    change selected facility  ${MMT_FAC_ID}
    ${test_device_id}=  create test device  ${mmt_fac_id}
    set suite variable  ${test_device_id}
    ${created_device_log_id}=  create a device log  ${test_device_id}  ${mmt_fac_id}
    set suite variable  ${created_device_log_id}
    create test user  ${super_admin}


Multiple Teardown Methods
    delete test user    ${super_admin}
    delete test device    ${test_device_id}
#    RUN KEYWORD IF  $created_user_id is not None     delete user by id    ${created_user_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Logs Controller for Super Admin
    initialize access for logs controller  ${super_admin}

Login with Super Admin
    ${test_super_admin_token}=   test user login by role     ${super_admin}    ${None}
    set suite variable  ${test_super_admin_token}
    set token logs  ${test_super_admin_token}

# LOGS CONTROLLER
TC 001: SuperAdmin's access to Add Device Log Item
    [Tags]    SuperAdmin LogsController      AddDeviceLogItem
    post add device log item  ${test_device_id}  ${mmt_fac_id}      TC 001

TC 002: SuperAdmin's access to Get Facility Logs by Facility ID
    [Tags]    SuperAdmin LogsController      GetFacilityLogs
    get facility logs  ${MMT_FAC_ID}            TC 002

TC 003: SuperAdmin's access to Get Device Logs by Device ID
    [Tags]    SuperAdmin LogsController      GetDeviceLogs
    get device logs  ${test_device_id}  100         TC 003

TC 004: SuperAdmin's access to Get Log by Log ID
    [Tags]    SuperAdmin LogsController      GetLogById
    get log by id  ${created_device_log_id}         TC 004

TC 005: SuperAdmin's access to Get Logs
    [Tags]    SuperAdmin LogsController      GetLogs
    get logs            TC 005

TC 006: SuperAdmin's access to Post Log
    [Tags]    SuperAdmin LogsController      Post
    post log  ${test_device_id}  ${mmt_fac_id}          TC 006

TC 007: SuperAdmin's access to Patch Log
    [Tags]    SuperAdmin LogsController      Patch
    patch log  ${created_device_log_id}  ${test_device_id}  ${mmt_fac_id}       TC 007

TC 008: SuperAdmin's access to Put Log
    [Tags]    SuperAdmin LogsController      Put
    put log  ${created_device_log_id}  ${test_device_id}  ${mmt_fac_id}         TC 008

TC 009: SuperAdmin's access to Delete Log
    [Tags]    SuperAdmin LogsController      Delete
    delete log  ${created_device_log_id}            TC 009
