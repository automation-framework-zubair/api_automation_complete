*** Settings ***
Documentation    Test Cases to validate all Logs Controller endpoints authorization for Office Personnel role

Library  ../../TestCaseMethods/LogsControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***

${super_admin_token}
${test_office_personnel_token}
${test_device_id}
${created_device_log_id}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${office_personnel}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    change selected facility  ${MMT_FAC_ID}
    ${test_device_id}=  create test device  ${mmt_fac_id}
    set suite variable  ${test_device_id}
    ${created_device_log_id}=  create a device log  ${test_device_id}  ${mmt_fac_id}
    set suite variable  ${created_device_log_id}
    create test user  ${office_personnel}


Multiple Teardown Methods
    delete test user    ${office_personnel}
    delete test device    ${test_device_id}
#    RUN KEYWORD IF  $created_user_id is not None     delete user by id    ${created_user_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Logs Controller for Office Personnel
    initialize access for logs controller  ${office_personnel}

Login with Office Personnel
    ${test_office_personnel_token}=   test user login by role     ${office_personnel}    ${None}
    set suite variable  ${test_office_personnel_token}
    set token logs  ${test_office_personnel_token}

# LOGS CONTROLLER
TC 064: Office Personnel's access to Add Device Log Item
    [Tags]    OfficePersonnel LogsController      AddDeviceLogItem
    post add device log item  ${test_device_id}  ${mmt_fac_id}          TC 064

TC 065: Office Personnel's access to Get Facility Logs by Facility ID
    [Tags]    OfficePersonnel LogsController      GetFacilityLogs
    get facility logs  ${MMT_FAC_ID}            TC 065

TC 066: Office Personnel's access to Get Device Logs by Device ID
    [Tags]    OfficePersonnel LogsController      GetDeviceLogs
    get device logs  ${test_device_id}  100         TC 066

TC 067: Office Personnel's access to Get Log by Log ID
    [Tags]    OfficePersonnel LogsController      GetLogById
    get log by id  ${created_device_log_id}         TC 067

TC 068: Office Personnel's access to Get Logs
    [Tags]    OfficePersonnel LogsController      GetLogs
    get logs            TC 068

TC 069: Office Personnel's access to Post Log
    [Tags]    OfficePersonnel LogsController      Post
    post log  ${test_device_id}  ${mmt_fac_id}          TC 069

TC 070: Office Personnel's access to Patch Log
    [Tags]    OfficePersonnel LogsController      Patch
    patch log  ${created_device_log_id}  ${test_device_id}  ${mmt_fac_id}           TC 070

TC 071: Office Personnel's access to Put Log
    [Tags]    OfficePersonnel LogsController      Put
    put log  ${created_device_log_id}  ${test_device_id}  ${mmt_fac_id}         TC 071

TC 072: Office Personnel's access to Delete Log
    [Tags]    OfficePersonnel LogsController      Delete
    delete log  ${created_device_log_id}            TC 072
