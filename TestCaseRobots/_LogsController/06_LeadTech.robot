*** Settings ***
Documentation    Test Cases to validate all Logs Controller endpoints authorization for Lead Tech role

Library  ../../TestCaseMethods/LogsControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***

${super_admin_token}
${test_lead_tech_token}
${test_device_id}
${created_device_log_id}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${lead_tech}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    change selected facility  ${MMT_FAC_ID}
    ${test_device_id}=  create test device  ${mmt_fac_id}
    set suite variable  ${test_device_id}
    ${created_device_log_id}=  create a device log  ${test_device_id}  ${mmt_fac_id}
    set suite variable  ${created_device_log_id}
    create test user  ${lead_tech}


Multiple Teardown Methods
    delete test user    ${lead_tech}
    delete test device    ${test_device_id}
#    RUN KEYWORD IF  $created_user_id is not None     delete user by id    ${created_user_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Logs Controller for Lead Tech
    initialize access for logs controller  ${lead_tech}

Login with Lead Tech
    ${test_lead_tech_token}=   test user login by role     ${lead_tech}    ${None}
    set suite variable  ${test_lead_tech_token}
    set token logs  ${test_lead_tech_token}

# LOGS CONTROLLER
TC 046: Lead Tech's access to Add Device Log Item
    [Tags]    LeadTech LogsController      AddDeviceLogItem
    post add device log item  ${test_device_id}  ${mmt_fac_id}          TC 046

TC 047: Lead Tech's access to Get Facility Logs by Facility ID
    [Tags]    LeadTech LogsController      GetFacilityLogs
    get facility logs  ${MMT_FAC_ID}            TC 047

TC 048: Lead Tech's access to Get Device Logs by Device ID
    [Tags]    LeadTech LogsController      GetDeviceLogs
    get device logs  ${test_device_id}  100         TC 048

TC 049: Lead Tech's access to Get Log by Log ID
    [Tags]    LeadTech LogsController      GetLogById
    get log by id  ${created_device_log_id}         TC 049

TC 050: Lead Tech's access to Get Logs
    [Tags]    LeadTech LogsController      GetLogs
    get logs            TC 050

TC 051: Lead Tech's access to Post Log
    [Tags]    LeadTech LogsController      Post
    post log  ${test_device_id}  ${mmt_fac_id}      TC 051

TC 052: Lead Tech's access to Patch Log
    [Tags]    LeadTech LogsController      Patch
    patch log  ${created_device_log_id}  ${test_device_id}  ${mmt_fac_id}           TC 052

TC 053: Lead Tech's access to Put Log
    [Tags]    LeadTech LogsController      Put
    put log  ${created_device_log_id}  ${test_device_id}  ${mmt_fac_id}         TC 053

TC 054: Lead Tech's access to Delete Log
    [Tags]    LeadTech LogsController      Delete
    delete log  ${created_device_log_id}                TC 054
