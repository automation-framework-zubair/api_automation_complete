*** Settings ***
Documentation    Test Cases to validate all Logs Controller endpoints authorization for Facility Admin role

Library  ../../TestCaseMethods/LogsControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***

${super_admin_token}
${test_facility_admin_token}
${test_device_id}
${created_device_log_id}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${facility_admin}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    change selected facility  ${MMT_FAC_ID}
    ${test_device_id}=  create test device  ${mmt_fac_id}
    set suite variable  ${test_device_id}
    ${created_device_log_id}=  create a device log  ${test_device_id}  ${mmt_fac_id}
    set suite variable  ${created_device_log_id}
    create test user  ${facility_admin}


Multiple Teardown Methods
    delete test user    ${facility_admin}
    delete test device    ${test_device_id}
#    RUN KEYWORD IF  $created_user_id is not None     delete user by id    ${created_user_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Logs Controller for Facility Admin
    initialize access for logs controller  ${facility_admin}

Login with Facility Admin
    ${test_facility_admin_token}=   test user login by role     ${facility_admin}    ${None}
    set suite variable  ${test_facility_admin_token}
    set token logs  ${test_facility_admin_token}

# LOGS CONTROLLER
TC 028: Facility Admin's access to Add Device Log Item
    [Tags]    FacilityAdmin LogsController      AddDeviceLogItem
    post add device log item  ${test_device_id}  ${mmt_fac_id}              TC 028

TC 029: Facility Admin's access to Get Facility Logs by Facility ID
    [Tags]    FacilityAdmin LogsController      GetFacilityLogs
    get facility logs  ${MMT_FAC_ID}                        TC 029

TC 030: Facility Admin's access to Get Device Logs by Device ID
    [Tags]    FacilityAdmin LogsController      GetDeviceLogs
    get device logs  ${test_device_id}  100             TC 030

TC 031: Facility Admin's access to Get Log by Log ID
    [Tags]    FacilityAdmin LogsController      GetLogById
    get log by id  ${created_device_log_id}             TC 031

TC 032: Facility Admin's access to Get Logs
    [Tags]    FacilityAdmin LogsController      GetLogs
    get logs                        TC 032

TC 033: Facility Admin's access to Post Log
    [Tags]    FacilityAdmin LogsController      Post
    post log  ${test_device_id}  ${mmt_fac_id}              TC 033

TC 034: Facility Admin's access to Patch Log
    [Tags]    FacilityAdmin LogsController      Patch
    patch log  ${created_device_log_id}  ${test_device_id}  ${mmt_fac_id}           TC 034

TC 035: Facility Admin's access to Put Log
    [Tags]    FacilityAdmin LogsController      Put
    put log  ${created_device_log_id}  ${test_device_id}  ${mmt_fac_id}         TC 035

TC 036: Facility Admin's access to Delete Log
    [Tags]    FacilityAdmin LogsController      Delete
    delete log  ${created_device_log_id}            TC 036
