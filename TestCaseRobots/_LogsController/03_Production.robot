*** Settings ***
Documentation    Test Cases to validate all Logs Controller endpoints authorization for Production role

Library  ../../TestCaseMethods/LogsControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***

${super_admin_token}
${test_production_token}
${test_device_id}
${created_device_log_id}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${production}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    change selected facility  ${MMT_FAC_ID}
    ${test_device_id}=  create test device  ${mmt_fac_id}
    set suite variable  ${test_device_id}
    ${created_device_log_id}=  create a device log  ${test_device_id}  ${mmt_fac_id}
    set suite variable  ${created_device_log_id}
    create test user  ${production}


Multiple Teardown Methods
    delete test user    ${production}
    delete test device    ${test_device_id}
#    RUN KEYWORD IF  $created_user_id is not None     delete user by id    ${created_user_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Logs Controller for Production
    initialize access for logs controller  ${production}

Login with Production
    ${test_production_token}=   test user login by role     ${production}    ${None}
    set suite variable  ${test_production_token}
    set token logs  ${test_production_token}

# LOGS CONTROLLER
TC 019: Production's access to Add Device Log Item
    [Tags]    Production LogsController      AddDeviceLogItem
    post add device log item  ${test_device_id}  ${mmt_fac_id}          TC 019

TC 020: Production's access to Get Facility Logs by Facility ID
    [Tags]    Production LogsController      GetFacilityLogs
    get facility logs  ${MMT_FAC_ID}                TC 020

TC 021: Production's access to Get Device Logs by Device ID
    [Tags]    Production LogsController      GetDeviceLogs
    get device logs  ${test_device_id}  100             TC 021

TC 022: Production's access to Get Log by Log ID
    [Tags]    Production LogsController      GetLogById
    get log by id  ${created_device_log_id}             TC 022

TC 023: Production's access to Get Logs
    [Tags]    Production LogsController      GetLogs
    get logs                            TC 023

TC 024: Production's access to Post Log
    [Tags]    Production LogsController      Post
    post log  ${test_device_id}  ${mmt_fac_id}              TC 024

TC 025: Production's access to Patch Log
    [Tags]    Production LogsController      Patch
    patch log  ${created_device_log_id}  ${test_device_id}  ${mmt_fac_id}           TC 025

TC 026: Production's access to Put Log
    [Tags]    Production LogsController      Put
    put log  ${created_device_log_id}  ${test_device_id}  ${mmt_fac_id}             TC 026

TC 027: Production's access to Delete Log
    [Tags]    Production LogsController      Delete
    delete log  ${created_device_log_id}                TC 027
