*** Settings ***
Documentation    Test Cases to validate all Logs Controller endpoints authorization for Review Doctor role

Library  ../../TestCaseMethods/LogsControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***

${super_admin_token}
${test_review_doctor_token}
${test_device_id}
${created_device_log_id}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${review_doctor}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    change selected facility  ${MMT_FAC_ID}
    ${test_device_id}=  create test device  ${mmt_fac_id}
    set suite variable  ${test_device_id}
    ${created_device_log_id}=  create a device log  ${test_device_id}  ${mmt_fac_id}
    set suite variable  ${created_device_log_id}
    create test user  ${review_doctor}


Multiple Teardown Methods
    delete test user    ${review_doctor}
    delete test device    ${test_device_id}
#    RUN KEYWORD IF  $created_user_id is not None     delete user by id    ${created_user_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Logs Controller for Review Doctor
    initialize access for logs controller  ${review_doctor}

Login with Review Doctor
    ${test_review_doctor_token}=   test user login by role     ${review_doctor}    ${None}
    set suite variable  ${test_review_doctor_token}
    set token logs  ${test_review_doctor_token}

# LOGS CONTROLLER
TC 037: Review Doctor's access to Add Device Log Item
    [Tags]    ReviewDoctor LogsController      AddDeviceLogItem
    post add device log item  ${test_device_id}  ${mmt_fac_id}          TC 037

TC 038: Review Doctor's access to Get Facility Logs by Facility ID
    [Tags]    ReviewDoctor LogsController      GetFacilityLogs
    get facility logs  ${MMT_FAC_ID}            TC 038

TC 039: Review Doctor's access to Get Device Logs by Device ID
    [Tags]    ReviewDoctor LogsController      GetDeviceLogs
    get device logs  ${test_device_id}  100         TC 039

TC 040: Review Doctor's access to Get Log by Log ID
    [Tags]    ReviewDoctor LogsController      GetLogById
    get log by id  ${created_device_log_id}         TC 040

TC 041: Review Doctor's access to Get Logs
    [Tags]    ReviewDoctor LogsController      GetLogs
    get logs                TC 041

TC 042: Review Doctor's access to Post Log
    [Tags]    ReviewDoctor LogsController      Post
    post log  ${test_device_id}  ${mmt_fac_id}          TC 042

TC 043: Review Doctor's access to Patch Log
    [Tags]    ReviewDoctor LogsController      Patch
    patch log  ${created_device_log_id}  ${test_device_id}  ${mmt_fac_id}       TC 043

TC 044: Review Doctor's access to Put Log
    [Tags]    ReviewDoctor LogsController      Put
    put log  ${created_device_log_id}  ${test_device_id}  ${mmt_fac_id}         TC 044

TC 045: Review Doctor's access to Delete Log
    [Tags]    ReviewDoctor LogsController      Delete
    delete log  ${created_device_log_id}                TC 045
