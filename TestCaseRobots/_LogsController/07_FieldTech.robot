*** Settings ***
Documentation    Test Cases to validate all Logs Controller endpoints authorization for Field Tech role

Library  ../../TestCaseMethods/LogsControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***

${super_admin_token}
${test_field_tech_token}
${test_device_id}
${created_device_log_id}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${field_tech}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    change selected facility  ${MMT_FAC_ID}
    ${test_device_id}=  create test device  ${mmt_fac_id}
    set suite variable  ${test_device_id}
    ${created_device_log_id}=  create a device log  ${test_device_id}  ${mmt_fac_id}
    set suite variable  ${created_device_log_id}
    create test user  ${field_tech}


Multiple Teardown Methods
    delete test user    ${field_tech}
    delete test device    ${test_device_id}
#    RUN KEYWORD IF  $created_user_id is not None     delete user by id    ${created_user_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Logs Controller for Field Tech
    initialize access for logs controller  ${field_tech}

Login with Field Tech
    ${test_field_tech_token}=   test user login by role     ${field_tech}    ${None}
    set suite variable  ${test_field_tech_token}
    set token logs  ${test_field_tech_token}

# LOGS CONTROLLER
TC 055: Field Tech's access to Add Device Log Item
    [Tags]    FieldTech LogsController      AddDeviceLogItem
    post add device log item  ${test_device_id}  ${mmt_fac_id}          TC 055

TC 056: Field Tech's access to Get Facility Logs by Facility ID
    [Tags]    FieldTech LogsController      GetFacilityLogs
    get facility logs  ${MMT_FAC_ID}            TC 056

TC 057: Field Tech's access to Get Device Logs by Device ID
    [Tags]    FieldTech LogsController      GetDeviceLogs
    get device logs  ${test_device_id}  100         TC 057

TC 058: Field Tech's access to Get Log by Log ID
    [Tags]    FieldTech LogsController      GetLogById
    get log by id  ${created_device_log_id}         TC 058

TC 059: Field Tech's access to Get Logs
    [Tags]    FieldTech LogsController      GetLogs
    get logs                TC 059

TC 060: Field Tech's access to Post Log
    [Tags]    FieldTech LogsController      Post
    post log  ${test_device_id}  ${mmt_fac_id}              TC 060

TC 061: Field Tech's access to Patch Log
    [Tags]    FieldTech LogsController      Patch
    patch log  ${created_device_log_id}  ${test_device_id}  ${mmt_fac_id}           TC 061

TC 062: Field Tech's access to Put Log
    [Tags]    FieldTech LogsController      Put
    put log  ${created_device_log_id}  ${test_device_id}  ${mmt_fac_id}             TC 062

TC 063: Field Tech's access to Delete Log
    [Tags]    FieldTech LogsController      Delete
    delete log  ${created_device_log_id}                TC 063
