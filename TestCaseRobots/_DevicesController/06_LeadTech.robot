*** Settings ***
Documentation    Test Cases to validate all Devices Controller endpoints authorization for Lead Tech role

Library     ../../TestCaseMethods/DevicesControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***
${super_admin_token}
${test_lead_tech_token}
${created_device_id}
${created_by_post_id}
${test_device_id}
${test_device_sn}

*** Keywords ***
Multiple Setup Methods
    delete test user     ${lead_tech}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    change selected facility    ${mmt_fac_id}
    create test user  ${lead_tech}
    ${test_device_id}=  create test device  ${mmt_fac_id}
    set suite variable  ${test_device_id}
    ${test_device_sn}=  get test device by sn  ${test_device_id}
    set suite variable  ${test_device_sn}

Multiple Teardown Methods
    delete test user    ${lead_tech}
    delete test device    ${test_device_id}
    RUN KEYWORD IF  $created_device_id is not None    delete test device    ${created_device_id}
    RUN KEYWORD IF  $created_by_post_id is not None   delete test device   ${created_by_post_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Device Controller for Lead Tech
    initialize access for devices controller  ${lead_tech}

Login with Lead Tech
    ${test_lead_tech_token}=  test user login by role  ${lead_tech}  ${None}
    set suite variable   ${test_lead_tech_token}
    set token devices    ${test_lead_tech_token}

#DEVICE CONTROLLER
TC 041: Lead Tech's access to create device in MMT facility
    [Tags]     LeadTech DevicesController        CreateDevice
    ${created_device_id}=  post create device tc  ${none}  ${none}  ${none}  ${none}  ${none}  TC 041
    set suite variable  ${created_device_id}

TC 042: Lead Tech's access to Get By SN
    [Tags]      LeadTech DevicesController       GetBySn
    get devices by sn  ${test_device_sn}    TC 042

TC 043: Lead Tech's access to Get Devices
    [Tags]      LeadTech DevicesController       GetDevices
    get devices     TC 043

TC 044: Lead Tech's access to Get Devices with ID
    [Tags]      LeadTech DevicesController       Get?id=
    get devices by id  ${test_device_id}    TC 044

TC 045: Lead Tech's access to Post Device with ID
    [Tags]      LeadTech DevicesController       Post
    ${created_by_post_id}=   post devices tc    TC 045
    set suite variable  ${created_by_post_id}

TC 046: Lead Tech's access to Patch Device with ID
    [Tags]      LeadTech DevicesController       Patch
    patch devices  ${test_device_id}    TC 046

TC 047: Lead Tech's access to Put Device with ID
    [Tags]      LeadTech DevicesController       Put
    put devices  ${test_device_id}  TC 047

TC 048: Lead Tech's access to Delete Device
    [Tags]     LeadTech DevicesController        Delete
    delete delete devices  ${test_device_id}    TC 048