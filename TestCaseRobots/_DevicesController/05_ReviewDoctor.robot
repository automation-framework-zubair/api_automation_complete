*** Settings ***
Documentation    Test Cases to validate all Devices Controller endpoints authorization for Review Doctor role

Library     ../../TestCaseMethods/DevicesControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***
${super_admin_token}
${test_review_doctor_token}
${created_device_id}
${created_by_post_id}
${test_device_id}
${test_device_sn}

*** Keywords ***
Multiple Setup Methods
    delete test user     ${review_doctor}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    change selected facility   ${mmt_fac_id}
    create test user  ${review_doctor}
    ${test_device_id}=  create test device  ${mmt_fac_id}
    set suite variable  ${test_device_id}
    ${test_device_sn}=  get test device by sn  ${test_device_id}
    set suite variable  ${test_device_sn}

Multiple Teardown Methods
    delete test user    ${review_doctor}
    delete test device  ${test_device_id}
    RUN KEYWORD IF  $created_device_id is not None    delete test device    ${created_device_id}
    RUN KEYWORD IF  $created_by_post_id is not None   delete test device   ${created_by_post_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Device Controller for Review Doctor
    initialize access for devices controller  ${review_doctor}

Login with Review Doctor
    ${test_review_doctor_token}=  test user login by role  ${review_doctor}  ${None}
    set suite variable  ${test_review_doctor_token}
    set token devices   ${test_review_doctor_token}

#DEVICE CONTROLLER
TC 033: Review Doctor's access to create device in MMT facility
    [Tags]     ReviewDoctor DevicesController        CreateDevice
    ${created_device_id}=  post create device tc  ${none}  ${none}  ${none}  ${none}  ${none}  TC 033
    set suite variable  ${created_device_id}

TC 034: Review Doctor's access to Get By SN
    [Tags]      ReviewDoctor DevicesController       GetBySn
    get devices by sn   ${test_device_sn}   TC 034

TC 035: Review Doctor's access to Get Devices
    [Tags]      ReviewDoctor DevicesController       GetDevices
    get devices     TC 035

TC 036: Review Doctor's access to Get Devices with ID
    [Tags]      ReviewDoctor DevicesController       Get?id=
    get devices by id  ${test_device_id}    TC 036

TC 037: Review Doctor's access to Post Device with ID
    [Tags]      ReviewDoctor DevicesController       Post
    ${created_by_post_id}=   post devices tc    TC 037
    set suite variable  ${created_by_post_id}

TC 038: Review Doctor's access to Patch Device with ID
    [Tags]      ReviewDoctor DevicesController       Patch
    patch devices  ${test_device_id}    TC 038

TC 039: Review Doctor's access to Put Device with ID
    [Tags]      ReviewDoctor DevicesController       Put
    put devices  ${test_device_id}      TC 039

TC 040: Review Doctor's access to Delete Device
    [Tags]     ReviewDoctor DevicesController        Delete
    delete delete devices  ${test_device_id}    TC 040