*** Settings ***
Documentation    Test Cases to validate all Devices Controller endpoints authorization for Office Personal role

Library     ../../TestCaseMethods/DevicesControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***
${super_admin_token}
${test_office_personal_token}
${created_device_id}
${created_by_post_id}
${test_device_id}
${test_device_sn}

*** Keywords ***
Multiple Setup Methods
    delete test user     ${office_personnel}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    change selected facility   ${mmt_fac_id}
    create test user  ${office_personnel}
    ${test_device_id}=  create test device  ${mmt_fac_id}
    set suite variable  ${test_device_id}
    ${test_device_sn}=  get test device by sn  ${test_device_id}
    set suite variable  ${test_device_sn}

Multiple Teardown Methods
    delete test user    ${office_personnel}
    delete test device    ${test_device_id}
    RUN KEYWORD IF  $created_device_id is not None    delete test device    ${created_device_id}
    RUN KEYWORD IF  $created_by_post_id is not None   delete test device   ${created_by_post_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Device Controller for Office Personalf
    initialize access for devices controller  ${office_personnel}

Login with Office Personal
    ${test_office_personal_token}=  test user login by role  ${office_personnel}  ${None}
    set suite variable  ${test_office_personal_token}
    set token devices   ${test_office_personal_token}

#DEVICE CONTROLLER
TC 057: Office Personal's access to create device in MMT facility
    [Tags]     OfficePersonnel DevicesController        CreateDevice
    ${created_device_id}=  post create device tc  ${none}  ${none}  ${none}  ${none}  ${none}  TC 057
    set suite variable  ${created_device_id}

TC 058: Office Office Personnel's access to Get By SN
    [Tags]      OfficePersonnel DevicesController       GetBySn
    get devices by sn   ${test_device_sn}   TC 058

TC 059: Office Office Personnel's access to Get Devices
    [Tags]      OfficePersonnel DevicesController       GetDevices
    get devices     TC 059

TC 060: Office Office Personnel's access to Get Devices with ID
    [Tags]      OfficePersonnel DevicesController       Get?id=
    get devices by id  ${test_device_id}    TC 060

TC 061: Office Office Personnel's access to Post Device with ID
    [Tags]      OfficePersonnel DevicesController       Post
    ${created_by_post_id}=   post devices tc    TC 061
    set suite variable  ${created_by_post_id}

TC 062: Office Office Personnel's access to Patch Device with ID
    [Tags]      OfficePersonnel DevicesController       Patch
    patch devices  ${test_device_id}    TC 062

TC 063: Office Office Personnel's access to Put Device with ID
    [Tags]      OfficePersonnel DevicesController       Put
    put devices  ${test_device_id}  TC 063

TC 064: Office Office Personnel's access to Delete Device
    [Tags]     OfficePersonnel DevicesController        Delete
    delete delete devices  ${test_device_id}    TC 064