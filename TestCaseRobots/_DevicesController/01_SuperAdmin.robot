*** Settings ***
Documentation    Test Cases to validate all Devices Controller endpoints authorization for Super Admin role

Library     ../../TestCaseMethods/DevicesControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***
${super_admin_token}
${test_super_admin_token}
${created_device_id}
${created_by_post_id}
${test_device_id}
${test_device_sn}


*** Keywords ***
Multiple Setup Methods
    delete test user    ${super_admin}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    get_version_number  ${super_admin_token}
    change selected facility   ${mmt_fac_id}
    create test user  ${super_admin}
    ${test_device_id}=  create test device   ${mmt_fac_id}
    set suite variable   ${test_device_id}
    ${test_device_sn}=   get test device by sn    ${test_device_id}
    set suite variable   ${test_device_sn}


Multiple Teardown Methods
    delete test user    ${super_admin}
    delete test device  ${test_device_id}
    RUN KEYWORD IF  $created_device_id is not None    delete test device    ${created_device_id}
    RUN KEYWORD IF  $created_by_post_id is not None   delete test device   ${created_by_post_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Device Controller for Super Admin
    initialize access for devices controller  ${super_admin}

Login with SuperAdmin
    ${test_super_admin_token}=  test user login by role  ${super_admin}  ${None}
    set suite variable  ${test_super_admin_token}
    set token devices  ${test_super_admin_token}

#DEVICE CONTROLLER
TC 001: SuperAdmin's access to create device in MMT facility
    [Tags]     SuperAdmin DevicesController        CreateDevice
    ${created_device_id}=  post create device tc  ${none}  ${none}  ${none}  ${none}  ${none}  TC 001
    set suite variable  ${created_device_id}
    Log  ${created_device_id}

TC 002: SuperAdmin's access to Get By SN
    [Tags]      SuperAdmin DevicesController       GetBySn
    get devices by sn   ${test_device_sn}   TC 002

TC 003: SuperAdmin's access to Get Devices
    [Tags]      SuperAdmin DevicesController       GetDevices
    get devices     TC 003

TC 004: SuperAdmin's access to Get Devices with ID
    [Tags]      SuperAdmin DevicesController       Get?id=
    get devices by id  ${test_device_id}    TC 004

TC 005: SuperAdmin's access to Post Device with ID
    [Tags]      SuperAdmin DevicesController       Post
    ${created_by_post_id}=   post devices tc    TC 005
    set suite variable  ${created_by_post_id}

TC 006: SuperAdmin's access to Patch Device with ID
    [Tags]      SuperAdmin DevicesController       Patch
    patch devices   ${None}   TC 006

TC 007: SuperAdmin's access to Put Device with ID
    [Tags]      SuperAdmin DevicesController       Put
    put devices   ${None}     TC 007

TC 008: SuperAdmin's access to Delete Device
    [Tags]     SuperAdmin DevicesController        Delete
    delete delete devices  ${test_device_id}    TC 008