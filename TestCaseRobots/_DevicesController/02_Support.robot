*** Settings ***
Documentation    Test Cases to validate all Devices Controller endpoints authorization for Support role

Library     ../../TestCaseMethods/DevicesControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***
${super_admin_token}
${test_support_token}
${created_device_id}
${created_by_post_id}
${test_device_id}
${test_device_sn}

*** Keywords ***
Multiple Setup Methods
    delete test user     ${support}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    change selected facility   ${mmt_fac_id}
    create test user  ${support}
    ${test_device_id}=  create test device  ${mmt_fac_id}
    set suite variable  ${test_device_id}
    ${test_device_sn}=  get test device by sn  ${test_device_id}
    set suite variable  ${test_device_sn}

Multiple Teardown Methods
    delete test user    ${support}
    delete test device    ${test_device_id}
    RUN KEYWORD IF  $created_device_id is not None    delete test device    ${created_device_id}
    RUN KEYWORD IF  $created_by_post_id is not None   delete test device   ${created_by_post_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Device Controller for Support
    initialize access for devices controller  ${support}

Login with Support
    ${test_support_token}=  test user login by role  ${support}  ${None}
    set suite variable  ${test_support_token}
    set token devices  ${test_support_token}

#DEVICE CONTROLLER
TC 009: Support's access to create device in MMT facility
    [Tags]     Support DevicesController        CreateDevice
    ${created_device_id}=  post create device tc  ${none}  ${none}  ${none}  ${none}  ${none}  TC 009
    set suite variable  ${created_device_id}

TC 010: Support's access to Get By SN
    [Tags]      Support DevicesController       GetBySn
    get devices by sn  ${test_device_sn}    TC 010

TC 011: Support's access to Get Devices
    [Tags]      Support DevicesController       GetDevices
    get devices     TC 011

TC 012: Support's access to Get Devices with ID
    [Tags]      Support DevicesController       Get?id=
    get devices by id  ${test_device_id}    TC 012

TC 013: Support's access to Post Device with ID
    [Tags]      Support DevicesController       Post
    ${created_by_post_id}=   post devices tc    TC 013
    set suite variable  ${created_by_post_id}

TC 014: Support's access to Patch Device with ID
    [Tags]      Support DevicesController       Patch
    patch devices  ${test_device_id}    TC 014

TC 015: Support's access to Put Device with ID
    [Tags]      Support DevicesController       Put
    put devices  ${test_device_id}      TC 015

TC 016: Support's access to Delete Device
    [Tags]     Support DevicesController        Delete
    delete delete devices  ${test_device_id}    TC 016