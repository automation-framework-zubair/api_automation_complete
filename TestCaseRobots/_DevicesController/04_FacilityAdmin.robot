*** Settings ***
Documentation    Test Cases to validate all Devices Controller endpoints authorization for Facility Admin role

Library     ../../TestCaseMethods/DevicesControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***
${super_admin_token}
${test_facility_admin_token}
${created_device_id}
${created_by_post_id}
${test_device_id}
${test_device_sn}

*** Keywords ***
Multiple Setup Methods
    delete test user     ${facility_admin}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    change selected facility   ${mmt_fac_id}
    create test user  ${facility_admin}
    ${test_device_id}=  create test device  ${mmt_fac_id}
    set suite variable  ${test_device_id}
    ${test_device_sn}=  get test device by sn  ${test_device_id}
    set suite variable  ${test_device_sn}

Multiple Teardown Methods
    delete test user    ${facility_admin}
    delete test device  ${test_device_id}
    RUN KEYWORD IF  $created_device_id is not None    delete test device    ${created_device_id}
    RUN KEYWORD IF  $created_by_post_id is not None   delete test device   ${created_by_post_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Device Controller for Facility Admin
    initialize access for devices controller  ${facility_admin}

Login with Facility Admin
    ${test_facility_admin_token}=  test user login by role  ${facility_admin}  ${None}
    set suite variable  ${test_facility_admin_token}
    set token devices   ${test_facility_admin_token}

#DEVICE CONTROLLER
TC 025: Facility Admin's access to device in MMT facility
    [Tags]     FacilityAdmin DevicesController        CreateDevice
    ${created_device_id}=  post create device tc  ${none}  ${none}  ${none}  ${none}  ${none}  TC 025
    set suite variable  ${created_device_id}

TC 026: Facility Admin's access to Get By SN
    [Tags]      FacilityAdmin DevicesController       GetBySn
    get devices by sn   ${test_device_sn}   TC 026

TC 027: Facility Admin's access to Get Devices
    [Tags]      FacilityAdmin DevicesController       GetDevices
    get devices     TC 027

TC 028: Facility Admin's access to Get Devices with ID
    [Tags]      FacilityAdmin DevicesController       Get?id=
    get devices by id  ${test_device_id}    TC 028

TC 029: Facility Admin's access to Post Device with ID
    [Tags]      FacilityAdmin DevicesController       Post
    ${created_by_post_id}=   post devices tc    TC 029
    set suite variable  ${created_by_post_id}

TC 030: Facility Admin's access to Patch Device with ID
    [Tags]      FacilityAdmin DevicesController       Patch
    patch devices  ${test_device_id}    TC 030

TC 031: Facility Admin's access to Put Device with ID
    [Tags]      FacilityAdmin DevicesController       Put
    put devices  ${test_device_id}  TC 031

TC 032: Facility Admin's access to Delete Device
    [Tags]     FacilityAdmin DevicesController        Delete
    delete delete devices  ${test_device_id}    TC 032