*** Settings ***
Documentation    Test Cases to validate all Devices Controller endpoints authorization for Field Tech role

Library     ../../TestCaseMethods/DevicesControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***
${super_admin_token}
${test_field_tech_token}
${created_device_id}
${created_by_post_id}
${test_device_id}
${test_device_sn}

*** Keywords ***
Multiple Setup Methods
    delete test user     ${field_tech}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    change selected facility   ${mmt_fac_id}
    create test user  ${field_tech}
    ${test_device_id}=  create test device  ${mmt_fac_id}
    set suite variable  ${test_device_id}
    ${test_device_sn}=  get test device by sn  ${test_device_id}
    set suite variable  ${test_device_sn}

Multiple Teardown Methods
    delete test user    ${field_tech}
    delete test device    ${test_device_id}
    RUN KEYWORD IF  $created_device_id is not None    delete test device    ${created_device_id}
    RUN KEYWORD IF  $created_by_post_id is not None   delete test device   ${created_by_post_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Device Controller for Field Tech
    initialize access for devices controller  ${field_tech}

Login with Field Tech
    ${test_field_tech_token}=  test user login by role  ${field_tech}  ${None}
    set suite variable  ${test_field_tech_token}
    set token devices  ${test_field_tech_token}

#DEVICE CONTROLLER
TC 049: Field Tech's access to create device in MMT facility
    [Tags]     FieldTech DevicesController        CreateDevice
    ${created_device_id}=  post create device tc  ${none}  ${none}  ${none}  ${none}  ${none}  TC 049
    set suite variable  ${created_device_id}

TC 050: Field Tech's access to Get By SN
    [Tags]      FieldTech DevicesController       GetBySn
    get devices by sn   ${test_device_sn}   TC 050

TC 051: Field Tech's access to Get Devices
    [Tags]      FieldTech DevicesController       GetDevices
    get devices     TC 051

TC 052: Field Tech's access to Get Devices with ID
    [Tags]      FieldTech DevicesController       Get?id=
    get devices by id  ${test_device_id}    TC 052

TC 053: Field Tech's access to Post Device with ID
    [Tags]      FieldTech DevicesController       Post
    ${created_by_post_id}=   post devices tc    TC 053
    set suite variable  ${created_by_post_id}

TC 054: Field Tech's access to Patch Device with ID
    [Tags]      FieldTech DevicesController       Patch
    patch devices  ${test_device_id}    TC 054

TC 055: Field Tech's access to Put Device with ID
    [Tags]      FieldTech DevicesController       Put
    put devices  ${test_device_id}  TC 055

TC 056: Field Tech's access to Delete Device
    [Tags]     FieldTech DevicesController        Delete
    delete delete devices  ${test_device_id}    TC 056