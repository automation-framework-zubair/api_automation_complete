*** Settings ***
Documentation    Test Cases to validate all Devices Controller endpoints authorization for Production role

Library     ../../TestCaseMethods/DevicesControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***
${super_admin_token}
${test_production_token}
${created_device_id}
${created_by_post_id}
${test_device_id}
${test_device_sn}

*** Keywords ***
Multiple Setup Methods
    delete test user     ${production}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    change selected facility   ${mmt_fac_id}
    create test user  ${production}
    ${test_device_id}=  create test device  ${mmt_fac_id}
    set suite variable  ${test_device_id}
    ${test_device_sn}=  get test device by sn  ${test_device_id}
    set suite variable  ${test_device_sn}

Multiple Teardown Methods
    delete test user    ${production}
    delete test device  ${test_device_id}
    RUN KEYWORD IF  $created_device_id is not None    delete test device    ${created_device_id}
    RUN KEYWORD IF  $created_by_post_id is not None   delete test device   ${created_by_post_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Device Controller for Production
    initialize access for devices controller  ${production}

Login with Production
    ${test_production_token}=  test user login by role  ${production}  ${None}
    set suite variable  ${test_production_token}
    set token devices  ${test_production_token}

#DEVICE CONTROLLER
TC 017: Production's access to create device in MMT facility
    [Tags]     Production DevicesController        CreateDevice
    ${created_device_id}=  post create device tc  ${none}  ${none}  ${none}  ${none}  ${none}  TC 017
    set suite variable  ${created_device_id}

TC 018: Production's access to Get By SN
    [Tags]      Production DevicesController       GetBySn
    get devices by sn  ${test_device_sn}    TC 018

TC 019: Production's access to Get Devices
    [Tags]      Production DevicesController       GetDevices
    get devices     TC 019

TC 020: Production's access to Get Devices with ID
    [Tags]      Production DevicesController       Get?id=
    get devices by id  ${test_device_id}    TC 020

TC 021: Production's access to Post Device with ID
    [Tags]      Production DevicesController       Post
    ${created_by_post_id}=   post devices tc    TC 021
    set suite variable  ${created_by_post_id}

TC 022: Production's access to Patch Device with ID
    [Tags]      Production DevicesController       Patch
    patch devices  ${test_device_id}    TC 022

TC 023: Production's access to Put Device with ID
    [Tags]      Production DevicesController       Put
    put devices  ${test_device_id}      TC 023

TC 024: Production's access to Delete Device
    [Tags]     Production DevicesController        Delete
    delete delete devices  ${test_device_id}    TC 024