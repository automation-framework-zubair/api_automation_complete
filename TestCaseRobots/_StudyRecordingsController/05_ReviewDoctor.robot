*** Settings ***
Documentation    Test Cases to validate all StudyRecordings Controller endpoints authorization for ReviewDoctor role

Library     ../../TestCaseMethods/StudyRecordingsControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py
Library  ../../TestCaseMethods/_CommonStudiesTCs.py

Resource  ../global_robot_variables.robot


*** Variables ***
${super_admin_token}
${test_review_doctor_token}
${test_review_doctor_id}
${test_study_id}
${test_video_index}
${test_recording_index}
${test_camera_index}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${review_doctor}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    set common studies token  ${super_admin_token}
    change selected facility   ${mmt_fac_id}
    ${test_review_doctor_id}=  create test user  ${review_doctor}
    ${test_study_id}=  get video study id
    set suite variable   ${test_study_id}
    get test study information   ${test_study_id}
    ${test_video_index}=   get video index
    set suite variable   ${test_video_index}
    ${test_recording_index}=  get recording index
    set suite variable   ${test_recording_index}
    ${test_camera_index}=    get camera index
    set suite variable   ${test_camera_index}
    share test study   ${test_study_id}   ${test_review_doctor_id}


Multiple Teardown Methods
    delete test user    ${review_doctor}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Study Event Types Controller for ReviewDoctor
    initialize access for study recordings controller   ${review_doctor}

Login with ReviewDoctor
    ${test_review_doctor_id}=  test user login by role  ${review_doctor}  ${None}
    set suite variable  ${test_review_doctor_id}
    set token study recordings   ${test_review_doctor_id}

# STUDY RECORDINGS CONTROLLER
TC 021: Review Doctor's access to Get GetLastSyncPacketIndex
    [Tags]      ReviewDoctor StudyRecordingsController   GetLastSyncPacketIndex
    get last sync packet index   ${test_study_id}   ${test_recording_index}     TC 021

TC 022: Review Doctor's access to Get GetServerVideoSize
    [Tags]      ReviewDoctor StudyRecordingsController   GetServerVideoSize
    Log    ${test_camera_index}
    get server video size     ${test_study_id}    ${test_recording_index}    ${test_camera_index}    ${test_video_index}    TC 022

TC 023: Review Doctor's access to UpdateStudyRecordingNotes
    [Tags]      ReviewDoctor StudyRecordingsController   UpdateStudyRecordingNotes
    update study recording notes    ${test_study_id}   ${test_recording_index}      TC 023

TC 024: Review Doctor's access to Get GetStudyRecording
    [Tags]      ReviewDoctor StudyRecordingsController   GetStudyRecording
    get study recordings     ${test_study_id}    ${test_recording_index}        TC 024

TC 025: Review Doctor's access to Get GetMapping
    [Tags]      ReviewDoctor StudyRecordingsController   GetMapping
    get mappings          ${test_study_id}      ${test_recording_index}     TC 025