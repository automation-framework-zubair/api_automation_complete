*** Settings ***
Documentation    Test Cases to validate all StudyRecordings Controller endpoints authorization for SuperAdmin role

Library     ../../TestCaseMethods/StudyRecordingsControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py
Library  ../../TestCaseMethods/_CommonStudiesTCs.py

Resource  ../global_robot_variables.robot


*** Variables ***
${super_admin_token}
${test_super_admin_token}
${test_study_id}
${test_video_index}
${test_recording_index}
${test_camera_index}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${super_admin}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    get_version_number  ${super_admin_token}
    set common studies token  ${super_admin_token}
    change selected facility   ${mmt_fac_id}
    create test user  ${super_admin}
    ${test_study_id}=  get video study id
    set suite variable   ${test_study_id}
    get test study information   ${test_study_id}
    ${test_video_index}=   get video index
    set suite variable   ${test_video_index}
    ${test_recording_index}=  get recording index
    set suite variable   ${test_recording_index}
    ${test_camera_index}=    get camera index
    set suite variable   ${test_camera_index}


Multiple Teardown Methods
    delete test user    ${super_admin}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Study Event Types Controller for Super Admin
    initialize access for study recordings controller   ${super_admin}

Login with SuperAdmin
    ${test_super_admin_token}=  test user login by role  ${super_admin}  ${None}
    set suite variable  ${test_super_admin_token}
    set token study recordings   ${test_super_admin_token}

# STUDY RECORDINGS CONTROLLER
TC 001: SuperAdmin's access to Get GetLastSyncPacketIndex
    [Tags]      SuperAdmin StudyRecordingsController   GetLastSyncPacketIndex
    get last sync packet index   ${test_study_id}   ${test_recording_index}     TC 001

TC 002: SuperAdmin's access to Get GetServerVideoSize
    [Tags]      SuperAdmin StudyRecordingsController   GetServerVideoSize
    Log    ${test_camera_index}
    get server video size     ${test_study_id}    ${test_recording_index}    ${test_camera_index}    ${test_video_index}    TC 002

TC 003: SuperAdmin's access to Post UpdateStudyRecordingNotes
    [Tags]      SuperAdmin StudyRecordingsController   UpdateStudyRecordingNotes
    update study recording notes    ${test_study_id}   ${test_recording_index}      TC 003

TC 004: SuperAdmin's access to Get GetStudyRecording
    [Tags]      SuperAdmin StudyRecordingsController   GetStudyRecording
    get study recordings     ${test_study_id}    ${test_recording_index}    TC 004

TC 005: SuperAdmin's access to Get GetMapping
    [Tags]      SuperAdmin StudyRecordingsController   GetMapping
    get mappings          ${test_study_id}      ${test_recording_index}     TC 005