*** Settings ***
Documentation    Test Cases to validate all StudyRecordings Controller endpoints authorization for Support role

Library     ../../TestCaseMethods/StudyRecordingsControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py
Library  ../../TestCaseMethods/_CommonStudiesTCs.py

Resource  ../global_robot_variables.robot


*** Variables ***
${super_admin_token}
${test_support_token}
${test_study_id}
${test_video_index}
${test_recording_index}
${test_camera_index}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${support}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    set common studies token  ${super_admin_token}
    change selected facility   ${mmt_fac_id}
    create test user  ${support}
    ${test_study_id}=  get video study id
    set suite variable   ${test_study_id}
    get test study information   ${test_study_id}
    ${test_video_index}=   get video index
    set suite variable   ${test_video_index}
    ${test_recording_index}=  get recording index
    set suite variable   ${test_recording_index}
    ${test_camera_index}=    get camera index
    set suite variable   ${test_camera_index}


Multiple Teardown Methods
    delete test user    ${support}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Study Event Types Controller for Support
    initialize access for study recordings controller   ${support}

Login with Support
    ${test_support_token}=  test user login by role  ${support}  ${None}
    set suite variable  ${test_support_token}
    set token study recordings   ${test_support_token}

# STUDY RECORDINGS CONTROLLER
TC 006: Support's access to Get GetLastSyncPacketIndex
    [Tags]      Support StudyRecordingsController   GetLastSyncPacketIndex
    get last sync packet index   ${test_study_id}   ${test_recording_index}     TC 006

TC 007: Support's access to Get GetServerVideoSize
    [Tags]      Support StudyRecordingsController   GetServerVideoSize
    Log    ${test_camera_index}
    get server video size     ${test_study_id}    ${test_recording_index}    ${test_camera_index}    ${test_video_index}    TC 007

TC 008: Support's access to Post UpdateStudyRecordingNotes
    [Tags]      Support StudyRecordingsController   UpdateStudyRecordingNotes
    update study recording notes    ${test_study_id}   ${test_recording_index}      TC 008

TC 009: Support's access to Get GetStudyRecording
    [Tags]      Support StudyRecordingsController   GetStudyRecording
    get study recordings     ${test_study_id}    ${test_recording_index}    TC 009

TC 010: Support's access to Get GetMapping
    [Tags]      Support StudyRecordingsController   GetMapping
    get mappings          ${test_study_id}      ${test_recording_index}     TC 010