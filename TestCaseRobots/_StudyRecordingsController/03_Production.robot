*** Settings ***
Documentation    Test Cases to validate all StudyRecordings Controller endpoints authorization for Production role

Library     ../../TestCaseMethods/StudyRecordingsControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py
Library  ../../TestCaseMethods/_CommonStudiesTCs.py

Resource  ../global_robot_variables.robot


*** Variables ***
${super_admin_token}
${test_production_token}
${test_study_id}
${test_video_index}
${test_recording_index}
${test_camera_index}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${production}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    set common studies token  ${super_admin_token}
    change selected facility   ${mmt_fac_id}
    create test user  ${production}
    ${test_study_id}=  get video study id
    set suite variable   ${test_study_id}
    get test study information   ${test_study_id}
    ${test_video_index}=   get video index
    set suite variable   ${test_video_index}
    ${test_recording_index}=  get recording index
    set suite variable   ${test_recording_index}
    ${test_camera_index}=    get camera index
    set suite variable   ${test_camera_index}


Multiple Teardown Methods
    delete test user    ${production}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Study Event Types Controller for Production
    initialize access for study recordings controller   ${production}

Login with Production
    ${test_production_token}=  test user login by role  ${production}  ${None}
    set suite variable  ${test_production_token}
    set token study recordings   ${test_production_token}

# STUDY RECORDINGS CONTROLLER
TC 011: Production's access to Get GetLastSyncPacketIndex
    [Tags]      Production StudyRecordingsController   GetLastSyncPacketIndex
    get last sync packet index   ${test_study_id}   ${test_recording_index}     TC 011

TC 012: Production's access to Get GetServerVideoSize
    [Tags]      Production StudyRecordingsController   GetServerVideoSize
    Log    ${test_camera_index}
    get server video size     ${test_study_id}    ${test_recording_index}    ${test_camera_index}    ${test_video_index}    TC 012

TC 013: Production's access to Post UpdateStudyRecordingNotes
    [Tags]      Production StudyRecordingsController   UpdateStudyRecordingNotes
    update study recording notes    ${test_study_id}   ${test_recording_index}      TC 013

TC 014: Production's access to Get GetStudyRecording
    [Tags]      Production StudyRecordingsController   GetStudyRecording
    get study recordings     ${test_study_id}    ${test_recording_index}    TC 014

TC 015: Production's access to Get GetMapping
    [Tags]      Production StudyRecordingsController   GetMapping
    get mappings          ${test_study_id}      ${test_recording_index}     TC 015