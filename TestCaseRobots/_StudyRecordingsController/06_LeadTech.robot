*** Settings ***
Documentation    Test Cases to validate all StudyRecordings Controller endpoints authorization for LeadTech role

Library     ../../TestCaseMethods/StudyRecordingsControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py
Library  ../../TestCaseMethods/_CommonStudiesTCs.py

Resource  ../global_robot_variables.robot


*** Variables ***
${super_admin_token}
${test_lead_tech_token}
${test_study_id}
${test_video_index}
${test_recording_index}
${test_camera_index}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${lead_tech}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    set common studies token  ${super_admin_token}
    change selected facility   ${mmt_fac_id}
    create test user  ${lead_tech}
    ${test_study_id}=  get video study id
    set suite variable   ${test_study_id}
    get test study information   ${test_study_id}
    ${test_video_index}=   get video index
    set suite variable   ${test_video_index}
    ${test_recording_index}=  get recording index
    set suite variable   ${test_recording_index}
    ${test_camera_index}=    get camera index
    set suite variable   ${test_camera_index}


Multiple Teardown Methods
    delete test user    ${lead_tech}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Study Event Types Controller for LeadTech
    initialize access for study recordings controller   ${lead_tech}

Login with LeadTech
    ${test_facility_admin_token}=  test user login by role  ${lead_tech}  ${None}
    set suite variable  ${test_facility_admin_token}
    set token study recordings   ${test_facility_admin_token}

# STUDY RECORDINGS CONTROLLER
TC 026: Lead Tech's access to Get GetLastSyncPacketIndex
    [Tags]      LeadTech StudyRecordingsController   GetLastSyncPacketIndex
    get last sync packet index   ${test_study_id}   ${test_recording_index}     TC 026

TC 027: Lead Tech's access to Get GetServerVideoSize
    [Tags]      LeadTech StudyRecordingsController   GetServerVideoSize
    Log    ${test_camera_index}
    get server video size     ${test_study_id}    ${test_recording_index}    ${test_camera_index}    ${test_video_index}    TC 027

TC 028: Lead Tech's access to Post UpdateStudyRecordingNotes
    [Tags]      LeadTech StudyRecordingsController   UpdateStudyRecordingNotes
    update study recording notes    ${test_study_id}   ${test_recording_index}      TC 028

TC 029: Lead Tech's access to Get GetStudyRecording
    [Tags]      LeadTech StudyRecordingsController   GetStudyRecording
    get study recordings     ${test_study_id}    ${test_recording_index}        TC 029

TC 030: Lead Tech's access to Get GetMapping
    [Tags]      LeadTech StudyRecordingsController   GetMapping
    get mappings          ${test_study_id}      ${test_recording_index}     TC 030