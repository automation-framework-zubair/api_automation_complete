*** Settings ***
Documentation    Test Cases to validate all StudyRecordings Controller endpoints authorization for OfficePersonnel role

Library     ../../TestCaseMethods/StudyRecordingsControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py
Library  ../../TestCaseMethods/_CommonStudiesTCs.py

Resource  ../global_robot_variables.robot


*** Variables ***
${super_admin_token}
${test_office_personnel_token}
${test_study_id}
${test_video_index}
${test_recording_index}
${test_camera_index}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${office_personnel}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    set common studies token  ${super_admin_token}
    change selected facility   ${mmt_fac_id}
    create test user  ${office_personnel}
    ${test_study_id}=  get video study id
    set suite variable   ${test_study_id}
    get test study information   ${test_study_id}
    ${test_video_index}=   get video index
    set suite variable   ${test_video_index}
    ${test_recording_index}=  get recording index
    set suite variable   ${test_recording_index}
    ${test_camera_index}=    get camera index
    set suite variable   ${test_camera_index}


Multiple Teardown Methods
    delete test user    ${office_personnel}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Study Event Types Controller for OfficePersonnel
    initialize access for study recordings controller   ${office_personnel}

Login with OfficePersonnel
    ${test_office_personnel_token}=  test user login by role  ${office_personnel}  ${None}
    set suite variable  ${test_office_personnel_token}
    set token study recordings   ${test_office_personnel_token}

# STUDY RECORDINGS CONTROLLER
TC 036: Office Personnel's access to Get GetLastSyncPacketIndex
    [Tags]      OfficePersonnel StudyRecordingsController   GetLastSyncPacketIndex
    get last sync packet index   ${test_study_id}   ${test_recording_index}     TC 036

TC 037: Office Personnel's access to Get GetServerVideoSize
    [Tags]      OfficePersonnel StudyRecordingsController   GetServerVideoSize
    Log    ${test_camera_index}
    get server video size     ${test_study_id}    ${test_recording_index}    ${test_camera_index}    ${test_video_index}    TC 037

TC 038: Office Personnel's access to Post UpdateStudyRecordingNotes
    [Tags]      OfficePersonnel StudyRecordingsController   UpdateStudyRecordingNotes
    update study recording notes    ${test_study_id}   ${test_recording_index}      TC 038

TC 039: Office Personnel's access to Get GetStudyRecording
    [Tags]      OfficePersonnel StudyRecordingsController   GetStudyRecording
    get study recordings     ${test_study_id}    ${test_recording_index}    TC 039

TC 040: Office Personnel's access to Get GetMapping
    [Tags]      OfficePersonnel StudyRecordingsController   GetMapping
    get mappings          ${test_study_id}      ${test_recording_index}     TC 040