*** Settings ***
Documentation    Test Cases to validate all StudyRecordings Controller endpoints authorization for FieldTech role

Library     ../../TestCaseMethods/StudyRecordingsControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py
Library  ../../TestCaseMethods/_CommonStudiesTCs.py

Resource  ../global_robot_variables.robot


*** Variables ***
${super_admin_token}
${test_field_tech_token}
${test_field_tech_id}
${test_study_id}
${test_video_index}
${test_recording_index}
${test_camera_index}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${field_tech}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    set common studies token  ${super_admin_token}
    change selected facility   ${mmt_fac_id}
    ${test_field_tech_id}=  create test user  ${field_tech}
    ${test_study_id}=  get video study id
    set suite variable   ${test_study_id}
    get test study information   ${test_study_id}
    ${test_video_index}=   get video index
    set suite variable   ${test_video_index}
    ${test_recording_index}=  get recording index
    set suite variable   ${test_recording_index}
    ${test_camera_index}=    get camera index
    set suite variable   ${test_camera_index}
    share test study   ${test_study_id}   ${test_field_tech_id}


Multiple Teardown Methods
    delete test user    ${field_tech}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Study Event Types Controller for FieldTech
    initialize access for study recordings controller   ${field_tech}

Login with FieldTech
    ${test_field_tech_id}=  test user login by role  ${field_tech}  ${None}
    set suite variable  ${test_field_tech_id}
    set token study recordings   ${test_field_tech_id}

# STUDY RECORDINGS CONTROLLER
TC 031: Field Tech's access to Get GetLastSyncPacketIndex
    [Tags]      FieldTech StudyRecordingsController   GetLastSyncPacketIndex
    get last sync packet index   ${test_study_id}   ${test_recording_index}     TC 031

TC 032: Field Tech's access to Get GetServerVideoSize
    [Tags]      FieldTech StudyRecordingsController   GetServerVideoSize
    Log    ${test_camera_index}
    get server video size     ${test_study_id}    ${test_recording_index}    ${test_camera_index}    ${test_video_index}    TC 032

TC 033: Field Tech's access to UpdateStudyRecordingNotes
    [Tags]      FieldTech StudyRecordingsController   UpdateStudyRecordingNotes
    update study recording notes    ${test_study_id}   ${test_recording_index}      TC 033

TC 034: Field Tech's access to Get GetStudyRecording
    [Tags]      FieldTech StudyRecordingsController   GetStudyRecording
    get study recordings     ${test_study_id}    ${test_recording_index}    TC 034

TC 035: Field Tech's access to Get GetMapping
    [Tags]      FieldTech StudyRecordingsController   GetMapping
    get mappings          ${test_study_id}      ${test_recording_index}     TC 035