*** Settings ***
Documentation    Test Cases to validate all Patients Controller endpoints authorization for FacilityAdmin role

Library  ../../TestCaseMethods/PatientsControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py

Resource  ../global_robot_variables.robot


*** Variables ***

${super_admin_token}
${test_facility_admin_token}
${created_patient_id}
${created_test_patient_id}
${created_by_post_patient_id}
${patient_custom_id}
${study_id}=  123
${pat_id}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${facility_admin}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    change selected facility  ${MMT_FAC_ID}
    create test user  ${facility_admin}
    ${created_patient_id}=  create test patient
    set suite variable  ${created_patient_id}
    ${patient_custom_id}=  get patient custom id by guid  ${created_patient_id}
    set suite variable  ${patient_custom_id}

Multiple Teardown Methods
    delete test user    ${facility_admin}
    ${created_test_patient_id}=   get created patient id   ${pat_id}
    RUN KEYWORD IF  $created_patient_id is not None     delete test patient    ${created_patient_id}
    RUN KEYWORD IF  $created_test_patient_id is not None     delete test patient    ${created_test_patient_id}
    RUN KEYWORD IF  $created_by_post_patient_id is not None     delete test patient    ${created_by_post_patient_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Patients Controller for FacilityAdmin
    initialize access for patients controller  ${facility_admin}

Login with FacilityAdmin
    ${test_facility_admin_token}=   test user login by role     ${facility_admin}    ${None}
    set suite variable  ${test_facility_admin_token}
    set token patients  ${test_facility_admin_token}

# PATIENTS CONTROLLER
TC 052: Facility Admin's access to CreatePatients
    [Tags]    Facility Admin PatientsController      CreatePatients
    ${pat_id} =  set pat id
    set suite variable  ${pat_id}
    ${created_test_patient_id}=  create patient tc    API_Test   Patient    TC 052
    set suite variable  ${created_test_patient_id}

TC 053: Facility Admin's access to SearchPatients
    [Tags]    Facility Admin PatientsController      SearchPatients
    search patient  ${patient_custom_id}    TC 053

TC 054: Facility Admin's access to GetDocuments
    [Tags]    Facility Admin PatientsController      GetDocuments
    get documents   ${created_patient_id}   TC 054

TC 055: Facility Admin's access to SetDocumentLock
    [Tags]    Facility Admin PatientsController      SetDocumentLock
    set document lock  ${created_patient_id}    test.pdf    TC 055

TC 056: Facility Admin's access to DeleteDocument
    [Tags]    Facility Admin PatientsController      DeleteDocument
    delete document  ${created_patient_id}    test.pdf      TC 056

TC 057: Facility Admin's access to GetDocumentUploadCreds
    [Tags]    Facility Admin PatientsController      GetDocumentUploadCreds
    get document upload creds  ${created_patient_id}    TC 057

TC 058: Facility Admin's access to GetDocumentDownloadURL
    [Tags]    Facility Admin PatientsController      GetDocumentDownloadURL
    get document download url  ${created_patient_id}    test.pdf    TC 058

TC 059: Facility Admin's access to LogDocumentCreation
    [Tags]    Facility Admin PatientsController      LogDocumentCreation
    log document creation  ${created_patient_id}    test.pdf    TC 059

TC 060: Facility Admin's access to GetPatientForEditing
    [Tags]    Facility Admin PatientsController      GetPatientForEditing
    get patient for editing  ${created_patient_id}      TC 060

TC 061: Facility Admin's access to GetPatientByPatientId
    [Tags]    Facility Admin PatientsController      GetPatientByPatientId
    get patient for editing  ${created_patient_id}      TC 061

TC 062: Facility Admin's access to GetPatientByStudyIds
    [Tags]    Facility Admin PatientsController      GetPatientByStudyIds
    get patient by study ids  ${study_id}       TC 062

TC 063: Facility Admin's access to GetPatientByID
    [Tags]    Facility Admin PatientsController      GetPatientByID
    get patient by id  ${created_patient_id}    TC 063

TC 064: Facility Admin's access to Patch
    [Tags]    Facility Admin PatientsController      Patch
    patch patient  ${created_patient_id}    edit    edit    TC 064

TC 065: Facility Admin's access to Put
    [Tags]    Facility Admin PatientsController      Put
    put patient  ${created_patient_id}    edit    edit      TC 065

TC 066: Facility Admin's access to Post
    [Tags]    Facility Admin PatientsController      Post
    ${created_by_post_patient_id}=  post patient tc  first_name    last_name    TC 066
    set suite variable  ${created_by_post_patient_id}

TC 067: Facility Admin's access to GetPatients
    [Tags]    Facility Admin PatientsController      GetPatients
    get patients    TC 067

TC 068: Facility Admin's access to Delete
    [Tags]    Facility Admin PatientsController      Delete
    delete patient  ${created_patient_id}   TC 068
