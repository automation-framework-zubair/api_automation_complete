*** Settings ***
Documentation    Test Cases to validate all Patients Controller endpoints authorization for ReviewDoctor role

Library  ../../TestCaseMethods/PatientsControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py

Resource  ../global_robot_variables.robot


*** Variables ***

${super_admin_token}
${test_review_doctor_token}
${created_patient_id}
${created_test_patient_id}
${created_by_post_patient_id}
${patient_custom_id}
${study_id}=  123
${pat_id}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${review_doctor}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    change selected facility  ${MMT_FAC_ID}
    create test user  ${review_doctor}
    ${created_patient_id}=  create test patient
    set suite variable  ${created_patient_id}
    ${patient_custom_id}=  get patient custom id by guid  ${created_patient_id}
    set suite variable  ${patient_custom_id}

Multiple Teardown Methods
    delete test user    ${review_doctor}
    ${created_test_patient_id}=   get created patient id   ${pat_id}
    RUN KEYWORD IF  $created_patient_id is not None     delete test patient    ${created_patient_id}
    RUN KEYWORD IF  $created_test_patient_id is not None     delete test patient    ${created_test_patient_id}
    RUN KEYWORD IF  $created_by_post_patient_id is not None     delete test patient    ${created_by_post_patient_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Patients Controller for ReviewDoctor
    initialize access for patients controller  ${review_doctor}

Login with ReviewDoctor
    ${test_review_doctor_token}=   test user login by role     ${review_doctor}    ${None}
    set suite variable  ${test_review_doctor_token}
    set token patients  ${test_review_doctor_token}

# PATIENTS CONTROLLER
TC 069: Review Doctor's access to CreatePatients
    [Tags]    ReviewDoctor PatientsController      CreatePatients
    ${pat_id} =  set pat id
    set suite variable  ${pat_id}
    ${created_test_patient_id}=  create patient tc    API_Test   Patient    TC 069
    set suite variable  ${created_test_patient_id}

TC 070: Review Doctor's access to SearchPatients
    [Tags]    ReviewDoctor PatientsController      SearchPatients
    search patient  ${patient_custom_id}    TC 070

TC 071: Review Doctor's access to GetDocuments
    [Tags]    ReviewDoctor PatientsController      GetDocuments
    get documents   ${created_patient_id}   TC 071

TC 072: Review Doctor's access to SetDocumentLock
    [Tags]    ReviewDoctor PatientsController      SetDocumentLock
    set document lock  ${created_patient_id}    test.pdf    TC 072

TC 073: Review Doctor's access to DeleteDocument
    [Tags]    ReviewDoctor PatientsController      DeleteDocument
    delete document  ${created_patient_id}    test.pdf      TC 073

TC 074: Review Doctor's access to GetDocumentUploadCreds
    [Tags]    ReviewDoctor PatientsController      GetDocumentUploadCreds
    get document upload creds  ${created_patient_id}    TC 074

TC 075: Review Doctor's access to GetDocumentDownloadURL
    [Tags]    ReviewDoctor PatientsController      GetDocumentDownloadURL
    get document download url  ${created_patient_id}    test.pdf    TC 075

TC 076: Review Doctor's access to LogDocumentCreation
    [Tags]    ReviewDoctor PatientsController      LogDocumentCreation
    log document creation  ${created_patient_id}    test.pdf    TC 076

TC 077: Review Doctor's access to GetPatientForEditing
    [Tags]    ReviewDoctor PatientsController      GetPatientForEditing
    get patient for editing  ${created_patient_id}      TC 077

TC 078: Review Doctor's access to GetPatientByPatientId
    [Tags]    ReviewDoctor PatientsController      GetPatientByPatientId
    get patient for editing  ${created_patient_id}      TC 078

TC 079: Review Doctor's access to GetPatientByStudyIds
    [Tags]    ReviewDoctor PatientsController      GetPatientByStudyIds
    get patient by study ids  ${study_id}   TC 079

TC 080: Review Doctor's access to GetPatientByID
    [Tags]    ReviewDoctor PatientsController      GetPatientByID
    get patient by id  ${created_patient_id}    TC 080

TC 081: Review Doctor's access to Patch
    [Tags]    ReviewDoctor PatientsController      Patch
    patch patient  ${created_patient_id}    edit    edit    TC 081

TC 082: Review Doctor's access to Put
    [Tags]    ReviewDoctor PatientsController      Put
    put patient  ${created_patient_id}    edit    edit      TC 082

TC 083: Review Doctor's access to Post
    [Tags]    ReviewDoctor PatientsController      Post
    ${created_by_post_patient_id}=  post patient tc  first_name    last_name    TC 083
    set suite variable  ${created_by_post_patient_id}

TC 084: Review Doctor's access to GetPatients
    [Tags]    ReviewDoctor PatientsController      GetPatients
    get patients    TC 084

TC 085: Review Doctor's access to Delete
    [Tags]    ReviewDoctor PatientsController      Delete
    delete patient  ${created_patient_id}       TC 085
