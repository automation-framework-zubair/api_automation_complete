*** Settings ***
Documentation    Test Cases to validate all Patients Controller endpoints authorization for FieldTech role

Library  ../../TestCaseMethods/PatientsControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py

Resource  ../global_robot_variables.robot


*** Variables ***

${super_admin_token}
${test_field_tech_token}
${created_patient_id}
${created_test_patient_id}
${created_by_post_patient_id}
${patient_custom_id}
${study_id}=  123
${pat_id}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${field_tech}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    change selected facility  ${MMT_FAC_ID}
    create test user  ${field_tech}
    ${created_patient_id}=  create test patient
    set suite variable  ${created_patient_id}
    ${patient_custom_id}=  get patient custom id by guid  ${created_patient_id}
    set suite variable  ${patient_custom_id}

Multiple Teardown Methods
    delete test user    ${field_tech}
    ${created_test_patient_id}=   get created patient id   ${pat_id}
    RUN KEYWORD IF  $created_patient_id is not None     delete test patient    ${created_patient_id}
    RUN KEYWORD IF  $created_test_patient_id is not None     delete test patient    ${created_test_patient_id}
    RUN KEYWORD IF  $created_by_post_patient_id is not None     delete test patient    ${created_by_post_patient_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Patients Controller for FieldTech
    initialize access for patients controller  ${field_tech}

Login with FieldTech
    ${test_field_tech_token}=   test user login by role     ${field_tech}    ${None}
    set suite variable  ${test_field_tech_token}
    set token patients  ${test_field_tech_token}

# PATIENTS CONTROLLER
TC 103: Field Tech's access to CreatePatients
    [Tags]    FieldTech PatientsController      CreatePatients
    ${pat_id} =  set pat id
    set suite variable  ${pat_id}
    ${created_test_patient_id}=  create patient tc    API_Test   Patient    TC 103
    set suite variable  ${created_test_patient_id}

TC 104: Field Tech's access to SearchPatients
    [Tags]    FieldTech PatientsController      SearchPatients
    search patient  ${patient_custom_id}    TC 104

TC 105: Field Tech's access to GetDocuments
    [Tags]    FieldTech PatientsController      GetDocuments
    get documents   ${created_patient_id}   TC 105

TC 106: Field Tech's access to SetDocumentLock
    [Tags]    FieldTech PatientsController      SetDocumentLock
    set document lock  ${created_patient_id}    test.pdf    TC 106

TC 107: Field Tech's access to DeleteDocument
    [Tags]    FieldTech PatientsController      DeleteDocument
    delete document  ${created_patient_id}    test.pdf      TC 107

TC 108: Field Tech's access to GetDocumentUploadCreds
    [Tags]    FieldTech PatientsController      GetDocumentUploadCreds
    get document upload creds  ${created_patient_id}    TC 108

TC 109: Field Tech's access to GetDocumentDownloadURL
    [Tags]    FieldTech PatientsController      GetDocumentDownloadURL
    get document download url  ${created_patient_id}    test.pdf    TC 109

TC 110: Field Tech's access to LogDocumentCreation
    [Tags]    FieldTech PatientsController      LogDocumentCreation
    log document creation  ${created_patient_id}    test.pdf    TC 110

TC 111: Field Tech's access to GetPatientForEditing
    [Tags]    FieldTech PatientsController      GetPatientForEditing
    get patient for editing  ${created_patient_id}      TC 111

TC 112: Field Tech's access to GetPatientByPatientId
    [Tags]    FieldTech PatientsController      GetPatientByPatientId
    get patient for editing  ${created_patient_id}      TC 112

TC 113: Field Tech's access to GetPatientByStudyIds
    [Tags]    FieldTech PatientsController      GetPatientByStudyIds
    get patient by study ids  ${study_id}       TC 113

TC 114: Field Tech's access to GetPatientByID
    [Tags]    FieldTech PatientsController      GetPatientByID
    get patient by id  ${created_patient_id}    TC 114

TC 115: Field Tech's access to Patch
    [Tags]    FieldTech PatientsController      Patch
    patch patient  ${created_patient_id}    edit    edit        TC 115

TC 116: Field Tech's access to Put
    [Tags]    FieldTech PatientsController      Put
    put patient  ${created_patient_id}    edit    edit      TC 116

TC 117: Field Tech's access to Post
    [Tags]    FieldTech PatientsController      Post
    ${created_by_post_patient_id}=  post patient tc  first_name    last_name    TC 117
    set suite variable  ${created_by_post_patient_id}

TC 118: Field Tech's access to GetPatients
    [Tags]    FieldTech PatientsController      GetPatients
    get patients    TC 118

TC 119: Field Tech's access to Delete
    [Tags]    FieldTech PatientsController      Delete
    delete patient  ${created_patient_id}   TC 119
