*** Settings ***
Documentation    Test Cases to validate all Patients Controller endpoints authorization for Super Admin role

Library  ../../TestCaseMethods/PatientsControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py

Resource  ../global_robot_variables.robot


*** Variables ***

${super_admin_token}
${test_super_admin_token}
${created_patient_id}
${created_test_patient_id}
${created_by_post_patient_id}
${patient_custom_id}
${study_id}=  123
${pat_id}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${super_admin}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    get_version_number  ${super_admin_token}
    change selected facility  ${MMT_FAC_ID}
    create test user  ${super_admin}
    ${created_patient_id}=  create test patient
    set suite variable  ${created_patient_id}
    ${patient_custom_id}=  get patient custom id by guid  ${created_patient_id}
    set suite variable  ${patient_custom_id}

Multiple Teardown Methods
    delete test user    ${super_admin}
    ${created_test_patient_id}=   get created patient id   ${pat_id}
    RUN KEYWORD IF  $created_patient_id is not None     delete test patient    ${created_patient_id}
    RUN KEYWORD IF  $created_test_patient_id is not None     delete test patient    ${created_test_patient_id}
    RUN KEYWORD IF  $created_by_post_patient_id is not None     delete test patient    ${created_by_post_patient_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Patients Controller for Super Admin
    initialize access for patients controller  ${super_admin}

Login with Super Admin
    ${test_super_admin_token}=   test user login by role     ${super_admin}    ${None}
    set suite variable  ${test_super_admin_token}
    set token patients  ${test_super_admin_token}

# PATIENTS CONTROLLER
TC 001: SuperAdmin's access to CreatePatients
    [Tags]    SuperAdmin PatientsController      CreatePatients
    ${pat_id} =  set pat id
    set suite variable  ${pat_id}
    ${created_test_patient_id}=  create patient tc    API_Test   Patient    TC 001
    set suite variable  ${created_test_patient_id}

TC 002: SuperAdmin's access to SearchPatients
    [Tags]    SuperAdmin PatientsController      SearchPatients
    search patient  ${patient_custom_id}    TC 002

TC 003: SuperAdmin's access to GetDocuments
    [Tags]    SuperAdmin PatientsController      GetDocuments
    get documents   ${created_patient_id}   TC 003

TC 004: SuperAdmin's access to SetDocumentLock
    [Tags]    SuperAdmin PatientsController      SetDocumentLock
    set document lock  ${created_patient_id}    test.pdf    TC 004

TC 005: SuperAdmin's access to DeleteDocument
    [Tags]    SuperAdmin PatientsController      DeleteDocument
    delete document  ${created_patient_id}    test.pdf      TC 005

TC 006: SuperAdmin's access to GetDocumentUploadCreds
    [Tags]    SuperAdmin PatientsController      GetDocumentUploadCreds
    get document upload creds  ${created_patient_id}    TC 006

TC 007: SuperAdmin's access to GetDocumentDownloadURL
    [Tags]    SuperAdmin PatientsController      GetDocumentDownloadURL
    get document download url  ${created_patient_id}    test.pdf    TC 007

TC 008: SuperAdmin's access to LogDocumentCreation
    [Tags]    SuperAdmin PatientsController      LogDocumentCreation
    log document creation  ${created_patient_id}    test.pdf    TC 008

TC 009: SuperAdmin's access to GetPatientForEditing
    [Tags]    SuperAdmin PatientsController      GetPatientForEditing
    get patient for editing  ${created_patient_id}      TC 009

TC 010: SuperAdmin's access to GetPatientByPatientId
    [Tags]    SuperAdmin PatientsController      GetPatientByPatientId
    get patient for editing  ${created_patient_id}      TC 010

TC 011: SuperAdmin's access to GetPatientByStudyIds
    [Tags]    SuperAdmin PatientsController      GetPatientByStudyIds
    get patient by study ids  ${study_id}   TC 011

TC 012: SuperAdmin's access to GetPatientByID
    [Tags]    SuperAdmin PatientsController      GetPatientByID
    get patient by id  ${created_patient_id}    TC 012

TC 013: SuperAdmin's access to Patch
    [Tags]    SuperAdmin PatientsController      Patch
    patch patient  ${created_patient_id}    edit    edit    TC 013

TC 014: SuperAdmin's access to Put
    [Tags]    SuperAdmin PatientsController      Put
    put patient  ${created_patient_id}    edit    edit  TC 014

TC 015: SuperAdmin's access to Post
    [Tags]    SuperAdmin PatientsController      Post
    ${created_by_post_patient_id}=  post patient tc  first_name    last_name    TC 015
    set suite variable  ${created_by_post_patient_id}

TC 016: SuperAdmin's access to GetPatients
    [Tags]    SuperAdmin PatientsController      GetPatients
    get patients    TC 016

TC 017: SuperAdmin's access to Delete
    [Tags]    SuperAdmin PatientsController      Delete
    delete patient  ${created_patient_id}   TC 017
