*** Settings ***
Documentation    Test Cases to validate all Patients Controller endpoints authorization for Support role

Library  ../../TestCaseMethods/PatientsControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py

Resource  ../global_robot_variables.robot


*** Variables ***

${super_admin_token}
${test_support_token}
${created_patient_id}
${created_test_patient_id}
${created_by_post_patient_id}
${patient_custom_id}
${study_id}=  123
${pat_id}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${support}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    change selected facility  ${MMT_FAC_ID}
    create test user  ${support}
    ${created_patient_id}=  create test patient
    set suite variable  ${created_patient_id}
    ${patient_custom_id}=  get patient custom id by guid  ${created_patient_id}
    set suite variable  ${patient_custom_id}

Multiple Teardown Methods
    delete test user    ${support}
    ${created_test_patient_id}=   get created patient id   ${pat_id}
    RUN KEYWORD IF  $created_patient_id is not None     delete test patient    ${created_patient_id}
    RUN KEYWORD IF  $created_test_patient_id is not None     delete test patient    ${created_test_patient_id}
    RUN KEYWORD IF  $created_by_post_patient_id is not None     delete test patient    ${created_by_post_patient_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Patients Controller for Support
    initialize access for patients controller  ${support}

Login with Support
    ${test_support_token}=   test user login by role     ${support}    ${None}
    set suite variable  ${test_support_token}
    set token patients  ${test_support_token}

# PATIENTS CONTROLLER
TC 018: Support's access to CreatePatients
    [Tags]    Support PatientsController      CreatePatients
    ${pat_id} =  set pat id
    set suite variable  ${pat_id}
    ${created_test_patient_id}=  create patient tc    API_Test   Patient    TC 018
    set suite variable  ${created_test_patient_id}

TC 019: Support's access to SearchPatients
    [Tags]    Support PatientsController      SearchPatients
    search patient  ${patient_custom_id}    TC 019

TC 020: Support's access to GetDocuments
    [Tags]    Support PatientsController      GetDocuments
    get documents   ${created_patient_id}   TC 020

TC 021: Support's access to SetDocumentLock
    [Tags]    Support PatientsController      SetDocumentLock
    set document lock  ${created_patient_id}    test.pdf    TC 021

TC 022: Support's access to DeleteDocument
    [Tags]    Support PatientsController      DeleteDocument
    delete document  ${created_patient_id}    test.pdf      TC 022

TC 023: Support's access to GetDocumentUploadCreds
    [Tags]    Support PatientsController      GetDocumentUploadCreds
    get document upload creds  ${created_patient_id}    TC 023

TC 024: Support's access to GetDocumentDownloadURL
    [Tags]    Support PatientsController      GetDocumentDownloadURL
    get document download url  ${created_patient_id}    test.pdf    TC 024

TC 025: Support's access to LogDocumentCreation
    [Tags]    Support PatientsController      LogDocumentCreation
    log document creation  ${created_patient_id}    test.pdf    TC 025

TC 026: Support's access to GetPatientForEditing
    [Tags]    Support PatientsController      GetPatientForEditing
    get patient for editing  ${created_patient_id}      TC 026

TC 027: Support's access to GetPatientByPatientId
    [Tags]    Support PatientsController      GetPatientByPatientId
    get patient for editing  ${created_patient_id}      TC 027

TC 028: Support's access to GetPatientByStudyIds
    [Tags]    Support PatientsController      GetPatientByStudyIds
    get patient by study ids  ${study_id}       TC 028

TC 029: Support's access to GetPatientByID
    [Tags]    Support PatientsController      GetPatientByID
    get patient by id  ${created_patient_id}        TC 029

TC 030: Support's access to Patch
    [Tags]    Support PatientsController      Patch
    patch patient  ${created_patient_id}    edit    edit        TC 030

TC 031: Support's access to Put
    [Tags]    Support PatientsController      Put
    put patient  ${created_patient_id}    edit    edit      TC 031

TC 032: Support's access to Post
    [Tags]    Support PatientsController      Post
    ${created_by_post_patient_id}=  post patient tc  first_name    last_name    TC 032
    set suite variable  ${created_by_post_patient_id}

TC 033: Support's access to GetPatients
    [Tags]    Support PatientsController      GetPatients
    get patients    TC 033

TC 034: Support's access to Delete
    [Tags]    Support PatientsController      Delete
    delete patient  ${created_patient_id}   TC 034
