*** Settings ***
Documentation    Test Cases to validate all Patients Controller endpoints authorization for LeadTech role

Library  ../../TestCaseMethods/PatientsControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py

Resource  ../global_robot_variables.robot


*** Variables ***

${super_admin_token}
${test_lead_tech_token}
${created_patient_id}
${created_test_patient_id}
${created_by_post_patient_id}
${patient_custom_id}
${study_id}=  123
${pat_id}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${lead_tech}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    change selected facility  ${MMT_FAC_ID}
    create test user  ${lead_tech}
    ${created_patient_id}=  create test patient
    set suite variable  ${created_patient_id}
    ${patient_custom_id}=  get patient custom id by guid  ${created_patient_id}
    set suite variable  ${patient_custom_id}

Multiple Teardown Methods
    delete test user    ${lead_tech}
    ${created_test_patient_id}=   get created patient id   ${pat_id}
    RUN KEYWORD IF  $created_patient_id is not None     delete test patient    ${created_patient_id}
    RUN KEYWORD IF  $created_test_patient_id is not None     delete test patient    ${created_test_patient_id}
    RUN KEYWORD IF  $created_by_post_patient_id is not None     delete test patient    ${created_by_post_patient_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Patients Controller for LeadTech
    initialize access for patients controller  ${lead_tech}

Login with LeadTech
    ${test_lead_tech_token}=   test user login by role     ${lead_tech}    ${None}
    set suite variable  ${test_lead_tech_token}
    set token patients  ${test_lead_tech_token}

# PATIENTS CONTROLLER
TC 086: Lead Tech's access to CreatePatients
    [Tags]    LeadTech PatientsController      CreatePatients
    ${pat_id} =  set pat id
    set suite variable  ${pat_id}
    ${created_test_patient_id}=  create patient tc    API_Test   Patient    TC 086
    set suite variable  ${created_test_patient_id}

TC 087: Lead Tech's access to SearchPatients
    [Tags]    LeadTech PatientsController      SearchPatients
    search patient  ${patient_custom_id}    TC 087

TC 088: Lead Tech's access to GetDocuments
    [Tags]    LeadTech PatientsController      GetDocuments
    get documents   ${created_patient_id}   TC 088

TC 089: Lead Tech's access to SetDocumentLock
    [Tags]    LeadTech PatientsController      SetDocumentLock
    set document lock  ${created_patient_id}    test.pdf    TC 089

TC 090: Lead Tech's access to DeleteDocument
    [Tags]    LeadTech PatientsController      DeleteDocument
    delete document  ${created_patient_id}    test.pdf      TC 090

TC 091: Lead Tech's access to GetDocumentUploadCreds
    [Tags]    LeadTech PatientsController      GetDocumentUploadCreds
    get document upload creds  ${created_patient_id}        TC 091

TC 092: Lead Tech's access to GetDocumentDownloadURL
    [Tags]    LeadTech PatientsController      GetDocumentDownloadURL
    get document download url  ${created_patient_id}    test.pdf    TC 092

TC 093: Lead Tech's access to LogDocumentCreation
    [Tags]    LeadTech PatientsController      LogDocumentCreation
    log document creation  ${created_patient_id}    test.pdf    TC 093

TC 094: Lead Tech's access to GetPatientForEditing
    [Tags]    LeadTech PatientsController      GetPatientForEditing
    get patient for editing  ${created_patient_id}      TC 094

TC 095: Lead Tech's access to GetPatientByPatientId
    [Tags]    LeadTech PatientsController      GetPatientByPatientId
    get patient for editing  ${created_patient_id}      TC 095

TC 096: Lead Tech's access to GetPatientByStudyIds
    [Tags]    LeadTech PatientsController      GetPatientByStudyIds
    get patient by study ids  ${study_id}       TC 096

TC 097: Lead Tech's access to GetPatientByID
    [Tags]    LeadTech PatientsController      GetPatientByID
    get patient by id  ${created_patient_id}        TC 097

TC 098: Lead Tech's access to Patch
    [Tags]    LeadTech PatientsController      Patch
    patch patient  ${created_patient_id}    edit    edit        TC 098

TC 099: Lead Tech's access to Put
    [Tags]    LeadTech PatientsController      Put
    put patient  ${created_patient_id}    edit    edit      TC 099

TC 100: Lead Tech's access to Post
    [Tags]    LeadTech PatientsController      Post
    ${created_by_post_patient_id}=  post patient tc  first_name    last_name    TC 100
    set suite variable  ${created_by_post_patient_id}

TC 101: Lead Tech's access to GetPatients
    [Tags]    LeadTech PatientsController      GetPatients
    get patients    TC 101

TC 102: Lead Tech's access to Delete
    [Tags]    LeadTech PatientsController      Delete
    delete patient  ${created_patient_id}       TC 102
