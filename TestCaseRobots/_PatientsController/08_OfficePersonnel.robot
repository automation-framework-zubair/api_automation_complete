*** Settings ***
Documentation    Test Cases to validate all Patients Controller endpoints authorization for OfficePersonnel role

Library  ../../TestCaseMethods/PatientsControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py

Resource  ../global_robot_variables.robot


*** Variables ***

${super_admin_token}
${test_office_personnel_token}
${created_patient_id}
${created_test_patient_id}
${created_by_post_patient_id}
${patient_custom_id}
${study_id}=  123
${pat_id}



*** Keywords ***
Multiple Setup Methods
    delete test user    ${office_personnel}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    change selected facility  ${MMT_FAC_ID}
    create test user  ${office_personnel}
    ${created_patient_id}=  create test patient
    set suite variable  ${created_patient_id}
    ${patient_custom_id}=  get patient custom id by guid  ${created_patient_id}
    set suite variable  ${patient_custom_id}

Multiple Teardown Methods
    delete test user    ${office_personnel}
    ${created_test_patient_id}=   get created patient id   ${pat_id}
    RUN KEYWORD IF  $created_patient_id is not None     delete test patient    ${created_patient_id}
    RUN KEYWORD IF  $created_test_patient_id is not None     delete test patient    ${created_test_patient_id}
    RUN KEYWORD IF  $created_by_post_patient_id is not None     delete test patient    ${created_by_post_patient_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods


*** Test Cases ***
Set Access Info of Patients Controller for OfficePersonnel
    initialize access for patients controller  ${office_personnel}

Login with OfficePersonnel
    ${test_office_personnel_token}=   test user login by role     ${office_personnel}    ${None}
    set suite variable  ${test_office_personnel_token}
    set token patients  ${test_office_personnel_token}

# PATIENTS CONTROLLER
TC 120: Office Personnel's access to CreatePatients
    [Tags]    OfficePersonnel PatientsController      CreatePatients
    ${pat_id} =  set pat id
    set suite variable  ${pat_id}
    ${created_test_patient_id}=  create patient tc    API_Test   Patient    TC 120
    set suite variable  ${created_test_patient_id}

TC 121: Office Personnel's access to SearchPatients
    [Tags]    OfficePersonnel PatientsController      SearchPatients
    search patient  ${patient_custom_id}    TC 121

TC 122: Office Personnel's access to GetDocuments
    [Tags]    OfficePersonnel PatientsController      GetDocuments
    get documents   ${created_patient_id}   TC 122

TC 123: Office Personnel's access to SetDocumentLock
    [Tags]    OfficePersonnel PatientsController      SetDocumentLock
    set document lock  ${created_patient_id}    test.pdf    TC 123

TC 124: Office Personnel's access to DeleteDocument
    [Tags]    OfficePersonnel PatientsController      DeleteDocument
    delete document  ${created_patient_id}    test.pdf      TC 124

TC 125: Office Personnel's access to GetDocumentUploadCreds
    [Tags]    OfficePersonnel PatientsController      GetDocumentUploadCreds
    get document upload creds  ${created_patient_id}    TC 125

TC 126: Office Personnel's access to GetDocumentDownloadURL
    [Tags]    OfficePersonnel PatientsController      GetDocumentDownloadURL
    get document download url  ${created_patient_id}    test.pdf    TC 126

TC 127: Office Personnel's access to LogDocumentCreation
    [Tags]    OfficePersonnel PatientsController      LogDocumentCreation
    log document creation  ${created_patient_id}    test.pdf    TC 127

TC 128: Office Personnel's access to GetPatientForEditing
    [Tags]    OfficePersonnel PatientsController      GetPatientForEditing
    get patient for editing  ${created_patient_id}      TC 128

TC 129: Office Personnel's access to GetPatientByPatientId
    [Tags]    OfficePersonnel PatientsController      GetPatientByPatientId
    get patient for editing  ${created_patient_id}      TC 129

TC 130: Office Personnel's access to GetPatientByStudyIds
    [Tags]    OfficePersonnel PatientsController      GetPatientByStudyIds
    get patient by study ids  ${study_id}   TC 130

TC 131: Office Personnel's access to GetPatientByID
    [Tags]    OfficePersonnel PatientsController      GetPatientByID
    get patient by id  ${created_patient_id}    TC 131

TC 132: Office Personnel's access to Patch
    [Tags]    OfficePersonnel PatientsController      Patch
    patch patient  ${created_patient_id}    edit    edit    TC 132

TC 133: Office Personnel's access to Put
    [Tags]    OfficePersonnel PatientsController      Put
    put patient  ${created_patient_id}    edit    edit      TC 133

TC 134: Office Personnel's access to Post
    [Tags]    OfficePersonnel PatientsController      Post
    ${created_by_post_patient_id}=  post patient tc  first_name    last_name    TC 134
    set suite variable  ${created_by_post_patient_id}

TC 135: Office Personnel's access to GetPatients
    [Tags]    OfficePersonnel PatientsController      GetPatients
    get patients    TC 135

TC 136: Office Personnel's access to Delete
    [Tags]    OfficePersonnel PatientsController      Delete
    delete patient  ${created_patient_id}   TC 136
