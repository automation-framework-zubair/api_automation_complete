*** Settings ***
Documentation    Test Cases to validate all Patients Controller endpoints authorization for Production role

Library  ../../TestCaseMethods/PatientsControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py

Resource  ../global_robot_variables.robot


*** Variables ***

${super_admin_token}
${test_production_token}
${created_patient_id}
${created_test_patient_id}
${created_by_post_patient_id}
${patient_custom_id}
${study_id}=  123
${pat_id}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${production}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    change selected facility  ${MMT_FAC_ID}
    create test user  ${production}
    ${created_patient_id}=  create test patient
    set suite variable  ${created_patient_id}
    ${patient_custom_id}=  get patient custom id by guid  ${created_patient_id}
    set suite variable  ${patient_custom_id}

Multiple Teardown Methods
    delete test user    ${production}
    ${created_test_patient_id}=   get created patient id   ${pat_id}
    RUN KEYWORD IF  $created_patient_id is not None     delete test patient    ${created_patient_id}
    RUN KEYWORD IF  $created_test_patient_id is not None     delete test patient    ${created_test_patient_id}
    RUN KEYWORD IF  $created_by_post_patient_id is not None     delete test patient    ${created_by_post_patient_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Patients Controller for Production
    initialize access for patients controller  ${production}

Login with Production
    ${test_production_token}=   test user login by role     ${production}    ${None}
    set suite variable  ${test_production_token}
    set token patients  ${test_production_token}

# PATIENTS CONTROLLER
TC 035: Production's access to CreatePatients
    [Tags]    Production PatientsController      CreatePatients
    ${pat_id} =  set pat id
    set suite variable  ${pat_id}
    ${created_test_patient_id}=  create patient tc    API_Test   Patient    TC 035
    set suite variable  ${created_test_patient_id}

TC 036: Production's access to SearchPatients
    [Tags]    Production PatientsController      SearchPatients
    search patient  ${patient_custom_id}    TC 036

TC 037: Production's access to GetDocuments
    [Tags]    Production PatientsController      GetDocuments
    get documents   ${created_patient_id}   TC 037

TC 038: Production's access to SetDocumentLock
    [Tags]    Production PatientsController      SetDocumentLock
    set document lock  ${created_patient_id}    test.pdf        TC 038

TC 039: Production's access to DeleteDocument
    [Tags]    Production PatientsController      DeleteDocument
    delete document  ${created_patient_id}    test.pdf      TC 039

TC 040: Production's access to GetDocumentUploadCreds
    [Tags]    Production PatientsController      GetDocumentUploadCreds
    get document upload creds  ${created_patient_id}    TC 040

TC 041: Production's access to GetDocumentDownloadURL
    [Tags]    Production PatientsController      GetDocumentDownloadURL
    get document download url  ${created_patient_id}    test.pdf    TC 041

TC 042: Production's access to LogDocumentCreation
    [Tags]    Production PatientsController      LogDocumentCreation
    log document creation  ${created_patient_id}    test.pdf    TC 042

TC 043: Production's access to GetPatientForEditing
    [Tags]    Production PatientsController      GetPatientForEditing
    get patient for editing  ${created_patient_id}      TC 043

TC 044: Production's access to GetPatientByPatientId
    [Tags]    Production PatientsController      GetPatientByPatientId
    get patient for editing  ${created_patient_id}      TC 044

TC 045: Production's access to GetPatientByStudyIds
    [Tags]    Production PatientsController      GetPatientByStudyIds
    get patient by study ids  ${study_id}       TC 045

TC 046: Production's access to GetPatientByID
    [Tags]    Production PatientsController      GetPatientByID
    get patient by id  ${created_patient_id}        TC 046

TC 047: Production's access to Patch
    [Tags]    Production PatientsController      Patch
    patch patient  ${created_patient_id}    edit    edit    TC 047

TC 048: Production's access to Put
    [Tags]    Production PatientsController      Put
    put patient  ${created_patient_id}    edit    edit      TC 048

TC 049: Production's access to Post
    [Tags]    Production PatientsController      Post
    ${created_by_post_patient_id}=  post patient tc  first_name    last_name    TC 049
    set suite variable  ${created_by_post_patient_id}

TC 050: Production's access to GetPatients
    [Tags]    Production PatientsController      GetPatients
    get patients    TC 050

TC 051: Production's access to Delete
    [Tags]    Production PatientsController      Delete
    delete patient  ${created_patient_id}   TC 051
