*** Settings ***
Documentation    Test Cases to validate all Facility Users Controller endpoints authorization for Office Personnel role

Library  ../../TestCaseMethods/FacilityUsersControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***

${super_admin_token}
${test_office_personnel_token}
${created_facility_id}
${created_user_id}
${created_user_email}
${created_test_facility_user_id}
${created_test_facility_user_id_test}
${random_facility_user_id}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${office_personnel}
    delete test user    ${review_doctor}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    change selected facility  ${MMT_FAC_ID}

    ${random_facility_user_id}=  get random facility user id
    set suite variable  ${random_facility_user_id}
    ${created_user_id}=  create test user  ${review_doctor}
    set suite variable  ${created_user_id}
    ${created_user_email}=  get test user email  ${review_doctor}
    set suite variable  ${created_user_email}

    ${created_facility_id}=  create test facility  API
    set suite variable  ${created_facility_id}
    change selected facility  ${created_facility_id}
    create test user  ${office_personnel}

    ${created_test_facility_user_id}=  create a facility user  ${created_user_email}
    set suite variable  ${created_test_facility_user_id}


Multiple Teardown Methods
    change selected facility  ${MMT_FAC_ID}
    delete test user    ${office_personnel}
    RUN KEYWORD IF  $created_user_id is not None     delete user by id    ${created_user_id}
    RUN KEYWORD IF  $created_facility_id is not None     delete test facility    ${created_facility_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Facility Users Controller for Office Personnel
    initialize access for facility users controller  ${office_personnel}

Login with Office Personnel
    ${test_office_personnel_token}=   test user login by role     ${office_personnel}    ${None}
    set suite variable  ${test_office_personnel_token}
    set token facility users  ${test_office_personnel_token}

# FACILITY USERS CONTROLLER
TC 064: Office Personnel's access to Get Users
    [Tags]    OfficePersonnel FacilityUsersController      GetUsers
    get users   TC 064

TC 065: Office Personnel's access to Get User
    [Tags]    OfficePersonnel FacilityUsersController      GetUser
    get user  ${random_facility_user_id}    TC 065

TC 066: Office Personnel's access to Create Facility User
    [Tags]    OfficePersonnel FacilityUsersController      CreateFacilityUser
    ${created_test_facility_user_id_test}=  create facility user tc  ${created_user_email}  TC 066
    set suite variable  ${created_test_facility_user_id_test}

TC 067: Office Personnel's access to Get User by Facility User ID
    [Tags]    OfficePersonnel FacilityUsersController      get user by id
    get user by fac user id  ${random_facility_user_id}     TC 067

TC 068: Office Personnel's access to Get Facility Users
    [Tags]    OfficePersonnel FacilityUsersController      get users
    get facility users      TC 068

TC 069: Office Personnel's access to Patch Facility User
    [Tags]    OfficePersonnel FacilityUsersController      patch
    patch facility user  ${mmt_fac_id}    ${created_test_facility_user_id}   ${created_user_id}     TC 069

TC 070: Office Personnel's access to Put Facility User
    [Tags]    OfficePersonnel FacilityUsersController      put
    put facility user  ${mmt_fac_id}    ${created_test_facility_user_id}   ${created_user_id}   TC 070

TC 071: Office Personnel's access to Post Facility User
    [Tags]    OfficePersonnel FacilityUsersController      post
    post facility user  ${mmt_fac_id}   ${created_user_id}      TC 071

TC 072: Office Personnel's access to Delete Facility User
    [Tags]    OfficePersonnel FacilityUsersController      delete
    delete facility user    ${created_test_facility_user_id}    TC 072
