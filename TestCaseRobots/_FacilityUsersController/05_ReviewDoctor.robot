*** Settings ***
Documentation    Test Cases to validate all Facility Users Controller endpoints authorization for Review Doctor role

Library  ../../TestCaseMethods/FacilityUsersControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***

${super_admin_token}
${test_review_doctor_token}
${created_facility_id}
${created_user_id}
${created_user_email}
${created_test_facility_user_id}
${created_test_facility_user_id_test}
${random_facility_user_id}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${review_doctor}
    delete test user    ${office_personnel}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    change selected facility  ${MMT_FAC_ID}

    ${random_facility_user_id}=  get random facility user id
    set suite variable  ${random_facility_user_id}
    ${created_user_id}=  create test user  ${office_personnel}
    set suite variable  ${created_user_id}
    ${created_user_email}=  get test user email  ${office_personnel}
    set suite variable  ${created_user_email}

    ${created_facility_id}=  create test facility  API
    set suite variable  ${created_facility_id}
    change selected facility  ${created_facility_id}
    create test user  ${review_doctor}

    ${created_test_facility_user_id}=  create a facility user  ${created_user_email}
    set suite variable  ${created_test_facility_user_id}


Multiple Teardown Methods
    change selected facility  ${MMT_FAC_ID}
    delete test user    ${review_doctor}
    RUN KEYWORD IF  $created_user_id is not None     delete user by id    ${created_user_id}
    RUN KEYWORD IF  $created_facility_id is not None     delete test facility    ${created_facility_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Facility Users Controller for Review Doctor
    initialize access for facility users controller  ${review_doctor}

Login with Review Doctor
    ${test_review_doctor_token}=   test user login by role     ${review_doctor}    ${None}
    set suite variable  ${test_review_doctor_token}
    set token facility users  ${test_review_doctor_token}

# FACILITY USERS CONTROLLER
TC 037: Review Doctor's access to Get Users
    [Tags]    ReviewDoctor FacilityUsersController      GetUsers
    get users   TC 037

TC 038: Review Doctor's access to Get User
    [Tags]    ReviewDoctor FacilityUsersController      GetUser
    get user  ${random_facility_user_id}    TC 038

TC 039: Review Doctor's access to Create Facility User
    [Tags]    ReviewDoctor FacilityUsersController      CreateFacilityUser
    ${created_test_facility_user_id_test}=  create facility user tc  ${created_user_email}      TC 039
    set suite variable  ${created_test_facility_user_id_test}

TC 040: Review Doctor's access to Get User by Facility User ID
    [Tags]    ReviewDoctor FacilityUsersController      get user by id
    get user by fac user id  ${random_facility_user_id}     TC 040

TC 041: Review Doctor's access to Get Facility Users
    [Tags]    ReviewDoctor FacilityUsersController      get users
    get facility users      TC 041

TC 042: Review Doctor's access to Patch Facility User
    [Tags]    ReviewDoctor FacilityUsersController      patch
    patch facility user  ${mmt_fac_id}    ${created_test_facility_user_id}   ${created_user_id}     TC 042

TC 043: Review Doctor's access to Put Facility User
    [Tags]    ReviewDoctor FacilityUsersController      put
    put facility user  ${mmt_fac_id}    ${created_test_facility_user_id}   ${created_user_id}   TC 043

TC 044: Review Doctor's access to Post Facility User
    [Tags]    ReviewDoctor FacilityUsersController      post
    post facility user  ${mmt_fac_id}   ${created_user_id}  TC 044

TC 045: Review Doctor's access to Delete Facility User
    [Tags]    ReviewDoctor FacilityUsersController      delete
    delete facility user    ${created_test_facility_user_id}    TC 045
