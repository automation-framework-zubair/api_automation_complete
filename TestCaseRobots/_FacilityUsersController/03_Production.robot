*** Settings ***
Documentation    Test Cases to validate all Facility Users Controller endpoints authorization for Production role

Library  ../../TestCaseMethods/FacilityUsersControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***

${super_admin_token}
${test_production_token}
${created_facility_id}
${created_user_id}
${created_user_email}
${created_test_facility_user_id}
${created_test_facility_user_id_test}
${random_facility_user_id}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${production}
    delete test user    ${office_personnel}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    change selected facility  ${MMT_FAC_ID}

    ${random_facility_user_id}=  get random facility user id
    set suite variable  ${random_facility_user_id}
    ${created_user_id}=  create test user  ${office_personnel}
    set suite variable  ${created_user_id}
    ${created_user_email}=  get test user email  ${office_personnel}
    set suite variable  ${created_user_email}

    ${created_facility_id}=  create test facility  API
    set suite variable  ${created_facility_id}
    change selected facility  ${created_facility_id}
    create test user  ${production}

    ${created_test_facility_user_id}=  create a facility user  ${created_user_email}
    set suite variable  ${created_test_facility_user_id}


Multiple Teardown Methods
    change selected facility  ${MMT_FAC_ID}
    delete test user    ${production}
    RUN KEYWORD IF  $created_user_id is not None     delete user by id    ${created_user_id}
    RUN KEYWORD IF  $created_facility_id is not None     delete test facility    ${created_facility_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Facility Users Controller for Production
    initialize access for facility users controller  ${production}

Login with Production
    ${test_production_token}=   test user login by role     ${production}    ${None}
    set suite variable  ${test_production_token}
    set token facility users  ${test_production_token}

# FACILITY USERS CONTROLLER
TC 019: Production's access to Get Users
    [Tags]    Production FacilityUsersController      GetUsers
    get users   TC 019

TC 020: Production's access to Get User
    [Tags]    Production FacilityUsersController      GetUser
    get user  ${random_facility_user_id}    TC 020

TC 021: Production's access to Create Facility User
    [Tags]    Production FacilityUsersController      CreateFacilityUser
    ${created_test_facility_user_id_test}=  create facility user tc  ${created_user_email}  TC 021
    set suite variable  ${created_test_facility_user_id_test}

TC 022: Production's access to Get User by Facility User ID
    [Tags]    Production FacilityUsersController      get user by id
    get user by fac user id  ${random_facility_user_id}     TC 022

TC 023: Production's access to Get Facility Users
    [Tags]    Production FacilityUsersController      get users
    get facility users  TC 023

TC 024: Production's access to Patch Facility User
    [Tags]    Production FacilityUsersController      patch
    patch facility user  ${mmt_fac_id}    ${created_test_facility_user_id}   ${created_user_id}     TC 024

TC 025: Production's access to Put Facility User
    [Tags]    Production FacilityUsersController      put
    put facility user  ${mmt_fac_id}    ${created_test_facility_user_id}   ${created_user_id}   TC 025

TC 026: Production's access to Post Facility User
    [Tags]    Production FacilityUsersController      post
    post facility user  ${mmt_fac_id}   ${created_user_id}  TC 026

TC 027: Production's access to Delete Facility User
    [Tags]    Production FacilityUsersController      delete
    delete facility user    ${created_test_facility_user_id}    TC 027
