*** Settings ***
Documentation    Test Cases to validate all Facility Users Controller endpoints authorization for Super Admin role

Library  ../../TestCaseMethods/FacilityUsersControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***

${super_admin_token}
${test_super_admin_token}
${created_facility_id}
${created_user_id}
${created_user_email}
${created_test_facility_user_id}
${created_test_facility_user_id_test}
${random_facility_user_id}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${super_admin}
    delete test user    ${office_personnel}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    get_version_number  ${super_admin_token}
    change selected facility  ${MMT_FAC_ID}

    ${random_facility_user_id}=  get random facility user id
    set suite variable  ${random_facility_user_id}
    ${created_user_id}=  create test user  ${office_personnel}
    set suite variable  ${created_user_id}
    ${created_user_email}=  get test user email  ${office_personnel}
    set suite variable  ${created_user_email}

    ${created_facility_id}=  create test facility  API
    set suite variable  ${created_facility_id}
    change selected facility  ${created_facility_id}
    create test user  ${super_admin}

    ${created_test_facility_user_id}=  create a facility user  ${created_user_email}
    set suite variable  ${created_test_facility_user_id}


Multiple Teardown Methods
    change selected facility  ${MMT_FAC_ID}
    delete test user    ${super_admin}
    RUN KEYWORD IF  $created_user_id is not None     delete user by id    ${created_user_id}
    RUN KEYWORD IF  $created_facility_id is not None     delete test facility    ${created_facility_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Facility Users Controller for Super Admin
    initialize access for facility users controller  ${super_admin}

Login with Super Admin
    ${test_super_admin_token}=   test user login by role     ${super_admin}    ${None}
    set suite variable  ${test_super_admin_token}
    set token facility users  ${test_super_admin_token}

# FACILITY USERS CONTROLLER
TC 001: SuperAdmin's access to Get Users
    [Tags]    SuperAdmin FacilityUsersController      GetUsers
    get users   TC 001

TC 002: SuperAdmin's access to Get User
    [Tags]    SuperAdmin FacilityUsersController      GetUser
    get user  ${random_facility_user_id}    TC 002

TC 003: SuperAdmin's access to Create Facility User
    [Tags]    SuperAdmin FacilityUsersController      CreateFacilityUser
    ${created_test_facility_user_id_test}=  create facility user tc  ${created_user_email}  TC 003
    set suite variable  ${created_test_facility_user_id_test}

TC 004: SuperAdmin's access to Get User by Facility User ID
    [Tags]    SuperAdmin FacilityUsersController      get user by id
    get user by fac user id  ${random_facility_user_id}     TC 004

TC 005: SuperAdmin's access to Get Facility Users
    [Tags]    SuperAdmin FacilityUsersController      get users
    get facility users      TC 005

TC 006: SuperAdmin's access to Patch Facility User
    [Tags]    SuperAdmin FacilityUsersController      patch
    patch facility user  ${mmt_fac_id}    ${created_test_facility_user_id}   ${created_user_id}     TC 006

TC 007: SuperAdmin's access to Put Facility User
    [Tags]    SuperAdmin FacilityUsersController      put
    put facility user  ${mmt_fac_id}    ${created_test_facility_user_id}   ${created_user_id}   TC 007

TC 008: SuperAdmin's access to Post Facility User
    [Tags]    SuperAdmin FacilityUsersController      post
    post facility user  ${mmt_fac_id}   ${created_user_id}  TC 008

TC 009: SuperAdmin's access to Delete Facility User
    [Tags]    SuperAdmin FacilityUsersController      delete
    delete facility user    ${created_test_facility_user_id}    TC 009
