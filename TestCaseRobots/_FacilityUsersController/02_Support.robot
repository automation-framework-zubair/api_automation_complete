*** Settings ***
Documentation    Test Cases to validate all Facility Users Controller endpoints authorization for Support role

Library  ../../TestCaseMethods/FacilityUsersControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***

${super_admin_token}
${test_support_token}
${created_facility_id}
${created_user_id}
${created_user_email}
${created_test_facility_user_id}
${created_test_facility_user_id_test}
${random_facility_user_id}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${support}
    delete test user    ${office_personnel}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    change selected facility  ${MMT_FAC_ID}

    ${random_facility_user_id}=  get random facility user id
    set suite variable  ${random_facility_user_id}
    ${created_user_id}=  create test user  ${office_personnel}
    set suite variable  ${created_user_id}
    ${created_user_email}=  get test user email  ${office_personnel}
    set suite variable  ${created_user_email}

    ${created_facility_id}=  create test facility  API
    set suite variable  ${created_facility_id}
    change selected facility  ${created_facility_id}
    create test user  ${support}

    ${created_test_facility_user_id}=  create a facility user  ${created_user_email}
    set suite variable  ${created_test_facility_user_id}


Multiple Teardown Methods
    change selected facility  ${MMT_FAC_ID}
    delete test user    ${support}
    RUN KEYWORD IF  $created_user_id is not None     delete user by id    ${created_user_id}
    RUN KEYWORD IF  $created_facility_id is not None     delete test facility    ${created_facility_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Facility Users Controller for Support
    initialize access for facility users controller  ${support}

Login with Support
    ${test_support_token}=   test user login by role     ${support}    ${None}
    set suite variable  ${test_support_token}
    set token facility users  ${test_support_token}

# FACILITY USERS CONTROLLER
TC 010: Support's access to Get Users
    [Tags]    Support FacilityUsersController      GetUsers
    get users   TC 010

TC 011: Support's access to Get User
    [Tags]    Support FacilityUsersController      GetUser
    get user  ${random_facility_user_id}    TC 011

TC 012: Support's access to Create Facility User
    [Tags]    Support FacilityUsersController      CreateFacilityUser
    ${created_test_facility_user_id_test}=  create facility user tc  ${created_user_email}      TC 012
    set suite variable  ${created_test_facility_user_id_test}

TC 013: Support's access to Get User by Facility User ID
    [Tags]    Support FacilityUsersController      get user by id
    get user by fac user id  ${random_facility_user_id}     TC 013

TC 014: Support's access to Get Facility Users
    [Tags]    Support FacilityUsersController      get users
    get facility users  TC 014

TC 015: Support's access to Patch Facility User
    [Tags]    Support FacilityUsersController      patch
    patch facility user  ${mmt_fac_id}    ${created_test_facility_user_id}   ${created_user_id}     TC 015

TC 016: Support's access to Put Facility User
    [Tags]    Support FacilityUsersController      put
    put facility user  ${mmt_fac_id}    ${created_test_facility_user_id}   ${created_user_id}   TC 016

TC 017: Support's access to Post Facility User
    [Tags]    Support FacilityUsersController      post
    post facility user  ${mmt_fac_id}   ${created_user_id}  TC 017

TC 018: Support's access to Delete Facility User
    [Tags]    Support FacilityUsersController      delete
    delete facility user    ${created_test_facility_user_id}    TC 018
