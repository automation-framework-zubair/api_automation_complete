*** Settings ***
Documentation    Test Cases to validate all Facility Users Controller endpoints authorization for Facility Admin role

Library  ../../TestCaseMethods/FacilityUsersControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***

${super_admin_token}
${test_facility_admin_token}
${created_facility_id}
${created_user_id}
${created_user_email}
${created_test_facility_user_id}
${created_test_facility_user_id_test}
${random_facility_user_id}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${facility_admin}
    delete test user    ${office_personnel}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    change selected facility  ${MMT_FAC_ID}

    ${random_facility_user_id}=  get random facility user id
    set suite variable  ${random_facility_user_id}
    ${created_user_id}=  create test user  ${office_personnel}
    set suite variable  ${created_user_id}
    ${created_user_email}=  get test user email  ${office_personnel}
    set suite variable  ${created_user_email}

    ${created_facility_id}=  create test facility  API
    set suite variable  ${created_facility_id}
    change selected facility  ${created_facility_id}
    create test user  ${facility_admin}

    ${created_test_facility_user_id}=  create a facility user  ${created_user_email}
    set suite variable  ${created_test_facility_user_id}


Multiple Teardown Methods
    change selected facility  ${MMT_FAC_ID}
    delete test user    ${facility_admin}
    RUN KEYWORD IF  $created_user_id is not None     delete user by id    ${created_user_id}
    RUN KEYWORD IF  $created_facility_id is not None     delete test facility    ${created_facility_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Facility Users Controller for Facility Admin
    initialize access for facility users controller  ${facility_admin}

Login with Facility Admin
    ${test_facility_admin_token}=   test user login by role     ${facility_admin}    ${None}
    set suite variable  ${test_facility_admin_token}
    set token facility users  ${test_facility_admin_token}

# FACILITY USERS CONTROLLER
TC 028: Facility Admin's access to Get Users
    [Tags]    FacilityAdmin FacilityUsersController      GetUsers
    get users   TC 028

TC 029: Facility Admin's access to Get User
    [Tags]    FacilityAdmin FacilityUsersController      GetUser
    get user  ${random_facility_user_id}    TC 029

TC 030: Facility Admin's access to Create Facility User
    [Tags]    FacilityAdmin FacilityUsersController      CreateFacilityUser
    ${created_test_facility_user_id_test}=  create facility user tc  ${created_user_email}      TC 030
    set suite variable  ${created_test_facility_user_id_test}

TC 031: Facility Admin's access to Get User by Facility User ID
    [Tags]    FacilityAdmin FacilityUsersController      get user by id
    get user by fac user id  ${random_facility_user_id}     TC 031

TC 032: Facility Admin's access to Get Facility Users
    [Tags]    FacilityAdmin FacilityUsersController      get users
    get facility users  TC 032

TC 033: Facility Admin's access to Patch Facility User
    [Tags]    FacilityAdmin FacilityUsersController      patch
    patch facility user  ${mmt_fac_id}    ${created_test_facility_user_id}   ${created_user_id}     TC 033

TC 034: Facility Admin's access to Put Facility User
    [Tags]    FacilityAdmin FacilityUsersController      put
    put facility user  ${mmt_fac_id}    ${created_test_facility_user_id}   ${created_user_id}   TC 034

TC 035: Facility Admin's access to Post Facility User
    [Tags]    FacilityAdmin FacilityUsersController      post
    post facility user  ${mmt_fac_id}   ${created_user_id}      TC 035

TC 036: Facility Admin's access to Delete Facility User
    [Tags]    FacilityAdmin FacilityUsersController      delete
    delete facility user    ${created_test_facility_user_id}    TC 036
