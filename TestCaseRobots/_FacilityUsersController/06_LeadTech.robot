*** Settings ***
Documentation    Test Cases to validate all Facility Users Controller endpoints authorization for Lead Tech role

Library  ../../TestCaseMethods/FacilityUsersControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***

${super_admin_token}
${test_lead_tech_token}
${created_facility_id}
${created_user_id}
${created_user_email}
${created_test_facility_user_id}
${created_test_facility_user_id_test}
${random_facility_user_id}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${lead_tech}
    delete test user    ${office_personnel}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    change selected facility  ${MMT_FAC_ID}

    ${random_facility_user_id}=  get random facility user id
    set suite variable  ${random_facility_user_id}
    ${created_user_id}=  create test user  ${office_personnel}
    set suite variable  ${created_user_id}
    ${created_user_email}=  get test user email  ${office_personnel}
    set suite variable  ${created_user_email}

    ${created_facility_id}=  create test facility  API
    set suite variable  ${created_facility_id}
    change selected facility  ${created_facility_id}
    create test user  ${lead_tech}

    ${created_test_facility_user_id}=  create a facility user  ${created_user_email}
    set suite variable  ${created_test_facility_user_id}


Multiple Teardown Methods
    change selected facility  ${MMT_FAC_ID}
    delete test user    ${lead_tech}
    RUN KEYWORD IF  $created_user_id is not None     delete user by id    ${created_user_id}
    RUN KEYWORD IF  $created_facility_id is not None     delete test facility    ${created_facility_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Facility Users Controller for Lead Tech
    initialize access for facility users controller  ${lead_tech}

Login with Lead Tech
    ${test_lead_tech_token}=   test user login by role     ${lead_tech}    ${None}
    set suite variable  ${test_lead_tech_token}
    set token facility users  ${test_lead_tech_token}

# FACILITY USERS CONTROLLER
TC 046: Lead Tech's access to Get Users
    [Tags]    LeadTech FacilityUsersController      GetUsers
    get users   TC 046

TC 047: Lead Tech's access to Get User
    [Tags]    LeadTech FacilityUsersController      GetUser
    get user  ${random_facility_user_id}    TC 047

TC 048: Lead Tech's access to Create Facility User
    [Tags]    LeadTech FacilityUsersController      CreateFacilityUser
    ${created_test_facility_user_id_test}=  create facility user tc  ${created_user_email}      TC 048
    set suite variable  ${created_test_facility_user_id_test}

TC 049: Lead Tech's access to Get User by Facility User ID
    [Tags]    LeadTech FacilityUsersController      get user by id
    get user by fac user id  ${random_facility_user_id}     TC 049

TC 050: Lead Tech's access to Get Facility Users
    [Tags]    LeadTech FacilityUsersController      get users
    get facility users  TC 050

TC 051: Lead Tech's access to Patch Facility User
    [Tags]    LeadTech FacilityUsersController      patch
    patch facility user  ${mmt_fac_id}    ${created_test_facility_user_id}   ${created_user_id}     TC 051

TC 052: Lead Tech's access to Put Facility User
    [Tags]    LeadTech FacilityUsersController      put
    put facility user  ${mmt_fac_id}    ${created_test_facility_user_id}   ${created_user_id}   TC 052

TC 053: Lead Tech's access to Post Facility User
    [Tags]    LeadTech FacilityUsersController      post
    post facility user  ${mmt_fac_id}   ${created_user_id}      TC 053

TC 054: Lead Tech's access to Delete Facility User
    [Tags]    LeadTech FacilityUsersController      delete
    delete facility user    ${created_test_facility_user_id}    TC 054
