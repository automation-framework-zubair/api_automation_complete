*** Settings ***
Documentation    Test Cases to validate all Audit Logs Controller endpoints authorization for Production role

Library  ../../TestCaseMethods/AuditLogsControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***
${super_admin_token}
${test_production_token}
${random_id}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${production}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    change selected facility  ${MMT_FAC_ID}
    create test user  ${production}
    ${random_id}=   get_random_facility_user_id
    set suite variable  ${random_id}

Multiple Teardown Methods
    delete test user    ${production}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Amplifiers Controller for Production
    initialize access for audit logs controller  ${super_admin}

Login with Production
    ${test_production_token}=   test user login by role     ${production}    ${None}
    set suite variable  ${test_production_token}
    set token audit logs  ${test_production_token}

# AMPLIFIERS CONTROLLER
TC 027: Production's access to Filtered Audit Logs
    [Tags]    Production AuditLogController      FilteredAuditLogs
    post filtered audit logs   ${none}  ${none}  ${none}  ${none}             TC 027

TC 028: Production's access to Log Audit Information
    [Tags]    Production AuditLogController      LogAuditInformation
    post log audit information  ${MMT_FAC_ID}           TC 028

TC 029: Production's access to Add Audit Log Items
    [Tags]    Production AuditLogController      AddAuditLogItems
    post add audit log items  1T1E2S2T3             TC 029

TC 030: Production's access to Delete Audit Log
    [Tags]    Production AuditLogController      Delete
    delete audit log  ${random_id}              TC 030

TC 031: Production's access to Get Audit Log by ID
    [Tags]    Production AuditLogController      GetByID
    get audit log by id  ${random_id}               TC 031

TC 032: Production's access to Get Audit Logs
    [Tags]    Production AuditLogController      Get
    get audit logs                      TC 032

TC 033: Production's access to Patch
    [Tags]    Production AuditLogController      Patch
    patch audit  ${random_id}                   TC 033

TC 034: Production's access to Put
    [Tags]    Production AuditLogController      Put
    put audit  ${random_id}                     TC 034

TC 035: Production's access to Post
    [Tags]    Production AuditLogController      Put
    post audit log                          TC 035
