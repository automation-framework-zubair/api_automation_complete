*** Settings ***
Documentation    Test Cases to validate all Audit Logs Controller endpoints authorization for Super Admin role

Library  ../../TestCaseMethods/AuditLogsControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***
${super_admin_token}
${test_super_admin_token}
${random_id}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${super_admin}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    get_version_number  ${super_admin_token}
    change selected facility  ${MMT_FAC_ID}
    create test user  ${super_admin}
    ${random_id}=   get_random_facility_user_id
    set suite variable  ${random_id}

Multiple Teardown Methods
    delete test user    ${super_admin}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Amplifiers Controller for Super Admin
    initialize access for audit logs controller  ${super_admin}

Login with Super Admin
    ${test_super_admin_token}=   test user login by role     ${super_admin}    ${None}
    set suite variable  ${test_super_admin_token}
    set token audit logs  ${test_super_admin_token}

# AMPLIFIERS CONTROLLER
TC 001: SuperAdmin's access to Filtered Audit Logs
    [Tags]    SuperAdmin AuditLogController      FilteredAuditLogs
    post filtered audit logs   ${none}  ${none}  ${none}  ${none}       TC 001

TC 002: SuperAdmin's access to Log Audit Information
    [Tags]    SuperAdmin AuditLogController      LogAuditInformation
    post log audit information  ${MMT_FAC_ID}       TC 002

TC 003: SuperAdmin's access to Add Audit Log Items
    [Tags]    SuperAdmin AuditLogController      AddAuditLogItems
    post add audit log items  1T1E2S2T3         TC 003

TC 004: SuperAdmin's access to Delete Audit Log
    [Tags]    SuperAdmin AuditLogController      Delete
    delete audit log  ${random_id}          TC 004

TC 005: SuperAdmin's access to Get Audit Log by ID
    [Tags]    SuperAdmin AuditLogController      GetByID
    get audit log by id  ${random_id}           TC 005

TC 006: SuperAdmin's access to Get Audit Logs
    [Tags]    SuperAdmin AuditLogController      Get
    get audit logs          TC 006

TC 007: SuperAdmin's access to Patch
    [Tags]    SuperAdmin AuditLogController      Patch
    patch audit  ${random_id}           TC 007

TC 008: SuperAdmin's access to Put
    [Tags]    SuperAdmin AuditLogController      Put
    put audit  ${random_id}         TC 008

TC 009: SuperAdmin's access to Post
    [Tags]    SuperAdmin AuditLogController      Put
    post audit log          TC 009
