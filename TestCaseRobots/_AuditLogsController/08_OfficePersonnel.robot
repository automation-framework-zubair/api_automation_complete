*** Settings ***
Documentation    Test Cases to validate all Audit Logs Controller endpoints authorization for OfficePersonnel role

Library  ../../TestCaseMethods/AuditLogsControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***
${super_admin_token}
${test_office_personnel_token}
${random_id}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${office_personnel}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    change selected facility  ${MMT_FAC_ID}
    create test user  ${office_personnel}
    ${random_id}=   get_random_facility_user_id
    set suite variable  ${random_id}

Multiple Teardown Methods
    delete test user    ${office_personnel}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Amplifiers Controller for OfficePersonnel
    initialize access for audit logs controller  ${super_admin}

Login with OfficePersonnel
    ${test_office_personnel_token}=   test user login by role     ${office_personnel}    ${None}
    set suite variable  ${test_office_personnel_token}
    set token audit logs  ${test_office_personnel_token}

# AMPLIFIERS CONTROLLER
TC 092: Office Personnel's access to Filtered Audit Logs
    [Tags]    OfficePersonnel AuditLogController      FilteredAuditLogs
    post filtered audit logs     ${none}  ${none}  ${none}  ${none}                           TC 092

TC 093: Office Personnel's access to Log Audit Information
    [Tags]    OfficePersonnel AuditLogController      LogAuditInformation
    post log audit information  ${MMT_FAC_ID}               TC 093

TC 094: Office Personnel's access to Add Audit Log Items
    [Tags]    OfficePersonnel AuditLogController      AddAuditLogItems
    post add audit log items  1T1E2S2T3                     TC 094

TC 095: Office Personnel's access to Delete Audit Log
    [Tags]    OfficePersonnel AuditLogController      Delete
    delete audit log  ${random_id}                          TC 095

TC 096: Office Personnel's access to Get Audit Log by ID
    [Tags]    OfficePersonnel AuditLogController      GetByID
    get audit log by id  ${random_id}                       TC 096

TC 097: Office Personnel's access to Get Audit Logs
    [Tags]    OfficePersonnel AuditLogController      Get
    get audit logs                                          TC 097

TC 098: Office Personnel's access to Patch
    [Tags]    OfficePersonnel AuditLogController      Patch
    patch audit  ${random_id}                               TC 098

TC 099: Office Personnel's access to Put
    [Tags]    OfficePersonnel AuditLogController      Put
    put audit  ${random_id}                                 TC 099

TC 100:Office Personnel's access to Post
    [Tags]    OfficePersonnel AuditLogController      Put
    post audit log                                          TC 100
