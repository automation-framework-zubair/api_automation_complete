*** Settings ***
Documentation    Test Cases to validate all Audit Logs Controller endpoints authorization for Support role

Library  ../../TestCaseMethods/AuditLogsControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***
${super_admin_token}
${test_support_token}
${random_id}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${support}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    change selected facility  ${MMT_FAC_ID}
    create test user  ${support}
    ${random_id}=   get_random_facility_user_id
    set suite variable  ${random_id}

Multiple Teardown Methods
    delete test user    ${support}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Amplifiers Controller for Support
    initialize access for audit logs controller  ${super_admin}

Login with Support
    ${test_support_token}=   test user login by role     ${support}    ${None}
    set suite variable  ${test_support_token}
    set token audit logs  ${test_support_token}

# AMPLIFIERS CONTROLLER
TC 014: Support access to Filtered Audit Logs
    [Tags]    Support AuditLogController      FilteredAuditLogs
    post filtered audit logs        ${none}  ${none}  ${none}  ${none}       TC 014

TC 015: Support's access to Log Audit Information
    [Tags]    Support AuditLogController      LogAuditInformation
    post log audit information  ${MMT_FAC_ID}           TC 015

TC 016: Support's access to Add Audit Log Items
    [Tags]    Support AuditLogController      AddAuditLogItems
    post add audit log items  1T1E2S2T3         TC 016

TC 017: Support's access to Delete Audit Log
    [Tags]    Support AuditLogController      Delete
    delete audit log  ${random_id}          TC 017

TC 018: Support's access to Get Audit Log by ID
    [Tags]    Support AuditLogController      GetByID
    get audit log by id  ${random_id}           TC 018

TC 019: Support's access to Get Audit Logs
    [Tags]    Support AuditLogController      Get
    get audit logs              TC 019

TC 020: Support's access to Patch
    [Tags]    Support AuditLogController      Patch
    patch audit  ${random_id}               TC 020

TC 021: Support's access to Put
    [Tags]    Support AuditLogController      Put
    put audit  ${random_id}                 TC 021

TC 022: Support's access to Post
    [Tags]    Support AuditLogController      Put
    post audit log                  TC 022
