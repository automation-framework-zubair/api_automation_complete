*** Settings ***
Documentation    Test Cases to validate all Audit Logs Controller endpoints authorization for FieldTech role

Library  ../../TestCaseMethods/AuditLogsControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***
${super_admin_token}
${test_lead_tech_token}
${random_id}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${lead_tech}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    change selected facility  ${MMT_FAC_ID}
    create test user  ${lead_tech}
    ${random_id}=   get_random_facility_user_id
    set suite variable  ${random_id}

Multiple Teardown Methods
    delete test user    ${lead_tech}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Amplifiers Controller for FieldTech
    initialize access for audit logs controller  ${super_admin}

Login with FieldTech
    ${test_lead_tech_token}=   test user login by role     ${lead_tech}    ${None}
    set suite variable  ${test_lead_tech_token}
    set token audit logs  ${test_lead_tech_token}

# AMPLIFIERS CONTROLLER
TC 079: Field Tech's access to Filtered Audit Logs
    [Tags]    FieldTech AuditLogController      FilteredAuditLogs
    post filtered audit logs   ${none}  ${none}  ${none}  ${none}      TC 079

TC 080: Field Tech's access to Log Audit Information
    [Tags]    FieldTech AuditLogController      LogAuditInformation
    post log audit information  ${MMT_FAC_ID}           TC 080

TC 081: Field Tech's access to Add Audit Log Items
    [Tags]    FieldTech AuditLogController      AddAuditLogItems
    post add audit log items  1T1E2S2T3                 TC 081

TC 082: Field Tech's access to Delete Audit Log
    [Tags]    FieldTech AuditLogController      Delete
    delete audit log  ${random_id}                      TC 082

TC 083: Field Tech's access to Get Audit Log by ID
    [Tags]    FieldTech AuditLogController      GetByID
    get audit log by id  ${random_id}                   TC 083

TC 084: Field Tech's access to Get Audit Logs
    [Tags]    FieldTech AuditLogController      Get
    get audit logs                                      TC 084

TC 085: Field Tech's access to Patch
    [Tags]    FieldTech AuditLogController      Patch
    patch audit  ${random_id}                           TC 085

TC 086: Field Tech's access to Put
    [Tags]    FieldTech AuditLogController      Put
    put audit  ${random_id}                             TC 086

TC 087: Field Tech's access to Post
    [Tags]    FieldTech AuditLogController      Put
    post audit log                                      TC 087
