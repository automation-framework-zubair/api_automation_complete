*** Settings ***
Documentation    Test Cases to validate all Audit Logs Controller endpoints authorization for LeadTech role

Library  ../../TestCaseMethods/AuditLogsControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***
${super_admin_token}
${test_lead_tech_token}
${random_id}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${lead_tech}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    change selected facility  ${MMT_FAC_ID}
    create test user  ${lead_tech}
    ${random_id}=   get_random_facility_user_id
    set suite variable  ${random_id}

Multiple Teardown Methods
    delete test user    ${lead_tech}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Amplifiers Controller for LeadTech
    initialize access for audit logs controller  ${super_admin}

Login with LeadTech
    ${test_lead_tech_token}=   test user login by role     ${lead_tech}    ${None}
    set suite variable  ${test_lead_tech_token}
    set token audit logs  ${test_lead_tech_token}

# AMPLIFIERS CONTROLLER
TC 066: Lead Tech's access to Filtered Audit Logs
    [Tags]    LeadTech AuditLogController      FilteredAuditLogs
    post filtered audit logs   ${none}  ${none}  ${none}  ${none}       TC 066

TC 067: Lead Tech's access to Log Audit Information
    [Tags]    LeadTech AuditLogController      LogAuditInformation
    post log audit information  ${MMT_FAC_ID}               TC 067

TC 068: Lead Tech's access to Add Audit Log Items
    [Tags]    LeadTech AuditLogController      AddAuditLogItems
    post add audit log items  1T1E2S2T3                     TC 068

TC 069: Lead Tech's access to Delete Audit Log
    [Tags]    LeadTech AuditLogController      Delete
    delete audit log  ${random_id}                          TC 069

TC 070: Lead Tech's access to Get Audit Log by ID
    [Tags]    LeadTech AuditLogController      GetByID
    get audit log by id  ${random_id}                       TC 070

TC 071: Lead Tech's access to Get Audit Logs
    [Tags]    LeadTech AuditLogController      Get
    get audit logs                                          TC 071

TC 072: Lead Tech's access to Patch
    [Tags]    LeadTech AuditLogController      Patch
    patch audit  ${random_id}                               TC 072

TC 073: Lead Tech's access to Put
    [Tags]    LeadTech AuditLogController      Put
    put audit  ${random_id}                                 TC 073

TC 074: Lead Tech's access to Post
    [Tags]    LeadTech AuditLogController      Put
    post audit log                                          TC 074
