*** Settings ***
Documentation    Test Cases to validate all Audit Logs Controller endpoints authorization for FacilityAdmin role

Library  ../../TestCaseMethods/AuditLogsControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***
${super_admin_token}
${test_facility_admin_token}
${random_id}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${facility_admin}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    change selected facility  ${MMT_FAC_ID}
    create test user  ${facility_admin}
    ${random_id}=   get_random_facility_user_id
    set suite variable  ${random_id}

Multiple Teardown Methods
    delete test user    ${facility_admin}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Amplifiers Controller for FacilityAdmin
    initialize access for audit logs controller  ${super_admin}

Login with FacilityAdmin
    ${test_facility_admin_token}=   test user login by role     ${facility_admin}    ${None}
    set suite variable  ${test_facility_admin_token}
    set token audit logs  ${test_facility_admin_token}

# AMPLIFIERS CONTROLLER
TC 040: Facility Admin's access to Filtered Audit Logs
    [Tags]    FacilityAdmin AuditLogController      FilteredAuditLogs
    post filtered audit logs   ${none}  ${none}  ${none}  ${none}             TC 040

TC 041: Facility Admin's access to Log Audit Information
    [Tags]    FacilityAdmin AuditLogController      LogAuditInformation
    post log audit information  ${MMT_FAC_ID}           TC 041

TC 042: Facility Admin's access to Add Audit Log Items
    [Tags]    FacilityAdmin AuditLogController      AddAuditLogItems
    post add audit log items  1T1E2S2T3             TC 042

TC 043: Facility Admin's access to Delete Audit Log
    [Tags]    FacilityAdmin AuditLogController      Delete
    delete audit log  ${random_id}              TC 043

TC 044: Facility Admin's access to Get Audit Log by ID
    [Tags]    FacilityAdmin AuditLogController      GetByID
    get audit log by id  ${random_id}           TC 044

TC 045: Facility Admin's access to Get Audit Logs
    [Tags]    FacilityAdmin AuditLogController      Get
    get audit logs                              TC 045

TC 046: Facility Admin's access to Patch
    [Tags]    FacilityAdmin AuditLogController      Patch
    patch audit  ${random_id}                   TC 046

TC 047: Facility Admin's access to Put
    [Tags]    FacilityAdmin AuditLogController      Put
    put audit  ${random_id}                     TC 047

TC 048: Facility Admin's access to Post
    [Tags]    FacilityAdmin AuditLogController      Put
    post audit log                              TC 048
