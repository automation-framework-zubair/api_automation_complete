*** Settings ***
Documentation    Test Cases to validate all Audit Logs Controller endpoints authorization for ReviewDoctor role

Library  ../../TestCaseMethods/AuditLogsControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***
${super_admin_token}
${test_review_doctor_token}
${random_id}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${review_doctor}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    change selected facility  ${MMT_FAC_ID}
    create test user  ${review_doctor}
    ${random_id}=   get_random_facility_user_id
    set suite variable  ${random_id}

Multiple Teardown Methods
    delete test user    ${review_doctor}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Amplifiers Controller for ReviewDoctor
    initialize access for audit logs controller  ${super_admin}

Login with ReviewDoctor
    ${test_review_doctor_token}=   test user login by role     ${review_doctor}    ${None}
    set suite variable  ${test_review_doctor_token}
    set token audit logs  ${test_review_doctor_token}

# AMPLIFIERS CONTROLLER
TC 053: ReviewDoctor's access to Filtered Audit Logs
    [Tags]    ReviewDoctor AuditLogController      FilteredAuditLogs
    post filtered audit logs    ${none}  ${none}  ${none}  ${none}            TC 053

TC 054: Review Doctor's access to Log Audit Information
    [Tags]    ReviewDoctor AuditLogController      LogAuditInformation
    post log audit information  ${MMT_FAC_ID}               TC 054

TC 055: Review Doctor's access to Add Audit Log Items
    [Tags]    ReviewDoctor AuditLogController      AddAuditLogItems
    post add audit log items  1T1E2S2T3                     TC 055

TC 056: Review Doctor's access to Delete Audit Log
    [Tags]    ReviewDoctor AuditLogController      Delete
    delete audit log  ${random_id}                          TC 056

TC 057: Review Doctor's access to Get Audit Log by ID
    [Tags]    ReviewDoctor AuditLogController      GetByID
    get audit log by id  ${random_id}                       TC 057

TC 058: Review Doctor's access to Get Audit Logs
    [Tags]    ReviewDoctor AuditLogController      Get
    get audit logs                                          TC 058

TC 059: Review Doctor's access to Patch
    [Tags]    ReviewDoctor AuditLogController      Patch
    patch audit  ${random_id}                               TC 059

TC 060: Review Doctor's access to Put
    [Tags]    ReviewDoctor AuditLogController      Put
    put audit  ${random_id}                                 TC 060

TC 061: Review Doctor's access to Post
    [Tags]    ReviewDoctor AuditLogController      Put
    post audit log                                          TC 061
