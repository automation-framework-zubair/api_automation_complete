*** Settings ***
Documentation    Test Cases to validate all Studies Controller endpoints authorization for Office Personnel role

Library     ../../TestCaseMethods/StudiesControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py
Library  ../../TestCaseMethods/_CommonStudiesTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***
${super_admin_token}
${test_study_id}
${test_patient_id}
${test_study_report_id}
${test_review_doctor_id}
${test_share_study_id}
${test_office_personnel_token}
${test_upload_study_id}

*** Keywords ***
Multiple Setup Methods
    delete test user        ${office_personnel}
    delete test patient     ${test_patient_id}
    delete test user        ${review_doctor}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable      ${super_admin_token}
    set common studies token  ${super_admin_token}
    change selected facility   ${mmt_fac_id}
    create test user        ${office_personnel}
    ${test_patient_id}=  create test patient
    set suite variable      ${test_patient_id}
    ${test_study_id}=  get video study id
    set suite variable      ${test_study_id}
    get test study information   ${test_study_id}
    ${test_review_doctor_id}=  create test user    ${review_doctor}
    set suite variable      ${test_review_doctor_id}
    share test study   ${test_study_id}   ${test_review_doctor_id}
    change test study state  ${test_study_id}
    ${test_share_study_id}=  get share study id   ${test_study_id}  ${test_review_doctor_id}
    set suite variable   ${test_share_study_id}

Multiple Teardown Methods
    delete test user       ${office_personnel}
    delete test patient    ${test_patient_id}
    delete test user       ${review_doctor}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Studies Controller for Office Personnel
    initialize access for studies controller  ${office_personnel}

Login with Office Personnel
    ${test_office_personnel_token}=  test user login by role  ${office_personnel}  ${None}
    set suite variable  ${test_office_personnel_token}
    set token studies   ${test_office_personnel_token}

#STUDIES CONTROLLER
TC 169: Office Personnel's access to Assign Patient
    [Tags]    OfficePersonnel StudiesController       AssignPatient
    log  UNABLE TO TEST ADD ASSIGN PATIENT. ACCESS USERS' INFORMATION MISSING    WARN

TC 170: Office Personnel's access to StudyUploadSetup
    [Tags]      OfficePersonnel StudiesController       StudyUploadSetup
    ${test_upload_study_id}=  post study upload study setup tc  ${test_patient_id}  ${none}  ${none}  ${none}  ${none}   ${none}      TC 170
    set suite variable   ${test_upload_study_id}

TC 171: Office Personnel's access to AwsBucketForStudy
    [Tags]      OfficePersonnel StudiesController       AwsBucketForStudy
    post aws bucket for study  ${test_study_id}         TC 171

TC 172: Office Personnel's access to Study List View
    [Tags]      OfficePersonnel StudiesController       StudyListView
    get search list view             ${none}  ${none}  ${none}            TC 172

TC 173: Office Personnel's access to Study View Study ID
    [Tags]      OfficePersonnel StudiesController       StudyView
    get study view   ${test_study_id}               TC 173

TC 174: Office Personnel's access to Study Sync Info with Study ID
    [Tags]      OfficePersonnel StudiesController       StudySyncInfo
    get study sync info   ${test_study_id}          TC 174

TC 175: Office Personnel's access to Study Serialize with Study ID
    [Tags]      OfficePersonnel StudiesController       StudySerialize
    get serialize study    ${test_study_id}             TC 175

TC 176: Office Personnel's access to Study Deserialize
    [Tags]      OfficePersonnel StudiesController       StudyDeserializa
    log  UNABLE TO TEST ADD STUDY DESERIALIZE.PARAMS ARE UNKNOWN    WARN

TC 177: Office Personnel's access to Study Dat File with Study ID
    [Tags]      OfficePersonnel StudiesController       StudyDatFile
    post generate study dat file   ${test_study_id}         TC 177

TC 178: Office Personnel's access to Studies with ID
    [Tags]      OfficePersonnel StudiesController       StudiesID
    get studies by id  ${test_study_id}             TC 176

TC 179: Office Personnel's access to Video Recording Index Camera Index
    [Tags]      OfficePersonnel StudiesController       VideoRecordingIndexCameraIndex
    ${test_video_index}=   get video index
    ${test_recording_index}=  get recording index
    ${test_camera_index}=  get camera index
    get videorecording index camera index    ${test_study_id}   ${test_recording_index}     ${test_camera_index}       ${test_video_index}      ${none}     TC 179

TC 180: Office Personnel's access to Update Study Notes with Study ID
    [Tags]      OfficePersonnel StudiesController           UpdateStudyNotes
    post update study notes    ${test_study_id}             TC 180

TC 181: Office Personnel's access to Study Report with Study ID
    [Tags]      OfficePersonnel StudiesController       StudyReport
    ${test_study_report_id}=    get study report tc    ${test_study_id}         TC 181
    set suite variable    ${test_study_report_id}

TC 182: Office Personnel's access to Update Study Notes with Study ID and Report ID
    [Tags]      OfficePersonnel StudiesController       UpdateStudyReport
    post update study report    ${test_study_id}    ${test_study_report_id}             TC 182

TC 183: Office Personnel's access to Restore Study with ID
    [Tags]      OfficePersonnel StudiesController       RestoreStudy
    post restore study   ${test_study_id}   ${mmt_fac_id}           TC 183

TC 184: Office Personnel's access to Create Study
    [Tags]      OfficePersonnel StudiesController       CreateStudy
    log  UNABLE TO TEST CREATE STUDY. STUDY CANNOT BE CREATED VIA API   WARN

TC 185: Office Personnel's access to Change Study State with ID
    [Tags]      OfficePersonnel StudiesController       StudySerialize
    post change study state    ${test_study_id}         ${none}         TC 185

TC 186: Office Personnel's access to Shared Studies with ID
    [Tags]      OfficePersonnel StudiesController       SharedStudies
    get shared studies    ${test_study_id}              TC 186

TC 187: Office Personnel's access to Remove Shared Study
    [Tags]      OfficePersonnel StudiesController       RemoveShareStudies
    post remove shared study   ${test_share_study_id}           TC 187

TC 188: Office Personnel's access to Get Studies
    [Tags]    OfficePersonnel StudiesController        Get
    get studies             TC 188

TC 189: Office Personnel's access to Put Studies with ID
    [Tags]    OfficePersonnel StudiesController        PutStudies
    ${test_sync_state}=  get study state
    ${test_study_state}=  get study state
    ${test_amplifier_id}=  get amplifier id
    ${test_created_date}=  get created study date
    put studies tc  ${test_study_id}   ${test_sync_state}   ${test_study_state}   ${test_amplifier_id}  ${mmt_fac_id}  ${test_created_date}     TC 189

TC 190: Office Personnel's access to Patch Studies with ID
    [Tags]    OfficePersonnel StudiesController        PatchStudies
    patch studies    ${test_study_id}           TC 190

TC 191: Office Personnel's access to Post Delete Study with ID
    [Tags]    OfficePersonnel StudiesController        DeleteStudy
    post delete study   ${test_study_id}        ${none}        TC 191

TC 192: Office Personnel's access to Delete Studies with ID
    [Tags]    OfficePersonnel StudiesController        DeleteStudies
    delete study id   ${test_study_id}           TC 192



