*** Settings ***
Documentation    Test Cases to validate all Studies Controller endpoints authorization for Field Tech role

Library     ../../TestCaseMethods/StudiesControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py
Library  ../../TestCaseMethods/_CommonStudiesTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***
${super_admin_token}
${test_study_id}
${test_patient_id}
${test_study_report_id}
${test_field_tech_id}
${test_share_study_id}
${test_field_tech_token}
${test_upload_study_id}

*** Keywords ***
Multiple Setup Methods
    delete test user        ${field_tech}
    delete test patient     ${test_patient_id}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable      ${super_admin_token}
    set common studies token  ${super_admin_token}
    change selected facility   ${mmt_fac_id}
    ${test_field_tech_id}=  create test user        ${field_tech}
    ${test_patient_id}=  create test patient
    set suite variable      ${test_patient_id}
    ${test_study_id}=  get video study id
    set suite variable      ${test_study_id}
    get test study information   ${test_study_id}
    change test study state  ${test_study_id}
    share test study   ${test_study_id}   ${test_field_tech_id}
    ${test_share_study_id}=  get share study id   ${test_study_id}  ${test_field_tech_id}
    set suite variable   ${test_share_study_id}

Multiple Teardown Methods
    delete test user       ${field_tech}
    delete test patient    ${test_patient_id}
    delete test user       ${review_doctor}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Studies Controller for Field Tech
    initialize access for studies controller  ${field_tech}

Login with Field Tech
    ${test_field_tech_token}=  test user login by role  ${field_tech}  ${None}
    set suite variable  ${test_field_tech_token}
    set token studies   ${test_field_tech_token}

#STUDIES CONTROLLER
TC 145: Field Tech's access to Assign Patient
    [Tags]    FieldTech StudiesController       AssignPatient
    log  UNABLE TO TEST ADD ASSIGN PATIENT. ACCESS USERS' INFORMATION MISSING    WARN

TC 146: Field Tech's access to StudyUploadSetup
    [Tags]      FieldTech StudiesController       StudyUploadSetup
    ${test_upload_study_id}=  post study upload study setup tc  ${test_patient_id}  ${none}  ${none}  ${none}  ${none}   ${none}      TC 146
    set suite variable   ${test_upload_study_id}

TC 147: Field Tech's access to AwsBucketForStudy
    [Tags]      FieldTech StudiesController       AwsBucketForStudy
    post aws bucket for study  ${test_study_id}     TC 147

TC 148: Field Tech's access to Study List View
    [Tags]      FieldTech StudiesController       StudyListView
    get search list view            ${none}  ${none}  ${none}           TC 148

TC 149: Field Tech's access to Study View Study ID
    [Tags]      FieldTech StudiesController       StudyView
    get study view   ${test_study_id}       TC 149

TC 150: Field Tech's access to Study Sync Info with Study ID
    [Tags]      FieldTech StudiesController       StudySyncInfo
    get study sync info   ${test_study_id}      TC 150

TC 151: Field Tech's access to Study Serialize with Study ID
    [Tags]      FieldTech StudiesController       StudySerialize
    get serialize study    ${test_study_id}         TC 151

TC 152: Field Tech's access to Study Deserialize
    [Tags]      FieldTech StudiesController       StudyDeserializa
    log  UNABLE TO TEST ADD STUDY DESERIALIZE.PARAMS ARE UNKNOWN    WARN

TC 153: Field Tech's access to Study Dat File with Study ID
    [Tags]      FieldTech StudiesController       StudyDatFile
    post generate study dat file   ${test_study_id}         TC 153

TC 154: Field Tech's access to Studies with ID
    [Tags]      FieldTech StudiesController       StudiesID
    get studies by id  ${test_study_id}             TC 154

TC 155: Field Tech's access to Video Recording Index Camera Index
    [Tags]      FieldTech StudiesController       VideoRecordingIndexCameraIndex
    ${test_video_index}=   get video index
    ${test_recording_index}=  get recording index
    ${test_camera_index}=  get camera index
    get videorecording index camera index    ${test_study_id}   ${test_recording_index}     ${test_camera_index}       ${test_video_index}      ${none}     TC 155

TC 156: Field Tech's access to Update Study Notes with Study ID
    [Tags]      FieldTech StudiesController           UpdateStudyNotes
    post update study notes    ${test_study_id}         TC 156

TC 157: Field Tech's access to Study Report with Study ID
    [Tags]      FieldTech StudiesController       StudyReport
    ${test_study_report_id}=    get study report tc    ${test_study_id}             TC 157
    set suite variable    ${test_study_report_id}

TC 158: Field Tech's access to Update Study Notes with Study ID and Report ID
    [Tags]      FieldTech StudiesController       UpdateStudyReport
    post update study report    ${test_study_id}    ${test_study_report_id}         TC 158

TC 159: Field Tech's access to Restore Study with ID
    [Tags]      FieldTech StudiesController       RestoreStudy
    post restore study   ${test_study_id}   ${mmt_fac_id}           TC 159

TC 160: Field Tech's access to Create Study
    [Tags]      FieldTech StudiesController       CreateStudy
    log  UNABLE TO TEST CREATE STUDY. STUDY CANNOT BE CREATED VIA API   WARN

TC 161: Field Tech's access to Change Study State with ID
    [Tags]      FieldTech StudiesController       StudySerialize
    post change study state    ${test_study_id}     ${none}         TC 161

TC 162: Field Tech's access to Shared Studies with ID
    [Tags]      FieldTech StudiesController       SharedStudies
    get shared studies    ${test_study_id}          TC 162

TC 163: Field Tech's access to Remove Shared Study
    [Tags]      FieldTech StudiesController       RemoveShareStudies
    post remove shared study   ${test_share_study_id}           TC 163

TC 164: Field Tech's access to Get Studies
    [Tags]    FieldTech StudiesController        Get
    get studies         TC 164

TC 165: Field Tech's access to Put Studies with ID
    [Tags]    FieldTech StudiesController        PutStudies
    ${test_sync_state}=  get study state
    ${test_study_state}=  get study state
    ${test_amplifier_id}=  get amplifier id
    ${test_created_date}=  get created study date
    put studies tc  ${test_study_id}   ${test_sync_state}   ${test_study_state}   ${test_amplifier_id}  ${mmt_fac_id}  ${test_created_date}     TC 165

TC 166: Field Tech's access to Patch Studies with ID
    [Tags]    FieldTech StudiesController        PatchStudies
    patch studies    ${test_study_id}           TC 166

TC 167: Field Tech's access to Post Delete Study with ID
    [Tags]    FieldTech StudiesController        DeleteStudy
    post delete study   ${test_study_id}            ${none}        TC 167

TC 168: Field Tech's access to Delete Studies with ID
    [Tags]    FieldTech StudiesController        DeleteStudies
    delete study id   ${test_study_id}           TC 168



