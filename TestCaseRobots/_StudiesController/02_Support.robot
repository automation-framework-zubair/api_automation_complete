*** Settings ***
Documentation    Test Cases to validate all Studies Controller endpoints authorization for Support role

Library     ../../TestCaseMethods/StudiesControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py
Library  ../../TestCaseMethods/_CommonStudiesTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***
${super_admin_token}
${test_study_id}
${test_patient_id}
${test_study_report_id}
${test_review_doctor_id}
${test_share_study_id}
${test_support_token}
${test_upload_study_id}

*** Keywords ***
Multiple Setup Methods
    delete test user        ${support}
    delete test patient     ${test_patient_id}
    delete test user        ${review_doctor}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable      ${super_admin_token}
    set common studies token  ${super_admin_token}
    change selected facility   ${mmt_fac_id}
    create test user        ${support}
    ${test_patient_id}=  create test patient
    set suite variable      ${test_patient_id}
    ${test_study_id}=  get video study id
    set suite variable      ${test_study_id}
    get test study information   ${test_study_id}
    ${test_review_doctor_id}=  create test user    ${review_doctor}
    set suite variable      ${test_review_doctor_id}
    share test study   ${test_study_id}   ${test_review_doctor_id}
    ${test_share_study_id}=  get share study id   ${test_study_id}  ${test_review_doctor_id}
    set suite variable   ${test_share_study_id}

Multiple Teardown Methods
    delete test user       ${support}
    delete test patient    ${test_patient_id}
    delete test user       ${review_doctor}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Studies Controller for Support
    initialize access for studies controller  ${support}

Login with Support
    ${test_support_token}=  test user login by role  ${support}  ${None}
    set suite variable  ${test_support_token}
    set token studies   ${test_support_token}

#STUDIES CONTROLLER
TC 025: Support's access to Assign Patient
    [Tags]    Support StudiesController       AssignPatient
    log  UNABLE TO TEST ADD ASSIGN PATIENT. ACCESS USERS' INFORMATION MISSING    WARN

TC 026: Support's access to StudyUploadSetup
    [Tags]      Support StudiesController       StudyUploadSetup
    ${test_upload_study_id}=  post study upload study setup tc  ${test_patient_id}      ${none}  ${none}  ${none}  ${none}   ${none}      TC 026
    set suite variable   ${test_upload_study_id}

TC 027: Support's access to AwsBucketForStudy
    [Tags]      Support StudiesController       AwsBucketForStudy
    post aws bucket for study  ${test_study_id}                 TC 027

TC 028: Support's access to Study List View
    [Tags]      Support StudiesController       StudyListView
    get search list view            ${none}  ${none}  ${none}           TC 028

TC 029: Support's access to Study View Study ID
    [Tags]      Support StudiesController       StudyView
    get study view   ${test_study_id}           TC 029

TC 030: Support's access to Study Sync Info with Study ID
    [Tags]      Support StudiesController       StudySyncInfo
    get study sync info   ${test_study_id}          TC 030

TC 031: Support's access to Study Serialize with Study ID
    [Tags]      Support StudiesController       StudySerialize
    get serialize study    ${test_study_id}     TC 031

TC 032: Support's access to Study Deserialize
    [Tags]      Support StudiesController       StudyDeserialize
    log  UNABLE TO TEST ADD STUDY DESERIALIZE.PARAMS ARE UNKNOWN    WARN

TC 033: Support's access to Study Dat File with Study ID
    [Tags]      Support StudiesController       StudyDatFile
    post generate study dat file   ${test_study_id}         TC 033

TC 034: Support's access to Studies with ID
    [Tags]      Support StudiesController       StudiesID
    get studies by id  ${test_study_id}         TC 034

TC 035: Support's access to Video Recording Index Camera Index
    [Tags]      Support StudiesController       VideoRecordingIndexCameraIndex
    ${test_video_index}=   get video index
    ${test_recording_index}=  get recording index
    ${test_camera_index}=  get camera index
    get videorecording index camera index    ${test_study_id}   ${test_recording_index}     ${test_camera_index}       ${test_video_index}      ${none}     TC 035

TC 036: Support's access to Update Study Notes with Study ID
    [Tags]      Support StudiesController           UpdateStudyNotes
    post update study notes    ${test_study_id}         TC 036

TC 037: Support's access to Study Report with Study ID
    [Tags]      Support StudiesController       StudyReport
    ${test_study_report_id}=    get study report tc    ${test_study_id}         TC 037
    set suite variable    ${test_study_report_id}

TC 038: Support's access to Update Study Notes with Study ID and Report ID
    [Tags]      Support StudiesController       UpdateStudyReport
    post update study report    ${test_study_id}    ${test_study_report_id}     TC 038

TC 039: Support's access to Restore Study with ID
    [Tags]      Support StudiesController       RestoreStudy
    post restore study   ${test_study_id}   ${mmt_fac_id}       TC 039

TC 040: Support's access to Create Study
    [Tags]      Support StudiesController       CreateStudy
     log  UNABLE TO TEST CREATE STUDY. STUDY CANNOT BE CREATED VIA API   WARN

TC 041: Support's access to Change Study State with ID
    [Tags]      Support StudiesController       StudySerialize
    post change study state    ${test_study_id}         ${none}         TC 041

TC 042: Support's access to Shared Studies with ID
    [Tags]      Support StudiesController       SharedStudies
    get shared studies    ${test_study_id}          TC 042

TC 043: Support's access to Remove Shared Study
    [Tags]      Support StudiesController       RemoveShareStudies
    post remove shared study   ${test_share_study_id}           TC 043

TC 044: Support's access to Get Studies
    [Tags]    Support StudiesController        Get
    get studies         TC 044

TC 045: Support's access to Put Studies with ID
    [Tags]    Support StudiesController        PutStudies
    ${test_sync_state}=  get study state
    ${test_study_state}=  get study state
    ${test_amplifier_id}=  get amplifier id
    ${test_created_date}=  get created study date
    put studies tc  ${test_study_id}   ${test_sync_state}   ${test_study_state}   ${test_amplifier_id}  ${mmt_fac_id}  ${test_created_date}     TC 045

TC 046: Support's access to Patch Studies with ID
    [Tags]    Support StudiesController        PatchStudies
    patch studies    ${test_study_id}           TC 046

TC 047: Support's access to Post Delete Study with ID
    [Tags]    Support StudiesController        DeleteStudy
    post delete study   ${test_study_id}        ${none}    TC 047

TC 048: Support's access to Delete Studies with ID
    [Tags]    Support StudiesController        DeleteStudies
    delete study id   ${test_study_id}          TC 048



