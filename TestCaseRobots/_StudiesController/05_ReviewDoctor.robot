*** Settings ***
Documentation    Test Cases to validate all Studies Controller endpoints authorization for Review Doctor role

Library     ../../TestCaseMethods/StudiesControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py
Library  ../../TestCaseMethods/_CommonStudiesTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***
${super_admin_token}
${test_study_id}
${test_patient_id}
${test_study_report_id}
${test_review_doctor_id}
${test_share_study_id}
${test_review_doctor_token}
${test_upload_study_id}

*** Keywords ***
Multiple Setup Methods
    delete test user        ${review_doctor}
    delete test patient     ${test_patient_id}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable      ${super_admin_token}
    set common studies token  ${super_admin_token}
    change selected facility   ${mmt_fac_id}
    ${test_review_doctor_id}=  create test user    ${review_doctor}
    set suite variable      ${test_review_doctor_id}
    ${test_patient_id}=  create test patient
    set suite variable      ${test_patient_id}
    ${test_study_id}=  get video study id
    set suite variable      ${test_study_id}
    get test study information   ${test_study_id}
    change test study state  ${test_study_id}
    share test study   ${test_study_id}   ${test_review_doctor_id}
    ${test_share_study_id}=  get share study id   ${test_study_id}  ${test_review_doctor_id}


Multiple Teardown Methods
    delete test user       ${review_doctor}
    delete test patient    ${test_patient_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Studies Controller for Review Doctor
    initialize access for studies controller  ${review_doctor}

Login with Review Doctor
    ${test_review_doctor_token}=  test user login by role  ${review_doctor}  ${None}
    set suite variable  ${test_review_doctor_token}
    set token studies   ${test_review_doctor_token}

#STUDIES CONTROLLER
TC 097: Review Doctor's access to Assign Patient
    [Tags]    ReviewDoctor StudiesController       AssignPatient
    log  UNABLE TO TEST ADD ASSIGN PATIENT. ACCESS USERS' INFORMATION MISSING    WARN

TC 098: Review Doctor's access to StudyUploadSetup
    [Tags]      ReviewDoctor StudiesController       StudyUploadSetup
    ${test_upload_study_id}=  post study upload study setup tc  ${test_patient_id}  ${none}  ${none}  ${none}  ${none}   ${none}      TC 098
    set suite variable   ${test_upload_study_id}

TC 099: Review Doctor's access to AwsBucketForStudy
    [Tags]      ReviewDoctor StudiesController       AwsBucketForStudy
    post aws bucket for study  ${test_study_id}         TC 099

TC 100: Review Doctor's access to Study List View
    [Tags]      ReviewDoctor StudiesController       StudyListView
    get search list view            ${none}  ${none}  ${none}           TC 100

TC 101: Review Doctor's access to Study View Study ID
    [Tags]      ReviewDoctor StudiesController       StudyView
    get study view   ${test_study_id}           TC 101

TC 102: Review Doctor's access to Study Sync Info with Study ID
    [Tags]      ReviewDoctor StudiesController       StudySyncInfo
    get study sync info   ${test_study_id}      TC 102

TC 103: Review Doctor's access to Study Serialize with Study ID
    [Tags]      ReviewDoctor StudiesController       StudySerialize
    get serialize study    ${test_study_id}     TC 103

TC 104: Review Doctor's access to Study Deserialize
    [Tags]      ReviewDoctor StudiesController       StudyDeserializa
    log  UNABLE TO TEST ADD STUDY DESERIALIZE.PARAMS ARE UNKNOWN    WARN

TC 105: Review Doctor's access to Study Dat File with Study ID
    [Tags]      ReviewDoctor StudiesController       StudyDatFile
    post generate study dat file   ${test_study_id}     TC 105

TC 106: Review Doctor's access to Studies with ID
    [Tags]      ReviewDoctor StudiesController       StudiesID
    get studies by id  ${test_study_id}     TC 106

TC 107: Review Doctor's access to Video Recording Index Camera Index
    [Tags]      ReviewDoctor StudiesController       VideoRecordingIndexCameraIndex
    ${test_video_index}=   get video index
    ${test_recording_index}=  get recording index
    ${test_camera_index}=  get camera index
    get videorecording index camera index    ${test_study_id}   ${test_recording_index}     ${test_camera_index}       ${test_video_index}      ${none}     TC 107

TC 108: Review Doctor's access to Update Study Notes with Study ID
    [Tags]      ReviewDoctor StudiesController           UpdateStudyNotes
    post update study notes    ${test_study_id}     TC 108

TC 109: Review Doctor's access to Study Report with Study ID
    [Tags]      ReviewDoctor StudiesController       StudyReport
    ${test_study_report_id}=    get study report tc    ${test_study_id}     TC 109
    set suite variable    ${test_study_report_id}

TC 110: Review Doctor's access to Update Study Notes with Study ID and Report ID
    [Tags]      ReviewDoctor StudiesController       UpdateStudyReport
    post update study report    ${test_study_id}    ${test_study_report_id}     TC 110

TC 111: Review Doctor's access to Restore Study with ID
    [Tags]      ReviewDoctor StudiesController       RestoreStudy
    post restore study   ${test_study_id}   ${mmt_fac_id}       TC 111

TC 112: Review Doctor's access to Create Study
    [Tags]      ReviewDoctor StudiesController       CreateStudy
    log  UNABLE TO TEST CREATE STUDY. STUDY CANNOT BE CREATED VIA API   WARN

TC 113: Review Doctor's access to Change Study State with ID
    [Tags]      ReviewDoctor StudiesController       StudySerialize
    post change study state    ${test_study_id}     ${none}         TC 113

TC 114: Review Doctor's access to Shared Studies with ID
    [Tags]      ReviewDoctor StudiesController       SharedStudies
    get shared studies    ${test_study_id}          TC 114

TC 115: Review Doctor's access to Remove Shared Study
    [Tags]      ReviewDoctor StudiesController       RemoveShareStudies
    post remove shared study   ${test_share_study_id}           TC 115

TC 116: Review Doctor's access to Get Studies
    [Tags]    ReviewDoctor StudiesController        Get
    get studies         TC 116

TC 117: Review Doctor's access to Put Studies with ID
    [Tags]    ReviewDoctor StudiesController        PutStudies
    ${test_sync_state}=  get study state
    ${test_study_state}=  get study state
    ${test_amplifier_id}=  get amplifier id
    ${test_created_date}=  get created study date
    put studies tc  ${test_study_id}   ${test_sync_state}   ${test_study_state}   ${test_amplifier_id}  ${mmt_fac_id}  ${test_created_date}         TC 117

TC 118: Review Doctor's access to Patch Studies with ID
    [Tags]    ReviewDoctor StudiesController        PatchStudies
    patch studies    ${test_study_id}       TC 118

TC 119: Review Doctor's access to Post Delete Study with ID
    [Tags]    ReviewDoctor StudiesController        DeleteStudy
    post delete study   ${test_study_id}     ${none}       TC 119

TC 120: Review Doctor's access to Delete Studies with ID
    [Tags]    ReviewDoctor StudiesController        DeleteStudies
    delete study id   ${test_study_id}       TC 120



