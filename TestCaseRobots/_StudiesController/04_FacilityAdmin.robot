*** Settings ***
Documentation    Test Cases to validate all Studies Controller endpoints authorization for Facility Admin role

Library     ../../TestCaseMethods/StudiesControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py
Library  ../../TestCaseMethods/_CommonStudiesTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***
${super_admin_token}
${test_study_id}
${test_patient_id}
${test_study_report_id}
${test_review_doctor_id}
${test_share_study_id}
${test_facility_admin_token}
${test_upload_study_id}

*** Keywords ***
Multiple Setup Methods
    delete test user        ${facility_admin}
    delete test patient     ${test_patient_id}
    delete test user        ${review_doctor}
    ${super_admin_token}=   user login by role   ${super_admin}
    set common studies token  ${super_admin_token}
    set suite variable      ${super_admin_token}
    change selected facility   ${mmt_fac_id}
    create test user        ${facility_admin}
    ${test_patient_id}=  create test patient
    set suite variable      ${test_patient_id}
    ${test_study_id}=  get video study id
    set suite variable      ${test_study_id}
    get test study information   ${test_study_id}
    ${test_review_doctor_id}=  create test user    ${review_doctor}
    set suite variable      ${test_review_doctor_id}
    share test study   ${test_study_id}   ${test_review_doctor_id}
    ${test_share_study_id}=  get share study id   ${test_study_id}  ${test_review_doctor_id}
    set suite variable   ${test_share_study_id}

Multiple Teardown Methods
    delete test user       ${facility_admin}
    delete test patient    ${test_patient_id}
    delete test user       ${review_doctor}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Studies Controller for Facility Admin
    initialize access for studies controller  ${facility_admin}

Login with Facility Admin
    ${test_facility_admin_token}=  test user login by role  ${facility_admin}  ${None}
    set suite variable  ${test_facility_admin_token}
    set token studies   ${test_facility_admin_token}

#STUDIES CONTROLLER
TC 073: Facility Admin's access to Assign Patient
    [Tags]    FacilityAdmin StudiesController       AssignPatient
    log  UNABLE TO TEST ADD ASSIGN PATIENT. ACCESS USERS' INFORMATION MISSING    WARN

TC 074: Facility Admin's access to StudyUploadSetup
    [Tags]      FacilityAdmin StudiesController       StudyUploadSetup
    ${test_upload_study_id}=  post study upload study setup tc  ${test_patient_id}  ${none}  ${none}  ${none}  ${none}   ${none}      TC 074
    set suite variable   ${test_upload_study_id}

TC 075: Facility Admin's access to AwsBucketForStudy
    [Tags]      FacilityAdmin StudiesController       AwsBucketForStudy
    post aws bucket for study  ${test_study_id}             TC 075

TC 076: Facility Admin's access to Study List View
    [Tags]      FacilityAdmin StudiesController       StudyListView
    get search list view            ${none}  ${none}  ${none}       TC 076

TC 077: Facility Admin's access to Study View Study ID
    [Tags]      FacilityAdmin StudiesController       StudyView
    get study view   ${test_study_id}           TC 077

TC 078: Facility Admin's access to Study Sync Info with Study ID
    [Tags]      FacilityAdmin StudiesController       StudySyncInfo
    get study sync info   ${test_study_id}          TC 078

TC 079: Facility Admin's access to Study Serialize with Study ID
    [Tags]      FacilityAdmin StudiesController       StudySerialize
    get serialize study    ${test_study_id}         TC 079

TC 080: Facility Admin's access to Study Deserialize
    [Tags]      FacilityAdmin StudiesController       StudyDeserializa
    log  UNABLE TO TEST ADD STUDY DESERIALIZE.PARAMS ARE UNKNOWN    WARN

TC 081: Facility Admin's access to Study Dat File with Study ID
    [Tags]      FacilityAdmin StudiesController       StudyDatFile
    post generate study dat file   ${test_study_id}         TC 081

TC 082: Facility Admin's access to Studies with ID
    [Tags]      FacilityAdmin StudiesController       StudiesID
    get studies by id  ${test_study_id}         TC 082

TC 083: Facility Admin's access to Video Recording Index Camera Index
    [Tags]      FacilityAdmin StudiesController       VideoRecordingIndexCameraIndex
    ${test_video_index}=   get video index
    ${test_recording_index}=  get recording index
    ${test_camera_index}=  get camera index
    get videorecording index camera index    ${test_study_id}   ${test_recording_index}     ${test_camera_index}       ${test_video_index}      ${none}     TC 083

TC 084: Facility Admin's access to Update Study Notes with Study ID
    [Tags]      FacilityAdmin StudiesController           UpdateStudyNotes
    post update study notes    ${test_study_id}         TC 084

TC 085: Facility Admin's access to Study Report with Study ID
    [Tags]      FacilityAdmin StudiesController       StudyReport
    ${test_study_report_id}=    get study report tc    ${test_study_id}             TC 085
    set suite variable    ${test_study_report_id}

TC 086: Facility Admin's access to Update Study Notes with Study ID and Report ID
    [Tags]      FacilityAdmin StudiesController       UpdateStudyReport
    post update study report    ${test_study_id}    ${test_study_report_id}         TC 086

TC 087: Facility Admin's access to Restore Study with ID
    [Tags]      FacilityAdmin StudiesController       RestoreStudy
    post restore study   ${test_study_id}   ${mmt_fac_id}           TC 087

TC 088: Facility Admin's access to Create Study
    [Tags]      FacilityAdmin StudiesController       CreateStudy
    log  UNABLE TO TEST CREATE STUDY. STUDY CANNOT BE CREATED VIA API   WARN

TC 089: Facility Admin's access to Change Study State with ID
    [Tags]      FacilityAdmin StudiesController       StudySerialize
    post change study state    ${test_study_id}         ${none}         TC 089

TC 090: Facility Admin's access to Shared Studies with ID
    [Tags]      FacilityAdmin StudiesController       SharedStudies
    get shared studies    ${test_study_id}          TC 090

TC 091: Facility Admin's access to Remove Shared Study
    [Tags]      FacilityAdmin StudiesController       RemoveShareStudies
    post remove shared study   ${test_share_study_id}       TC 091

TC 092: Facility Admin's access to Get Studies
    [Tags]    FacilityAdmin StudiesController        Get
    get studies             TC 092

TC 093: Facility Admin's access to Put Studies with ID
    [Tags]    FacilityAdmin StudiesController        PutStudies
    ${test_sync_state}=  get study state
    ${test_study_state}=  get study state
    ${test_amplifier_id}=  get amplifier id
    ${test_created_date}=  get created study date
    put studies tc  ${test_study_id}   ${test_sync_state}   ${test_study_state}   ${test_amplifier_id}  ${mmt_fac_id}  ${test_created_date}         TC 093

TC 094: Facility Admin's access to Patch Studies with ID
    [Tags]    FacilityAdmin StudiesController        PatchStudies
    patch studies    ${test_study_id}           TC 094

TC 095: Facility Admin's access to Post Delete Study with ID
    [Tags]    FacilityAdmin StudiesController        DeleteStudy
    post delete study   ${test_study_id}        ${none}      TC 095

TC 096: Facility Admin's access to Delete Studies with ID
    [Tags]    FacilityAdmin StudiesController        DeleteStudies
    delete study id   ${test_study_id}           TC 096



