*** Settings ***
Documentation    Test Cases to validate all Studies Controller endpoints authorization for Production role

Library     ../../TestCaseMethods/StudiesControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py
Library  ../../TestCaseMethods/_CommonStudiesTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***
${super_admin_token}
${test_study_id}
${test_patient_id}
${test_study_report_id}
${test_review_doctor_id}
${test_share_study_id}
${test_production_token}
${test_upload_study_id}

*** Keywords ***
Multiple Setup Methods
    delete test user        ${production}
    delete test patient     ${test_patient_id}
    delete test user        ${review_doctor}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable      ${super_admin_token}
    set common studies token  ${super_admin_token}
    change selected facility   ${mmt_fac_id}
    create test user        ${production}
    ${test_patient_id}=  create test patient
    set suite variable      ${test_patient_id}
    ${test_study_id}=  get video study id
    set suite variable      ${test_study_id}
    get test study information   ${test_study_id}
    ${test_review_doctor_id}=  create test user    ${review_doctor}
    set suite variable      ${test_review_doctor_id}
    share test study   ${test_study_id}   ${test_review_doctor_id}
    ${test_share_study_id}=  get share study id   ${test_study_id}  ${test_review_doctor_id}
    set suite variable   ${test_share_study_id}

Multiple Teardown Methods
    delete test user       ${production}
    delete test patient    ${test_patient_id}
    delete test user       ${review_doctor}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Studies Controller for Production
    initialize access for studies controller  ${production}

Login with Production
    ${test_production_token}=  test user login by role  ${production}  ${None}
    set suite variable  ${test_production_token}
    set token studies   ${test_production_token}

#STUDIES CONTROLLER
TC 049: Production's access to Assign Patient
    [Tags]    Production StudiesController       AssignPatient
    log  UNABLE TO TEST ADD ASSIGN PATIENT. ACCESS USERS' INFORMATION MISSING    WARN

TC 050: Production's access to StudyUploadSetup
    [Tags]      Production StudiesController       StudyUploadSetup
    ${test_upload_study_id}=  post study upload study setup tc  ${test_patient_id}  ${none}  ${none}  ${none}  ${none}   ${none}      TC 050
    set suite variable   ${test_upload_study_id}

TC 051: Production's access to AwsBucketForStudy
    [Tags]      Production StudiesController       AwsBucketForStudy
    post aws bucket for study  ${test_study_id}         TC 051

TC 052: Production's access to Study List View
    [Tags]      Production StudiesController       StudyListView
    get search list view            ${none}  ${none}  ${none}           TC 052

TC 053: Production's access to Study View Study ID
    [Tags]      Production StudiesController       StudyView
    get study view   ${test_study_id}           TC 053

TC 054: Production's access to Study Sync Info with Study ID
    [Tags]      Production StudiesController       StudySyncInfo
    get study sync info   ${test_study_id}      TC 054

TC 055: Production's access to Study Serialize with Study ID
    [Tags]      Production StudiesController       StudySerialize
    get serialize study    ${test_study_id}         TC 055

TC 056: Production's access to Study Deserialize
    [Tags]      Production StudiesController       StudyDeserializa
    log  UNABLE TO TEST ADD STUDY DESERIALIZE.PARAMS ARE UNKNOWN    WARN

TC 057: Production's access to Study Dat File with Study ID
    [Tags]      Production StudiesController       StudyDatFile
    post generate study dat file   ${test_study_id}         TC 057

TC 058: Production's access to Studies with ID
    [Tags]      Production StudiesController       StudiesID
    get studies by id  ${test_study_id}         TC 058

TC 059: Production's access to Video Recording Index Camera Index
    [Tags]      Production StudiesController       VideoRecordingIndexCameraIndex
    ${test_video_index}=   get video index
    ${test_recording_index}=  get recording index
    ${test_camera_index}=  get camera index
    get videorecording index camera index    ${test_study_id}   ${test_recording_index}     ${test_camera_index}       ${test_video_index}      ${none}     TC 059

TC 060: Production's access to Update Study Notes with Study ID
    [Tags]      Production StudiesController           UpdateStudyNotes
    post update study notes    ${test_study_id}         TC 060

TC 061: Production's access to Study Report with Study ID
    [Tags]      Production StudiesController       StudyReport
    ${test_study_report_id}=    get study report tc    ${test_study_id}     TC 061
    set suite variable    ${test_study_report_id}

TC 062: Production's access to Update Study Notes with Study ID and Report ID
    [Tags]      Production StudiesController       UpdateStudyReport
    post update study report    ${test_study_id}    ${test_study_report_id}         TC 062

TC 063: Production's access to Restore Study with ID
    [Tags]      Production StudiesController       RestoreStudy
    post restore study   ${test_study_id}   ${mmt_fac_id}           TC 063

TC 064: Production's access to Create Study
    [Tags]      Production StudiesController       CreateStudy
    log  UNABLE TO TEST CREATE STUDY. STUDY CANNOT BE CREATED VIA API   WARN

TC 065: Production's access to Change Study State with ID
    [Tags]      Production StudiesController       StudySerialize
    post change study state    ${test_study_id}     ${none}         TC 065

TC 066: Production's access to Shared Studies with ID
    [Tags]      Production StudiesController       SharedStudies
    get shared studies    ${test_study_id}          TC 066

TC 067: Production's access to Remove Shared Study
    [Tags]      Production StudiesController       RemoveShareStudies
    post remove shared study   ${test_share_study_id}       TC 067

TC 068: Production's access to Get Studies
    [Tags]    Production StudiesController        Get
    get studies         TC 068

TC 069: Production's access to Put Studies with ID
    [Tags]    Production StudiesController        PutStudies
    ${test_sync_state}=  get study state
    ${test_study_state}=  get study state
    ${test_amplifier_id}=  get amplifier id
    ${test_created_date}=  get created study date
    put studies tc  ${test_study_id}   ${test_sync_state}   ${test_study_state}   ${test_amplifier_id}  ${mmt_fac_id}  ${test_created_date}         TC 069

TC 070: Production's access to Patch Studies with ID
    [Tags]    Production StudiesController        PatchStudies
    patch studies    ${test_study_id}           TC 070

TC 071: Production's access to Post Delete Study with ID
    [Tags]    Production StudiesController        DeleteStudy
    post delete study   ${test_study_id}            ${none}       TC 071

TC 072: Production's access to Delete Studies with ID
    [Tags]    Production StudiesController        DeleteStudies
    delete study id   ${test_study_id}          TC 072



