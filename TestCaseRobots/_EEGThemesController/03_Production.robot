*** Settings ***
Documentation   Test cases to validate all EegThemes Controller endpoints authorization for Production role
Library     ../../TestCaseMethods/EegThemesControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py
Library  ../../TestCaseMethods/_CommonStudiesTCs.py

Resource  ../global_robot_variables.robot


*** Variables ***
${super_admin_token}
${test_production_token}
${test_theme_id}
${created_theme_id}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${production}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    set common studies token  ${super_admin_token}
    change selected facility   ${mmt_fac_id}
    create test user  ${production}
    ${test_theme_id}=  create test theme  ${mmt_fac_id}
    set suite variable  ${test_theme_id}

Multiple Teardown Methods
    delete test user    ${production}
    RUN KEYWORD IF  $test_theme_id is not None     delete theme by id     ${test_theme_id}
    RUN KEYWORD IF  $created_theme_id is not None    delete theme by id     ${created_theme_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of EEG Themes Controller for Support
    initialize access for eeg themes controller   ${production}

Login with Production
    ${test_production_token}=  test user login by role  ${production}  ${None}
    set suite variable      ${test_production_token}
    set token eeg themes    ${test_production_token}

# EEGTHEMES CONTROLLER
TC 013: Production's access to Post EegThemes
     [Tags]      Production EegThemesController     Post
    ${created_theme_id}=  post eeg theme tc  ${mmt_fac_id}      TC 013
    set suite variable   ${created_theme_id}

TC 014: Production's access to Get EegThemes
    [Tags]      Production EegThemesController     Get
    get eeg themes                  TC 014

TC 015: Production's access to Get EegThemes with ID
    [Tags]      Production EegThemesController     Get?id=
    get eeg theme by id  ${test_theme_id}               TC 015

TC 016: Production's access to Patch EegThemes
    [Tags]      Production EegThemesController     Patch
    patch eeg theme tc   ${test_theme_id}       ${none}  ${none}  ${none}  ${none}     TC 016

TC 017: Production's access to Put EegThemes
    [Tags]      Production EegThemesController     Put
    put eeg theme tc   ${test_theme_id}     ${none}  ${none}  ${none}  ${none}     TC 017

TC 018: Production's access to Delete EegThemes
    [Tags]      Production EegThemesController     Delete
    delete eeg theme   ${test_theme_id}         TC 018
