*** Settings ***
Documentation   Test cases to validate all EegThemes Controller endpoints authorization for LeadTech role

Library     ../../TestCaseMethods/EegThemesControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py
Library  ../../TestCaseMethods/_CommonStudiesTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***
${super_admin_token}
${test_lead_tech_token}
${test_theme_id}
${created_theme_id}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${lead_tech}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    set common studies token  ${super_admin_token}
    change selected facility   ${mmt_fac_id}
    create test user  ${lead_tech}
    ${test_theme_id}=  create test theme  ${mmt_fac_id}
    set suite variable  ${test_theme_id}

Multiple Teardown Methods
    delete test user    ${lead_tech}
    RUN KEYWORD IF  $test_theme_id is not None     delete theme by id       ${test_theme_id}
    RUN KEYWORD IF  $created_theme_id is not None    delete theme by id     ${created_theme_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of EEG Themes Controller for Lead Tech
    initialize access for eeg themes controller   ${lead_tech}

Login with Lead Tech
    ${test_lead_tech_token}=  test user login by role  ${lead_tech}  ${None}
    set suite variable      ${test_lead_tech_token}
    set token eeg themes    ${test_lead_tech_token}

# EEGTHEMES CONTROLLER
TC 031: Lead Tech's access to Post EegThemes
    [Tags]      LeadTech EegThemesController     Post
    ${created_theme_id}=  post eeg theme tc  ${mmt_fac_id}      TC 031
    set suite variable   ${created_theme_id}

TC 032: Lead Tech's access to Get EegThemes
    [Tags]      LeadTech EegThemesController     Get
    get eeg themes                  TC 032

TC 033: Lead Tech's access to Get EegThemes with ID
    [Tags]      LeadTech EegThemesController     Get?id=
    get eeg theme by id   ${test_theme_id}          TC 033

TC 034: Lead Tech's access to Patch EegThemes
    [Tags]      LeadTech EegThemesController     Patch
    patch eeg theme tc   ${test_theme_id}       ${none}  ${none}  ${none}  ${none}     TC 034

TC 035: Lead Tech's access to Put EegThemes
    [Tags]      LeadTech EegThemesController     Put
    put eeg theme tc   ${test_theme_id}     ${none}  ${none}  ${none}  ${none}     TC 035

TC 036: Lead Tech's access to Delete EegThemes
    [Tags]      LeadTech EegThemesController     Delete
    delete eeg theme   ${test_theme_id}     TC 036


