*** Settings ***
Documentation   Test cases to validate all EegThemes Controller endpoints authorization for OfficePersonnel role

Library     ../../TestCaseMethods/EegThemesControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py
Library  ../../TestCaseMethods/_CommonStudiesTCs.py

Resource  ../global_robot_variables.robot


*** Variables ***
${super_admin_token}
${test_office_personnel_token}
${test_theme_id}
${created_theme_id}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${office_personnel}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    set common studies token  ${super_admin_token}
    change selected facility   ${mmt_fac_id}
    create test user  ${office_personnel}
    ${test_theme_id}=  create test theme  ${mmt_fac_id}
    set suite variable  ${test_theme_id}

Multiple Teardown Methods
    delete test user    ${office_personnel}
    RUN KEYWORD IF  $test_theme_id is not None     delete theme by id       ${test_theme_id}
    RUN KEYWORD IF  $created_theme_id is not None    delete theme by id     ${created_theme_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of EEG Themes Controller for Office Personnel
    initialize access for eeg themes controller   ${office_personnel}

Login with Office Personnel
    ${test_office_personnel_token}=  test user login by role  ${office_personnel}   ${None}
    set suite variable      ${test_office_personnel_token}
    set token eeg themes    ${test_office_personnel_token}


# EEGTHEMES CONTROLLER
TC 043: Office Personnel's access to Post EegThemes
   [Tags]      OfficePersonnel EegThemesController     Post
    ${created_theme_id}=  post eeg theme tc  ${mmt_fac_id}      TC 043
    set suite variable   ${created_theme_id}

TC 044: Office Personnel's access to Get EegThemes
    [Tags]      OfficePersonnel EegThemesController     Get
    get eeg themes          TC 044

TC 045: Office Personnel's access to Get EegThemes with ID
    [Tags]      OfficePersonnel EegThemesController     Get?id=
    get eeg theme by id   ${test_theme_id}          TC 045

TC 046: Office Personnel's access to Patch EegThemes
    [Tags]      OfficePersonnel EegThemesController     Patch
    patch eeg theme tc   ${test_theme_id}       ${none}  ${none}  ${none}  ${none}     TC 046

TC 047: Office Personnel's access to Put EegThemes
    [Tags]      OfficePersonnel EegThemesController     Put
    put eeg theme tc   ${test_theme_id}         ${none}  ${none}  ${none}  ${none}     TC 047

TC 048: Office Personnel's access to Delete EegThemes
    [Tags]      OfficePersonnel EegThemesController     Delete
    delete eeg theme   ${test_theme_id}         TC 048
