*** Settings ***
Documentation   Test cases to validate all EegThemes Controller endpoints authorization for FacilityAdmin role

Library     ../../TestCaseMethods/EegThemesControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py
Library  ../../TestCaseMethods/_CommonStudiesTCs.py

Resource  ../global_robot_variables.robot


*** Variables ***
${super_admin_token}
${test_facility_admin__token}
${test_theme_id}
${created_theme_id}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${facility_admin}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    set common studies token  ${super_admin_token}
    change selected facility   ${mmt_fac_id}
    create test user  ${facility_admin}
    ${test_theme_id}=  create test theme  ${mmt_fac_id}
    set suite variable  ${test_theme_id}

Multiple Teardown Methods
    delete test user    ${facility_admin}
    RUN KEYWORD IF  $test_theme_id is not None     delete theme by id     ${test_theme_id}
    RUN KEYWORD IF  $created_theme_id is not None    delete theme by id     ${created_theme_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of EEG Themes Controller for FacililtyAdmin
    initialize access for eeg themes controller   ${facility_admin}

Login with FacilityAdmin
    ${test_facility_admin__token}=  test user login by role  ${facility_admin}  ${None}
    set suite variable      ${test_facility_admin__token}
    set token eeg themes    ${test_facility_admin__token}

# EEGTHEMES CONTROLLER
TC 019: Facility Admin's access to Post EegThemes
     [Tags]      FacilityAdmin EegThemesController     Post
    ${created_theme_id}=  post eeg theme tc  ${mmt_fac_id}      TC 019
    set suite variable   ${created_theme_id}

TC 020: Facility Admin's access to Get EegThemes
    [Tags]      FacilityAdmin EegThemesController     Get
    get eeg themes                      TC 020

TC 021: Facility Admin's access to Get EegThemes with ID
    [Tags]      FacilityAdmin EegThemesController     Get?id=
    get eeg theme by id    ${test_theme_id}             TC 021

TC 022: Facility Admin's access to Patch EegThemes
    [Tags]      FacilityAdmin EegThemesController     Patch
    patch eeg theme tc   ${test_theme_id}   ${none}  ${none}  ${none}  ${none}     TC 022

TC 023: Facility Admin's access to Put EegThemes
    [Tags]      FacilityAdmin EegThemesController     Put
    put eeg theme tc   ${test_theme_id}     ${none}  ${none}  ${none}  ${none}     TC 023

TC 024: Facility Admin's access to Delete EegThemes
    [Tags]      FacilityAdmin EegThemesController     Delete
    delete eeg theme   ${test_theme_id}                 TC 024
