*** Settings ***
Documentation   Test cases to validate all EegThemes Controller endpoints authorization for SuperAdmin role
Library     ../../TestCaseMethods/EegThemesControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py
Library  ../../TestCaseMethods/_CommonStudiesTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***
${super_admin_token}
${test_super_admin_token}
${test_theme_id}
${created_theme_id}


*** Keywords ***
Multiple Setup Methods
    delete test user    ${super_admin}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    get_version_number  ${super_admin_token}
    set common studies token  ${super_admin_token}
    change selected facility   ${mmt_fac_id}
    create test user  ${super_admin}
    ${test_theme_id}=  create test theme  ${mmt_fac_id}
    set suite variable  ${test_theme_id}


Multiple Teardown Methods
    delete test user    ${super_admin}
    RUN KEYWORD IF  $test_theme_id is not None     delete theme by id     ${test_theme_id}
    RUN KEYWORD IF  $created_theme_id is not None    delete theme by id     ${created_theme_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of EEG Themes Controller for Super Admin
    initialize access for eeg themes controller   ${super_admin}

Login with SuperAdmin
    ${test_super_admin_token}=  test user login by role  ${super_admin}  ${None}
    set suite variable  ${test_super_admin_token}
    set token eeg themes    ${test_super_admin_token}

# EEGTHEMES CONTROLLER
TC 001: SuperAdmin's access to Post EegThemes
    [Tags]      SuperAdmin EegThemesController     Post
    ${created_theme_id}=  post eeg theme tc  ${mmt_fac_id}      TC 001
    set suite variable   ${created_theme_id}

TC 002: SuperAdmin's access to Get EegThemes
    [Tags]      SuperAdmin EegThemesController     Get
    get eeg themes                                                  TC 002

TC 003: SuperAdmin's access to Get EegThemes with ID
    [Tags]      SuperAdmin EegThemesController     Get?id=
    get eeg theme by id    ${test_theme_id}                         TC 003

TC 004: SuperAdmin's access to Patch EegThemes
    [Tags]      SuperAdmin EegThemesController     Patch
    patch eeg theme tc    ${test_theme_id}  ${none}  ${none}  ${none}  ${none}     TC 004

TC 005: SuperAdmin's access to Put EegThemes
    [Tags]      SuperAdmin EegThemesController     Put
    put eeg theme tc  ${test_theme_id}  ${none}  ${none}  ${none}  ${none}     TC 005

TC 006: SuperAdmin's access to Delete EegThemes
    [Tags]      SuperAdmin EegThemesController     Delete
    delete eeg theme    ${test_theme_id}        TC 006

