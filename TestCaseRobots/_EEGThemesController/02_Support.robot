*** Settings ***
Documentation   Test cases to validate all EegThemes Controller endpoints authorization for Support role
Library     ../../TestCaseMethods/EegThemesControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py
Library  ../../TestCaseMethods/_CommonStudiesTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***
${super_admin_token}
${test_support_token}
${test_theme_id}
${created_theme_id}


*** Keywords ***
Multiple Setup Methods
    delete test user    ${support}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    set common studies token  ${super_admin_token}
    change selected facility   ${mmt_fac_id}
    create test user  ${support}
    ${test_theme_id}=  create test theme  ${mmt_fac_id}
    set suite variable  ${test_theme_id}

Multiple Teardown Methods
    delete test user    ${support}
    RUN KEYWORD IF  $test_theme_id is not None     delete theme by id     ${test_theme_id}
    RUN KEYWORD IF  $created_theme_id is not None    delete theme by id     ${created_theme_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of EEG Themes Controller for Support
    initialize access for eeg themes controller   ${support}

Login with SuperAdmin
    ${test_support_token}=  test user login by role  ${support}  ${None}
    set suite variable      ${test_support_token}
    set token eeg themes    ${test_support_token}

# EEGTHEMES CONTROLLER
TC 007: Support's access to Post EegThemes
    [Tags]      Support EegThemesController     Post
    ${created_theme_id}=  post eeg theme tc  ${mmt_fac_id}          TC 007
    set suite variable   ${created_theme_id}

TC 008: Support's access to Get EegThemes
    [Tags]      Support EegThemesController     Get
    get eeg themes          TC 008

TC 009: Support's access to Get EegThemes with ID
    [Tags]      Support EegThemesController     Get?id=
    get eeg theme by id   ${test_theme_id}              TC 009

TC 010: Support's access to Patch EegThemes
    [Tags]      Support EegThemesController     Patch
    patch eeg theme tc    ${test_theme_id}      ${none}  ${none}  ${none}  ${none}     TC 010

TC 011: Support's access to Put EegThemes
    [Tags]      Support EegThemesController     Put
    put eeg theme tc    ${test_theme_id}        ${none}  ${none}  ${none}  ${none}     TC 011

TC 012: Support's access to Delete EegThemes
    [Tags]      Support EegThemesController     Delete
    delete eeg theme    ${test_theme_id}        TC 012
