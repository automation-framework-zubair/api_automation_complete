*** Settings ***
Documentation   Test cases to validate all EegThemes Controller endpoints authorization for FieldTech role

Library     ../../TestCaseMethods/EegThemesControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py
Library  ../../TestCaseMethods/_CommonStudiesTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***
${super_admin_token}
${test_lead_tech_token}
${test_theme_id}
${created_theme_id}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${field_tech}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    set common studies token  ${super_admin_token}
    change selected facility   ${mmt_fac_id}
    create test user  ${field_tech}
    ${test_theme_id}=  create test theme  ${mmt_fac_id}
    set suite variable  ${test_theme_id}

Multiple Teardown Methods
    delete test user    ${field_tech}
    RUN KEYWORD IF  $test_theme_id is not None     delete theme by id       ${test_theme_id}
    RUN KEYWORD IF  $created_theme_id is not None    delete theme by id     ${created_theme_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of EEG Themes Controller for Field Tech
    initialize access for eeg themes controller   ${field_tech}

Login with Field Tech
    ${test_field_tech_token}=  test user login by role  ${field_tech}    ${None}
    set suite variable      ${test_field_tech_token}
    set token eeg themes    ${test_field_tech_token}

# EEGTHEMES CONTROLLER
TC 037: Field Tech's access to Post EegThemes
    [Tags]      FieldTech EegThemesController     Post
    ${created_theme_id}=  post eeg theme tc  ${mmt_fac_id}      TC 037
    set suite variable   ${created_theme_id}

TC 038: Field Tech's access to Get EegThemes
    [Tags]      FieldTech EegThemesController     Get
    get eeg themes              TC 038

TC 039: Field Tech's access to Get EegThemes with ID
    [Tags]      FieldTech EegThemesController     Get?id=
    get eeg theme by id   ${test_theme_id}              TC 039

TC 040: Field Tech's access to Patch EegThemes
    [Tags]      FieldTech EegThemesController     Patch
    patch eeg theme tc   ${test_theme_id}       ${none}  ${none}  ${none}  ${none}     TC 040

TC 041: Field Tech's access to Put EegThemes
    [Tags]      FieldTech EegThemesController     Put
    put eeg theme tc   ${test_theme_id}     ${none}  ${none}  ${none}  ${none}     TC 041

TC 042: Field Tech's access to Delete EegThemes
    [Tags]      FieldTech EegThemesController     Delete
    delete eeg theme   ${test_theme_id}         TC 042
