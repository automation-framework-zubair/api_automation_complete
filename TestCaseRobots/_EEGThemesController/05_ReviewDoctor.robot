*** Settings ***
Documentation   Test cases to validate all EegThemes Controller endpoints authorization for ReviewDoctor role

Library     ../../TestCaseMethods/EegThemesControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py
Library  ../../TestCaseMethods/_CommonStudiesTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***
${super_admin_token}
${test_review_doctor_token}
${test_theme_id}
${created_theme_id}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${review_doctor}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    set common studies token  ${super_admin_token}
    change selected facility   ${mmt_fac_id}
    create test user  ${review_doctor}
    ${test_theme_id}=  create test theme  ${mmt_fac_id}
    set suite variable  ${test_theme_id}

Multiple Teardown Methods
    delete test user    ${review_doctor}
    RUN KEYWORD IF  $test_theme_id is not None     delete theme by id       ${test_theme_id}
    RUN KEYWORD IF  $created_theme_id is not None    delete theme by id     ${created_theme_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of EEG Themes Controller for Review Doctor
    initialize access for eeg themes controller   ${review_doctor}

Login with Review Doctor
    ${test_review_doctor_token}=  test user login by role  ${review_doctor}  ${None}
    set suite variable      ${test_review_doctor_token}
    set token eeg themes    ${test_review_doctor_token}

# EEGTHEMES CONTROLLER
TC 025: Review Doctor's access to Post EegThemes
    [Tags]      ReviewDoctor EegThemesController     Post
    ${created_theme_id}=  post eeg theme tc  ${mmt_fac_id}      TC 025
    set suite variable   ${created_theme_id}

TC 026: Review Doctor's access to Get EegThemes
    [Tags]      ReviewDoctor EegThemesController     Get
    get eeg themes                  TC 026

TC 027: Review Doctor's access to Get EegThemes with ID
    [Tags]      ReviewDoctor EegThemesController     Get?id=
    get eeg theme by id    ${test_theme_id}        TC 027

TC 028: Review Doctor's access to Patch EegThemes
    [Tags]      ReviewDoctor EegThemesController     Patch
    patch eeg theme tc   ${test_theme_id}       ${none}  ${none}  ${none}  ${none}     TC 028

TC 029: Review Doctor's access to Put EegThemes
    [Tags]      ReviewDoctor EegThemesController     Put
    put eeg theme tc    ${test_theme_id}     ${none}  ${none}  ${none}  ${none}     TC 029

TC 030: Review Doctor's access to Delete EegThemes
    [Tags]      ReviewDoctor EegThemesController     Delete
    delete eeg theme   ${test_theme_id}         TC 030
