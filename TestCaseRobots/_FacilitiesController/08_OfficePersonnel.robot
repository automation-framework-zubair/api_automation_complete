*** Settings ***
Documentation    Test Cases to validate all Facilities Controller endpoints authorization for Office Personnel role

Library  ../../TestCaseMethods/FacilitiesControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***

${super_admin_token}
${test_office_personnel_token}
${created_facility_id}
${created_test_facility_id}
${test_invitation_id}
${test_device_id}
${supported_camera_id}
${added_camera_id}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${office_personnel}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    change selected facility  ${MMT_FAC_ID}
    ${test_device_id}=  create_test_device  ${MMT_FAC_ID}
    set suite variable  ${test_device_id}
    ${supported_camera_id}=  get random supported facility camera
    set suite variable  ${supported_camera_id}
    add facility camera for deletion test   ${supported_camera_id}  ${test_device_id}
    ${added_camera_id}=  get facility network camera based on device id  ${test_device_id}
    set suite variable  ${added_camera_id}

    create test user  ${office_personnel}
    ${created_facility_id}=  create test facility  API
    set suite variable  ${created_facility_id}
    ${test_invitation_id}=  get random invitation id  False
    set suite variable  ${test_invitation_id}

Multiple Teardown Methods
    delete test user    ${office_personnel}
    RUN KEYWORD IF  $created_facility_id is not None     delete test facility    ${created_facility_id}
    RUN KEYWORD IF  $created_test_facility_id is not None     delete test patient    ${created_test_facility_id}
    RUN KEYWORD IF  $test_device_id is not None     delete test device  ${test_device_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Facilities Controller for Office Personnel
    initialize access for facilities controller  ${office_personnel}

Login with Office Personnel
    ${test_office_personnel_token}=   test user login by role     ${office_personnel}    ${None}
    set suite variable  ${test_office_personnel_token}
    set token facilities  ${test_office_personnel_token}

# FACILITIES CONTROLLER
TC 099: Office Personnel's access to RotateToken
    [Tags]    OfficePersonnel FacilitiesController      RotateToken
    post rotate token  ${created_facility_id}   TC 099

TC 100: Office Personnel's access to Metrics
    [Tags]    OfficePersonnel FacilitiesController      metrics
    get metrics  ${created_facility_id}     TC 100

TC 101: Office Personnel's access to GetFacilities
    [Tags]    OfficePersonnel FacilitiesController      GetFacilities
    get facilities      TC 101

TC 102: Office Personnel's access to CreateFacility
    [Tags]    OfficePersonnel FacilitiesController      CreateFacility
    ${created_test_facility_id}=  post create facility tc  TEST_NAME    TC 102
    set suite variable  ${created_test_facility_id}

TC 103: Office Personnel's access to SendInvitation
    [Tags]    OfficePersonnel FacilitiesController      SendInvitation
    get send invitation  ${office_personnel}    ${None}     TC 103

TC 104: Office Personnel's access to AcceptInvitation
    [Tags]    OfficePersonnel FacilitiesController      AcceptInvitation
    post accept invitation  ${test_invitation_id}   TC 104

TC 105: Office Personnel's access to GetInvitation
    [Tags]    OfficePersonnel FacilitiesController      GetInvitation
    get invitation  ${test_invitation_id}   TC 105

TC 106: Office Personnel's access to GetInvitations
    [Tags]    OfficePersonnel FacilitiesController      GetInvitations
    get invitations  True   TC 106

TC 107: Office Personnel's access to DoesInvitedUserExist
    [Tags]    OfficePersonnel FacilitiesController      DoesInvitedUserExist
    get does invited user exist  ${test_invitation_id}  TC 107

TC 108: Office Personnel's access to IsInvitationExpired
    [Tags]    OfficePersonnel FacilitiesController      IsInvitationExpired
    get is invitation expired  ${test_invitation_id}    TC 108

TC 109: Office Personnel's access to GetFacilityByID
    [Tags]    OfficePersonnel FacilitiesController      GetFacilityByID
    get facility by id tc  ${created_facility_id}   TC 109

TC 110: Office Personnel's access to Patch
    [Tags]    OfficePersonnel FacilitiesController      Patch
    patch facility id  ${created_facility_id}   TC 110

TC 111: Office Personnel's access to Put
    [Tags]    OfficePersonnel FacilitiesController      Put
    put facility id  ${created_facility_id}     TC 111

TC 112: Office Personnel's access to DeleteFacility
    [Tags]    OfficePersonnel FacilitiesController      delete
    delete facility  ${created_facility_id}     TC 112

TC 148: Office Personnel's access to Retrieve Token
    [Tags]    Office Personnel FacilitiesController      RetrieveToken
    get retrieve token      TC 148

TC 149: Office Personnel's access to GetSupportedNetworkCameras
    [Tags]    Office Personnel FacilitiesController      GetSupportedNetworkCameras
    get supported network cameras   TC 149

TC 150: Office Personnel's access to GetFacilityNetworkCameras
    [Tags]    Office Personnel FacilitiesController      GetFacilityNetworkCameras
    get facility network cameras    TC 150

TC 151: Office Personnel's access to AddFacilityNetworkCamera
    [Tags]    Office Personnel FacilitiesController      AddFacilityNetworkCamera
    add facility network camera  ${supported_camera_id}  ${test_device_id}  TC 151

TC 152: Office Personnel's access to RemoveFacilityNetworkCamera
    [Tags]    Office Personnel FacilitiesController      RemoveFacilityNetworkCamera
    remove facility network camera  ${added_camera_id}  TC 152
