*** Settings ***
Documentation    Test Cases to validate all Facilities Controller endpoints authorization for Lead Tech role

Library  ../../TestCaseMethods/FacilitiesControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***

${super_admin_token}
${test_lead_tech_token}
${created_facility_id}
${created_test_facility_id}
${test_invitation_id}
${test_device_id}
${supported_camera_id}
${added_camera_id}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${lead_tech}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    change selected facility  ${MMT_FAC_ID}
    ${test_device_id}=  create_test_device  ${MMT_FAC_ID}
    set suite variable  ${test_device_id}
    ${supported_camera_id}=  get random supported facility camera
    set suite variable  ${supported_camera_id}
    add facility camera for deletion test   ${supported_camera_id}  ${test_device_id}
    ${added_camera_id}=  get facility network camera based on device id  ${test_device_id}
    set suite variable  ${added_camera_id}

    create test user  ${lead_tech}
    ${created_facility_id}=  create test facility  API
    set suite variable  ${created_facility_id}
    ${test_invitation_id}=  get random invitation id  False
    set suite variable  ${test_invitation_id}

Multiple Teardown Methods
    delete test user    ${lead_tech}
    RUN KEYWORD IF  $created_facility_id is not None     delete test facility    ${created_facility_id}
    RUN KEYWORD IF  $created_test_facility_id is not None     delete test patient    ${created_test_facility_id}
    RUN KEYWORD IF  $test_device_id is not None     delete test device  ${test_device_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Facilities Controller for Lead Tech
    initialize access for facilities controller  ${lead_tech}

Login with Lead Tech
    ${test_lead_tech_token}=   test user login by role     ${lead_tech}    ${None}
    set suite variable  ${test_lead_tech_token}
    set token facilities  ${test_lead_tech_token}

# FACILITIES CONTROLLER
TC 071: Lead Tech's access to RotateToken
    [Tags]    LeadTech FacilitiesController      RotateToken
    post rotate token  ${created_facility_id}   TC 071

TC 072: Lead Tech's access to Metrics
    [Tags]    LeadTech FacilitiesController      metrics
    get metrics  ${created_facility_id}     TC 072

TC 073: Lead Tech's access to GetFacilities
    [Tags]    LeadTech FacilitiesController      GetFacilities
    get facilities      TC 073

TC 074: Lead Tech's access to CreateFacility
    [Tags]    LeadTech FacilitiesController      CreateFacility
    ${created_test_facility_id}=  post create facility tc  TEST_NAME    TC 074
    set suite variable  ${created_test_facility_id}

TC 075: Lead Tech's access to SendInvitation
    [Tags]    LeadTech FacilitiesController      SendInvitation
    get send invitation  ${office_personnel}    ${None}     TC 075

TC 076: Lead Tech's access to AcceptInvitation
    [Tags]    LeadTech FacilitiesController      AcceptInvitation
    post accept invitation  ${test_invitation_id}       TC 076

TC 077: Lead Tech's access to GetInvitation
    [Tags]    LeadTech FacilitiesController      GetInvitation
    get invitation  ${test_invitation_id}       TC 077

TC 078: Lead Tech's access to GetInvitations
    [Tags]    LeadTech FacilitiesController      GetInvitations
    get invitations  True   TC 078

TC 079: Lead Tech's access to DoesInvitedUserExist
    [Tags]    LeadTech FacilitiesController      DoesInvitedUserExist
    get does invited user exist  ${test_invitation_id}      TC 079

TC 080: Lead Tech's access to IsInvitationExpired
    [Tags]    LeadTech FacilitiesController      IsInvitationExpired
    get is invitation expired  ${test_invitation_id}    TC 080

TC 081: Lead Tech's access to GetFacilityByID
    [Tags]    LeadTech FacilitiesController      GetFacilityByID
    get facility by id tc  ${created_facility_id}   TC 081

TC 082: Lead Tech's access to Patch
    [Tags]    LeadTech FacilitiesController      Patch
    patch facility id  ${created_facility_id}   TC 082

TC 083: Lead Tech's access to Put
    [Tags]    LeadTech FacilitiesController      Put
    put facility id  ${created_facility_id}     TC 083

TC 084: Lead Tech's access to DeleteFacility
    [Tags]    LeadTech FacilitiesController      delete
    delete facility  ${created_facility_id}     TC 084

TC 138: Lead Tech's access to Retrieve Token
    [Tags]    Lead Tech FacilitiesController      RetrieveToken
    get retrieve token      TC 138

TC 139: Lead Tech's access to GetSupportedNetworkCameras
    [Tags]    Lead Tech FacilitiesController      GetSupportedNetworkCameras
    get supported network cameras   TC 139

TC 140: Lead Tech's access to GetFacilityNetworkCameras
    [Tags]    Lead Tech FacilitiesController      GetFacilityNetworkCameras
    get facility network cameras    TC 140

TC 141: Lead Tech's access to AddFacilityNetworkCamera
    [Tags]    Lead Tech FacilitiesController      AddFacilityNetworkCamera
    add facility network camera  ${supported_camera_id}  ${test_device_id}  TC 141

TC 142: Lead Tech's access to RemoveFacilityNetworkCamera
    [Tags]    Lead Tech FacilitiesController      RemoveFacilityNetworkCamera
    remove facility network camera  ${added_camera_id}  TC 142
