*** Settings ***
Documentation    Test Cases to validate all Facilities Controller endpoints authorization for Support role

Library  ../../TestCaseMethods/FacilitiesControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***

${super_admin_token}
${test_support_token}
${created_facility_id}
${created_test_facility_id}
${test_invitation_id}
${test_device_id}
${supported_camera_id}
${added_camera_id}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${support}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    change selected facility  ${MMT_FAC_ID}
    ${test_device_id}=  create_test_device  ${MMT_FAC_ID}
    set suite variable  ${test_device_id}
    ${supported_camera_id}=  get random supported facility camera
    set suite variable  ${supported_camera_id}
    add facility camera for deletion test   ${supported_camera_id}  ${test_device_id}
    ${added_camera_id}=  get facility network camera based on device id  ${test_device_id}
    set suite variable  ${added_camera_id}

    create test user  ${support}
    ${created_facility_id}=  create test facility  API
    set suite variable  ${created_facility_id}
    ${test_invitation_id}=  get random invitation id  False
    set suite variable  ${test_invitation_id}

Multiple Teardown Methods
    delete test user    ${support}
    RUN KEYWORD IF  $created_facility_id is not None     delete test facility    ${created_facility_id}
    RUN KEYWORD IF  $created_test_facility_id is not None     delete test patient    ${created_test_facility_id}
    RUN KEYWORD IF  $test_device_id is not None     delete test device  ${test_device_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Facilities Controller for Support
    initialize access for facilities controller  ${support}

Login with Support
    ${test_support_token}=   test user login by role     ${support}    ${None}
    set suite variable  ${test_support_token}
    set token facilities  ${test_support_token}

# FACILITIES CONTROLLER
TC 015: Support's access to RotateToken
    [Tags]    Support FacilitiesController      RotateToken
    post rotate token  ${created_facility_id}       TC 015

TC 016: Support's access to Metrics
    [Tags]    Support FacilitiesController      metrics
    get metrics  ${created_facility_id}     TC 016

TC 017: Support's access to GetFacilities
    [Tags]    Support FacilitiesController      GetFacilities
    get facilities      TC 017

TC 018: Support's access to CreateFacility
    [Tags]    Support FacilitiesController      CreateFacility
    ${created_test_facility_id}=  post create facility tc  TEST_NAME    TC 018
    set suite variable  ${created_test_facility_id}

TC 019: Support's access to SendInvitation
    [Tags]    Support FacilitiesController      SendInvitation
    get send invitation  ${office_personnel}    ${None}     TC 019

TC 020: Support's access to AcceptInvitation
    [Tags]    Support FacilitiesController      AcceptInvitation
    post accept invitation  ${test_invitation_id}   TC 020

TC 021: Support's access to GetInvitation
    [Tags]    Support FacilitiesController      GetInvitation
    get invitation  ${test_invitation_id}   TC 021

TC 022: Support's access to GetInvitations
    [Tags]    Support FacilitiesController      GetInvitations
    get invitations  True   TC 022

TC 023: Support's access to DoesInvitedUserExist
    [Tags]    Support FacilitiesController      DoesInvitedUserExist
    get does invited user exist  ${test_invitation_id}      TC 023

TC 024: Support's access to IsInvitationExpired
    [Tags]    Support FacilitiesController      IsInvitationExpired
    get is invitation expired  ${test_invitation_id}        TC 024

TC 025: Support's access to GetFacilityByID
    [Tags]    Support FacilitiesController      GetFacilityByID
    get facility by id tc  ${created_facility_id}       TC 025

TC 026: Support's access to Patch
    [Tags]    Support FacilitiesController      Patch
    patch facility id  ${created_facility_id}       TC 026

TC 027: Support's access to Put
    [Tags]    Support FacilitiesController      Put
    put facility id  ${created_facility_id}     TC 027

TC 028: Support's access to DeleteFacility
    [Tags]    Support FacilitiesController      delete
    delete facility  ${created_facility_id}     TC 028

TC 118: Support's access to Retrieve Token
    [Tags]    Support FacilitiesController      RetrieveToken
    get retrieve token      TC 118

TC 119: Support's access to GetSupportedNetworkCameras
    [Tags]    Support FacilitiesController      GetSupportedNetworkCameras
    get supported network cameras   TC 119

TC 120: Support's access to GetFacilityNetworkCameras
    [Tags]    Support FacilitiesController      GetFacilityNetworkCameras
    get facility network cameras    TC 120

TC 121: Support's access to AddFacilityNetworkCamera
    [Tags]    Support FacilitiesController      AddFacilityNetworkCamera
    add facility network camera  ${supported_camera_id}  ${test_device_id}  TC 121

TC 122: Support's access to RemoveFacilityNetworkCamera
    [Tags]    Support FacilitiesController      RemoveFacilityNetworkCamera
    remove facility network camera  ${added_camera_id}  TC 122
