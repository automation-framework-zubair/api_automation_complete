*** Settings ***
Documentation    Test Cases to validate all Facilities Controller endpoints authorization for Field Tech role

Library  ../../TestCaseMethods/FacilitiesControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***

${super_admin_token}
${test_field_tech_token}
${created_facility_id}
${created_test_facility_id}
${test_invitation_id}
${test_device_id}
${supported_camera_id}
${added_camera_id}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${field_tech}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    change selected facility  ${MMT_FAC_ID}
    ${test_device_id}=  create_test_device  ${MMT_FAC_ID}
    set suite variable  ${test_device_id}
    ${supported_camera_id}=  get random supported facility camera
    set suite variable  ${supported_camera_id}
    add facility camera for deletion test   ${supported_camera_id}  ${test_device_id}
    ${added_camera_id}=  get facility network camera based on device id  ${test_device_id}
    set suite variable  ${added_camera_id}

    create test user  ${field_tech}
    ${created_facility_id}=  create test facility  API
    set suite variable  ${created_facility_id}
    ${test_invitation_id}=  get random invitation id  False
    set suite variable  ${test_invitation_id}

Multiple Teardown Methods
    delete test user    ${field_tech}
    RUN KEYWORD IF  $created_facility_id is not None     delete test facility    ${created_facility_id}
    RUN KEYWORD IF  $created_test_facility_id is not None     delete test patient    ${created_test_facility_id}
    RUN KEYWORD IF  $test_device_id is not None     delete test device  ${test_device_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Facilities Controller for Field Tech
    initialize access for facilities controller  ${field_tech}

Login with Field Tech
    ${test_field_tech_token}=   test user login by role     ${field_tech}    ${None}
    set suite variable  ${test_field_tech_token}
    set token facilities  ${test_field_tech_token}

# FACILITIES CONTROLLER
TC 085: Field Tech's access to RotateToken
    [Tags]    FieldTech FacilitiesController      RotateToken
    post rotate token  ${created_facility_id}   TC 085

TC 086: Field Tech's access to Metrics
    [Tags]    FieldTech FacilitiesController      metrics
    get metrics  ${created_facility_id}     TC 086

TC 087: Field Tech's access to GetFacilities
    [Tags]    FieldTech FacilitiesController      GetFacilities
    get facilities      TC 087

TC 088: Field Tech's access to CreateFacility
    [Tags]    FieldTech FacilitiesController      CreateFacility
    ${created_test_facility_id}=  post create facility tc  TEST_NAME    TC 088
    set suite variable  ${created_test_facility_id}

TC 089: Field Tech's access to SendInvitation
    [Tags]    FieldTech FacilitiesController      SendInvitation
    get send invitation  ${office_personnel}    ${None}     TC 089

TC 090: Field Tech's access to AcceptInvitation
    [Tags]    FieldTech FacilitiesController      AcceptInvitation
    post accept invitation  ${test_invitation_id}       TC 090

TC 091: Field Tech's access to GetInvitation
    [Tags]    FieldTech FacilitiesController      GetInvitation
    get invitation  ${test_invitation_id}   TC 091

TC 092: Field Tech's access to GetInvitations
    [Tags]    FieldTech FacilitiesController      GetInvitations
    get invitations  True   TC 092

TC 093: Field Tech's access to DoesInvitedUserExist
    [Tags]    FieldTech FacilitiesController      DoesInvitedUserExist
    get does invited user exist  ${test_invitation_id}      TC 093

TC 094: Field Tech's access to IsInvitationExpired
    [Tags]    FieldTech FacilitiesController      IsInvitationExpired
    get is invitation expired  ${test_invitation_id}    TC 094

TC 095: Field Tech's access to GetFacilityByID
    [Tags]    FieldTech FacilitiesController      GetFacilityByID
    get facility by id tc  ${created_facility_id}   TC 095

TC 096: Field Tech's access to Patch
    [Tags]    FieldTech FacilitiesController      Patch
    patch facility id  ${created_facility_id}   TC 096

TC 097: Field Tech's access to Put
    [Tags]    FieldTech FacilitiesController      Put
    put facility id  ${created_facility_id}     TC 097

TC 098: Field Tech's access to DeleteFacility
    [Tags]    FieldTech FacilitiesController      delete
    delete facility  ${created_facility_id}     TC 098

TC 143: Field Tech's access to Retrieve Token
    [Tags]    Field Tech FacilitiesController      RetrieveToken
    get retrieve token      TC 143

TC 144: Field Tech's access to GetSupportedNetworkCameras
    [Tags]    Field Tech FacilitiesController      GetSupportedNetworkCameras
    get supported network cameras   TC 144

TC 145: Field Tech's access to GetFacilityNetworkCameras
    [Tags]    Field Tech FacilitiesController      GetFacilityNetworkCameras
    get facility network cameras    TC 145

TC 146: Field Tech's access to AddFacilityNetworkCamera
    [Tags]    Field Tech FacilitiesController      AddFacilityNetworkCamera
    add facility network camera  ${supported_camera_id}  ${test_device_id}  TC 146

TC 147: Field Tech's access to RemoveFacilityNetworkCamera
    [Tags]    Field Tech FacilitiesController      RemoveFacilityNetworkCamera
    remove facility network camera  ${added_camera_id}  TC 147
