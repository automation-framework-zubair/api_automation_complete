*** Settings ***
Documentation    Test Cases to validate all Facilities Controller endpoints authorization for Facility Admin role

Library  ../../TestCaseMethods/FacilitiesControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***

${super_admin_token}
${test_facility_admin_token}
${created_facility_id}
${created_test_facility_id}
${test_invitation_id}
${test_device_id}
${supported_camera_id}
${added_camera_id}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${facility_admin}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    change selected facility  ${MMT_FAC_ID}
    ${test_device_id}=  create_test_device  ${MMT_FAC_ID}
    set suite variable  ${test_device_id}
    ${supported_camera_id}=  get random supported facility camera
    set suite variable  ${supported_camera_id}
    add facility camera for deletion test   ${supported_camera_id}  ${test_device_id}
    ${added_camera_id}=  get facility network camera based on device id  ${test_device_id}
    set suite variable  ${added_camera_id}

    create test user  ${facility_admin}
    ${created_facility_id}=  create test facility  API
    set suite variable  ${created_facility_id}
    ${test_invitation_id}=  get random invitation id  False
    set suite variable  ${test_invitation_id}

Multiple Teardown Methods
    delete test user    ${facility_admin}
    RUN KEYWORD IF  $created_facility_id is not None     delete test facility    ${created_facility_id}
    RUN KEYWORD IF  $created_test_facility_id is not None     delete test patient    ${created_test_facility_id}
    RUN KEYWORD IF  $test_device_id is not None     delete test device  ${test_device_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Facilities Controller for Facility Admin
    initialize access for facilities controller  ${facility_admin}

Login with Facility Admin
    ${test_facility_admin_token}=   test user login by role     ${facility_admin}    ${None}
    set suite variable  ${test_facility_admin_token}
    set token facilities  ${test_facility_admin_token}

# FACILITIES CONTROLLER
TC 043: Facility Admin's access to RotateToken
    [Tags]    FacilityAdmin FacilitiesController      RotateToken
    post rotate token  ${created_facility_id}   TC 043

TC 044: Facility Admin's access to Metrics
    [Tags]    FacilityAdmin FacilitiesController      metrics
    get metrics  ${created_facility_id}     TC 044

TC 045: Facility Admin's access to GetFacilities
    [Tags]    FacilityAdmin FacilitiesController      GetFacilities
    get facilities      TC 045

TC 046: Facility Admin's access to CreateFacility
    [Tags]    FacilityAdmin FacilitiesController      CreateFacility
    ${created_test_facility_id}=  post create facility tc  TEST_NAME    TC 046
    set suite variable  ${created_test_facility_id}

TC 047: Facility Admin's access to SendInvitation
    [Tags]    FacilityAdmin FacilitiesController      SendInvitation
    get send invitation  ${office_personnel}    ${None}     TC 047

TC 048: Facility Admin's access to AcceptInvitation
    [Tags]    FacilityAdmin FacilitiesController      AcceptInvitation
    post accept invitation  ${test_invitation_id}   TC 048

TC 049: Facility Admin's access to GetInvitation
    [Tags]    FacilityAdmin FacilitiesController      GetInvitation
    get invitation  ${test_invitation_id}   TC 049

TC 050: Facility Admin's access to GetInvitations
    [Tags]    FacilityAdmin FacilitiesController      GetInvitations
    get invitations  True   TC 050

TC 051: Facility Admin's access to DoesInvitedUserExist
    [Tags]    FacilityAdmin FacilitiesController      DoesInvitedUserExist
    get does invited user exist  ${test_invitation_id}      TC 051

TC 052: Facility Admin's access to IsInvitationExpired
    [Tags]    FacilityAdmin FacilitiesController      IsInvitationExpired
    get is invitation expired  ${test_invitation_id}    TC 052

TC 053: Facility Admin's access to GetFacilityByID
    [Tags]    FacilityAdmin FacilitiesController      GetFacilityByID
    get facility by id tc  ${created_facility_id}   TC 053

TC 054: Facility Admin's access to Patch
    [Tags]    FacilityAdmin FacilitiesController      Patch
    patch facility id  ${created_facility_id}       TC 054

TC 055: Facility Admin's access to Put
    [Tags]    FacilityAdmin FacilitiesController      Put
    put facility id  ${created_facility_id}     TC 055

TC 056: Facility Admin's access to DeleteFacility
    [Tags]    FacilityAdmin FacilitiesController      delete
    delete facility  ${created_facility_id}     TC 056

TC 128: Facility Admin's access to Retrieve Token
    [Tags]    Facility Admin FacilitiesController      RetrieveToken
    get retrieve token      TC 128

TC 129: Facility Admin's access to GetSupportedNetworkCameras
    [Tags]    Facility Admin FacilitiesController      GetSupportedNetworkCameras
    get supported network cameras   TC 129

TC 130: Facility Admin's access to GetFacilityNetworkCameras
    [Tags]    Facility Admin FacilitiesController      GetFacilityNetworkCameras
    get facility network cameras    TC 130

TC 131: Facility Admin's access to AddFacilityNetworkCamera
    [Tags]    Facility Admin FacilitiesController      AddFacilityNetworkCamera
    add facility network camera  ${supported_camera_id}  ${test_device_id}  TC 131

TC 132: Facility Admin's access to RemoveFacilityNetworkCamera
    [Tags]    Facility Admin FacilitiesController      RemoveFacilityNetworkCamera
    remove facility network camera  ${added_camera_id}  TC 132
