*** Settings ***
Documentation    Test Cases to validate all Facilities Controller endpoints authorization for Review Doctor role

Library  ../../TestCaseMethods/FacilitiesControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***

${super_admin_token}
${test_review_doctor_token}
${created_facility_id}
${created_test_facility_id}
${test_invitation_id}
${test_device_id}
${supported_camera_id}
${added_camera_id}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${review_doctor}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    change selected facility  ${MMT_FAC_ID}
    ${test_device_id}=  create_test_device  ${MMT_FAC_ID}
    set suite variable  ${test_device_id}
    ${supported_camera_id}=  get random supported facility camera
    set suite variable  ${supported_camera_id}
    add facility camera for deletion test   ${supported_camera_id}  ${test_device_id}
    ${added_camera_id}=  get facility network camera based on device id  ${test_device_id}
    set suite variable  ${added_camera_id}

    create test user  ${review_doctor}
    ${created_facility_id}=  create test facility  API
    set suite variable  ${created_facility_id}
    ${test_invitation_id}=  get random invitation id  False
    set suite variable  ${test_invitation_id}

Multiple Teardown Methods
    delete test user    ${review_doctor}
    RUN KEYWORD IF  $created_facility_id is not None     delete test facility    ${created_facility_id}
    RUN KEYWORD IF  $created_test_facility_id is not None     delete test patient    ${created_test_facility_id}
    RUN KEYWORD IF  $test_device_id is not None     delete test device  ${test_device_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Facilities Controller for Review Doctor
    initialize access for facilities controller  ${review_doctor}

Login with Review Doctor
    ${test_review_doctor_token}=   test user login by role     ${review_doctor}    ${None}
    set suite variable  ${test_review_doctor_token}
    set token facilities  ${test_review_doctor_token}

# FACILITIES CONTROLLER
TC 057: Review Doctor's access to RotateToken
    [Tags]    ReviewDoctor FacilitiesController      RotateToken
    post rotate token  ${created_facility_id}   TC 057

TC 058: Review Doctor's access to Metrics
    [Tags]    ReviewDoctor FacilitiesController      metrics
    get metrics  ${created_facility_id}     TC 058

TC 059: Review Doctor's access to GetFacilities
    [Tags]    ReviewDoctor FacilitiesController      GetFacilities
    get facilities  TC 059

TC 060: Review Doctor's access to CreateFacility
    [Tags]    ReviewDoctor FacilitiesController      CreateFacility
    ${created_test_facility_id}=  post create facility tc  TEST_NAME    TC 060
    set suite variable  ${created_test_facility_id}

TC 061: Review Doctor's access to SendInvitation
    [Tags]    ReviewDoctor FacilitiesController      SendInvitation
    get send invitation  ${office_personnel}    ${None}     TC 061

TC 062: Review Doctor's access to AcceptInvitation
    [Tags]    ReviewDoctor FacilitiesController      AcceptInvitation
    post accept invitation  ${test_invitation_id}   TC 062

TC 063: Review Doctor's access to DoesInvitedUserExist
    [Tags]    ReviewDoctor FacilitiesController      DoesInvitedUserExist
    get does invited user exist  ${test_invitation_id}      TC 063

TC 064: Review Doctor's access to IsInvitationExpired
    [Tags]    ReviewDoctor FacilitiesController      IsInvitationExpired
    get is invitation expired  ${test_invitation_id}    TC 064

TC 065: Review Doctor's access to GetInvitation
    [Tags]    ReviewDoctor FacilitiesController      GetInvitation
    get invitation  ${test_invitation_id}   TC 065

TC 066: Review Doctor's access to GetInvitations
    [Tags]    ReviewDoctor FacilitiesController      GetInvitations
    get invitations  True   TC 066

TC 067: Review Doctor's access to GetFacilityByID
    [Tags]    ReviewDoctor FacilitiesController      GetFacilityByID
    get facility by id tc  ${created_facility_id}   TC 067

TC 068: Review Doctor's access to Patch
    [Tags]    ReviewDoctor FacilitiesController      Patch
    patch facility id  ${created_facility_id}   TC 068

TC 069: Review Doctor's access to Put
    [Tags]    ReviewDoctor FacilitiesController      Put
    put facility id  ${created_facility_id}     TC 069

TC 070: Review Doctor's access to DeleteFacility
    [Tags]    ReviewDoctor FacilitiesController      delete
    delete facility  ${created_facility_id}     TC 070

TC 133: Review Doctor's access to Retrieve Token
    [Tags]    Review Doctor FacilitiesController      RetrieveToken
    get retrieve token      TC 133

TC 134: Review Doctor's access to GetSupportedNetworkCameras
    [Tags]    Review Doctor FacilitiesController      GetSupportedNetworkCameras
    get supported network cameras   TC 134

TC 135: Review Doctor's access to GetFacilityNetworkCameras
    [Tags]    Review Doctor FacilitiesController      GetFacilityNetworkCameras
    get facility network cameras    TC 135

TC 136: Review Doctor's access to AddFacilityNetworkCamera
    [Tags]    Review Doctor FacilitiesController      AddFacilityNetworkCamera
    add facility network camera  ${supported_camera_id}  ${test_device_id}  TC 136

TC 137: Review Doctor's access to RemoveFacilityNetworkCamera
    [Tags]    Review Doctor FacilitiesController      RemoveFacilityNetworkCamera
    remove facility network camera  ${added_camera_id}  TC 137
