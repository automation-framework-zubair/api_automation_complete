*** Settings ***
Documentation    Test Cases to validate all Facilities Controller endpoints authorization for Production role

Library  ../../TestCaseMethods/FacilitiesControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***

${super_admin_token}
${test_production_token}
${created_facility_id}
${created_test_facility_id}
${test_invitation_id}
${test_device_id}
${supported_camera_id}
${added_camera_id}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${production}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    change selected facility  ${MMT_FAC_ID}
    ${test_device_id}=  create_test_device  ${MMT_FAC_ID}
    set suite variable  ${test_device_id}
    ${supported_camera_id}=  get random supported facility camera
    set suite variable  ${supported_camera_id}
    add facility camera for deletion test   ${supported_camera_id}  ${test_device_id}
    ${added_camera_id}=  get facility network camera based on device id  ${test_device_id}
    set suite variable  ${added_camera_id}

    create test user  ${production}
    ${created_facility_id}=  create test facility  API
    set suite variable  ${created_facility_id}
    ${test_invitation_id}=  get random invitation id  False
    set suite variable  ${test_invitation_id}

Multiple Teardown Methods
    delete test user    ${production}
    RUN KEYWORD IF  $created_facility_id is not None     delete test facility    ${created_facility_id}
    RUN KEYWORD IF  $created_test_facility_id is not None     delete test patient    ${created_test_facility_id}
    RUN KEYWORD IF  $test_device_id is not None     delete test device  ${test_device_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Facilities Controller for Production
    initialize access for facilities controller  ${production}

Login with Production
    ${test_production_token}=   test user login by role     ${production}    ${None}
    set suite variable  ${test_production_token}
    set token facilities  ${test_production_token}

# FACILITIES CONTROLLER
TC 029: Production's access to RotateToken
    [Tags]    Production FacilitiesController      RotateToken
    post rotate token  ${created_facility_id}   TC 029

TC 030: Production's access to Metrics
    [Tags]    Production FacilitiesController      metrics
    get metrics  ${created_facility_id}     TC 030

TC 031: Production's access to GetFacilities
    [Tags]    Production FacilitiesController      GetFacilities
    get facilities      TC 031

TC 032: Production's access to CreateFacility
    [Tags]    Production FacilitiesController      CreateFacility
    ${created_test_facility_id}=  post create facility tc  TEST_NAME    TC 032
    set suite variable  ${created_test_facility_id}

TC 033: Production's access to SendInvitation
    [Tags]    Production FacilitiesController      SendInvitation
    get send invitation  ${office_personnel}    ${None}     TC 033

TC 034: Production's access to AcceptInvitation
    [Tags]    Production FacilitiesController      AcceptInvitation
    post accept invitation  ${test_invitation_id}   TC 034

TC 035: Production's access to GetInvitation
    [Tags]    Production FacilitiesController      GetInvitation
    get invitation  ${test_invitation_id}   TC 035

TC 036: Production's access to GetInvitations
    [Tags]    Production FacilitiesController      GetInvitations
    get invitations  True   TC 036

TC 037: Production's access to DoesInvitedUserExist
    [Tags]    Production FacilitiesController      DoesInvitedUserExist
    get does invited user exist  ${test_invitation_id}      TC 037

TC 038: Production's access to IsInvitationExpired
    [Tags]    Production FacilitiesController      IsInvitationExpired
    get is invitation expired  ${test_invitation_id}    TC 038

TC 039: Production's access to GetFacilityByID
    [Tags]    Production FacilitiesController      GetFacilityByID
    get facility by id tc  ${created_facility_id}   TC 039

TC 040: Production's access to Patch
    [Tags]    Production FacilitiesController      Patch
    patch facility id  ${created_facility_id}   TC 040

TC 041: Production's access to Put
    [Tags]    Production FacilitiesController      Put
    put facility id  ${created_facility_id}     TC 041

TC 042: Production's access to DeleteFacility
    [Tags]    Production FacilitiesController      delete
    delete facility  ${created_facility_id}     TC 042

TC 123: Production's access to Retrieve Token
    [Tags]    Production FacilitiesController      RetrieveToken
    get retrieve token      TC 123

TC 124: Production's access to GetSupportedNetworkCameras
    [Tags]    Production FacilitiesController      GetSupportedNetworkCameras
    get supported network cameras   TC 124

TC 125: Production's access to GetFacilityNetworkCameras
    [Tags]    Production FacilitiesController      GetFacilityNetworkCameras
    get facility network cameras    TC 125

TC 126: Production's access to AddFacilityNetworkCamera
    [Tags]    Production FacilitiesController      AddFacilityNetworkCamera
    add facility network camera  ${supported_camera_id}  ${test_device_id}  TC 126

TC 127: Production's access to RemoveFacilityNetworkCamera
    [Tags]    Production FacilitiesController      RemoveFacilityNetworkCamera
    remove facility network camera  ${added_camera_id}  TC 127
