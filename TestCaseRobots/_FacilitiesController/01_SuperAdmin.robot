*** Settings ***
Documentation    Test Cases to validate all Facilities Controller endpoints authorization for Super Admin role

Library  ../../TestCaseMethods/FacilitiesControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***

${super_admin_token}
${test_super_admin_token}
${created_facility_id}
${created_test_facility_id}
${test_invitation_id}
${test_device_id}
${supported_camera_id}
${added_camera_id}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${super_admin}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    get version number  ${super_admin_token}
    change selected facility  ${MMT_FAC_ID}
    ${test_device_id}=  create_test_device  ${MMT_FAC_ID}
    set suite variable  ${test_device_id}
    ${supported_camera_id}=  get random supported facility camera
    set suite variable  ${supported_camera_id}
    add facility camera for deletion test   ${supported_camera_id}  ${test_device_id}
    ${added_camera_id}=  get facility network camera based on device id  ${test_device_id}
    set suite variable  ${added_camera_id}

    create test user  ${super_admin}
    ${created_facility_id}=  create test facility  API
    set suite variable  ${created_facility_id}
    ${test_invitation_id}=  get random invitation id  False
    set suite variable  ${test_invitation_id}

Multiple Teardown Methods
    delete test user    ${super_admin}
    RUN KEYWORD IF  $created_facility_id is not None     delete test facility    ${created_facility_id}
    RUN KEYWORD IF  $created_test_facility_id is not None     delete test patient    ${created_test_facility_id}
    RUN KEYWORD IF  $test_device_id is not None     delete test device  ${test_device_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Facilities Controller for Super Admin
    initialize access for facilities controller  ${super_admin}

Login with Super Admin
    ${test_super_admin_token}=   test user login by role     ${super_admin}    ${None}
    set suite variable  ${test_super_admin_token}
    set token facilities  ${test_super_admin_token}

# FACILITIES CONTROLLER
TC 001: SuperAdmin's access to RotateToken
    [Tags]    SuperAdmin FacilitiesController      RotateToken
    post rotate token  ${created_facility_id}   TC 001

TC 002: SuperAdmin's access to Metrics
    [Tags]    SuperAdmin FacilitiesController      metrics
    get metrics  ${created_facility_id}     TC 002

TC 003: SuperAdmin's access to GetFacilities
    [Tags]    SuperAdmin FacilitiesController      GetFacilities
    get facilities      TC 003

TC 004: SuperAdmin's access to CreateFacility
    [Tags]    SuperAdmin FacilitiesController      CreateFacility
    ${created_test_facility_id}=  post create facility tc  TEST_NAME    TC 004
    set suite variable  ${created_test_facility_id}

TC 005: SuperAdmin's access to SendInvitation
    [Tags]    SuperAdmin FacilitiesController      SendInvitation
    get send invitation  ${office_personnel}    ${None}     TC 005


TC 006: SuperAdmin's access to AcceptInvitation
    [Tags]    SuperAdmin FacilitiesController      AcceptInvitation
    post accept invitation  ${test_invitation_id}   TC 006

TC 007: SuperAdmin's access to GetInvitation
    [Tags]    SuperAdmin FacilitiesController      GetInvitation
    get invitation  ${test_invitation_id}   TC 007

TC 008: SuperAdmin's access to GetInvitations
    [Tags]    SuperAdmin FacilitiesController      GetInvitations
    get invitations  True   TC 008

TC 009: SuperAdmin's access to DoesInvitedUserExist
    [Tags]    SuperAdmin FacilitiesController      DoesInvitedUserExist
    get does invited user exist  ${test_invitation_id}      TC 009

TC 010: SuperAdmin's access to IsInvitationExpired
    [Tags]    SuperAdmin FacilitiesController      IsInvitationExpired
    get is invitation expired  ${test_invitation_id}    TC 010

TC 011: SuperAdmin's access to GetFacilityByID
    [Tags]    SuperAdmin FacilitiesController      GetFacilityByID
    get facility by id tc  ${created_facility_id}   TC 011

TC 012: SuperAdmin's access to Patch
    [Tags]    SuperAdmin FacilitiesController      Patch
    patch facility id  ${created_facility_id}   TC 012

TC 013: SuperAdmin's access to Put
    [Tags]    SuperAdmin FacilitiesController      Put
    put facility id  ${created_facility_id}     TC 013

TC 014: SuperAdmin's access to DeleteFacility
    [Tags]    SuperAdmin FacilitiesController      delete
    delete facility  ${created_facility_id}     TC 014

TC 113: SuperAdmin's access to Retrieve Token
    [Tags]    SuperAdmin FacilitiesController      RetrieveToken
    get retrieve token      TC 113

TC 114: SuperAdmin's access to GetSupportedNetworkCameras
    [Tags]    SuperAdmin FacilitiesController      GetSupportedNetworkCameras
    get supported network cameras   TC 114

TC 115: SuperAdmin's access to GetFacilityNetworkCameras
    [Tags]    SuperAdmin FacilitiesController      GetFacilityNetworkCameras
    get facility network cameras    TC 115

TC 116: SuperAdmin's access to AddFacilityNetworkCamera
    [Tags]    SuperAdmin FacilitiesController      AddFacilityNetworkCamera
    add facility network camera  ${supported_camera_id}  ${test_device_id}  TC 116

TC 117: SuperAdmin's access to RemoveFacilityNetworkCamera
    [Tags]    SuperAdmin FacilitiesController      RemoveFacilityNetworkCamera
    remove facility network camera  ${added_camera_id}  TC 117
