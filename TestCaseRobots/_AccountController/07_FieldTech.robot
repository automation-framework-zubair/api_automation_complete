*** Settings ***
Documentation    Test Cases to validate all Account Controller endpoints authorization for Field Tech role

Library  ../../TestCaseMethods/AccountControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***
${super_admin_token}
${test_field_tech_user_token}
${test_review_doctor_id}
${test_device_id}
${new_password}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${field_tech}
    delete test user    ${review_doctor}
    delete test device  ${test_device_id}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    create test user  ${field_tech}
    ${test_review_doctor_id}=  create test user  ${review_doctor}
    set suite variable  ${test_review_doctor_id}
    ${test_device_id}=  create test device  ${mmt_fac_id}
    set suite variable  ${test_device_id}

Multiple Teardown Methods
    delete test user    ${field_tech}
    delete test user    ${review_doctor}
    delete test device  ${test_device_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Account Controller for Field Tech User
    initialize access for account controller  ${field_tech}

Login with Field Tech
    ${test_field_tech_user_token}=   test user login by role     ${field_tech}    ${None}
    set suite variable  ${test_field_tech_user_token}
    set token accounts  ${test_field_tech_user_token}

# ACCOUNT CONTROLLER
TC 085: Field Tech's access to Authorized
    [Tags]    FieldTech AccountController      Authorized
    authorized  TC 085

TC 086: Field Tech's access to Reset Password
    [Tags]    FieldTech AccountController      ResetUserPassword
    ${new_password}=  reset user password  ${test_review_doctor_id}     TC 086
    set suite variable  ${new_password}
    RUN KEYWORD IF  $new_password is not None     test user login by role     ${review_doctor}    ${new_password}

TC 087: Field Tech's access to Forget Password
    [Tags]    FieldTech AccountController      ForgotPassword
    forget password  ${email}   TC 087

TC 088: Field Tech's access to Account Recovery
    [Tags]    FieldTech AccountController      AccountRecovery
    log  UNABLE TO TEST ACCOUNT RECOVERY. REQUIRED PARAMS ARE UNKNOWN!   WARN

TC 089: Field Tech's access to User Info
    [Tags]    FieldTech AccountController      UserInfo
    user info   TC 089

TC 090: Field Tech's access to IsAuthorizedForDeviceUse
    [Tags]    FieldTech AccountController      IsAuthorizedForDeviceUse
    is authorized for device use  ${test_device_id}     TC 090

TC 091: Field Tech's access to ManageInfo
    [Tags]    FieldTech AccountController      GetManageInfo
    log  UNABLE TO TEST MANAGE INFO. REQUIRED PARAMS ARE UNKNOWN!   WARN

TC 092: Field Tech's access to ChangePassword
    [Tags]    FieldTech AccountController      ChangePassword
    change password     Enosis123   Enosis123   TC 092

TC 093: Field Tech's access to Set Password
    [Tags]    FieldTech AccountController      SetPassword
    set password    Enosis123   TC 093

TC 094: Field Tech's access to AddExternalLogin
    [Tags]    FieldTech AccountController      AddExternalLogin
    log  UNABLE TO TEST ADD EXTERNAL LOGIN. REQUIRED PARAMS ARE UNKNOWN!   WARN

TC 095: Field Tech's access to RemoveLogin
    [Tags]    FieldTech AccountController      RemoveLogin
    log  UNABLE TO TEST ADD REMOVE LOGIN. REQUIRED PARAMS ARE UNKNOWN!   WARN

TC 096: Field Tech's access to ExternalLogin
    [Tags]    FieldTech AccountController      GetExternalLogin
    log  UNABLE TO TEST EXTERNAL LOGIN. REQUIRED PARAMS ARE UNKNOWN!   WARN

TC 097: Field Tech's access to ExternalLogins
    [Tags]    FieldTech AccountController      GetExternalLogins
    log  UNABLE TO TEST EXTERNAL LOGINS. REQUIRED PARAMS ARE UNKNOWN!   WARN

TC 098: Field Tech's access to LogOut
    [Tags]    FieldTech AccountController      Logout
    log out     TC 098
