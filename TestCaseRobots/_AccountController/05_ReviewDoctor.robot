*** Settings ***
Documentation    Test Cases to validate all Account Controller endpoints authorization for Review Doctor role

Library  ../../TestCaseMethods/AccountControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***
${super_admin_token}
${test_review_doctor_user_token}
${test_lead_tech_id}
${test_device_id}
${new_password}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${review_doctor}
    delete test user    ${lead_tech}
    delete test device  ${test_device_id}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    create test user  ${review_doctor}
    ${test_lead_tech_id}=  create test user  ${lead_tech}
    set suite variable  ${test_lead_tech_id}
    ${test_device_id}=  create test device  ${mmt_fac_id}
    set suite variable  ${test_device_id}

Multiple Teardown Methods
    delete test user    ${review_doctor}
    delete test user    ${lead_tech}
    delete test device  ${test_device_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Account Controller for Review Doctor User
    initialize access for account controller  ${review_doctor}

Login with Review Doctor
    ${test_review_doctor_user_token}=   test user login by role     ${review_doctor}    ${None}
    set suite variable  ${test_review_doctor_user_token}
    set token accounts  ${test_review_doctor_user_token}

# ACCOUNT CONTROLLER
TC 057: Review Doctor's access to Authorized
    [Tags]    ReviewDoctor AccountController      Authorized
    authorized  TC 057

TC 058: Review Doctor's access to Reset Password
    [Tags]    ReviewDoctor AccountController      ResetUserPassword
    ${new_password}=  reset user password  ${test_lead_tech_id}     TC 058
    set suite variable  ${new_password}
    RUN KEYWORD IF  $new_password is not None     test user login by role     ${lead_tech}    ${new_password}

TC 059: Review Doctor's access to Forget Password
    [Tags]    ReviewDoctor AccountController      ForgotPassword
    forget password  ${email}   TC 059

TC 060: Review Doctor's access to Account Recovery
    [Tags]    ReviewDoctor AccountController      AccountRecovery
    log  UNABLE TO TEST ACCOUNT RECOVERY. REQUIRED PARAMS ARE UNKNOWN!   WARN

TC 061: Review Doctor's access to User Info
    [Tags]    ReviewDoctor AccountController      UserInfo
    user info   TC 061

TC 062: Review Doctor's access to IsAuthorizedForDeviceUse
    [Tags]    ReviewDoctor AccountController      IsAuthorizedForDeviceUse
    is authorized for device use  ${test_device_id}     TC 062

TC 063: Review Doctor's access to ManageInfo
    [Tags]    ReviewDoctor AccountController      GetManageInfo
    log  UNABLE TO TEST MANAGE INFO. REQUIRED PARAMS ARE UNKNOWN!   WARN

TC 064: Review Doctor's access to ChangePassword
    [Tags]    ReviewDoctor AccountController      ChangePassword
    change password     Enosis123   Enosis123   TC 064

TC 065: Review Doctor's access to SetPassword
    [Tags]    ReviewDoctor AccountController      SetPassword
    set password    Enosis123   TC 065

TC 066: Review Doctor's access to AddExternalLogin
    [Tags]    ReviewDoctor AccountController      AddExternalLogin
    log  UNABLE TO TEST ADD EXTERNAL LOGIN. REQUIRED PARAMS ARE UNKNOWN!   WARN

TC 067: Review Doctor's access to RemoveLogin
    [Tags]    ReviewDoctor AccountController      RemoveLogin
    log  UNABLE TO TEST ADD REMOVE LOGIN. REQUIRED PARAMS ARE UNKNOWN!   WARN

TC 068: Review Doctor's access to ExternalLogin
    [Tags]    ReviewDoctor AccountController      GetExternalLogin
    log  UNABLE TO TEST EXTERNAL LOGIN. REQUIRED PARAMS ARE UNKNOWN!   WARN

TC 069: Review Doctor's access to ExternalLogins
    [Tags]    ReviewDoctor AccountController      GetExternalLogins
    log  UNABLE TO TEST EXTERNAL LOGINS. REQUIRED PARAMS ARE UNKNOWN!   WARN

TC 070: Review Doctor's access to LogOut
    [Tags]    ReviewDoctor AccountController      Logout
    log out     TC 070
