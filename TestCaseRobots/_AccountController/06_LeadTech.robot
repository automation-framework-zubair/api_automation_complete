*** Settings ***
Documentation    Test Cases to validate all Account Controller endpoints authorization for Lead Tech role

Library  ../../TestCaseMethods/AccountControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***
${super_admin_token}
${test_lead_tech_user_token}
${test_review_doctor_id}
${test_device_id}
${new_password}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${lead_tech}
    delete test user    ${review_doctor}
    delete test device  ${test_device_id}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    create test user  ${lead_tech}
    ${test_review_doctor_id}=  create test user  ${review_doctor}
    set suite variable  ${test_review_doctor_id}
    ${test_device_id}=  create test device  ${mmt_fac_id}
    set suite variable  ${test_device_id}

Multiple Teardown Methods
    delete test user    ${lead_tech}
    delete test user    ${review_doctor}
    delete test device  ${test_device_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Account Controller for Lead Tech User
    initialize access for account controller  ${lead_tech}

Login with Lead Tech
    ${test_lead_tech_user_token}=   test user login by role     ${lead_tech}    ${None}
    set suite variable  ${test_lead_tech_user_token}
    set token accounts  ${test_lead_tech_user_token}

# ACCOUNT CONTROLLER
TC 071: Lead Tech's access to Authorized
    [Tags]    LeadTech AccountController      Authorized
    authorized  TC 071

TC 072: Lead Tech's access to Reset Password
    [Tags]    LeadTech AccountController      ResetUserPassword
    ${new_password}=  reset user password  ${test_review_doctor_id}     TC 072
    set suite variable  ${new_password}
    RUN KEYWORD IF  $new_password is not None     test user login by role     ${review_doctor}    ${new_password}

TC 073: Lead Tech's access to Forget Password
    [Tags]    LeadTech AccountController      ForgotPassword
    forget password  ${email}   TC 073

TC 074: Lead Tech's access to Account Recovery
    [Tags]    LeadTech AccountController      AccountRecovery
    log  UNABLE TO TEST ACCOUNT RECOVERY. REQUIRED PARAMS ARE UNKNOWN!   WARN

TC 075: Lead Tech's access to User Info
    [Tags]    LeadTech AccountController      UserInfo
    user info   TC 075

TC 076: Lead Tech's access to IsAuthorizedForDeviceUse
    [Tags]    LeadTech AccountController      IsAuthorizedForDeviceUse
    is authorized for device use  ${test_device_id}     TC 076

TC 077: Lead Tech's access to ManageInfo
    [Tags]    LeadTech AccountController      GetManageInfo
    log  UNABLE TO TEST MANAGE INFO. REQUIRED PARAMS ARE UNKNOWN!   WARN

TC 078: Lead Tech's access to ChangePassword
    [Tags]    LeadTech AccountController      ChangePassword
    change password     Enosis123   Enosis123   TC 078

TC 079: Lead Tech's access to SetPassword
    [Tags]    LeadTech AccountController      SetPassword
    set password    Enosis123   TC 079

TC 080: Lead Tech's access to AddExternalLogin
    [Tags]    LeadTech AccountController      AddExternalLogin
    log  UNABLE TO TEST ADD EXTERNAL LOGIN. REQUIRED PARAMS ARE UNKNOWN!   WARN

TC 081: Lead Tech's access to RemoveLogin
    [Tags]    LeadTech AccountController      RemoveLogin
    log  UNABLE TO TEST ADD REMOVE LOGIN. REQUIRED PARAMS ARE UNKNOWN!   WARN

TC 082: Lead Tech's access to ExternalLogin
    [Tags]    LeadTech AccountController      GetExternalLogin
    log  UNABLE TO TEST EXTERNAL LOGIN. REQUIRED PARAMS ARE UNKNOWN!   WARN

TC 083: Lead Tech's access to ExternalLogins
    [Tags]    LeadTech AccountController      GetExternalLogins
    log  UNABLE TO TEST EXTERNAL LOGINS. REQUIRED PARAMS ARE UNKNOWN!   WARN

TC 084: Lead Tech's access to LogOut
    [Tags]    LeadTech AccountController      Logout
    log out     TC 084
