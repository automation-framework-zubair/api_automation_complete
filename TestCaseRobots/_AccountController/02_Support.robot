*** Settings ***
Documentation    Test Cases to validate all Account Controller endpoints authorization for Support role

Library  ../../TestCaseMethods/AccountControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***
${super_admin_token}
${test_support_user_token}
${test_review_doctor_id}
${test_device_id}
${new_password}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${support}
    delete test user    ${review_doctor}
    delete test device  ${test_device_id}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    create test user  ${support}
    ${test_review_doctor_id}=  create test user  ${review_doctor}
    set suite variable  ${test_review_doctor_id}
    ${test_device_id}=  create test device  ${mmt_fac_id}
    set suite variable  ${test_device_id}

Multiple Teardown Methods
    delete test user    ${support}
    delete test user    ${review_doctor}
    delete test device  ${test_device_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Account Controller for Support User
    initialize access for account controller  ${support}

Login with Support User
    ${test_support_token}=   test user login by role     ${support}    ${None}
    set suite variable  ${test_support_token}
    set token accounts  ${test_support_token}

# ACCOUNT CONTROLLER
TC 015: Support User's access to Authorized
    [Tags]    Support AccountController      Authorized
    authorized  TC 015

TC 016: Support User's access to Reset Password
    [Tags]    Support AccountController      ResetUserPassword
    ${new_password}=  reset user password  ${test_review_doctor_id}     TC 016
    set suite variable  ${new_password}
    RUN KEYWORD IF  $new_password is not None     test user login by role     ${review_doctor}    ${new_password}

TC 017: Support User's access to Forget Password
    [Tags]    Support AccountController      ForgotPassword
    forget password  ${email}   TC 017

TC 018: Support User's access to Account Recovery
    [Tags]    Support AccountController      AccountRecovery
    log  UNABLE TO TEST ACCOUNT RECOVERY. REQUIRED PARAMS ARE UNKNOWN!   WARN

TC 019: Support User's access to User Info
    [Tags]    Support AccountController      UserInfo
    user info   TC 019

TC 020: Support User's access to IsAuthorizedForDeviceUse
    [Tags]    Support AccountController      IsAuthorizedForDeviceUse
    is authorized for device use  ${test_device_id}     TC 020

TC 021: Support User's access to ManageInfo
    [Tags]    Support AccountController      GetManageInfo
    log  UNABLE TO TEST MANAGE INFO. REQUIRED PARAMS ARE UNKNOWN!   WARN

TC 022: Support User's access to ChangePassword
    [Tags]    Support AccountController      ChangePassword
    change password     Enosis123   Enosis123   TC 022

TC 023: Support User's access to SetPassword
    [Tags]    Support AccountController      SetPassword
    set password    Enosis123   TC 023

TC 024: Support User's access to Add ExternalLogin
    [Tags]    Support AccountController      AddExternalLogin
    log  UNABLE TO TEST ADD EXTERNAL LOGIN. REQUIRED PARAMS ARE UNKNOWN!   WARN

TC 025: Support User's access to RemoveLogin
    [Tags]    Support AccountController      RemoveLogin
    log  UNABLE TO TEST ADD REMOVE LOGIN. REQUIRED PARAMS ARE UNKNOWN!   WARN

TC 026: Support User's access to ExternalLogin
    [Tags]    Support AccountController      GetExternalLogin
    log  UNABLE TO TEST EXTERNAL LOGIN. REQUIRED PARAMS ARE UNKNOWN!   WARN

TC 027: Support User's access to ExternalLogins
    [Tags]    Support AccountController      GetExternalLogins
    log  UNABLE TO TEST EXTERNAL LOGINS. REQUIRED PARAMS ARE UNKNOWN!   WARN

TC 028: Support User's access to LogOut
    [Tags]    Support AccountController      Logout
    log out     TC 028
