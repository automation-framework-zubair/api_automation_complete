*** Settings ***
Documentation    Test Cases to validate all Account Controller endpoints authorization for Production role

Library  ../../TestCaseMethods/AccountControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***
${super_admin_token}
${test_production_user_token}
${test_review_doctor_id}
${test_device_id}
${new_password}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${production}
    delete test user    ${review_doctor}
    delete test device  ${test_device_id}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    create test user  ${production}
    ${test_review_doctor_id}=  create test user  ${review_doctor}
    set suite variable  ${test_review_doctor_id}
    ${test_device_id}=  create test device  ${mmt_fac_id}
    set suite variable  ${test_device_id}

Multiple Teardown Methods
    delete test user    ${production}
    delete test user    ${review_doctor}
    delete test device  ${test_device_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Account Controller for Production User
    initialize access for account controller  ${production}

Login with Production User
    ${test_production_user_token}=   test user login by role     ${production}    ${None}
    set suite variable  ${test_production_user_token}
    set token accounts  ${test_production_user_token}

# ACCOUNT CONTROLLER
TC 029: Production User's access to Authorized
    [Tags]   Production AccountController      Authorized
    authorized  TC 029

TC 030: Production User's access to Reset Password
    [Tags]   Production AccountController      ResetUserPassword
    ${new_password}=  reset user password  ${test_review_doctor_id}     TC 030
    set suite variable  ${new_password}
    RUN KEYWORD IF  $new_password is not None     test user login by role     ${review_doctor}    ${new_password}

TC 031: Production User's access to Forget Password
    [Tags]   Production AccountController      ForgotPassword
    forget password  ${email}   TC 031

TC 032: Production User's access to Account Recovery
    [Tags]    Production AccountController      AccountRecovery
    log  UNABLE TO TEST ACCOUNT RECOVERY. REQUIRED PARAMS ARE UNKNOWN!   WARN

TC 033: Production User's access to User Info
    [Tags]    Production AccountController      UserInfo
    user info   TC 033

TC 034: Production User's access to IsAuthorizedForDeviceUse
    [Tags]    Production AccountController      IsAuthorizedForDeviceUse
    is authorized for device use  ${test_device_id}     TC 034

TC 035: Production User's access to ManageInfo
    [Tags]    Production AccountController      GetManageInfo
    log  UNABLE TO TEST MANAGE INFO. REQUIRED PARAMS ARE UNKNOWN!   WARN

TC 036: Production User's access to ChangePassword
    [Tags]    Production AccountController      ChangePassword
    change password     Enosis123   Enosis123   TC 036

TC 037: Production User's access to SetPassword
    [Tags]    Production AccountController      SetPassword
    set password    Enosis123   TC 037

TC 038: Production User's access to AddExternalLogin
    [Tags]    Production AccountController      AddExternalLogin
    log  UNABLE TO TEST ADD EXTERNAL LOGIN. REQUIRED PARAMS ARE UNKNOWN!   WARN

TC 039: Production User's access to RemoveLogin
    [Tags]    Production AccountController      RemoveLogin
    log  UNABLE TO TEST ADD REMOVE LOGIN. REQUIRED PARAMS ARE UNKNOWN!   WARN

TC 040: Production User's access to ExternalLogin
    [Tags]    Production AccountController      GetExternalLogin
    log  UNABLE TO TEST EXTERNAL LOGIN. REQUIRED PARAMS ARE UNKNOWN!   WARN

TC 041: Production User's access to ExternalLogins
    [Tags]    Production AccountController      GetExternalLogins
    log  UNABLE TO TEST EXTERNAL LOGINS. REQUIRED PARAMS ARE UNKNOWN!   WARN

TC 042: Production User's access to LogOut
    [Tags]    Production AccountController      Logout
    log out     TC 042
