*** Settings ***
Documentation    Test Cases to validate all Account Controller endpoints authorization for Facility Admin role

Library  ../../TestCaseMethods/AccountControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***
${super_admin_token}
${test_facility_admin_user_token}
${test_review_doctor_id}
${test_device_id}
${new_password}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${facility_admin}
    delete test user    ${review_doctor}
    delete test device  ${test_device_id}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    create test user  ${facility_admin}
    ${test_review_doctor_id}=  create test user  ${review_doctor}
    set suite variable  ${test_review_doctor_id}
    ${test_device_id}=  create test device  ${mmt_fac_id}
    set suite variable  ${test_device_id}

Multiple Teardown Methods
    delete test user    ${facility_admin}
    delete test user    ${review_doctor}
    delete test device  ${test_device_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Account Controller for Facility Admin User
    initialize access for account controller  ${facility_admin}

Login with Facility Admin
    ${test_facility_admin_user_token}=   test user login by role     ${facility_admin}    ${None}
    set suite variable  ${test_facility_admin_user_token}
    set token accounts  ${test_facility_admin_user_token}

# ACCOUNT CONTROLLER
TC 043: Facility Admin's access to Authorized
    [Tags]    FacilityAdmin AccountController      Authorized
    authorized  TC 043

TC 044: Facility Admin's access to Reset Password
    [Tags]    FacilityAdmin AccountController      ResetUserPassword
    ${new_password}=  reset user password  ${test_review_doctor_id}     TC 044
    set suite variable  ${new_password}
    RUN KEYWORD IF  $new_password is not None     test user login by role     ${review_doctor}    ${new_password}

TC 045: Facility Admin's access to Forget Password
    [Tags]    FacilityAdmin AccountController      ForgotPassword
    forget password  ${email}   TC 045

TC 046: Facility Admin's access to Account Recovery
    [Tags]    FacilityAdmin AccountController      AccountRecovery
    log  UNABLE TO TEST ACCOUNT RECOVERY. REQUIRED PARAMS ARE UNKNOWN!   WARN

TC 047: Facility Admin's access to User Info
    [Tags]    FacilityAdmin AccountController      UserInfo
    user info   TC 047

TC 048: Facility Admin's access to IsAuthorizedForDeviceUse
    [Tags]    FacilityAdmin AccountController      IsAuthorizedForDeviceUse
    is authorized for device use  ${test_device_id}     TC 048

TC 049: Facility Admin's access to ManageInfo
    [Tags]    FacilityAdmin AccountController      GetManageInfo
    log  UNABLE TO TEST MANAGE INFO. REQUIRED PARAMS ARE UNKNOWN!   WARN

TC 050: Facility Admin's access to ChangePassword
    [Tags]    FacilityAdmin AccountController      ChangePassword
    change password     Enosis123   Enosis123   TC 050

TC 051: Facility Admin's access to SetPassword
    [Tags]    FacilityAdmin AccountController      SetPassword
    set password    Enosis123   TC 051

TC 052: Facility Admin's access to AddExternalLogin
    [Tags]    FacilityAdmin AccountController      AddExternalLogin
    log  UNABLE TO TEST ADD EXTERNAL LOGIN. REQUIRED PARAMS ARE UNKNOWN!   WARN

TC 053: Facility Admin's access to RemoveLogin
    [Tags]    FacilityAdmin AccountController      RemoveLogin
    log  UNABLE TO TEST ADD REMOVE LOGIN. REQUIRED PARAMS ARE UNKNOWN!   WARN

TC 054: Facility Admin's access to ExternalLogin
    [Tags]    FacilityAdmin AccountController      GetExternalLogin
    log  UNABLE TO TEST EXTERNAL LOGIN. REQUIRED PARAMS ARE UNKNOWN!   WARN

TC 055: Facility Admin's access to ExternalLogins
    [Tags]    FacilityAdmin AccountController      GetExternalLogins
    log  UNABLE TO TEST EXTERNAL LOGINS. REQUIRED PARAMS ARE UNKNOWN!   WARN

TC 056: Facility Admin's access to LogOut
    [Tags]    FacilityAdmin AccountController      Logout
    log out     TC 056
