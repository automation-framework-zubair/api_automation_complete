*** Settings ***
Documentation    Test Cases to validate all Account Controller endpoints authorization for Office Personnel role

Library  ../../TestCaseMethods/AccountControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***
${super_admin_token}
${test_office_personnel_user_token}
${test_review_doctor_id}
${test_device_id}
${new_password}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${office_personnel}
    delete test user    ${review_doctor}
    delete test device  ${test_device_id}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    create test user  ${office_personnel}
    ${test_review_doctor_id}=  create test user  ${review_doctor}
    set suite variable  ${test_review_doctor_id}
    ${test_device_id}=  create test device  ${mmt_fac_id}
    set suite variable  ${test_device_id}

Multiple Teardown Methods
    delete test user    ${office_personnel}
    delete test user    ${review_doctor}
    delete test device  ${test_device_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Account Controller for Office Personnel User
    initialize access for account controller  ${office_personnel}

Login with Office Personnel
    ${test_office_personnel_user_token}=   test user login by role     ${office_personnel}    ${None}
    set suite variable  ${test_office_personnel_user_token}
    set token accounts  ${test_office_personnel_user_token}

# ACCOUNT CONTROLLER
TC 099: Office Personnel's access to Authorized
    [Tags]    OfficePersonnel AccountController      Authorized
    authorized  TC 099

TC 100: Office Personnel's access to Reset Password
    [Tags]    OfficePersonnel AccountController      ResetUserPassword
    ${new_password}=  reset user password  ${test_review_doctor_id}     TC 100
    set suite variable  ${new_password}
    RUN KEYWORD IF  $new_password is not None     test user login by role     ${review_doctor}    ${new_password}

TC 101: Office Personnel's access to Forget Password
    [Tags]    OfficePersonnel AccountController      ForgotPassword
    forget password  ${email}   TC 101

TC 102: Office Personnel's access to Account Recovery
    [Tags]    OfficePersonnel AccountController      AccountRecovery
    log  UNABLE TO TEST ACCOUNT RECOVERY. REQUIRED PARAMS ARE UNKNOWN!   WARN

TC 103: Office Personnel's access to User Info
    [Tags]    OfficePersonnel AccountController      UserInfo
    user info   TC 103

TC 104: Office Personnel's access to IsAuthorizedForDeviceUse
    [Tags]    OfficePersonnel AccountController      IsAuthorizedForDeviceUse
    is authorized for device use  ${test_device_id}     TC 104

TC 105: Office Personnel's access to ManageInfo
    [Tags]    OfficePersonnel AccountController      GetManageInfo
    log  UNABLE TO TEST MANAGE INFO. REQUIRED PARAMS ARE UNKNOWN!   WARN

TC 106: Office Personnel's access to ChangePassword
    [Tags]    OfficePersonnel AccountController      ChangePassword
    change password     Enosis123   Enosis123   TC 106

TC 107: Office Personnel's access to SetPassword
    [Tags]    OfficePersonnel AccountController      SetPassword
    set password    Enosis123   TC 107

TC 108: Office Personnel's access to AddExternalLogin
    [Tags]    OfficePersonnel AccountController      AddExternalLogin
    log  UNABLE TO TEST ADD EXTERNAL LOGIN. REQUIRED PARAMS ARE UNKNOWN!   WARN

TC 109: Office Personnel's access to RemoveLogin
    [Tags]    OfficePersonnel AccountController      RemoveLogin
    log  UNABLE TO TEST ADD REMOVE LOGIN. REQUIRED PARAMS ARE UNKNOWN!   WARN

TC 110: Office Personnel's access to ExternalLogin
    [Tags]    OfficePersonnel AccountController      GetExternalLogin
    log  UNABLE TO TEST EXTERNAL LOGIN. REQUIRED PARAMS ARE UNKNOWN!   WARN

TC 111: Office Personnel's access to ExternalLogins
    [Tags]    OfficePersonnel AccountController      GetExternalLogins
    log  UNABLE TO TEST EXTERNAL LOGINS. REQUIRED PARAMS ARE UNKNOWN!   WARN

TC 112: Office Personnel's access to LogOut
    [Tags]    OfficePersonnel AccountController      Logout
    log out     TC 112
