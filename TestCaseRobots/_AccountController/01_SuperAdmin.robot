*** Settings ***
Documentation    Test Cases to validate all Account Controller endpoints authorization for Super Admin role

Library  ../../TestCaseMethods/AccountControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***
${super_admin_token}
${test_super_admin_token}
${test_review_doctor_id}
${test_device_id}
${new_password}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${super_admin}
    delete test user    ${review_doctor}
    delete test device  ${test_device_id}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    get_version_number  ${super_admin_token}
    create test user  ${super_admin}
    ${test_review_doctor_id}=  create test user  ${review_doctor}
    set suite variable  ${test_review_doctor_id}
    ${test_device_id}=  create test device  ${mmt_fac_id}
    set suite variable  ${test_device_id}

Multiple Teardown Methods
    delete test user    ${super_admin}
    delete test user    ${review_doctor}
    delete test device  ${test_device_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***

Set Access Info of Account Controller for Super Admin
    initialize access for account controller  ${super_admin}

Login with Super Admin
    ${test_super_admin_token}=   test user login by role     ${super_admin}    ${None}
    set suite variable  ${test_super_admin_token}
    set token accounts  ${test_super_admin_token}

# ACCOUNT CONTROLLER
TC 001: SuperAdmin's access to Authorized end Point
    [Tags]  SuperAdmin  AccountController      Authorized
    authorized  TC 001

TC 002: SuperAdmin's access to Reset Password
    [Tags]  SuperAdmin  AccountController      ResetUserPassword
    ${new_password}=  reset user password  ${test_review_doctor_id}     TC 002
    set suite variable  ${new_password}
    RUN KEYWORD IF  $new_password is not None     test user login by role     ${review_doctor}    ${new_password}

TC 003: SuperAdmin's access to Forget Password
    [Tags]  SuperAdmin  AccountController      ForgotPassword
    forget password  ${email}   TC 003

TC 004: SuperAdmin's access to Account Recovery
    [Tags]  SuperAdmin  AccountController      AccountRecovery
    log  UNABLE TO TEST ACCOUNT RECOVERY. REQUIRED PARAMS ARE UNKNOWN!   WARN

TC 005: SuperAdmin's access to User Info
    [Tags]  SuperAdmin  AccountController      UserInfo
    user info   TC 005

TC 006: SuperAdmin's access to IsAuthorizedForDeviceUse
    [Tags]  SuperAdmin  AccountController      IsAuthorizedForDeviceUse
    is authorized for device use  ${test_device_id}     TC 006

TC 007: SuperAdmin's access to ManageInfo
    [Tags]  SuperAdmin  AccountController      GetManageInfo
    log  UNABLE TO TEST MANAGE INFO. REQUIRED PARAMS ARE UNKNOWN!   WARN

TC 008: SuperAdmin's access to ChangePassword
    [Tags]  SuperAdmin  AccountController      ChangePassword
    change password     Enosis123   Enosis123   TC 008

TC 009: SuperAdmin's access to SetPassword
    [Tags]  SuperAdmin  AccountController      SetPassword
    set password    Enosis123   TC 009

TC 010: SuperAdmin's access to AddExternalLogin
    [Tags]  SuperAdmin  AccountController      AddExternalLogin
    log  UNABLE TO TEST ADD EXTERNAL LOGIN. REQUIRED PARAMS ARE UNKNOWN!   WARN

TC 011: SuperAdmin's access to RemoveLogin
    [Tags]  SuperAdmin  AccountController      RemoveLogin
    log  UNABLE TO TEST ADD REMOVE LOGIN. REQUIRED PARAMS ARE UNKNOWN!   WARN

TC 012: SuperAdmin's access to ExternalLogin
    [Tags]  SuperAdmin  AccountController      GetExternalLogin
    log  UNABLE TO TEST EXTERNAL LOGIN. REQUIRED PARAMS ARE UNKNOWN!   WARN

TC 013: SuperAdmin's access to ExternalLogins
    [Tags]  SuperAdmin  AccountController      GetExternalLogins
    log  UNABLE TO TEST EXTERNAL LOGINS. REQUIRED PARAMS ARE UNKNOWN!   WARN

TC 014: SuperAdmin's access to LogOut
    [Tags]  SuperAdmin  AccountController      Logout
    log out     TC 014
