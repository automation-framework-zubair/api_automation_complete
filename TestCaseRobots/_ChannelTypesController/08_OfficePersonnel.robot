*** Settings ***
Documentation    Test Cases to validate all Channel Types Controller endpoints authorization for Office Personnel role

Library     ../../TestCaseMethods/ChannelTypesControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py
Library  ../../TestCaseMethods/_CommonStudiesTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***
${super_admin_token}
${test_office_personnel_token}


*** Keywords ***
Multiple Setup Methods
    delete test user    ${office_personnel}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    set common studies token  ${super_admin_token}
    change selected facility   ${mmt_fac_id}
    create test user  ${office_personnel}


Multiple Teardown Methods
    delete test user    ${office_personnel}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Channel Types Controller for Office Personnel
    initialize access for channel types controller   ${office_personnel}

Login with Office Personnel
    ${test_office_personnel_token}=  test user login by role  ${office_personnel}  ${None}
    set suite variable        ${test_office_personnel_token}
    set token channel types   ${test_office_personnel_token}

# CHANNEL TYPES CONTROLLER
TC 008: Office Personnel's access to Get Supported Channel Types
    [Tags]      OfficePersonnel ChannelTypesController   SupportedChannelTypes
    get supported channel types                 TC 008
