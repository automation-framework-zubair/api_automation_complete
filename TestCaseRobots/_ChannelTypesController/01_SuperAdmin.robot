*** Settings ***
Documentation    Test Cases to validate all Channel Types Controller endpoints authorization for Super Admin role

Library     ../../TestCaseMethods/ChannelTypesControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py
Library  ../../TestCaseMethods/_CommonStudiesTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***
${super_admin_token}
${test_super_admin_token}


*** Keywords ***
Multiple Setup Methods
    delete test user    ${super_admin}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    get_version_number  ${super_admin_token}
    set common studies token  ${super_admin_token}
    change selected facility   ${mmt_fac_id}
    create test user  ${super_admin}


Multiple Teardown Methods
    delete test user    ${super_admin}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Channel Types Controller for Super Admin
    initialize access for channel types controller   ${super_admin}

Login with SuperAdmin
    ${test_super_admin_token}=  test user login by role  ${super_admin}  ${None}
    set suite variable  ${test_super_admin_token}
    set token channel types   ${test_super_admin_token}

# CHANNEL TYPES CONTROLLER
TC 001: SuperAdmin access to Get Supported Channel Types
    [Tags]      SuperAdmin ChannelTypesController   SupportedChannelTypes
    get supported channel types         TC 001
