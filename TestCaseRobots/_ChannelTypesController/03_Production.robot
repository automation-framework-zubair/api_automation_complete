*** Settings ***
Documentation    Test Cases to validate all Channel Types Controller endpoints authorization for Production role

Library     ../../TestCaseMethods/ChannelTypesControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py
Library  ../../TestCaseMethods/_CommonStudiesTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***
${super_admin_token}
${test_production_token}


*** Keywords ***
Multiple Setup Methods
    delete test user    ${production}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    set common studies token  ${super_admin_token}
    change selected facility   ${mmt_fac_id}
    create test user  ${production}


Multiple Teardown Methods
    delete test user    ${production}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Channel Types Controller for Production
    initialize access for channel types controller   ${production}

Login with Production
    ${test_production_token}=  test user login by role  ${production}  ${None}
    set suite variable  ${test_production_token}
    set token channel types   ${test_production_token}

# CHANNEL TYPES CONTROLLER
TC 003: Production's access to Get Supported Channel Types
    [Tags]      Production ChannelTypesController   SupportedChannelTypes
    get supported channel types             TC 003
