*** Settings ***
Documentation    Test Cases to validate all Channel Types Controller endpoints authorization for Review Doctor role

Library     ../../TestCaseMethods/ChannelTypesControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py
Library  ../../TestCaseMethods/_CommonStudiesTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***
${super_admin_token}
${test_review_doctor_token}


*** Keywords ***
Multiple Setup Methods
    delete test user    ${review_doctor}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    set common studies token  ${super_admin_token}
    change selected facility   ${mmt_fac_id}
    create test user  ${review_doctor}


Multiple Teardown Methods
    delete test user    ${review_doctor}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Channel Types Controller for Review Doctor
    initialize access for channel types controller   ${review_doctor}

Login with Review Doctor
    ${test_review_doctor_token}=  test user login by role  ${review_doctor}  ${None}
    set suite variable    ${test_review_doctor_token}
    set token channel types   ${test_review_doctor_token}

# CHANNEL TYPES CONTROLLER
TC 005: Review Doctor's access to Get Supported Channel Types
    [Tags]      ChannelTypesController   SupportedChannelTypes
    get supported channel types             TC 005
