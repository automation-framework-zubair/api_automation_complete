*** Settings ***
Documentation    Test Cases to validate all Channel Types Controller endpoints authorization for Lead Tech role

Library     ../../TestCaseMethods/ChannelTypesControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py
Library  ../../TestCaseMethods/_CommonStudiesTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***
${super_admin_token}
${test_lead_tech_token}


*** Keywords ***
Multiple Setup Methods
    delete test user    ${lead_tech}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    set common studies token  ${super_admin_token}
    change selected facility   ${mmt_fac_id}
    create test user  ${lead_tech}


Multiple Teardown Methods
    delete test user    ${lead_tech}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Channel Types Controller for Lead Tech
    initialize access for channel types controller   ${lead_tech}

Login with Lead Tech
    ${test_lead_tech_token}=  test user login by role  ${lead_tech}  ${None}
    set suite variable    ${test_lead_tech_token}
    set token channel types   ${test_lead_tech_token}

# CHANNEL TYPES CONTROLLER
TC 006: Lead Tech's access to Get Supported Channel Types
    [Tags]      LeadTech ChannelTypesController   SupportedChannelTypes
    get supported channel types                 TC 006
