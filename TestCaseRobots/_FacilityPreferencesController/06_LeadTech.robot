*** Settings ***
Documentation    Test Cases to validate all FacilityPreferences Controller endpoints authorization for Lead Tech role

Library  ../../TestCaseMethods/FacilityPreferencesControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***

${super_admin_token}
${test_lead_tech_token}
${created_facility_id}
${preference_id}
${created_preference_id}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${lead_tech}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    change selected facility  ${MMT_FAC_ID}
    ${created_facility_id}=  create test facility  API
    set suite variable  ${created_facility_id}
    change selected facility  ${created_facility_id}
    create test user  ${lead_tech}
    ${preference_id}=  get random facility preference id  ${created_facility_id}    MaxDocumentsPerPatient
    set suite variable  ${preference_id}
    change selected facility  ${MMT_FAC_ID}

Multiple Teardown Methods
    delete test user    ${lead_tech}
    change selected facility  ${MMT_FAC_ID}
    RUN KEYWORD IF  $created_facility_id is not None     delete test facility    ${created_facility_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Facilities Controller for LeadTech
    initialize access for facility preferences controller  ${lead_tech}

Login with LeadTech
    ${test_lead_tech_token}=   test user login by role     ${lead_tech}    ${None}
    set suite variable  ${test_lead_tech_token}
    set token facility preferences  ${test_lead_tech_token}

# FACILITY PREFERENCES CONTROLLER
TC 036: Lead Tech's access to GetPreferencesById
    [Tags]    LeadTech FacilitiesPreferencesController      GetPreferencesById
    get facility preferences tc  ${created_facility_id}    MaxDocumentsPerPatient       TC 036

TC 037: Lead Tech's access to GetPreferences
    [Tags]    LeadTech FacilitiesPreferencesController      GetPreferences
    get preferences     TC 037

TC 038: Lead Tech's access to GetById
    [Tags]    LeadTech FacilitiesPreferencesController      GetByID
    get preference by id    ${preference_id}        TC 038

TC 039: Lead Tech's access to Patch Preference
    [Tags]    LeadTech FacilitiesPreferencesController      Patch
    patch preference    ${preference_id}    50      TC 039

TC 040: Lead Tech's access to Put Preference
    [Tags]    LeadTech FacilitiesPreferencesController      Put
    put preference    ${preference_id}    50    ${created_facility_id}   1      TC 040

TC 041: Lead Tech's access to Post Preference
    [Tags]    LeadTech FacilitiesPreferencesController      Post
    post preference    70    ${created_facility_id}     ${None}     TC 041

TC 042: Lead Tech's access to Delete Preference
    [Tags]    LeadTech FacilitiesPreferencesController      Delete
    delete preference  ${preference_id}     TC 042
