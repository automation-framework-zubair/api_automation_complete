*** Settings ***
Documentation    Test Cases to validate all FacilityPreferences Controller endpoints authorization for Facility Admin role

Library  ../../TestCaseMethods/FacilityPreferencesControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***

${super_admin_token}
${test_facility_admin_token}
${created_facility_id}
${preference_id}
${created_preference_id}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${facility_admin}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    change selected facility  ${MMT_FAC_ID}
    ${created_facility_id}=  create test facility  API
    set suite variable  ${created_facility_id}
    change selected facility  ${created_facility_id}
    create test user  ${facility_admin}
    ${preference_id}=  get random facility preference id  ${created_facility_id}    MaxDocumentsPerPatient
    set suite variable  ${preference_id}
    change selected facility  ${MMT_FAC_ID}

Multiple Teardown Methods
    delete test user    ${facility_admin}
    change selected facility  ${MMT_FAC_ID}
    RUN KEYWORD IF  $created_facility_id is not None     delete test facility    ${created_facility_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Facilities Controller for FacilityAdmin
    initialize access for facility preferences controller  ${facility_admin}

Login with FacilityAdmin
    ${test_facility_admin_token}=   test user login by role     ${facility_admin}    ${None}
    set suite variable  ${test_facility_admin_token}
    set token facility preferences  ${test_facility_admin_token}

# FACILITY PREFERENCES CONTROLLER
TC 022: Facility Admin's access to GetPreferencesById
    [Tags]    FacilityAdmin FacilitiesPreferencesController      GetPreferencesById
    get facility preferences tc  ${created_facility_id}    MaxDocumentsPerPatient       TC 022

TC 023: Facility Admin's access to GetPreferences
    [Tags]    FacilityAdmin FacilitiesPreferencesController      GetPreferences
    get preferences     TC 023

TC 024: Facility Admin's access to GetById
    [Tags]    FacilityAdmin FacilitiesPreferencesController      GetByID
    get preference by id    ${preference_id}        TC 024

TC 025: Facility Admin's access to Patch Preference
    [Tags]    FacilityAdmin FacilitiesPreferencesController      Patch
    patch preference    ${preference_id}    50      TC 025

TC 026: Facility Admin's access to Put Preference
    [Tags]    FacilityAdmin FacilitiesPreferencesController      Put
    put preference    ${preference_id}    50    ${created_facility_id}   1      TC 026

TC 027: Facility Admin's access to Post Preference
    [Tags]    FacilityAdmin FacilitiesPreferencesController      Post
    post preference    70    ${created_facility_id}     ${None}     TC 027

TC 028: Facility Admin's access to Delete Preference
    [Tags]    FacilityAdmin FacilitiesPreferencesController      Delete
    delete preference  ${preference_id}     TC 028
