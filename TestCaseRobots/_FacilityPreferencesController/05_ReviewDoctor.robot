*** Settings ***
Documentation    Test Cases to validate all FacilityPreferences Controller endpoints authorization for Review Doctor role

Library  ../../TestCaseMethods/FacilityPreferencesControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***

${super_admin_token}
${test_review_doctor_token}
${created_facility_id}
${preference_id}
${created_preference_id}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${review_doctor}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    change selected facility  ${MMT_FAC_ID}
    ${created_facility_id}=  create test facility  API
    set suite variable  ${created_facility_id}
    change selected facility  ${created_facility_id}
    create test user  ${review_doctor}
    ${preference_id}=  get random facility preference id  ${created_facility_id}    MaxDocumentsPerPatient
    set suite variable  ${preference_id}
    change selected facility  ${MMT_FAC_ID}

Multiple Teardown Methods
    delete test user    ${review_doctor}
    change selected facility  ${MMT_FAC_ID}
    RUN KEYWORD IF  $created_facility_id is not None     delete test facility    ${created_facility_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Facilities Controller for Review Doctor
    initialize access for facility preferences controller  ${review_doctor}

Login with Review Doctor
    ${test_review_doctor_token}=   test user login by role     ${review_doctor}    ${None}
    set suite variable  ${test_review_doctor_token}
    set token facility preferences  ${test_review_doctor_token}

# FACILITY PREFERENCES CONTROLLER
TC 029: Review Doctor's access to GetPreferencesById
    [Tags]    ReviewDoctor FacilitiesPreferencesController      GetPreferencesById
    get facility preferences tc  ${created_facility_id}    MaxDocumentsPerPatient       TC 029

TC 030: Review Doctor's access to GetPreferences
    [Tags]    ReviewDoctor FacilitiesPreferencesController      GetPreferences
    get preferences     TC 030

TC 031: Review Doctor's access to GetById
    [Tags]    ReviewDoctor FacilitiesPreferencesController      GetByID
    get preference by id    ${preference_id}        TC 031

TC 032: Review Doctor's access to Patch Preference
    [Tags]    ReviewDoctor FacilitiesPreferencesController      Patch
    patch preference    ${preference_id}    50      TC 032

TC 033: Review Doctor's access to Put Preference
    [Tags]    ReviewDoctor FacilitiesPreferencesController      Put
    put preference    ${preference_id}    50    ${created_facility_id}   1      TC 033

TC 034: Review Doctor's access to Post Preference
    [Tags]    ReviewDoctor FacilitiesPreferencesController      Post
    post preference    70    ${created_facility_id}     ${None}     TC 034

TC 035: Review Doctor's access to Delete Preference
    [Tags]    ReviewDoctor FacilitiesPreferencesController      Delete
    delete preference  ${preference_id}     TC 035
