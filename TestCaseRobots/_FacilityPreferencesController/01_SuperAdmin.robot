*** Settings ***
Documentation    Test Cases to validate all FacilityPreferences Controller endpoints authorization for Super Admin role

Library  ../../TestCaseMethods/FacilityPreferencesControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***

${super_admin_token}
${test_super_admin_token}
${created_facility_id}
${preference_id}
${created_preference_id}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${super_admin}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    get_version_number  ${super_admin_token}
    change selected facility  ${MMT_FAC_ID}
    ${created_facility_id}=  create test facility  API
    set suite variable  ${created_facility_id}
    change selected facility  ${created_facility_id}
    create test user  ${super_admin}
    ${preference_id}=  get random facility preference id  ${created_facility_id}    MaxDocumentsPerPatient
    set suite variable  ${preference_id}
    change selected facility  ${MMT_FAC_ID}

Multiple Teardown Methods
    delete test user    ${super_admin}
    change selected facility  ${MMT_FAC_ID}
    RUN KEYWORD IF  $created_facility_id is not None     delete test facility    ${created_facility_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Facilities Controller for Super Admin
    initialize access for facility preferences controller  ${super_admin}

Login with Super Admin
    ${test_super_admin_token}=   test user login by role     ${super_admin}    ${None}
    set suite variable  ${test_super_admin_token}
    set token facility preferences  ${test_super_admin_token}

# FACILITY PREFERENCES CONTROLLER
TC 001: SuperAdmin's access to GetPreferences
    [Tags]    SuperAdmin FacilitiesPreferencesController      GetPreferencesById
    get facility preferences tc  ${created_facility_id}    MaxDocumentsPerPatient   TC 001

TC 002: SuperAdmin's access to GetFacilityPreferences
    [Tags]    SuperAdmin FacilitiesPreferencesController      GetPreferences
    get preferences     TC 002

TC 003: SuperAdmin's access to GetById
    [Tags]    SuperAdmin FacilitiesPreferencesController      GetByID
    get preference by id    ${preference_id}    TC 003

TC 004: SuperAdmin's access to Patch Preference
    [Tags]    SuperAdmin FacilitiesPreferencesController      Patch
    patch preference    ${preference_id}    50      TC 004

TC 005: SuperAdmin's access to Put Preference
    [Tags]    SuperAdmin FacilitiesPreferencesController      Put
    put preference    ${preference_id}    50    ${created_facility_id}   1      TC 005

TC 006: SuperAdmin's access to Post Preference
    [Tags]    SuperAdmin FacilitiesPreferencesController      Post
    post preference    70    ${created_facility_id}     ${None}     TC 006

TC 007: SuperAdmin's access to Delete Preference
    [Tags]    SuperAdmin FacilitiesPreferencesController      Delete
    delete preference  ${preference_id}     TC 007
