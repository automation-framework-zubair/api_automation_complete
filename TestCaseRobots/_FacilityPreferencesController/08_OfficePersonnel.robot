*** Settings ***
Documentation    Test Cases to validate all FacilityPreferences Controller endpoints authorization for Office Personnel role

Library  ../../TestCaseMethods/FacilityPreferencesControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***

${super_admin_token}
${test_office_personnel_token}
${created_facility_id}
${preference_id}
${created_preference_id}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${office_personnel}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    change selected facility  ${MMT_FAC_ID}
    ${created_facility_id}=  create test facility  API
    set suite variable  ${created_facility_id}
    change selected facility  ${created_facility_id}
    create test user  ${office_personnel}
    ${preference_id}=  get random facility preference id  ${created_facility_id}    MaxDocumentsPerPatient
    set suite variable  ${preference_id}
    change selected facility  ${MMT_FAC_ID}

Multiple Teardown Methods
    delete test user    ${office_personnel}
    change selected facility  ${MMT_FAC_ID}
    RUN KEYWORD IF  $created_facility_id is not None     delete test facility    ${created_facility_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Facilities Controller for Office Personnel
    initialize access for facility preferences controller  ${office_personnel}

Login with Office Personnel
    ${test_office_personnel_token}=   test user login by role     ${office_personnel}    ${None}
    set suite variable  ${test_office_personnel_token}
    set token facility preferences  ${test_office_personnel_token}

# FACILITY PREFERENCES CONTROLLER
TC 050: Office Personnel's access to GetPreferencesById
    [Tags]    OfficePersonnel FacilitiesPreferencesController      GetPreferencesById
    get facility preferences tc  ${created_facility_id}    MaxDocumentsPerPatient   TC 050

TC 051: Office Personnel's access to GetPreferences
    [Tags]    OfficePersonnel FacilitiesPreferencesController      GetPreferences
    get preferences     TC 051

TC 052: Office Personnel's access to GetById
    [Tags]    OfficePersonnel FacilitiesPreferencesController      GetByID
    get preference by id    ${preference_id}        TC 052

TC 053: Office Personnel's access to Patch Preference
    [Tags]    OfficePersonnel FacilitiesPreferencesController      Patch
    patch preference    ${preference_id}    50      TC 053

TC 054: Office Personnel's access to Put Preference
    [Tags]    OfficePersonnel FacilitiesPreferencesController      Put
    put preference    ${preference_id}    50    ${created_facility_id}   1      TC 054

TC 055: Office Personnel's access to Post Preference
    [Tags]    OfficePersonnel FacilitiesPreferencesController      Post
    post preference    70    ${created_facility_id}     ${None}     TC 055

TC 056: Office Personnel's access to Delete Preference
    [Tags]    OfficePersonnel FacilitiesPreferencesController      Delete
    delete preference  ${preference_id}     TC 056
