*** Settings ***
Documentation    Test Cases to validate all FacilityPreferences Controller endpoints authorization for Field Tech role

Library  ../../TestCaseMethods/FacilityPreferencesControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***

${super_admin_token}
${test_field_tech_token}
${created_facility_id}
${preference_id}
${created_preference_id}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${field_tech}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    change selected facility  ${MMT_FAC_ID}
    ${created_facility_id}=  create test facility  API
    set suite variable  ${created_facility_id}
    change selected facility  ${created_facility_id}
    create test user  ${field_tech}
    ${preference_id}=  get random facility preference id  ${created_facility_id}    MaxDocumentsPerPatient
    set suite variable  ${preference_id}
    change selected facility  ${MMT_FAC_ID}

Multiple Teardown Methods
    delete test user    ${field_tech}
    change selected facility  ${MMT_FAC_ID}
    RUN KEYWORD IF  $created_facility_id is not None     delete test facility    ${created_facility_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Facilities Controller for FieldTech
    initialize access for facility preferences controller  ${field_tech}

Login with FieldTech
    ${test_field_tech_token}=   test user login by role     ${field_tech}    ${None}
    set suite variable  ${test_field_tech_token}
    set token facility preferences  ${test_field_tech_token}

# FACILITY PREFERENCES CONTROLLER
TC 043: Field Tech's access to GetPreferencesById
    [Tags]    FieldTech FacilitiesPreferencesController      GetPreferencesById
    get facility preferences tc  ${created_facility_id}    MaxDocumentsPerPatient       TC 043

TC 044: Field Tech's access to GetPreferences
    [Tags]    FieldTech FacilitiesPreferencesController      GetPreferences
    get preferences     TC 044

TC 045: Field Tech's access to GetById
    [Tags]    FieldTech FacilitiesPreferencesController      GetByID
    get preference by id    ${preference_id}    TC 045

TC 046: Field Tech's access to Patch Preference
    [Tags]    FieldTech FacilitiesPreferencesController      Patch
    patch preference    ${preference_id}    50      TC 046

TC 047: Field Tech's access to Put Preference
    [Tags]    FieldTech FacilitiesPreferencesController      Put
    put preference    ${preference_id}    50    ${created_facility_id}   1      TC 047

TC 048: Field Tech's access to Post Preference
    [Tags]    FieldTech FacilitiesPreferencesController      Post
    post preference    70    ${created_facility_id}     ${None}     TC 048

TC 049: Field Tech's access to Delete Preference
    [Tags]    FieldTech FacilitiesPreferencesController      Delete
    delete preference  ${preference_id}     TC 049
