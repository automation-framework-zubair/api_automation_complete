*** Settings ***
Documentation    Test Cases to validate all FacilityPreferences Controller endpoints authorization for Support role

Library  ../../TestCaseMethods/FacilityPreferencesControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***

${super_admin_token}
${test_support_token}
${created_facility_id}
${preference_id}
${created_preference_id}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${support}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    change selected facility  ${MMT_FAC_ID}
    ${created_facility_id}=  create test facility  API
    set suite variable  ${created_facility_id}
    change selected facility  ${created_facility_id}
    create test user  ${support}
    ${preference_id}=  get random facility preference id  ${created_facility_id}    MaxDocumentsPerPatient
    set suite variable  ${preference_id}
    change selected facility  ${MMT_FAC_ID}

Multiple Teardown Methods
    delete test user    ${support}
    change selected facility  ${MMT_FAC_ID}
    RUN KEYWORD IF  $created_facility_id is not None     delete test facility    ${created_facility_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Facilities Controller for Support
    initialize access for facility preferences controller  ${support}

Login with Support
    ${test_support_token}=   test user login by role     ${support}    ${None}
    set suite variable  ${test_support_token}
    set token facility preferences  ${test_support_token}

# FACILITY PREFERENCES CONTROLLER
TC 008: Suppport's access to GetPreferences
    [Tags]    Suppport FacilitiesPreferencesController      GetPreferencesById
    get facility preferences tc  ${created_facility_id}    MaxDocumentsPerPatient   TC 008

TC 009: Suppport's access to GetFacilityPreferences
    [Tags]    Suppport FacilitiesPreferencesController      GetPreferences
    get preferences     TC 009

TC 010: Suppport's access to GetById
    [Tags]    Suppport FacilitiesPreferencesController      GetByID
    get preference by id    ${preference_id}    TC 010

TC 011: Suppport's access to Patch Preference
    [Tags]    Suppport FacilitiesPreferencesController      Patch
    patch preference    ${preference_id}    50      TC 011

TC 012: Suppport's access to Put Preference
    [Tags]    Suppport FacilitiesPreferencesController      Put
    put preference    ${preference_id}    50    ${created_facility_id}   1      TC 012

TC 013: Suppport's access to Post Preference
    [Tags]    Suppport FacilitiesPreferencesController      Post
    post preference    70    ${created_facility_id}     ${None}     TC 013

TC 014: Suppport's access to Delete Preference
    [Tags]    Suppport FacilitiesPreferencesController      Delete
    delete preference  ${preference_id}     TC 014
