*** Settings ***
Documentation    Test Cases to validate all FacilityPreferences Controller endpoints authorization for Production role

Library  ../../TestCaseMethods/FacilityPreferencesControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***

${super_admin_token}
${test_production_token}
${created_facility_id}
${preference_id}
${created_preference_id}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${production}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    change selected facility  ${MMT_FAC_ID}
    ${created_facility_id}=  create test facility  API
    set suite variable  ${created_facility_id}
    change selected facility  ${created_facility_id}
    create test user  ${production}
    ${preference_id}=  get random facility preference id  ${created_facility_id}    MaxDocumentsPerPatient
    set suite variable  ${preference_id}
    change selected facility  ${MMT_FAC_ID}

Multiple Teardown Methods
    delete test user    ${production}
    change selected facility  ${MMT_FAC_ID}
    RUN KEYWORD IF  $created_facility_id is not None     delete test facility    ${created_facility_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Facilities Controller for Production
    initialize access for facility preferences controller  ${production}

Login with Production
    ${test_production_token}=   test user login by role     ${production}    ${None}
    set suite variable  ${test_production_token}
    set token facility preferences  ${test_production_token}

# FACILITY PREFERENCES CONTROLLER
TC 015: Production's access to GetPreferencesById
    [Tags]    Production FacilitiesPreferencesController      GetPreferencesById
    get facility preferences tc  ${created_facility_id}    MaxDocumentsPerPatient   TC 015

TC 016: Production's access to GetPreferences
    [Tags]    Production FacilitiesPreferencesController      GetPreferences
    get preferences     TC 016

TC 017: Production's access to GetById
    [Tags]    Production FacilitiesPreferencesController      GetByID
    get preference by id    ${preference_id}    TC 017

TC 018: Production's access to Patch Preference
    [Tags]    Production FacilitiesPreferencesController      Patch
    patch preference    ${preference_id}    50      TC 018

TC 019: Production's access to Put Preference
    [Tags]    Production FacilitiesPreferencesController      Put
    put preference    ${preference_id}    50    ${created_facility_id}   1      TC 019

TC 020: Production's access to Post Preference
    [Tags]    Production FacilitiesPreferencesController      Post
    post preference    70    ${created_facility_id}     ${None}     TC 020

TC 021: Production's access to Delete Preference
    [Tags]    Production FacilitiesPreferencesController      Delete
    delete preference  ${preference_id}     TC 021
