*** Settings ***
Documentation    Test Cases to validate all Users Controller endpoints authorization for Support role

Library  ../../TestCaseMethods/AccountControllerTCs.py
Library  ../../TestCaseMethods/UsersControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***
${super_admin_token}
${test_support_token}
${office_personnel_user_id}
${office_personnel_user_email}
${created_user_id}
${created_by_post_user_id}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${support}
    delete test user    ${office_personnel}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    create test user  ${support}
    ${office_personnel_user_id}=  create test user  ${office_personnel}
    set suite variable  ${office_personnel_user_id}
    ${office_personnel_user_email}=  get email of user id  ${office_personnel_user_id}
    set suite variable  ${office_personnel_user_email}

Multiple Teardown Methods
    delete test user    ${support}
    delete test user    ${office_personnel}
    RUN KEYWORD IF  $created_user_id is not None     delete user by id    ${created_user_id}
    RUN KEYWORD IF  $created_by_post_user_id is not None     delete user by id    ${created_by_post_user_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Users Controller for Support
    initialize access for users controller  ${support}

Login with Support
    ${test_support_token}=   test user login by role     ${support}    ${None}
    set suite variable  ${test_support_token}
    set token users  ${test_support_token}

# USERS CONTROLLER
TC 018: Support's access to CreateUser
    [Tags]    Support UserController      CreateUser
    ${created_user_id}=  create user tc  ${facility_admin}  TC 018

TC 019: Support's access to ChangeUserRole
    [Tags]    Support UserController      ChangeUserRole
    change user role  ${office_personnel_user_id}   ${office_personnel}     TC 019

TC 020: Support's access to GetMyRoles
    [Tags]    Support UserController      GetMyRoles
    get my roles    TC 020

TC 021: Support's access to GetMyTheme
    [Tags]    Support UserController      GetMyTheme
    get my theme    TC 021

TC 022: Support's access to SetMyTheme
    [Tags]    Support UserController      SetMyTheme
    set my theme  Dark  TC 022

TC 023: Support's access to SetSelectedFacility
    [Tags]    Support UserController      SetSelectedFacility
    set selected facility  ${mmt_fac_id}    TC 023

TC 024: Support's access to IsUserInRole
    [Tags]    Support UserController      IsUserInRole
    is user in role  Support    TC 024

TC 025: Support's access to GetUserInfo
    [Tags]    Support UserController      GetUserInfo
    get user info  ${office_personnel_user_id}  TC 025

TC 026: Support's access to GetUsers
    [Tags]    Support UserController      GetUsers
    get users   TC 026

TC 027: Support's access to GetUsersByIds
    [Tags]    Support UserController      GetUsers
    get users by ids  ${office_personnel_user_id}   TC 027

TC 028: Support's access to Get?id=
    [Tags]    Support UserController      Get?id=
    get user by id  ${office_personnel_user_id}     TC 028

TC 029: SuperAdmin's access to GetUserForCPTMonitoring
    [Tags]    SuperAdmin UserController      GetUserForCPTMonitoring
    get users cpt monitoring        ${office_personnel_user_id}         TC 029

TC 030: Support's access to Patch
    [Tags]    Support UserController      Patch
    patch user  ${office_personnel_user_id}     ${office_personnel_user_email}    UserFname   UserLname   ${office_personnel}   TC 030

TC 031: Support's access to Put
    [Tags]    Support UserController      Put
    put user  ${office_personnel_user_id}     ${office_personnel_user_email}    UserFname   UserLname   ${office_personnel}     TC 031

TC 032: Support's access to Post
    [Tags]    Support UserController      Post
    ${created_by_post_user_id}=  post user tc  ${review_doctor}     TC 032

TC 033: Support's access to DeleteFacilityUser
    [Tags]    Support UserController      DeleteFacilityUser
    delete facility user with id  ${office_personnel_user_id}   TC 033

TC 034: Support's access to Delete
    [Tags]    Support UserController      Delete
    delete user  ${office_personnel_user_id}    TC 034
