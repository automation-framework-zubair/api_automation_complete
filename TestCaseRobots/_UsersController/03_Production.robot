*** Settings ***
Documentation    Test Cases to validate all Users Controller endpoints authorization for Production role

Library  ../../TestCaseMethods/AccountControllerTCs.py
Library  ../../TestCaseMethods/UsersControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***
${super_admin_token}
${test_production_token}
${office_personnel_user_id}
${office_personnel_user_email}
${created_user_id}
${created_by_post_user_id}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${production}
    delete test user    ${office_personnel}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    create test user  ${production}
    ${office_personnel_user_id}=  create test user  ${office_personnel}
    set suite variable  ${office_personnel_user_id}
    ${office_personnel_user_email}=  get email of user id  ${office_personnel_user_id}
    set suite variable  ${office_personnel_user_email}

Multiple Teardown Methods
    delete test user    ${production}
    delete test user    ${office_personnel}
    RUN KEYWORD IF  $created_user_id is not None     delete user by id    ${created_user_id}
    RUN KEYWORD IF  $created_by_post_user_id is not None     delete user by id    ${created_by_post_user_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Users Controller for Production User
    initialize access for users controller  ${production}

Login with Production User
    ${test_production_token}=   test user login by role     ${production}    ${None}
    set suite variable  ${test_production_token}
    set token users  ${test_production_token}

# USERS CONTROLLER
TC 035: Production's access to CreateUser
    [Tags]    Production UserController      CreateUser
    ${created_user_id}=  create user tc  ${facility_admin}  TC 035

TC 036: Production's access to ChangeUserRole
    [Tags]    Production UserController      ChangeUserRole
    change user role  ${office_personnel_user_id}   ${office_personnel}     TC 036

TC 037: Production's access to GetMyRoles
    [Tags]    Production UserController      GetMyRoles
    get my roles    TC 037

TC 038: Production's access to GetMyTheme
    [Tags]    Production UserController      GetMyTheme
    get my theme    TC 038

TC 039: Production's access to SetMyTheme
    [Tags]    Production UserController      SetMyTheme
    set my theme  Dark  TC 039

TC 040: Production's access to SetSelectedFacility
    [Tags]    Production UserController      SetSelectedFacility
    set selected facility  ${mmt_fac_id}    TC 040

TC 041: Production's access to IsUserInRole
    [Tags]    Production UserController      IsUserInRole
    is user in role  Support    TC 041

TC 042: Production's access to GetUserInfo
    [Tags]    Production UserController      GetUserInfo
    get user info  ${office_personnel_user_id}  TC 042

TC 043: Production's access to GetUsers
    [Tags]    Production UserController      GetUsers
    get users   TC 043

TC 044: Production's access to GetUsersByIds
    [Tags]    Production UserController      GetUsers
    get users by ids  ${office_personnel_user_id}   TC 044

TC 045: Production's access to Get?id=
    [Tags]    Production UserController      Get?id=
    get user by id  ${office_personnel_user_id}     TC 045

TC 046: SuperAdmin's access to GetUserForCPTMonitoring
    [Tags]    SuperAdmin UserController      GetUserForCPTMonitoring
    get users cpt monitoring        ${office_personnel_user_id}         TC 046

TC 047: Production's access to Patch
    [Tags]    Production UserController      Patch
    patch user  ${office_personnel_user_id}     ${office_personnel_user_email}    UserFname   UserLname   ${office_personnel}   TC 047

TC 048: Production's access to Put
    [Tags]    Production UserController      Put
    put user  ${office_personnel_user_id}     ${office_personnel_user_email}    UserFname   UserLname   ${office_personnel}     TC 048

TC 049: Production's access to Post
    [Tags]    Production UserController      Post
    ${created_by_post_user_id}=  post user tc  ${review_doctor}     TC 049

TC 050: Production's access to DeleteFacilityUser
    [Tags]    Production UserController      DeleteFacilityUser
    delete facility user with id  ${office_personnel_user_id}   TC 050

TC 051: Production's access to Delete
    [Tags]    Production UserController      Delete
    delete user  ${office_personnel_user_id}    TC 051
