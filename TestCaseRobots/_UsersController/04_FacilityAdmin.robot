*** Settings ***
Documentation    Test Cases to validate all Users Controller endpoints authorization for Facility Admin role

Library  ../../TestCaseMethods/AccountControllerTCs.py
Library  ../../TestCaseMethods/UsersControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***
${super_admin_token}
${test_facility_admin_token}
${office_personnel_user_id}
${office_personnel_user_email}
${created_user_id}
${created_by_post_user_id}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${facility_admin}
    delete test user    ${office_personnel}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    create test user  ${facility_admin}
    ${office_personnel_user_id}=  create test user  ${office_personnel}
    set suite variable  ${office_personnel_user_id}
    ${office_personnel_user_email}=  get email of user id  ${office_personnel_user_id}
    set suite variable  ${office_personnel_user_email}

Multiple Teardown Methods
    delete test user    ${facility_admin}
    delete test user    ${office_personnel}
    RUN KEYWORD IF  $created_user_id is not None     delete user by id    ${created_user_id}
    RUN KEYWORD IF  $created_by_post_user_id is not None     delete user by id    ${created_by_post_user_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Users Controller for Facility Admin
    initialize access for users controller  ${facility_admin}

Login with Facility Admin
    ${test_facility_admin_token}=   test user login by role     ${facility_admin}    ${None}
    set suite variable  ${test_facility_admin_token}
    set token users  ${test_facility_admin_token}

# USERS CONTROLLER
TC 052: Facility Admin's access to CreateUser
    [Tags]    FacilityAdmin UserController      CreateUser
    ${created_user_id}=  create user tc  ${facility_admin}  TC 052

TC 053: Facility Admin's access to ChangeUserRole
    [Tags]    FacilityAdmin UserController      ChangeUserRole
    change user role  ${office_personnel_user_id}   ${office_personnel}     TC 053

TC 054: Facility Admin's access to GetMyRoles
    [Tags]    FacilityAdmin UserController      GetMyRoles
    get my roles    TC 054

TC 055: Facility Admin's access to GetMyTheme
    [Tags]    FacilityAdmin UserController      GetMyTheme
    get my theme    TC 055

TC 056: Facility Admin's access to SetMyTheme
    [Tags]    FacilityAdmin UserController      SetMyTheme
    set my theme  Dark  TC 056

TC 057: Facility Admin's access to SetSelectedFacility
    [Tags]    FacilityAdmin UserController      SetSelectedFacility
    set selected facility  ${mmt_fac_id}    TC 057

TC 058: Facility Admin's access to IsUserInRole
    [Tags]    FacilityAdmin UserController      IsUserInRole
    is user in role  Support    TC 058

TC 059: Facility Admin's access to GetUserInfo
    [Tags]    FacilityAdmin UserController      GetUserInfo
    get user info  ${office_personnel_user_id}  TC 059

TC 060: Facility Admin's access to GetUsers
    [Tags]    FacilityAdmin UserController      GetUsers
    get users   TC 060

TC 061: Facility Admin's access to GetUsersByIds
    [Tags]    FacilityAdmin UserController      GetUsers
    get users by ids  ${office_personnel_user_id}   TC 061

TC 062: Facility Admin's access to Get?id=
    [Tags]    FacilityAdmin UserController      Get?id=
    get user by id  ${office_personnel_user_id}     TC 062

TC 063: SuperAdmin's access to GetUserForCPTMonitoring
    [Tags]    SuperAdmin UserController      GetUserForCPTMonitoring
    get users cpt monitoring        ${office_personnel_user_id}         TC 063

TC 064: acility Admin can access Patch
    [Tags]    FacilityAdmin UserController      Patch
    patch user  ${office_personnel_user_id}     ${office_personnel_user_email}    UserFname   UserLname   ${office_personnel}   TC 064

TC 065: Facility Admin's access to Put
    [Tags]    FacilityAdmin UserController      Put
    put user  ${office_personnel_user_id}     ${office_personnel_user_email}    UserFname   UserLname   ${office_personnel}     TC 065

TC 066: Facility Admin's access to Post
    [Tags]    FacilityAdmin UserController      Post
    ${created_by_post_user_id}=  post user tc  ${review_doctor}     TC 066

TC 067: Facility Admin's access to DeleteFacilityUser
    [Tags]    FacilityAdmin UserController      DeleteFacilityUser
    delete facility user with id  ${office_personnel_user_id}   TC 067

TC 068: Facility Admin's access to Delete
    [Tags]    FacilityAdmin UserController      Delete
    delete user  ${office_personnel_user_id}    TC 068
