*** Settings ***
Documentation    Test Cases to validate all Users Controller endpoints authorization for Super Admin role

Library  ../../TestCaseMethods/AccountControllerTCs.py
Library  ../../TestCaseMethods/UsersControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***
${super_admin_token}
${test_super_admin_token}
${office_personnel_user_id}
${office_personnel_user_email}
${created_user_id}
${created_by_post_user_id}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${super_admin}
    delete test user    ${office_personnel}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    get_version_number  ${super_admin_token}
    create test user  ${super_admin}
    ${office_personnel_user_id}=  create test user  ${office_personnel}
    set suite variable  ${office_personnel_user_id}
    ${office_personnel_user_email}=  get email of user id  ${office_personnel_user_id}
    set suite variable  ${office_personnel_user_email}

Multiple Teardown Methods
    delete test user    ${super_admin}
    delete test user    ${office_personnel}
    RUN KEYWORD IF  $created_user_id is not None     delete user by id    ${created_user_id}
    RUN KEYWORD IF  $created_by_post_user_id is not None     delete user by id    ${created_by_post_user_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Users Controller for Super Admin
    initialize access for users controller  ${super_admin}

Login with Super Admin
    ${test_super_admin_token}=   test user login by role     ${super_admin}    ${None}
    set suite variable  ${test_super_admin_token}
    set token users  ${test_super_admin_token}

# USERS CONTROLLER
TC 001: SuperAdmin's access to CreateUser
    [Tags]    SuperAdmin UserController      CreateUser
    ${created_user_id}=  create user tc  ${super_admin}     TC 001

TC 002: SuperAdmin's access to ChangeUserRole
    [Tags]    SuperAdmin UserController      ChangeUserRole
    change user role  ${office_personnel_user_id}   ${office_personnel}     TC 002

TC 003: SuperAdmin's access to GetMyRoles
    [Tags]    SuperAdmin UserController      GetMyRoles
    get my roles    TC 003

TC 004: SuperAdmin's access to GetMyTheme
    [Tags]    SuperAdmin UserController      GetMyTheme
    get my theme    TC 004

TC 005: SuperAdmin's access to SetMyTheme
    [Tags]    SuperAdmin UserController      SetMyTheme
    set my theme  Dark  TC 005

TC 006: SuperAdmin's access to SetSelectedFacility
    [Tags]    SuperAdmin UserController      SetSelectedFacility
    set selected facility  ${mmt_fac_id}    TC 006

TC 007: SuperAdmin's access to IsUserInRole
    [Tags]    SuperAdmin UserController      IsUserInRole
    is user in role  SuperAdmin     TC 007

TC 008: SuperAdmin's access to GetUserInfo
    [Tags]    SuperAdmin UserController      GetUserInfo
    get user info  ${office_personnel_user_id}      TC 008

TC 009: SuperAdmin's access to GetUsers
    [Tags]    SuperAdmin UserController      GetUsers
    get users   TC 009

TC 010: SuperAdmin's access to GetUsersByIds
    [Tags]    SuperAdmin UserController      GetUsers
    get users by ids  ${office_personnel_user_id}   TC 010

TC 011: SuperAdmin's access to Get?id=
    [Tags]    SuperAdmin UserController      Get?id=
    get user by id  ${office_personnel_user_id}     TC 011

TC 012: SuperAdmin's access to GetUserForCPTMonitoring
    [Tags]    SuperAdmin UserController      GetUserForCPTMonitoring
    get users cpt monitoring        ${office_personnel_user_id}         TC 012

TC 013: SuperAdmin's access to Patch
    [Tags]    SuperAdmin UserController      Patch
    patch user  ${office_personnel_user_id}     ${office_personnel_user_email}    UserFname   UserLname   ${office_personnel}   TC 013

TC 014: SuperAdmin's access to Put
    [Tags]    SuperAdmin UserController      Put
    put user  ${office_personnel_user_id}     ${office_personnel_user_email}    UserFname   UserLname   ${office_personnel}     TC 014

TC 015: SuperAdmin's access to Post
    [Tags]    SuperAdmin UserController      Post
    ${created_by_post_user_id}=  post user tc  ${review_doctor}     TC 015

TC 016: SuperAdmin's access to DeleteFacilityUser
    [Tags]    SuperAdmin UserController      DeleteFacilityUser
    delete facility user with id  ${office_personnel_user_id}   TC 016

TC 017: SuperAdmin's access to Delete
    [Tags]    SuperAdmin UserController      Delete
    delete user  ${office_personnel_user_id}    TC 017
