*** Settings ***
Documentation    Test Cases to validate all Users Controller endpoints authorization for Lead Tech role

Library  ../../TestCaseMethods/AccountControllerTCs.py
Library  ../../TestCaseMethods/UsersControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***
${super_admin_token}
${test_lead_tech_token}
${office_personnel_user_id}
${office_personnel_user_email}
${created_user_id}
${created_by_post_user_id}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${lead_tech}
    delete test user    ${office_personnel}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    create test user  ${lead_tech}
    ${office_personnel_user_id}=  create test user  ${office_personnel}
    set suite variable  ${office_personnel_user_id}
    ${office_personnel_user_email}=  get email of user id  ${office_personnel_user_id}
    set suite variable  ${office_personnel_user_email}

Multiple Teardown Methods
    delete test user    ${lead_tech}
    delete test user    ${office_personnel}
    RUN KEYWORD IF  $created_user_id is not None     delete user by id    ${created_user_id}
    RUN KEYWORD IF  $created_by_post_user_id is not None     delete user by id    ${created_by_post_user_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Users Controller for Lead Tech
    initialize access for users controller  ${lead_tech}

Login with Lead Tech
    ${test_lead_tech_token}=   test user login by role     ${lead_tech}    ${None}
    set suite variable  ${test_lead_tech_token}
    set token users  ${test_lead_tech_token}

# USERS CONTROLLER
TC 086: Lead Tech's access to CreateUser
    [Tags]    LeadTech UserController      CreateUser
    ${created_user_id}=  create user tc  ${facility_admin}  TC 086

TC 087: Lead Tech's access to ChangeUserRole
    [Tags]    LeadTech UserController      ChangeUserRole
    change user role  ${office_personnel_user_id}   ${office_personnel}     TC 087

TC 088: Lead Tech's access to GetMyRoles
    [Tags]    LeadTech UserController      GetMyRoles
    get my roles    TC 088

TC 089: Lead Tech's access to GetMyTheme
    [Tags]    LeadTech UserController      GetMyTheme
    get my theme    TC 089

TC 090: Lead Tech's access to SetMyTheme
    [Tags]    LeadTech UserController      SetMyTheme
    set my theme  Dark  TC 090

TC 091: Lead Tech's access to SetSelectedFacility
    [Tags]    LeadTech UserController      SetSelectedFacility
    set selected facility  ${mmt_fac_id}    TC 091

TC 092: Lead Tech's access to IsUserInRole
    [Tags]    LeadTech UserController      IsUserInRole
    is user in role  Support    TC 092

TC 093: Lead Tech's access to GetUserInfo
    [Tags]    LeadTech UserController      GetUserInfo
    get user info  ${office_personnel_user_id}  TC 093

TC 094: Lead Tech's access to GetUsers
    [Tags]    LeadTech UserController      GetUsers
    get users   TC 094

TC 095: Lead Tech's access to GetUsersByIds
    [Tags]    LeadTech UserController      GetUsers
    get users by ids  ${office_personnel_user_id}   TC 095

TC 096: Lead Tech's access to Get?id=
    [Tags]    LeadTech UserController      Get?id=
    get user by id  ${office_personnel_user_id}     TC 096

TC 097: SuperAdmin's access to GetUserForCPTMonitoring
    [Tags]    SuperAdmin UserController      GetUserForCPTMonitoring
    get users cpt monitoring        ${office_personnel_user_id}         TC 097

TC 098: Lead Tech's access to Patch
    [Tags]    LeadTech UserController      Patch
    patch user  ${office_personnel_user_id}     ${office_personnel_user_email}    UserFname   UserLname   ${office_personnel}   TC 098

TC 099: Lead Tech's access to Put
    [Tags]    LeadTech UserController      Put
    put user  ${office_personnel_user_id}     ${office_personnel_user_email}    UserFname   UserLname   ${office_personnel}     TC 099

TC 100: Lead Tech's access to Post
    [Tags]    LeadTech UserController      Post
    ${created_by_post_user_id}=  post user tc  ${review_doctor}     TC 100

TC 101: Lead Tech's access to DeleteFacilityUser
    [Tags]    LeadTech UserController      DeleteFacilityUser
    delete facility user with id  ${office_personnel_user_id}   TC 101

TC 102: Lead Tech's access to Delete
    [Tags]    LeadTech UserController      Delete
    delete user  ${office_personnel_user_id}    TC 102
