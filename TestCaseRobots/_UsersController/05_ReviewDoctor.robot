*** Settings ***
Documentation    Test Cases to validate all Users Controller endpoints authorization for Review Doctor role

Library  ../../TestCaseMethods/AccountControllerTCs.py
Library  ../../TestCaseMethods/UsersControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***
${super_admin_token}
${test_review_doctor_token}
${office_personnel_user_id}
${office_personnel_user_email}
${created_user_id}
${created_by_post_user_id}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${review_doctor}
    delete test user    ${office_personnel}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    create test user  ${review_doctor}
    ${office_personnel_user_id}=  create test user  ${office_personnel}
    set suite variable  ${office_personnel_user_id}
    ${office_personnel_user_email}=  get email of user id  ${office_personnel_user_id}
    set suite variable  ${office_personnel_user_email}

Multiple Teardown Methods
    delete test user    ${review_doctor}
    delete test user    ${office_personnel}
    RUN KEYWORD IF  $created_user_id is not None     delete user by id    ${created_user_id}
    RUN KEYWORD IF  $created_by_post_user_id is not None     delete user by id    ${created_by_post_user_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Users Controller for Review Doctor
    initialize access for users controller  ${review_doctor}

Login with Review Doctor
    ${test_review_doctor_token}=   test user login by role     ${review_doctor}    ${None}
    set suite variable  ${test_review_doctor_token}
    set token users  ${test_review_doctor_token}

# USERS CONTROLLER
TC 069: Review Doctor's access to CreateUser
    [Tags]    ReviewDoctor UserController      CreateUser
    ${created_user_id}=  create user tc  ${facility_admin}  TC 069

TC 070: Review Doctor's access to ChangeUserRole
    [Tags]    ReviewDoctor UserController      ChangeUserRole
    change user role  ${office_personnel_user_id}   ${office_personnel}     TC 070

TC 071: Review Doctor's access to GetMyRoles
    [Tags]    ReviewDoctor UserController      GetMyRoles
    get my roles    TC 071

TC 072: Review Doctor's access to GetMyTheme
    [Tags]    ReviewDoctor UserController      GetMyTheme
    get my theme    TC 072

TC 073: Review Doctor's access to SetMyTheme
    [Tags]    ReviewDoctor UserController      SetMyTheme
    set my theme  Dark  TC 073

TC 074: Review Doctor's access to SetSelectedFacility
    [Tags]    ReviewDoctor UserController      SetSelectedFacility
    set selected facility  ${mmt_fac_id}    TC 074

TC 075: Review Doctor's access to IsUserInRole
    [Tags]    ReviewDoctor UserController      IsUserInRole
    is user in role  Support    TC 075

TC 076: Review Doctor's access to GetUserInfo
    [Tags]    ReviewDoctor UserController      GetUserInfo
    get user info  ${office_personnel_user_id}  TC 076

TC 077: Review Doctor's access to GetUsers
    [Tags]    ReviewDoctor UserController      GetUsers
    get users   TC 077

TC 078: Review Doctor's access to GetUsersByIds
    [Tags]    ReviewDoctor UserController      GetUsers
    get users by ids  ${office_personnel_user_id}   TC 078

TC 079: Review Doctor's access to Get?id=
    [Tags]    ReviewDoctor UserController      Get?id=
    get user by id  ${office_personnel_user_id}     TC 079

TC 080: SuperAdmin's access to GetUserForCPTMonitoring
    [Tags]    SuperAdmin UserController      GetUserForCPTMonitoring
    get users cpt monitoring        ${office_personnel_user_id}         TC 080

TC 081: Review Doctor's access to Patch
    [Tags]    ReviewDoctor UserController      Patch
    patch user  ${office_personnel_user_id}     ${office_personnel_user_email}    UserFname   UserLname   ${office_personnel}   TC 081

TC 082: Review Doctor's access to Put
    [Tags]    ReviewDoctor UserController      Put
    put user  ${office_personnel_user_id}     ${office_personnel_user_email}    UserFname   UserLname   ${office_personnel}     TC 082

TC 083: Review Doctor's access to Post
    [Tags]    ReviewDoctor UserController      Post
    ${created_by_post_user_id}=  post user tc  ${review_doctor}     TC 083

TC 084: Review Doctor's access to DeleteFacilityUser
    [Tags]    ReviewDoctor UserController      DeleteFacilityUser
    delete facility user with id  ${office_personnel_user_id}   TC 084

TC 085: Review Doctor's access to Delete
    [Tags]    ReviewDoctor UserController      Delete
    delete user  ${office_personnel_user_id}    TC 085
