*** Settings ***
Documentation    Test Cases to validate all Users Controller endpoints authorization for Office Personnel role

Library  ../../TestCaseMethods/AccountControllerTCs.py
Library  ../../TestCaseMethods/UsersControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***
${super_admin_token}
${test_office_personnel_token}
${review_doctor_user_id}
${review_doctor_user_email}
${created_user_id}
${created_by_post_user_id}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${office_personnel}
    delete test user    ${review_doctor}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    create test user  ${office_personnel}
    ${review_doctor_user_id}=  create test user  ${review_doctor}
    set suite variable  ${review_doctor_user_id}
    ${review_doctor_user_email}=  get email of user id  ${review_doctor_user_id}
    set suite variable  ${review_doctor_user_email}

Multiple Teardown Methods
    delete test user    ${office_personnel}
    delete test user    ${review_doctor}
    RUN KEYWORD IF  $created_user_id is not None     delete user by id    ${created_user_id}
    RUN KEYWORD IF  $created_by_post_user_id is not None     delete user by id    ${created_by_post_user_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Users Controller for Office Personnel
    initialize access for users controller  ${office_personnel}

Login with Office Personnel
    ${test_office_personnel_token}=   test user login by role     ${office_personnel}    ${None}
    set suite variable  ${test_office_personnel_token}
    set token users  ${test_office_personnel_token}

# USERS CONTROLLER
TC 120: Office Personnel's access to CreateUser
    [Tags]    OfficePersonnel UserController      CreateUser
    ${created_user_id}=  create user tc  ${facility_admin}  TC 120

TC 121: Office Personnel's access to ChangeUserRole
    [Tags]    OfficePersonnel UserController      ChangeUserRole
    change user role  ${review_doctor_user_id}   ${review_doctor}   TC 121

TC 122: Office Personnel's access to GetMyRoles
    [Tags]    OfficePersonnel UserController      GetMyRoles
    get my roles    TC 122

TC 123: Office Personnel's access to GetMyTheme
    [Tags]    OfficePersonnel UserController      GetMyTheme
    get my theme    TC 123

TC 124: Office Personnel's access to SetMyTheme
    [Tags]    OfficePersonnel UserController      SetMyTheme
    set my theme  Dark  TC 124

TC 125: Office Personnel's access to SetSelectedFacility
    [Tags]    OfficePersonnel UserController      SetSelectedFacility
    set selected facility  ${mmt_fac_id}    TC 125

TC 126: Office Personnel's access to IsUserInRole
    [Tags]    OfficePersonnel UserController      IsUserInRole
    is user in role  Support    TC 126

TC 127: Office Personnel's access to GetUserInfo
    [Tags]    OfficePersonnel UserController      GetUserInfo
    get user info  ${review_doctor_user_id}     TC 127

TC 128: Office Personnel's access to GetUsers
    [Tags]    OfficePersonnel UserController      GetUsers
    get users   TC 128

TC 129: Office Personnel's access to GetUsersByIds
    [Tags]    OfficePersonnel UserController      GetUsers
    get users by ids  ${review_doctor_user_id}  TC 129

TC 130: Office Personnel's access to Get?id=
    [Tags]    OfficePersonnel UserController      Get?id=
    get user by id  ${review_doctor_user_id}    TC 130

TC 131: SuperAdmin's access to GetUserForCPTMonitoring
    [Tags]    SuperAdmin UserController      GetUserForCPTMonitoring
    get users cpt monitoring        ${review_doctor_user_id}         TC 131

TC 132: Office Personnel's access to Patch
    [Tags]    OfficePersonnel UserController      Patch
    patch user  ${review_doctor_user_id}     ${review_doctor_user_email}    UserFname   UserLname   ${review_doctor}    TC 132

TC 133: Office Personnel's access to Put
    [Tags]    OfficePersonnel UserController      Put
    put user  ${review_doctor_user_id}     ${review_doctor_user_email}    UserFname   UserLname   ${review_doctor}      TC 133

TC 134: Office Personnel's access to Post
    [Tags]    OfficePersonnel UserController      Post
    ${created_by_post_user_id}=  post user tc  ${review_doctor}     TC 134

TC 135: Office Personnel's access to DeleteFacilityUser
    [Tags]    OfficePersonnel UserController      DeleteFacilityUser
    delete facility user with id  ${review_doctor_user_id}  TC 135

TC 136: Office Personnel's access to Delete
    [Tags]    OfficePersonnel UserController      Delete
    delete user  ${review_doctor_user_id}   TC 136
