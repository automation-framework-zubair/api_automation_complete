*** Settings ***
Documentation    Test Cases to validate all Users Controller endpoints authorization for Field Tech role

Library  ../../TestCaseMethods/AccountControllerTCs.py
Library  ../../TestCaseMethods/UsersControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***
${super_admin_token}
${test_field_tech_token}
${office_personnel_user_id}
${office_personnel_user_email}
${created_user_id}
${created_by_post_user_id}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${field_tech}
    delete test user    ${office_personnel}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    create test user  ${field_tech}
    ${office_personnel_user_id}=  create test user  ${office_personnel}
    set suite variable  ${office_personnel_user_id}
    ${office_personnel_user_email}=  get email of user id  ${office_personnel_user_id}
    set suite variable  ${office_personnel_user_email}

Multiple Teardown Methods
    delete test user    ${field_tech}
    delete test user    ${office_personnel}
    RUN KEYWORD IF  $created_user_id is not None     delete user by id    ${created_user_id}
    RUN KEYWORD IF  $created_by_post_user_id is not None     delete user by id    ${created_by_post_user_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Users Controller for Field Tech
    initialize access for users controller  ${field_tech}

Login with Field Tech
    ${test_field_tech_token}=   test user login by role     ${field_tech}    ${None}
    set suite variable  ${test_field_tech_token}
    set token users  ${test_field_tech_token}

# USERS CONTROLLER
TC 103: Field Tech's access to CreateUser
    [Tags]    FieldTech UserController      CreateUser
    ${created_user_id}=  create user tc  ${facility_admin}  TC 103

TC 104: Field Tech's access to ChangeUserRole
    [Tags]    FieldTech UserController      ChangeUserRole
    change user role  ${office_personnel_user_id}   ${office_personnel}     TC 104

TC 105: Field Tech's access to GetMyRoles
    [Tags]    FieldTech UserController      GetMyRoles
    get my roles    TC 105

TC 106: Field Tech's access to GetMyTheme
    [Tags]    FieldTech UserController      GetMyTheme
    get my theme    TC 106

TC 107: Field Tech's access to SetMyTheme
    [Tags]    FieldTech UserController      SetMyTheme
    set my theme  Dark  TC 107

TC 108: Field Tech's access to SetSelectedFacility
    [Tags]    FieldTech UserController      SetSelectedFacility
    set selected facility  ${mmt_fac_id}    TC 108

TC 109: Field Tech's access to IsUserInRole
    [Tags]    FieldTech UserController      IsUserInRole
    is user in role  Support    TC 109

TC 110: Field Tech's access to GetUserInfo
    [Tags]    FieldTech UserController      GetUserInfo
    get user info  ${office_personnel_user_id}  TC 110

TC 111: Field Tech's access to GetUsers
    [Tags]    FieldTech UserController      GetUsers
    get users   TC 111

TC 112: Field Tech's access to GetUsersByIds
    [Tags]    FieldTech UserController      GetUsers
    get users by ids  ${office_personnel_user_id}   TC 112

TC 113: Field Tech's access to Get?id=
    [Tags]    FieldTech UserController      Get?id=
    get user by id  ${office_personnel_user_id}     TC 113

TC 114: SuperAdmin's access to GetUserForCPTMonitoring
    [Tags]    SuperAdmin UserController      GetUserForCPTMonitoring
    get users cpt monitoring        ${office_personnel_user_id}         TC 114

TC 115: Field Tech's access to Patch
    [Tags]    FieldTech UserController      Patch
    patch user  ${office_personnel_user_id}     ${office_personnel_user_email}    UserFname   UserLname   ${office_personnel}   TC 115

TC 116: Field Tech's access to Put
    [Tags]    FieldTech UserController      Put
    put user  ${office_personnel_user_id}     ${office_personnel_user_email}    UserFname   UserLname   ${office_personnel}     TC 116

TC 117: Field Tech's access to Post
    [Tags]    FieldTech UserController      Post
    ${created_by_post_user_id}=  post user tc  ${review_doctor}     TC 117

TC 118: Field Tech's access to DeleteFacilityUser
    [Tags]    FieldTech UserController      DeleteFacilityUser
    delete facility user with id  ${office_personnel_user_id}   TC 118

TC 119: Field Tech's access to Delete
    [Tags]    FieldTech UserController      Delete
    delete user  ${office_personnel_user_id}    TC 119
