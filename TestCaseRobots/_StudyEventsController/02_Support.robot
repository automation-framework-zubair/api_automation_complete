*** Settings ***
Documentation    Test Cases to validate all Study Events Controller endpoints authorization for Support role

Library     ../../TestCaseMethods/StudyEventsControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py
Library  ../../TestCaseMethods/_CommonStudiesTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***
${super_admin_token}
${test_support_token}
${test_study_id}
${test_study_event_id}
${study_event_id}
${test_recording_index}
${test_packet_index}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${support}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    set common studies token  ${super_admin_token}
    change selected facility   ${mmt_fac_id}
    create test user  ${support}
    ${test_study_id}=  get random test study id
    set suite variable  ${test_study_id}
    ${test_packet_index}=   get packet index
    set suite variable    ${test_packet_index}
    ${test_recording_index}=   get recording index
    set suite variable      ${test_recording_index}
    ${test_study_event_id}=   create study event    ${test_study_id}    ${test_recording_index}    ${test_packet_index}
    set suite variable   ${test_study_event_id}
    Log     ${test_study_event_id}
    Log   ${test_study_id}


Multiple Teardown Methods
    delete test user    ${support}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Study Events Controller for Support
    initialize access for study events controller  ${support}

Login with Support
    ${test_support_token}=  test user login by role  ${support}  ${None}
    set suite variable  ${test_support_token}
    set token study events  ${test_support_token}

# STUDY EVENTS CONTROLLER
TC 010: Support's access to Add Study Events
    [Tags]    Support StudyEventsController        AddStudyEvent
    ${study_event_id}=  post add study events tc  ${test_study_id}    ${test_recording_index}  ${test_packet_index}                TC 010
    set suite variable   ${study_event_id}

TC 011: Support's access to Get Study Events by Study ID
    [Tags]    Support StudyEventsController        GetStudyEventsByStudyID
    get study events by study id    ${test_study_id}            TC 011

TC 012: Support's access to Get Study Events by Event ID
    [Tags]    Support StudyEventsController        Get?id=
    get study events by event id    ${test_study_event_id}          TC 012

TC 013: Support's access to Get Study Events
    [Tags]    Support StudyEventsController        Get
    get study events                TC 013

TC 014: Support's access to Patch Study Event
    [Tags]    Support StudyEventsController        Patch
    patch study event    ${test_study_event_id}         TC 014

TC 015: Support's access to Put Study Event
    [Tags]    Support StudyEventsController        Put
    put study event   ${test_study_event_id}            TC 015

TC 016: Support's access to Post Study Event
    [Tags]    Support StudyEventsController        Post
    post study event                TC 016

TC 017: Support's access to Set Important Study Events
     [Tags]    Support StudyEventsController        SetImportantStudyEvents
     set important study events   ${test_study_event_id}            TC 017

TC 018: Support's access to Delete Study Event by ID
    [Tags]    Support StudyEventsController        Delete
    delete study event   ${test_study_event_id}         TC 018