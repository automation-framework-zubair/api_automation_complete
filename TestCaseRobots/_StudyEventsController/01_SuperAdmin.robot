*** Settings ***
Documentation    Test Cases to validate all Study Events Controller endpoints authorization for Super Admin role

Library     ../../TestCaseMethods/StudyEventsControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py
Library  ../../TestCaseMethods/_CommonStudiesTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***
${super_admin_token}
${test_super_admin_token}
${test_study_id}
${test_study_event_id}
${study_event_id}
${test_recording_index}
${test_packet_index}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${super_admin}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    get_version_number  ${super_admin_token}
    set common studies token  ${super_admin_token}
    change selected facility   ${mmt_fac_id}
    create test user  ${super_admin}
    ${test_study_id}=  get random test study id
    set suite variable  ${test_study_id}
    ${test_packet_index}=   get packet index
    set suite variable    ${test_packet_index}
    ${test_recording_index}=   get recording index
    set suite variable      ${test_recording_index}
    ${test_study_event_id}=   create study event    ${test_study_id}    ${test_recording_index}    ${test_packet_index}
    set suite variable   ${test_study_event_id}
    Log     ${test_study_event_id}
    Log   ${test_study_id}

Multiple Teardown Methods
    delete test user    ${super_admin}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Study Events Controller for Super Admin
    initialize access for study events controller  ${super_admin}

Login with SuperAdmin
    ${test_super_admin_token}=  test user login by role  ${super_admin}  ${None}
    set suite variable  ${test_super_admin_token}
    set token study events  ${test_super_admin_token}

# STUDY EVENTS CONTROLLER
TC 001: SuperAdmin's access to Add Study Events
    [Tags]    SuperAdmin StudyEventsController        AddStudyEvent
    ${study_event_id}=  post add study events tc  ${test_study_id}    ${test_recording_index}  ${test_packet_index}                TC 001
    set suite variable   ${study_event_id}

TC 002: SuperAdmin's access to Get Study Events by Study ID
    [Tags]    SuperAdmin StudyEventsController        GetStudyEventsByStudyID
    get study events by study id    ${test_study_id}            TC 002

TC 003: SuperAdmin's access to Get Study Events by Event ID
    [Tags]    SuperAdmin StudyEventsController        Get?id=
    get study events by event id    ${test_study_event_id}              TC 003

TC 004: SuperAdmin's access to Get Study Events
    [Tags]    SuperAdmin StudyEventsController        Get
    get study events                TC 004

TC 005: SuperAdmin's access to Patch Study Event
    [Tags]    SuperAdmin StudyEventsController        Patch
    patch study event    ${test_study_event_id}             TC 005

TC 006: SuperAdmin's access to Put Study Event
    [Tags]    SuperAdmin StudyEventsController        Put
    put study event   ${test_study_event_id}                TC 006

TC 007: SuperAdmin's access to Post Study Event
    [Tags]    SuperAdmin StudyEventsController        Post
    post study event            TC 007

TC 008: SuperAdmin's access to Set Important Study Events
     [Tags]    SuperAdmin StudyEventsController        SetImportantStudyEvents
     set important study events   ${test_study_event_id}            TC 008

TC 009: SuperAdmin's access to Delete Study Event by ID
    [Tags]    SuperAdmin StudyEventsController        Delete
    delete study event   ${test_study_event_id}             TC 009