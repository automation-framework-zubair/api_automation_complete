*** Settings ***
Documentation    Test Cases to validate all Study Events Controller endpoints authorization for Facility Admin role

Library     ../../TestCaseMethods/StudyEventsControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py
Library  ../../TestCaseMethods/_CommonStudiesTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***
${super_admin_token}
${test_facility_admin_token}
${test_study_id}
${test_study_event_id}
${study_event_id}
${test_recording_index}
${test_packet_index}


*** Keywords ***
Multiple Setup Methods
    delete test user    ${facility_admin}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    set common studies token  ${super_admin_token}
    change selected facility   ${mmt_fac_id}
    create test user  ${facility_admin}
    ${test_study_id}=  get random test study id
    set suite variable  ${test_study_id}
    ${test_packet_index}=   get packet index
    set suite variable    ${test_packet_index}
    ${test_recording_index}=   get recording index
    set suite variable      ${test_recording_index}
    ${test_study_event_id}=   create study event    ${test_study_id}    ${test_recording_index}    ${test_packet_index}
    set suite variable   ${test_study_event_id}
    Log     ${test_study_event_id}
    Log   ${test_study_id}

Multiple Teardown Methods
    delete test user    ${facility_admin}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Study Events Controller for FacilityAdmin
    initialize access for study events controller  ${facility_admin}

Login with FacilityAdmin
    ${test_facility_admin_token}=  test user login by role  ${facility_admin}  ${None}
    set suite variable  ${test_facility_admin_token}
    set token study events  ${test_facility_admin_token}

# STUDY EVENTS CONTROLLER
TC 028: Facility Admin's access to Add Study Events
    [Tags]    FacilityAdmin StudyEventsController        AddStudyEvent
    ${study_event_id}=  post add study events tc  ${test_study_id}    ${test_recording_index}  ${test_packet_index}        TC 028
    set suite variable   ${study_event_id}

TC 029: Facility Admin's access to Get Study Events by Study ID
    [Tags]    FacilityAdmin StudyEventsController        GetStudyEventsByStudyID
    get study events by study id    ${test_study_id}            TC 029

TC 030: Facility Admin's access to Get Study Events by Event ID
    [Tags]    FacilityAdmin StudyEventsController        Get?id=
    get study events by event id    ${test_study_event_id}          TC 030

TC 031: Facility Admin's access to Get Study Events
    [Tags]    FacilityAdmin StudyEventsController        Get
    get study events        TC 031

TC 032: Facility Admin's access to Patch Study Event
    [Tags]    FacilityAdmin StudyEventsController        Patch
    patch study event    ${test_study_event_id}     TC 032

TC 033: Facility Admin's access to Put Study Event
    [Tags]    FacilityAdmin StudyEventsController        Put
    put study event   ${test_study_event_id}            TC 033

TC 034: Facility Admin's access to Post Study Event
    [Tags]    FacilityAdmin StudyEventsController        Post
    post study event        TC 034

TC 035: Facility Admin's access to Set Important Study Events
     [Tags]    FacilityAdmin StudyEventsController        SetImportantStudyEvents
     set important study events   ${test_study_event_id}        TC 035

TC 036: Facility Admin's access to Delete Study Event by ID
    [Tags]    FacilityAdmin StudyEventsController        Delete
    delete study event   ${test_study_event_id}         TC 036