*** Settings ***
Documentation    Test Cases to validate all Study Events Controller endpoints authorization for Office Personnel role

Library     ../../TestCaseMethods/StudyEventsControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py
Library  ../../TestCaseMethods/_CommonStudiesTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***
${super_admin_token}
${test_office_personnel_token}
${test_study_id}
${test_study_event_id}
${study_event_id}
${test_recording_index}
${test_packet_index}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${office_personnel}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    set common studies token  ${super_admin_token}
    change selected facility   ${mmt_fac_id}
    create test user  ${office_personnel}
    ${test_study_id}=  get random test study id
    set suite variable  ${test_study_id}
    ${test_packet_index}=   get packet index
    set suite variable    ${test_packet_index}
    ${test_recording_index}=   get recording index
    set suite variable      ${test_recording_index}
    ${test_study_event_id}=   create study event    ${test_study_id}    ${test_recording_index}    ${test_packet_index}
    set suite variable   ${test_study_event_id}
    Log     ${test_study_event_id}
    Log   ${test_study_id}

Multiple Teardown Methods
    delete test user    ${office_personnel}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Study Events Controller for Office Personnel
    initialize access for study events controller  ${office_personnel}

Login with Office Personnel
    ${test_office_personnel_token}=  test user login by role  ${office_personnel}  ${None}
    set suite variable  ${test_office_personnel_token}
    set token study events  ${test_office_personnel_token}

# STUDY EVENTS CONTROLLER
TC 064: Office Personnel's access to Add Study Events
    [Tags]    OfficePersonnel StudyEventsController        AddStudyEvent
    ${study_event_id}=  post add study events tc  ${test_study_id}    ${test_recording_index}  ${test_packet_index}            TC 064
    set suite variable   ${study_event_id}

TC 065: Office Personnel's access to Get Study Events by Study ID
    [Tags]    OfficePersonnel StudyEventsController        GetStudyEventsByStudyID
    get study events by study id    ${test_study_id}            TC 065

TC 066: Office Personnel's access to Get Study Events by Event ID
    [Tags]    OfficePersonnel StudyEventsController        Get?id=
    get study events by event id    ${test_study_event_id}            TC 066

TC 067: Office Personnel's access to Get Study Events
    [Tags]    OfficePersonnel StudyEventsController        Get
    get study events                TC 067

TC 068: Office Personnel's access to Patch Study Event
    [Tags]    OfficePersonnel StudyEventsController        Patch
    patch study event    ${test_study_event_id}               TC 068

TC 069: Office Personnel's access to Put Study Event
    [Tags]    OfficePersonnel StudyEventsController        Put
    put study event   ${test_study_event_id}          TC 069

TC 070: Office Personnel's access to Post Study Event
    [Tags]    OfficePersonnel StudyEventsController        Post
    post study event            TC 070

TC 071: Office Personnel's access to Set Important Study Events
     [Tags]    OfficePersonnel StudyEventsController        SetImportantStudyEvents
     set important study events   ${test_study_event_id}              TC 071

TC 072: Office Personnel's access to Delete Study Event by ID
    [Tags]    OfficePersonnel StudyEventsController        Delete
    delete study event   ${test_study_event_id}           TC 072