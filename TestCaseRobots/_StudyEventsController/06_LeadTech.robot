*** Settings ***
Documentation    Test Cases to validate all Study Events Controller endpoints authorization for Lead Tech role

Library     ../../TestCaseMethods/StudyEventsControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py
Library  ../../TestCaseMethods/_CommonStudiesTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***
${super_admin_token}
${test_lead_tech_token}
${test_study_id}
${test_study_event_id}
${study_event_id}
${test_recording_index}
${test_packet_index}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${lead_tech}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    set common studies token  ${super_admin_token}
    change selected facility   ${mmt_fac_id}
    create test user  ${lead_tech}
    ${test_study_id}=  get random test study id
    set suite variable  ${test_study_id}
    ${test_packet_index}=   get packet index
    set suite variable    ${test_packet_index}
    ${test_recording_index}=   get recording index
    set suite variable      ${test_recording_index}
    ${test_study_event_id}=   create study event    ${test_study_id}    ${test_recording_index}    ${test_packet_index}
    set suite variable   ${test_study_event_id}
    Log     ${test_study_event_id}
    Log   ${test_study_id}

Multiple Teardown Methods
    delete test user    ${lead_tech}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Study Events Controller for Lead Tech
    initialize access for study events controller  ${lead_tech}

Login with Lead Tech
    ${test_lead_tech_token}=  test user login by role  ${lead_tech}  ${None}
    set suite variable  ${test_lead_tech_token}
    set token study events  ${test_lead_tech_token}

# STUDY EVENTS CONTROLLER
TC 046: Lead Tech's access to Add Study Events
    [Tags]    LeadTech StudyEventsController        AddStudyEvent
    ${study_event_id}=  post add study events tc  ${test_study_id}    ${test_recording_index}  ${test_packet_index}            TC 046
    set suite variable   ${study_event_id}

TC 047: Lead Tech's access to Get Study Events by Study ID
    [Tags]    LeadTech StudyEventsController        GetStudyEventsByStudyID
    get study events by study id    ${test_study_id}            TC 047

TC 048: Lead Tech's access to Get Study Events by Event ID
    [Tags]    LeadTech StudyEventsController        Get?id=
    get study events by event id    ${test_study_event_id}              TC 048

TC 049: Lead Tech's access to Get Study Events
    [Tags]    LeadTech StudyEventsController        Get
    get study events                TC 049

TC 050: Lead Tech's access to Patch Study Event
    [Tags]    LeadTech StudyEventsController        Patch
    patch study event    ${test_study_event_id}         TC 050

TC 051: Lead Tech's access to Put Study Event
    [Tags]    LeadTech StudyEventsController        Put
    put study event   ${test_study_event_id}            TC 051

TC 052: Lead Tech's access to Post Study Event
    [Tags]    LeadTech StudyEventsController        Post
    post study event            TC 052

TC 053: Lead Tech's access to Set Important Study Events
     [Tags]    LeadTech StudyEventsController        SetImportantStudyEvents
     set important study events   ${test_study_event_id}            TC 053

TC 054: Lead Tech's access to Delete Study Event by ID
    [Tags]    LeadTech StudyEventsController        Delete
    delete study event   ${test_study_event_id}             TC 054