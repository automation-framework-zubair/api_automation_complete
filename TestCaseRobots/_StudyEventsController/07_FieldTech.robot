*** Settings ***
Documentation    Test Cases to validate all Study Events Controller endpoints authorization for Field Tech role

Library     ../../TestCaseMethods/StudyEventsControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py
Library  ../../TestCaseMethods/_CommonStudiesTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***
${super_admin_token}
${test_field_tech_token}
${test_user_id}
${test_study_id}
${test_study_event_id}
${study_event_id}
${test_recording_index}
${test_packet_index}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${field_tech}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    set common studies token  ${super_admin_token}
    change selected facility   ${mmt_fac_id}
    ${test_user_id}=  create test user  ${field_tech}
    set suite variable   ${test_user_id}
    ${test_study_id}=  get random test study id
    set suite variable  ${test_study_id}
    share test study   ${test_study_id}  ${test_user_id}
    ${test_packet_index}=   get packet index
    set suite variable    ${test_packet_index}
    ${test_recording_index}=   get recording index
    set suite variable      ${test_recording_index}
    ${test_study_event_id}=   create study event    ${test_study_id}    ${test_recording_index}    ${test_packet_index}
    set suite variable   ${test_study_event_id}
    Log     ${test_study_event_id}
    Log   ${test_study_id}

Multiple Teardown Methods
    delete test user    ${field_tech}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Study Events Controller for Field Tech
    initialize access for study events controller  ${field_tech}

Login with Field Tech
    ${test_field_tech_token}=  test user login by role  ${field_tech}  ${None}
    set suite variable  ${test_field_tech_token}
    set token study events  ${test_field_tech_token}

# STUDY EVENTS CONTROLLER
TC 055: Field Tech's access to Add Study Events
    [Tags]    FieldTech StudyEventsController        AddStudyEvent
    ${study_event_id}=  post add study events tc  ${test_study_id}    ${test_recording_index}  ${test_packet_index}        TC 055
    set suite variable   ${study_event_id}

TC 056: Field Tech's access to Get Study Events by Study ID
    [Tags]    FieldTech StudyEventsController        GetStudyEventsByStudyID
    get study events by study id    ${test_study_id}            TC 056

TC 057: Field Tech's access to Get Study Events by Event ID
    [Tags]    FieldTech StudyEventsController        Get?id=
    get study events by event id    ${test_study_event_id}          TC 057

TC 058: Field Tech's access to Get Study Events
    [Tags]    FieldTech StudyEventsController        Get            TC 058
    get study events                        TC 058

TC 059: Field Tech's access to Patch Study Event
    [Tags]    FieldTech StudyEventsController        Patch
    patch study event    ${test_study_event_id}                 TC 059

TC 060: Field Tech's access to Put Study Event
    [Tags]    FieldTech StudyEventsController        Put
    put study event   ${test_study_event_id}                    TC 060

TC 061: Field Tech's access to Post Study Event
    [Tags]    FieldTech StudyEventsController        Post
    post study event                    TC 061

TC 062: Field Tech's access to Set Important Study Events
     [Tags]    FieldTech StudyEventsController        SetImportantStudyEvents
     set important study events   ${test_study_event_id}            TC 062

TC 063: Field Tech's access to Delete Study Event by ID
    [Tags]    FieldTech StudyEventsController        Delete
    delete study event   ${test_study_event_id}             TC 063