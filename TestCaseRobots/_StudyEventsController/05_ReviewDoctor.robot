*** Settings ***
Documentation    Test Cases to validate all Study Events Controller endpoints authorization for Review Doctor role

Library     ../../TestCaseMethods/StudyEventsControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py
Library  ../../TestCaseMethods/_CommonStudiesTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***
${super_admin_token}
${test_review_doctor_token}
${test_user_id}
${test_study_id}
${test_study_event_id}
${study_event_id}
${test_recording_index}
${test_packet_index}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${review_doctor}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    set common studies token  ${super_admin_token}
    change selected facility   ${mmt_fac_id}
    ${test_user_id}=  create test user  ${review_doctor}
    set suite variable  ${test_user_id}
    ${test_study_id}=  get random test study id
    set suite variable  ${test_study_id}
    share test study   ${test_study_id}   ${test_user_id}
    ${test_packet_index}=   get packet index
    set suite variable    ${test_packet_index}
    ${test_recording_index}=   get recording index
    set suite variable      ${test_recording_index}
    ${test_study_event_id}=   create study event    ${test_study_id}    ${test_recording_index}    ${test_packet_index}
    set suite variable   ${test_study_event_id}
    Log     ${test_study_event_id}
    Log   ${test_study_id}

Multiple Teardown Methods
    delete test user    ${review_doctor}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Study Events Controller for Review Doctor
    initialize access for study events controller  ${review_doctor}

Login with Review Doctor
    ${test_review_doctor_token}=  test user login by role  ${review_doctor}  ${None}
    set suite variable  ${test_review_doctor_token}
    set token study events  ${test_review_doctor_token}

# STUDY EVENTS CONTROLLER
TC 037: Review Doctor's access to Add Study Events
    [Tags]    ReviewDoctor StudyEventsController        AddStudyEvent
    ${study_event_id}=  post add study events tc  ${test_study_id}    ${test_recording_index}  ${test_packet_index}            TC 037
    set suite variable   ${study_event_id}

TC 038: Review Doctor's access to Get Study Events by Study ID
    [Tags]    ReviewDoctor StudyEventsController        GetStudyEventsByStudyID
    get study events by study id    ${test_study_id}            TC 038

TC 039: Review Doctor's access to Get Study Events by Event ID
    [Tags]    ReviewDoctor StudyEventsController        Get?id=
    get study events by event id    ${test_study_event_id}          TC 039

TC 040: Review Doctor's access to Get Study Events
    [Tags]    ReviewDoctor StudyEventsController        Get
    get study events            TC 040

TC 041: Review Doctor's access to Patch Study Event
    [Tags]    ReviewDoctor StudyEventsController        Patch
    patch study event    ${test_study_event_id}         TC 041

TC 042: Review Doctor's access to Put Study Event
    [Tags]    ReviewDoctor StudyEventsController        Put
    put study event   ${test_study_event_id}                TC 042

TC 043: Review Doctor's access to Post Study Event
    [Tags]    ReviewDoctor StudyEventsController        Post
    post study event            TC 043

TC 044: Review Doctor's access to Set Important Study Events
     [Tags]    ReviewDoctor StudyEventsController        SetImportantStudyEvents
     set important study events   ${test_study_event_id}            TC 044

TC 045: Review Doctor's access to Delete Study Event by ID
    [Tags]    ReviewDoctor StudyEventsController        Delete
    delete study event   ${test_study_event_id}             TC 045