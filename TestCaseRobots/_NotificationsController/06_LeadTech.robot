*** Settings ***
Documentation    Test Cases to validate all Notifications Controller endpoints authorization for LeadTech role

Library     ../../TestCaseMethods/NotificationsControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py
Library  ../../TestCaseMethods/_CommonStudiesTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***
${super_admin_token}
${test_lead_tech_token}
${test_study_id}
${test_event_id}
${test_review_doctor_user_id}
${test_review_doctor_mail}

*** Keywords ***
Multiple Setup Methods
    delete test user  ${review_doctor}
    delete test user    ${super_admin}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    set common studies token  ${super_admin_token}
    change selected facility   ${mmt_fac_id}
    create test user  ${lead_tech}
    ${test_study_id}=   get random test study id
    set suite variable   ${test_study_id}
    ${test_event_id}=  get study event id  ${test_study_id}
    set suite variable   ${test_event_id}
    ${test_review_doctor_user_id}=   create test user  ${review_doctor}
    set suite variable   ${test_review_doctor_user_id}
    ${test_review_doctor_mail}=  get review doctor mail
    set suite variable   ${test_review_doctor_mail}

Multiple Teardown Methods
    delete test user    ${lead_tech}
    delete test user  ${review_doctor}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Notifications Controller for LeadTech
    initialize access for notifications controller   ${lead_tech}

Login with LeadTech
    ${test_lead_tech_token}=  test user login by role  ${lead_tech}  ${None}
    set suite variable  ${test_lead_tech_token}
    set token notifications    ${test_lead_tech_token}

#NOTIFICATIONS CONTROLLER
TC 026: Lead Tech's access to Post ShareStudy
    [Tags]      LeadTech NotificationsController   ShareStudy
    share study    ${test_study_id}   ${test_review_doctor_user_id}         TC 026

TC 027: Lead Tech's access to Post ShareStudyEvent
    [Tags]      LeadTech NotificationsController   ShareStudyEvent
    share study event  ${test_study_id}  ${test_event_id}  ${test_review_doctor_user_id}        TC 027

TC 028: Lead Tech's access to Post NotifyStudySubmitted
    [Tags]      LeadTech NotificationsController   NotifyStudySubmitted
    notify study submitted  ${test_study_id}   ${test_review_doctor_user_id}  ${test_review_doctor_mail}            TC 028

TC 029: Lead Tech's access to Get GetShareableUsersAndGroups
    [Tags]      LeadTech NotificationsController   GetShareableUsersAndGroups
    get shareable users and groups   ${mmt_fac_id}          TC 029

TC 030: Lead Tech's access to Get GetSubmittableUsersAndGroups
    [Tags]      LeadTech NotificationsController   GetSubmittableUsersAndGroups
    get submittable users and groups  ${mmt_fac_id}         TC 030
