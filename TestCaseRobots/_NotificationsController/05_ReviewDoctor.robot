*** Settings ***
Documentation    Test Cases to validate all Notifications Controller endpoints authorization for Review Doctor role

Library     ../../TestCaseMethods/NotificationsControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py
Library  ../../TestCaseMethods/_CommonStudiesTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***
${super_admin_token}
${test_review_doctor_token}
${test_review_doctor_id}
${test_study_id}
${test_event_id}
${test_field_tech_user_id}
${test_field_tech_mail}

*** Keywords ***
Multiple Setup Methods
    delete test user   ${field_tech}
    delete test user    ${review_doctor}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    set common studies token  ${super_admin_token}
    change selected facility   ${mmt_fac_id}
    ${test_review_doctor_id}=   create test user  ${review_doctor}
    set suite variable   ${test_review_doctor_id}
    ${test_study_id}=   get random test study id
    set suite variable   ${test_study_id}
    share test study   ${test_study_id}  ${test_review_doctor_id}
    ${test_event_id}=  get study event id  ${test_study_id}
    set suite variable   ${test_event_id}
    ${test_field_tech_user_id}=   create test user  ${field_tech}
    set suite variable   ${test_field_tech_user_id}
    ${test_field_tech_mail}=  get field tech mail
    set suite variable   ${test_field_tech_mail}

Multiple Teardown Methods
    delete test user    ${review_doctor}
    delete test user   ${field_tech}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Notifications Controller for Review Doctor
    initialize access for notifications controller   ${review_doctor}

Login with Review Doctor
    ${test_review_doctor_token}=  test user login by role  ${review_doctor}  ${None}
    set suite variable  ${test_review_doctor_token}
    set token notifications    ${test_review_doctor_token}


#NOTIFICATIONS CONTROLLER
TC 021: Review Doctor's access to Post ShareStudy
    [Tags]      ReviewDoctor NotificationsController   ShareStudy
    share study    ${test_study_id}   ${test_field_tech_user_id}        TC 021

TC 022: Review Doctor's access to Post ShareStudyEvent
    [Tags]      ReviewDoctor NotificationsController   ShareStudyEvent
    share study event  ${test_study_id}  ${test_event_id}  ${test_field_tech_user_id}           TC 022

TC 023: Review Doctor's access to Post NotifyStudySubmitted
    [Tags]      ReviewDoctor NotificationsController   NotifyStudySubmitted
    notify study submitted  ${test_study_id}   ${test_field_tech_user_id}  ${test_field_tech_mail}      TC 023

TC 024: Review Doctor's access to Get GetShareableUsersAndGroups
    [Tags]      ReviewDoctor NotificationsController   GetShareableUsersAndGroups
    get shareable users and groups   ${mmt_fac_id}          TC 024

TC 025: Review Doctor's access to Get GetSubmittableUsersAndGroups
    [Tags]      ReviewDoctor NotificationsController   GetSubmittableUsersAndGroups
    get submittable users and groups  ${mmt_fac_id}         TC 025