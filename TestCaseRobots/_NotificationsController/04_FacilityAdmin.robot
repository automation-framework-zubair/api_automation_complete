*** Settings ***
Documentation    Test Cases to validate all Notifications Controller endpoints authorization for SuperAdmin role

Library     ../../TestCaseMethods/NotificationsControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py
Library  ../../TestCaseMethods/_CommonStudiesTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***
${super_admin_token}
${test_facility_admin_token}
${test_study_id}
${test_event_id}
${test_review_doctor_user_id}
${test_review_doctor_mail}

*** Keywords ***
Multiple Setup Methods
    delete test user  ${review_doctor}
    delete test user    ${facility_admin}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    set common studies token  ${super_admin_token}
    change selected facility   ${mmt_fac_id}
    create test user  ${facility_admin}
    ${test_study_id}=   get random test study id
    set suite variable   ${test_study_id}
    ${test_event_id}=  get study event id  ${test_study_id}
    set suite variable   ${test_event_id}
    ${test_review_doctor_user_id}=   create test user  ${review_doctor}
    set suite variable   ${test_review_doctor_user_id}
    ${test_review_doctor_mail}=  get review doctor mail
    set suite variable   ${test_review_doctor_mail}

Multiple Teardown Methods
    delete test user  ${review_doctor}
    delete test user    ${facility_admin}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Notifications Controller for FacilityAdmin
    initialize access for notifications controller   ${facility_admin}

Login with FacilityAdmin
    ${test_facility_admin_token}=  test user login by role  ${facility_admin}  ${None}
    set suite variable  ${test_facility_admin_token}
    set token notifications    ${test_facility_admin_token}

# NOTIFICATIONS CONTROLLER
TC 016: Facility Admin's access to Post ShareStudy
    [Tags]      FacilityAdmin NotificationsController   ShareStudy
    share study    ${test_study_id}   ${test_review_doctor_user_id}         TC 016

TC 017: Facility Admin's access to Post ShareStudyEvent
    [Tags]      FacilityAdmin NotificationsController   ShareStudyEvent
    share study event  ${test_study_id}  ${test_event_id}  ${test_review_doctor_user_id}        TC 017

TC 018: Facility Admin's access to Post NotifyStudySubmitted
    [Tags]      FacilityAdmin NotificationsController   NotifyStudySubmitted
    notify study submitted  ${test_study_id}   ${test_review_doctor_user_id}  ${test_review_doctor_mail}        TC 018

TC 019: Facility Admin's access to Get GetShareableUsersAndGroups
    [Tags]      FacilityAdmin NotificationsController   GetShareableUsersAndGroups
    get shareable users and groups   ${mmt_fac_id}          TC 019

TC 020: Facility Admin's access to Get GetSubmittableUsersAndGroups
    [Tags]      FacilityAdmin NotificationsController   GetSubmittableUsersAndGroups
    get submittable users and groups  ${mmt_fac_id}         TC 020

