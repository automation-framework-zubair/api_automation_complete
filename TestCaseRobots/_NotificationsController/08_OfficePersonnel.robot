*** Settings ***
Documentation    Test Cases to validate all Notifications Controller endpoints authorization for OfficePersonnel role

Library     ../../TestCaseMethods/NotificationsControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py
Library  ../../TestCaseMethods/_CommonStudiesTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***
${super_admin_token}
${test_office_personnel_token}
${test_study_id}
${test_event_id}
${test_review_doctor_user_id}
${test_review_doctor_mail}

*** Keywords ***
Multiple Setup Methods
    delete test user   ${review_doctor}
    delete test user    ${office_personnel}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    set common studies token  ${super_admin_token}
    change selected facility   ${mmt_fac_id}
    create test user  ${office_personnel}
    ${test_study_id}=   get random test study id
    set suite variable   ${test_study_id}
    ${test_event_id}=  get study event id  ${test_study_id}
    set suite variable   ${test_event_id}
    ${test_review_doctor_user_id}=   create test user  ${review_doctor}
    set suite variable   ${test_review_doctor_user_id}
    ${test_review_doctor_mail}=  get review doctor mail
    set suite variable   ${test_review_doctor_mail}

Multiple Teardown Methods
    delete test user    ${office_personnel}
    delete test user  ${review_doctor}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Notifications Controller for Office Personnel
    initialize access for notifications controller   ${office_personnel}

Login with OfficePersonnel
    ${test_office_personnel_token}=  test user login by role  ${office_personnel}  ${None}
    set suite variable  ${test_office_personnel_token}
    set token notifications    ${test_office_personnel_token}

# NOTIFICATIONS CONTROLLER
TC 036: Officer Personnel's access to Post ShareStudy
    [Tags]      OfficerPersonnel NotificationsController   ShareStudy
    share study    ${test_study_id}   ${test_review_doctor_user_id}         TC 036

TC 037: Officer Personnel's access to Post ShareStudyEvent
    [Tags]      OfficerPersonnel NotificationsController   ShareStudyEvent
    share study event  ${test_study_id}  ${test_event_id}  ${test_review_doctor_user_id}        TC 037

TC 038: Officer Personnel's access to Post NotifyStudySubmitted
    [Tags]      OfficerPersonnel NotificationsController   NotifyStudySubmitted
    notify study submitted  ${test_study_id}   ${test_review_doctor_user_id}  ${test_review_doctor_mail}        TC 038

TC 039: Officer Personnel's access to Get GetShareableUsersAndGroups
    [Tags]      OfficerPersonnel NotificationsController   GetShareableUsersAndGroups
    get shareable users and groups   ${mmt_fac_id}          TC 039

TC 040: Officer Personnel's access to Get GetSubmittableUsersAndGroups
    [Tags]      OfficerPersonnel NotificationsController   GetSubmittableUsersAndGroups
    get submittable users and groups  ${mmt_fac_id}             TC 040
