*** Settings ***
Documentation    Test Cases to validate all Notifications Controller endpoints authorization for Production role

Library     ../../TestCaseMethods/NotificationsControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py
Library  ../../TestCaseMethods/_CommonStudiesTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***
${super_admin_token}
${test_production_token}
${test_study_id}
${test_event_id}
${test_review_doctor_user_id}
${test_review_doctor_mail}

*** Keywords ***
Multiple Setup Methods
    delete test user  ${review_doctor}
    delete test user    ${production}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    set common studies token  ${super_admin_token}
    change selected facility   ${mmt_fac_id}
    create test user  ${production}
    ${test_study_id}=   get random test study id
    set suite variable   ${test_study_id}
    ${test_event_id}=  get study event id  ${test_study_id}
    set suite variable   ${test_event_id}
    ${test_review_doctor_user_id}=   create test user  ${review_doctor}
    set suite variable   ${test_review_doctor_user_id}
    ${test_review_doctor_mail}=  get review doctor mail
    set suite variable   ${test_review_doctor_mail}

Multiple Teardown Methods
    delete test user    ${production}
    delete test user  ${review_doctor}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Notifications Controller for Production
    initialize access for notifications controller   ${production}

Login with Production
    ${test_production_token}=  test user login by role  ${production}  ${None}
    set suite variable  ${test_production_token}
    set token notifications    ${test_production_token}

# NOTIFICATIONS CONTROLLER
TC 011: Production's access to Post ShareStudy
    [Tags]      Production NotificationsController   ShareStudy
    share study    ${test_study_id}   ${test_review_doctor_user_id}         TC 011

TC 012: Production's access to Post ShareStudyEvent
    [Tags]      Production NotificationsController   ShareStudyEvent
    share study event  ${test_study_id}  ${test_event_id}  ${test_review_doctor_user_id}        TC 012

TC 013: Production's access to Post NotifyStudySubmitted
    [Tags]      Production NotificationsController   NotifyStudySubmitted
    notify study submitted  ${test_study_id}   ${test_review_doctor_user_id}  ${test_review_doctor_mail}        TC 013

TC 014: Production's access to Get GetShareableUsersAndGroups
    [Tags]      Production NotificationsController   GetShareableUsersAndGroups
    get shareable users and groups   ${mmt_fac_id}          TC 014

TC 015: Production's access to Get GetSubmittableUsersAndGroups
    [Tags]      Production NotificationsController   GetSubmittableUsersAndGroups
    get submittable users and groups  ${mmt_fac_id}         TC 015