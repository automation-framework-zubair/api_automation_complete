*** Settings ***
Documentation    Test Cases to validate all Notifications Controller endpoints authorization for FieldTech role

Library     ../../TestCaseMethods/NotificationsControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py
Library  ../../TestCaseMethods/_CommonStudiesTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***
${super_admin_token}
${test_field_tech_token}
${test_field_tech_id}
${test_study_id}
${test_event_id}
${test_review_doctor_user_id}
${test_review_doctor_mail}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${review_doctor}
    delete test user    ${field_tech}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    set common studies token  ${super_admin_token}
    change selected facility   ${mmt_fac_id}
    ${test_field_tech_id}=   create test user  ${field_tech}
    set suite variable    ${test_field_tech_id}
    ${test_study_id}=   get random test study id
    set suite variable   ${test_study_id}
    share test study   ${test_study_id}     ${test_field_tech_id}
    ${test_event_id}=  get study event id  ${test_study_id}
    set suite variable   ${test_event_id}
    ${test_review_doctor_user_id}=   create test user  ${review_doctor}
    set suite variable   ${test_review_doctor_user_id}
    ${test_review_doctor_mail}=  get review doctor mail
    set suite variable   ${test_review_doctor_mail}

Multiple Teardown Methods
    delete test user   ${field_tech}
    delete test user    ${review_doctor}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Notifications Controller for FieldTech
    initialize access for notifications controller   ${field_tech}

Login with FieldTech
    ${test_field_tech_token}=  test user login by role  ${field_tech}  ${None}
    set suite variable  ${test_field_tech_token}
    set token notifications    ${test_field_tech_token}

# NOTIFICATIONS CONTROLLER
TC 031: Field Tech's access to Post ShareStudy
    [Tags]      FieldTech NotificationsController   ShareStudy
    share study    ${test_study_id}   ${test_review_doctor_user_id}         TC 031

TC 032: Field Tech's access to Post ShareStudyEvent
    [Tags]      FieldTech NotificationsController   ShareStudyEvent
    share study event  ${test_study_id}  ${test_event_id}  ${test_review_doctor_user_id}        TC 032

TC 033: Field Tech's access to Post NotifyStudySubmitted
    [Tags]      FieldTech NotificationsController   NotifyStudySubmitted
    notify study submitted  ${test_study_id}   ${test_review_doctor_user_id}  ${test_review_doctor_mail}        TC 033

TC 034: Field Tech's access to Get GetShareableUsersAndGroups
    [Tags]      FieldTech NotificationsController   GetShareableUsersAndGroups
    get shareable users and groups   ${mmt_fac_id}          TC 034

TC 035: Field Tech's access to Get GetSubmittableUsersAndGroups
    [Tags]      FieldTech NotificationsController   GetSubmittableUsersAndGroups
    get submittable users and groups  ${mmt_fac_id}         TC 035
