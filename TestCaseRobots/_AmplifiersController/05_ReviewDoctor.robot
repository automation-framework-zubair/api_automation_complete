*** Settings ***
Documentation    Test Cases to validate all Amplifiers Controller endpoints authorization for Review Doctor role

Library  ../../TestCaseMethods/AmplifierControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***
${super_admin_token}
${test_review_doctor_token}
${created_amp_id}
${created_amp_sn}
${created_amp_created_date}
${created_test_amp_id}
${created_by_post_test_amp_id}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${review_doctor}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    change selected facility  ${MMT_FAC_ID}
    create test user  ${review_doctor}
    ${created_amp_id}=  create test amplifier  ${MMT_FAC_ID}
    set suite variable  ${created_amp_id}
    ${created_amp_sn}=  get amplifier sn by id  ${created_amp_id}
    set suite variable  ${created_amp_sn}
    ${created_amp_created_date}=  get amplifier created date by id  ${created_amp_id}
    set suite variable  ${created_amp_created_date}

Multiple Teardown Methods
    delete test user    ${review_doctor}
    RUN KEYWORD IF  $created_amp_id is not None     delete test amplifier    ${created_amp_id}
    RUN KEYWORD IF  $created_test_amp_id is not None     delete test amplifier    ${created_test_amp_id}
    RUN KEYWORD IF  $created_by_post_test_amp_id is not None     delete test amplifier    ${created_by_post_test_amp_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Amplifiers Controller for Review Doctor
    initialize access for amplifier controller  ${review_doctor}

Login with Review Doctor
    ${test_review_doctor_token}=   test user login by role     ${review_doctor}    ${None}
    set suite variable  ${test_review_doctor_token}
    set token amplifiers  ${test_review_doctor_token}

# AMPLIFIERS CONTROLLER
TC 049: Review Doctor's access to CreateAmplifier
    [Tags]    AmplifierController      CreateAmplifier
    ${created_test_amp_id}=  create amplifier tc    ${mmt_fac_id}   ${test_review_doctor_token}     TC 049
    set suite variable  ${created_test_amp_id}

TC 050: Review Doctor's access to GetBySn
    [Tags]    AmplifierController      GetBySn
    get by sn  ${created_amp_sn}    TC 050

TC 051: Review Doctor's access to GetAmplifierTypes
    [Tags]    AmplifierController      GetAmplifierTypes
    get amplifier types     TC 051

TC 052: Review Doctor's access to GetModesForAmplifier
    [Tags]    AmplifierController      GetModesForAmplifier
    get modes for amplifier  ${None}        TC 052

TC 053: Review Doctor's access to GetSampleRatesForAmplifier
    [Tags]    AmplifierController      GetSampleRatesForAmplifier
    get sample rates for amplifier  ${None}     TC 053

TC 054: Review Doctor's access to GetAmplifiers
    [Tags]    AmplifierController      GetAmplifiers
    get amplifiers  TC 054

TC 055: Review Doctor's access to GetByID
    [Tags]    AmplifierController      GetById
    get amplifier by id  ${created_amp_id}  TC 055

TC 056: Review Doctor's access to UpdateAmplifierName
    [Tags]    AmplifierController      PostUpdateAmplifierName
    update name amplifier   ${created_amp_id}   ${None}     TC 056

TC 057: Review Doctor's access to Post
    [Tags]    AmplifierController      Post
    post amplifier tc  ${mmt_fac_id}    TC 057

TC 058: Review Doctor's access to Patch
    [Tags]    AmplifierController      Patch
    patch amplifiers id   ${created_amp_id}     TC 058

TC 059: Review Doctor's access to Put
    [Tags]    AmplifierController      Put
    put amplifiers tc  ${created_amp_id}    ${created_amp_sn}   ${mmt_fac_id}   ${created_amp_created_date}     TC 059

TC 060: Review Doctor's access to Delete
    [Tags]    AmplifierController      Delete
    delete amplifier  ${created_amp_id}     TC 060
