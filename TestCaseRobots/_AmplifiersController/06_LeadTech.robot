*** Settings ***
Documentation    Test Cases to validate all Amplifiers Controller endpoints authorization for Lead Tech role

Library  ../../TestCaseMethods/AmplifierControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***
${super_admin_token}
${test_lead_tech_token}
${created_amp_id}
${created_amp_sn}
${created_amp_created_date}
${created_test_amp_id}
${created_by_post_test_amp_id}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${lead_tech}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    change selected facility  ${MMT_FAC_ID}
    create test user  ${lead_tech}
    ${created_amp_id}=  create test amplifier  ${MMT_FAC_ID}
    set suite variable  ${created_amp_id}
    ${created_amp_sn}=  get amplifier sn by id  ${created_amp_id}
    set suite variable  ${created_amp_sn}
    ${created_amp_created_date}=  get amplifier created date by id  ${created_amp_id}
    set suite variable  ${created_amp_created_date}

Multiple Teardown Methods
    delete test user    ${lead_tech}
    RUN KEYWORD IF  $created_amp_id is not None     delete test amplifier    ${created_amp_id}
    RUN KEYWORD IF  $created_test_amp_id is not None     delete test amplifier    ${created_test_amp_id}
    RUN KEYWORD IF  $created_by_post_test_amp_id is not None     delete test amplifier    ${created_by_post_test_amp_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Amplifiers Controller for Lead Tech
    initialize access for amplifier controller  ${lead_tech}

Login with Lead Tech
    ${test_lead_tech_token}=   test user login by role     ${lead_tech}    ${None}
    set suite variable  ${test_lead_tech_token}
    set token amplifiers  ${test_lead_tech_token}

# AMPLIFIERS CONTROLLER
TC 061: Lead Tech's access to CreateAmplifier
    [Tags]    LeadTech AmplifierController      CreateAmplifier
    ${created_test_amp_id}=  create amplifier tc    ${mmt_fac_id}   ${test_lead_tech_token}     TC 061
    set suite variable  ${created_test_amp_id}

TC 062: Lead Tech's access to GetBySn
    [Tags]    LeadTech AmplifierController      GetBySn
    get by sn  ${created_amp_sn}    TC 062

TC 063: Lead Tech's access to GetAmplifierTypes
    [Tags]    LeadTech AmplifierController      GetAmplifierTypes
    get amplifier types     TC 063

TC 064: Lead Tech's access to GetModesForAmplifier
    [Tags]    LeadTech AmplifierController      GetModesForAmplifier
    get modes for amplifier  ${None}    TC 064

TC 065: Lead Tech's access to GetSampleRatesForAmplifier
    [Tags]    LeadTech AmplifierController      GetSampleRatesForAmplifier
    get sample rates for amplifier  ${None}     TC 065

TC 066: Lead Tech's access to GetAmplifiers
    [Tags]    LeadTech AmplifierController      GetAmplifiers
    get amplifiers      TC 066

TC 067: Lead Tech's access to GetByID
    [Tags]    LeadTech AmplifierController      GetById
    get amplifier by id  ${created_amp_id}  TC 067

TC 068: Lead Tech's access to UpdateAmplifierName
    [Tags]    LeadTech AmplifierController      PostUpdateAmplifierName
    update name amplifier   ${created_amp_id}   ${None}     TC 068

TC 069: Lead Tech's access to Post
    [Tags]    LeadTech AmplifierController      Post
    post amplifier tc  ${mmt_fac_id}    TC 069

TC 070: Lead Tech's access to Patch
    [Tags]    LeadTech AmplifierController      Patch
    patch amplifiers id   ${created_amp_id}     TC 070

TC 071: Lead Tech's access to Put
    [Tags]    LeadTech AmplifierController      Put
    put amplifiers tc  ${created_amp_id}    ${created_amp_sn}   ${mmt_fac_id}   ${created_amp_created_date}     TC 071

TC 072: Lead Tech's access to Delete
    [Tags]    LeadTech AmplifierController      Delete
    delete amplifier  ${created_amp_id}     TC 072
