*** Settings ***
Documentation    Test Cases to validate all Amplifiers Controller endpoints authorization for Production role

Library  ../../TestCaseMethods/AmplifierControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***
${super_admin_token}
${test_production_token}
${created_amp_id}
${created_amp_sn}
${created_amp_created_date}
${created_test_amp_id}
${created_by_post_test_amp_id}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${production}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    change selected facility  ${MMT_FAC_ID}
    create test user  ${production}
    ${created_amp_id}=  create test amplifier  ${MMT_FAC_ID}
    set suite variable  ${created_amp_id}
    ${created_amp_sn}=  get amplifier sn by id  ${created_amp_id}
    set suite variable  ${created_amp_sn}
    ${created_amp_created_date}=  get amplifier created date by id  ${created_amp_id}
    set suite variable  ${created_amp_created_date}

Multiple Teardown Methods
    delete test user    ${production}
    RUN KEYWORD IF  $created_amp_id is not None     delete test amplifier    ${created_amp_id}
    RUN KEYWORD IF  $created_test_amp_id is not None     delete test amplifier    ${created_test_amp_id}
    RUN KEYWORD IF  $created_by_post_test_amp_id is not None     delete test amplifier    ${created_by_post_test_amp_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Amplifiers Controller for Production
    initialize access for amplifier controller  ${production}

Login with Production
    ${test_production_token}=   test user login by role     ${production}    ${None}
    set suite variable  ${test_production_token}
    set token amplifiers  ${test_production_token}

# AMPLIFIERS CONTROLLER
TC 025: Production's access to CreateAmplifier
    [Tags]    AmplifierController      CreateAmplifier
    ${created_test_amp_id}=  create amplifier tc    ${mmt_fac_id}   ${test_production_token}    TC 025
    set suite variable  ${created_test_amp_id}

TC 026: Production's access to GetBySn
    [Tags]    AmplifierController      GetBySn
    get by sn  ${created_amp_sn}    TC 026

TC 027: Production's access to GetAmplifierTypes
    [Tags]    AmplifierController      GetAmplifierTypes
    get amplifier types     TC 027

TC 028: Production's access to GetModesForAmplifier
    [Tags]    AmplifierController      GetModesForAmplifier
    get modes for amplifier  ${None}    TC 028

TC 029: Production's access to GetSampleRatesForAmplifier
    [Tags]    AmplifierController      GetSampleRatesForAmplifier
    get sample rates for amplifier  ${None}     TC 029

TC 030: Production's access to GetAmplifiers
    [Tags]    AmplifierController      GetAmplifiers
    get amplifiers      TC 030

TC 031: Production's access to GetByID
    [Tags]    AmplifierController      GetById
    get amplifier by id  ${created_amp_id}  TC 031

TC 032: Production's access to UpdateAmplifierName
    [Tags]    AmplifierController      PostUpdateAmplifierName
    update name amplifier   ${created_amp_id}   ${None}     TC 032

TC 033: Production's access to Post
    [Tags]    AmplifierController      Post
    post amplifier tc  ${mmt_fac_id}    TC 033

TC 034: Production's access to Patch
    [Tags]    AmplifierController      Patch
    patch amplifiers id   ${created_amp_id}     TC 034

TC 035: Production's access to Put
    [Tags]    AmplifierController      Put
    put amplifiers tc  ${created_amp_id}    ${created_amp_sn}   ${mmt_fac_id}   ${created_amp_created_date}     TC 035

TC 036: Production's access to Delete
    [Tags]    AmplifierController      Delete
    delete amplifier  ${created_amp_id}     TC 036
