*** Settings ***
Documentation    Test Cases to validate all Amplifiers Controller endpoints authorization for Office Personnel role

Library  ../../TestCaseMethods/AmplifierControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***
${super_admin_token}
${test_office_personnel_token}
${created_amp_id}
${created_amp_sn}
${created_amp_created_date}
${created_test_amp_id}
${created_by_post_test_amp_id}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${office_personnel}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    change selected facility  ${MMT_FAC_ID}
    create test user  ${office_personnel}
    ${created_amp_id}=  create test amplifier  ${MMT_FAC_ID}
    set suite variable  ${created_amp_id}
    ${created_amp_sn}=  get amplifier sn by id  ${created_amp_id}
    set suite variable  ${created_amp_sn}
    ${created_amp_created_date}=  get amplifier created date by id  ${created_amp_id}
    set suite variable  ${created_amp_created_date}

Multiple Teardown Methods
    delete test user    ${office_personnel}
    RUN KEYWORD IF  $created_amp_id is not None     delete test amplifier    ${created_amp_id}
    RUN KEYWORD IF  $created_test_amp_id is not None     delete test amplifier    ${created_test_amp_id}
    RUN KEYWORD IF  $created_by_post_test_amp_id is not None     delete test amplifier    ${created_by_post_test_amp_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Amplifiers Controller for Office Personnel
    initialize access for amplifier controller  ${office_personnel}

Login with Office Personnel
    ${test_office_personnel_token}=   test user login by role     ${office_personnel}    ${None}
    set suite variable  ${test_office_personnel_token}
    set token amplifiers  ${test_office_personnel_token}

# AMPLIFIERS CONTROLLER
TC 085: Office Personnel's access to CreateAmplifier
    [Tags]    OfficePersonnel AmplifierController      CreateAmplifier
    ${created_test_amp_id}=  create amplifier tc    ${mmt_fac_id}   ${test_office_personnel_token}      TC 085
    set suite variable  ${created_test_amp_id}

TC 086: Office Personnel's access to GetBySn
    [Tags]    OfficePersonnel AmplifierController      GetBySn
    get by sn  ${created_amp_sn}    TC 086

TC 087: Office Personnel's access to GetAmplifierTypes
    [Tags]    OfficePersonnel AmplifierController      GetAmplifierTypes
    get amplifier types     TC 087

TC 088: Office Personnel's access to GetModesForAmplifier
    [Tags]    OfficePersonnel AmplifierController      GetModesForAmplifier
    get modes for amplifier  ${None}    TC 088

TC 089: Office Personnel's access to GetSampleRatesForAmplifier
    [Tags]    OfficePersonnel AmplifierController      GetSampleRatesForAmplifier
    get sample rates for amplifier  ${None}     TC 089

TC 090: Office Personnel's access to GetAmplifiers
    [Tags]    OfficePersonnel AmplifierController      GetAmplifiers
    get amplifiers  TC 090

TC 091: Office Personnel's access to GetByID
    [Tags]    OfficePersonnel AmplifierController      GetById
    get amplifier by id  ${created_amp_id}  TC 091

TC 092: Office Personnel's access to UpdateAmplifierName
    [Tags]    OfficePersonnel AmplifierController      PostUpdateAmplifierName
    update name amplifier   ${created_amp_id}   ${None}     TC 092

TC 093: Office Personnel's access to Post
    [Tags]    OfficePersonnel AmplifierController      Post
    post amplifier tc  ${mmt_fac_id}    TC 093

TC 094: Office Personnel's access to Patch
    [Tags]    OfficePersonnel AmplifierController      Patch
    patch amplifiers id   ${created_amp_id}     TC 094

TC 095: Office Personnel's access to Put
    [Tags]    OfficePersonnel AmplifierController      Put
    put amplifiers tc  ${created_amp_id}    ${created_amp_sn}   ${mmt_fac_id}   ${created_amp_created_date}     TC 095

TC 096: Office Personnel's access to Delete
    [Tags]    OfficePersonnel AmplifierController      Delete
    delete amplifier  ${created_amp_id}     TC 096
