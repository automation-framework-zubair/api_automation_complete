*** Settings ***
Documentation    Test Cases to validate all Amplifiers Controller endpoints authorization for Super Admin role

Library  ../../TestCaseMethods/AmplifierControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***
${super_admin_token}
${test_super_admin_token}
${created_amp_id}
${created_amp_sn}
${created_amp_created_date}
${created_test_amp_id}
${created_by_post_test_amp_id}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${super_admin}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    get_version_number  ${super_admin_token}
    change selected facility  ${MMT_FAC_ID}
    create test user  ${super_admin}
    ${created_amp_id}=  create test amplifier  ${MMT_FAC_ID}
    set suite variable  ${created_amp_id}
    ${created_amp_sn}=  get amplifier sn by id  ${created_amp_id}
    set suite variable  ${created_amp_sn}
    ${created_amp_created_date}=  get amplifier created date by id  ${created_amp_id}
    set suite variable  ${created_amp_created_date}

Multiple Teardown Methods
    delete test user    ${super_admin}
    RUN KEYWORD IF  $created_amp_id is not None     delete test amplifier    ${created_amp_id}
    RUN KEYWORD IF  $created_test_amp_id is not None     delete test amplifier    ${created_test_amp_id}
    RUN KEYWORD IF  $created_by_post_test_amp_id is not None     delete test amplifier    ${created_by_post_test_amp_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Amplifiers Controller for Super Admin
    initialize access for amplifier controller  ${super_admin}

Login with Super Admin
    ${test_super_admin_token}=   test user login by role     ${super_admin}    ${None}
    set suite variable  ${test_super_admin_token}
    set token amplifiers  ${test_super_admin_token}

# AMPLIFIERS CONTROLLER
TC 001: SuperAdmin's access to CreateAmplifier
    [Tags]    SuperAdmin AmplifierController      CreateAmplifier
    ${created_test_amp_id}=  create amplifier tc    ${mmt_fac_id}   ${test_super_admin_token}   TC 001
    set suite variable  ${created_test_amp_id}

TC 002: SuperAdmin's access to GetBySn
    [Tags]    SuperAdmin AmplifierController      GetBySn
    get by sn  ${created_amp_sn}    TC 002

TC 003: SuperAdmin's access to GetAmplifierTypes
    [Tags]    SuperAdmin AmplifierController      GetAmplifierTypes
    get amplifier types     TC 003

TC 004: SuperAdmin's access to GetModesForAmplifier
    [Tags]    SuperAdmin AmplifierController      GetModesForAmplifier
    get modes for amplifier  ${None}    TC 004

TC 005: SuperAdmin's access to GetSampleRatesForAmplifier
    [Tags]    SuperAdmin AmplifierController      GetSampleRatesForAmplifier
    get sample rates for amplifier  ${None}     TC 005

TC 006: SuperAdmin's access to GetAmplifiers
    [Tags]    SuperAdmin AmplifierController      GetAmplifiers
    get amplifiers  TC 006

TC 007: SuperAdmin's access to GetByID
    [Tags]    SuperAdmin AmplifierController      GetById
    get amplifier by id  ${created_amp_id}  TC 007

TC 008: SuperAdmin's access to UpdateAmplifierName
    [Tags]    SuperAdmin AmplifierController      PostUpdateAmplifierName
    update name amplifier   ${created_amp_id}   ${None}   TC 008

TC 009: SuperAdmin's access to Post
    [Tags]    SuperAdmin AmplifierController      Post
    post amplifier tc  ${mmt_fac_id}    TC 009

TC 010: SuperAdmin's access to Patch
    [Tags]    SuperAdmin AmplifierController      Patch
    patch amplifiers id   ${created_amp_id}     TC 010

TC 011: SuperAdmin's access to Put
    [Tags]    SuperAdmin AmplifierController      Put
    put amplifiers tc  ${created_amp_id}    ${created_amp_sn}   ${mmt_fac_id}   ${created_amp_created_date}     TC 011

TC 012: SuperAdmin's access to Delete
    [Tags]    SuperAdmin AmplifierController      Delete
    delete amplifier  ${created_amp_id}     TC 012
