*** Settings ***
Documentation    Test Cases to validate all Amplifiers Controller endpoints authorization for Field Tech role

Library  ../../TestCaseMethods/AmplifierControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***
${super_admin_token}
${test_field_tech_token}
${created_amp_id}
${created_amp_sn}
${created_amp_created_date}
${created_test_amp_id}
${created_by_post_test_amp_id}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${field_tech}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    change selected facility  ${MMT_FAC_ID}
    create test user  ${field_tech}
    ${created_amp_id}=  create test amplifier  ${MMT_FAC_ID}
    set suite variable  ${created_amp_id}
    ${created_amp_sn}=  get amplifier sn by id  ${created_amp_id}
    set suite variable  ${created_amp_sn}
    ${created_amp_created_date}=  get amplifier created date by id  ${created_amp_id}
    set suite variable  ${created_amp_created_date}

Multiple Teardown Methods
    delete test user    ${field_tech}
    RUN KEYWORD IF  $created_amp_id is not None     delete test amplifier    ${created_amp_id}
    RUN KEYWORD IF  $created_test_amp_id is not None     delete test amplifier    ${created_test_amp_id}
    RUN KEYWORD IF  $created_by_post_test_amp_id is not None     delete test amplifier    ${created_by_post_test_amp_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Amplifiers Controller for Field Tech
    initialize access for amplifier controller  ${field_tech}

Login with Field Tech
    ${test_field_tech_token}=   test user login by role     ${field_tech}    ${None}
    set suite variable  ${test_field_tech_token}
    set token amplifiers  ${test_field_tech_token}

# AMPLIFIERS CONTROLLER
TC 073: Field Tech's access to CreateAmplifier
    [Tags]    FieldTech AmplifierController      CreateAmplifier
    ${created_test_amp_id}=  create amplifier tc    ${mmt_fac_id}   ${test_field_tech_token}    TC 073
    set suite variable  ${created_test_amp_id}

TC 074: Field Tech's access to GetBySn
    [Tags]    FieldTech AmplifierController      GetBySn
    get by sn  ${created_amp_sn}    TC 074

TC 075: Field Tech's access to GetAmplifierTypes
    [Tags]    FieldTech AmplifierController      GetAmplifierTypes
    get amplifier types     TC 075

TC 076: Field Tech's access to GetModesForAmplifier
    [Tags]    FieldTech AmplifierController      GetModesForAmplifier
    get modes for amplifier  ${None}    TC 076

TC 077: Field Tech's access to GetSampleRatesForAmplifier
    [Tags]    FieldTech AmplifierController      GetSampleRatesForAmplifier
    get sample rates for amplifier  ${None}     TC 077

TC 078: Field Tech's access to GetAmplifiers
    [Tags]    FieldTech AmplifierController      GetAmplifiers
    get amplifiers  TC 078

TC 079: Field Tech's access to GetByID
    [Tags]    FieldTech AmplifierController      GetById
    get amplifier by id  ${created_amp_id}  TC 079

TC 080: Field Tech's access to UpdateAmplifierName
    [Tags]    FieldTech AmplifierController      PostUpdateAmplifierName
    update name amplifier   ${created_amp_id}   ${None}     TC 080

TC 081: Field Tech's access to Post
    [Tags]    FieldTech AmplifierController      Post
    post amplifier tc  ${mmt_fac_id}    TC 081

TC 082: Field Tech's access to Patch
    [Tags]    FieldTech AmplifierController      Patch
    patch amplifiers id   ${created_amp_id}     TC 082

TC 083: Field Tech's access to Put
    [Tags]    FieldTech AmplifierController      Put
    put amplifiers tc  ${created_amp_id}    ${created_amp_sn}   ${mmt_fac_id}   ${created_amp_created_date}     TC 083

TC 084: Field Tech's access to Delete
    [Tags]    FieldTech AmplifierController      Delete
    delete amplifier  ${created_amp_id}     TC 084
