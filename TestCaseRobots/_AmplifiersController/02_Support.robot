*** Settings ***
Documentation    Test Cases to validate all Amplifiers Controller endpoints authorization for Support role

Library  ../../TestCaseMethods/AmplifierControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***
${super_admin_token}
${test_support_token}
${created_amp_id}
${created_amp_sn}
${created_amp_created_date}
${created_test_amp_id}
${created_by_post_test_amp_id}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${support}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    change selected facility  ${MMT_FAC_ID}
    create test user  ${support}
    ${created_amp_id}=  create test amplifier  ${MMT_FAC_ID}
    set suite variable  ${created_amp_id}
    ${created_amp_sn}=  get amplifier sn by id  ${created_amp_id}
    set suite variable  ${created_amp_sn}
    ${created_amp_created_date}=  get amplifier created date by id  ${created_amp_id}
    set suite variable  ${created_amp_created_date}

Multiple Teardown Methods
    delete test user    ${support}
    RUN KEYWORD IF  $created_amp_id is not None     delete test amplifier    ${created_amp_id}
    RUN KEYWORD IF  $created_test_amp_id is not None     delete test amplifier    ${created_test_amp_id}
    RUN KEYWORD IF  $created_by_post_test_amp_id is not None     delete test amplifier    ${created_by_post_test_amp_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Amplifiers Controller for Support
    initialize access for amplifier controller  ${support}

Login with Support
    ${test_support_token}=   test user login by role     ${support}    ${None}
    set suite variable  ${test_support_token}
    set token amplifiers  ${test_support_token}

# AMPLIFIERS CONTROLLER
TC 013: Support's access to CreateAmplifier
    [Tags]    Support AmplifierController      CreateAmplifier
    ${created_test_amp_id}=  create amplifier tc    ${mmt_fac_id}   ${test_support_token}   TC 013
    set suite variable  ${created_test_amp_id}

TC 014: Support's access to GetBySn
    [Tags]    Support AmplifierController      GetBySn
    get by sn  ${created_amp_sn}    TC 014

TC 015: Support's access to GetAmplifierTypes
    [Tags]    Support AmplifierController      GetAmplifierTypes
    get amplifier types     TC 015

TC 016: Support's access to GetModesForAmplifier
    [Tags]    Support AmplifierController      GetModesForAmplifier
    get modes for amplifier  ${None}    TC 016

TC 017: Support's access to GetSampleRatesForAmplifier
    [Tags]    Support AmplifierController      GetSampleRatesForAmplifier
    get sample rates for amplifier  ${None}     TC 017

TC 018: Support's access to GetAmplifiers
    [Tags]    Support AmplifierController      GetAmplifiers
    get amplifiers  TC 018

TC 019: Support's access to GetByID
    [Tags]    Support AmplifierController      GetById
    get amplifier by id  ${created_amp_id}  TC 019

TC 020: Support's access to UpdateAmplifierName
    [Tags]    Support AmplifierController      PostUpdateAmplifierName
    update name amplifier   ${created_amp_id}   ${None}     TC 020

TC 021: Support's access to Post
    [Tags]    Support AmplifierController      Post
    post amplifier tc  ${mmt_fac_id}    TC 021

TC 022: Support's access to Patch
    [Tags]    Support AmplifierController      Patch
    patch amplifiers id   ${created_amp_id}     TC 022

TC 023: Support's access to Put
    [Tags]    Support AmplifierController      Put
    put amplifiers tc  ${created_amp_id}    ${created_amp_sn}   ${mmt_fac_id}   ${created_amp_created_date}     TC 023

TC 024: Support's access to Delete
    [Tags]    Support AmplifierController      Delete
    delete amplifier  ${created_amp_id}     TC 024
