*** Settings ***
Documentation    Test Cases to validate all Amplifiers Controller endpoints authorization for Facility Admin role

Library  ../../TestCaseMethods/AmplifierControllerTCs.py
Library  ../../TestCaseMethods/_CommonTCs.py

Resource  ../global_robot_variables.robot

*** Variables ***
${super_admin_token}
${test_fac_admin_token}
${created_amp_id}
${created_amp_sn}
${created_amp_created_date}
${created_test_amp_id}
${created_by_post_test_amp_id}

*** Keywords ***
Multiple Setup Methods
    delete test user    ${facility_admin}
    ${super_admin_token}=   user login by role   ${super_admin}
    set suite variable  ${super_admin_token}
    change selected facility  ${MMT_FAC_ID}
    create test user  ${facility_admin}
    ${created_amp_id}=  create test amplifier  ${MMT_FAC_ID}
    set suite variable  ${created_amp_id}
    ${created_amp_sn}=  get amplifier sn by id  ${created_amp_id}
    set suite variable  ${created_amp_sn}
    ${created_amp_created_date}=  get amplifier created date by id  ${created_amp_id}
    set suite variable  ${created_amp_created_date}

Multiple Teardown Methods
    delete test user    ${facility_admin}
    RUN KEYWORD IF  $created_amp_id is not None     delete test amplifier    ${created_amp_id}
    RUN KEYWORD IF  $created_test_amp_id is not None     delete test amplifier    ${created_test_amp_id}
    RUN KEYWORD IF  $created_by_post_test_amp_id is not None     delete test amplifier    ${created_by_post_test_amp_id}

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
Set Access Info of Amplifiers Controller for Facility Admin
    initialize access for amplifier controller  ${facility_admin}

Login with Facility Admin
    ${test_fac_admin_token}=   test user login by role     ${facility_admin}    ${None}
    set suite variable  ${test_fac_admin_token}
    set token amplifiers  ${test_fac_admin_token}

# AMPLIFIERS CONTROLLER
TC 037: Facility Admin's access to CreateAmplifier
    [Tags]    AmplifierController      CreateAmplifier
    ${created_test_amp_id}=  create amplifier tc    ${mmt_fac_id}   ${test_fac_admin_token}     TC 037
    set suite variable  ${created_test_amp_id}

TC 038: Facility Admin's access to GetBySn
    [Tags]    AmplifierController      GetBySn
    get by sn  ${created_amp_sn}    TC 038

TC 039: Facility Admin's access to GetAmplifierTypes
    [Tags]    AmplifierController      GetAmplifierTypes
    get amplifier types     TC 039

TC 040: Facility Admin's access to GetModesForAmplifier
    [Tags]    AmplifierController      GetModesForAmplifier
    get modes for amplifier  ${None}    TC 040

TC 041: Facility Admin's access to GetSampleRatesForAmplifier
    [Tags]    AmplifierController      GetSampleRatesForAmplifier
    get sample rates for amplifier  ${None}     TC 041

TC 042: Facility Admin's access to GetAmplifiers
    [Tags]    AmplifierController      GetAmplifiers
    get amplifiers  TC 042

TC 043: Facility Admin's access to GetByID
    [Tags]    AmplifierController      GetById
    get amplifier by id  ${created_amp_id}  TC 043

TC 044: Facility Admin's access to UpdateAmplifierName
    [Tags]    AmplifierController      PostUpdateAmplifierName
    update name amplifier   ${created_amp_id}   ${None}     TC 044

TC 045: Facility Admin's access to Post
    [Tags]    AmplifierController      Post
    post amplifier tc  ${mmt_fac_id}    TC 045

TC 046: Facility Admin's access to Patch
    [Tags]    AmplifierController      Patch
    patch amplifiers id   ${created_amp_id}     TC 046

TC 047: Facility Admin's access to Put
    [Tags]    AmplifierController      Put
    put amplifiers tc  ${created_amp_id}    ${created_amp_sn}   ${mmt_fac_id}   ${created_amp_created_date}     TC 047

TC 048: Facility Admin's access to Delete
    [Tags]    AmplifierController      Delete
    delete amplifier  ${created_amp_id}     TC 048
