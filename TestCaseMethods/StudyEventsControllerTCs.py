import os
import sys

lib_path2 = os.path.abspath(os.path.join('..', '..', 'libraries'))
sys.path.insert(0, lib_path2)
import conf as init

lib_path = os.path.abspath(os.path.join('..', '..', 'libraries', 'common'))
sys.path.insert(0, lib_path)
from logger_robot import Logger as logger_extended
from assertion import code_match, code_match_update_checklist
from request import request

lib_path1 = os.path.abspath(os.path.join('..', '..', 'libraries', 'Rendr'))
sys.path.insert(0, lib_path1)
from TestDataProvider import TestDataProvider
from application_util import headers, dict_to_json


class StudyEventsControllerTCs(object):
    ROBOT_LIBRARY_SCOPE = 'TEST SUITE'
    log = logger_extended().get_logger('API_Automation')

    data = TestDataProvider()

    note_type_event_id = "14"

    payload_of_adding_event = ""
    response_body_of_created_study_event = ""
    created_event_id = ""
    study_id = ""
    recording_index = ""
    start_packet_index = ""
    end_packet_index = ""
    comment = "API Automation event"
    is_set_important = True

    user_token = ""
    header = {}

    sheet_name = "StudyEvents"

    # URLS
    url = init.url
    post_add_study_events_url = ""
    get_study_event_by_study_id_url = ""
    delete_study_event_url = ""
    get_study_event_by_id_url = ""
    get_study_event_url = ""
    patch_study_event_url = ""
    post_study_event_url = ""
    put_study_event_url = ""
    set_important_study_event_url = ""

    # Access Information
    post_add_study_events_access = ""
    get_study_event_by_study_id_access = ""
    delete_study_event_access = ""
    get_study_event_by_id_access = ""
    get_study_event_access = ""
    patch_study_event_access = ""
    post_study_event_access = ""
    put_study_event_access = ""
    set_important_study_event_access = ""

    con_name = ""

    def __init__(self):
        self._expression = ''
        self.con_name = "StudyEventsController"
        self.log.info(self.con_name + " class has been initialized")
        self.post_add_study_events_url = self.url + self.data.get_url_of_controller(self.con_name, "AddStudyEvent")
        self.get_study_event_by_study_id_url = self.url + self.data.get_url_of_controller(self.con_name,
                                                                                          "GetStudyEventsById")
        self.delete_study_event_url = self.url + self.data.get_url_of_controller(self.con_name, "Delete")
        self.get_study_event_by_id_url = self.url + self.data.get_url_of_controller(self.con_name, "Get?id=")
        self.get_study_event_url = self.url + self.data.get_url_of_controller(self.con_name, "Get")
        self.patch_study_event_url = self.url + self.data.get_url_of_controller(self.con_name, "Patch")
        self.post_study_event_url = self.url + self.data.get_url_of_controller(self.con_name, "Post")
        self.put_study_event_url = self.url + self.data.get_url_of_controller(self.con_name, "Put")
        self.set_important_study_event_url = self.url + self.data.get_url_of_controller(self.con_name, "SetImportant")

    def initialize_access_for_study_events_controller(self, role):
        list_of_roles = ['super admin', 'support', 'production', 'facility admin', 'review doctor', 'lead tech',
                         'field tech', 'office personnel']

        if role in list_of_roles:
            self.log.info("Gathering access info for " + role + " role")
            self.post_add_study_events_access = self.data.get_access_status_of_controller(
                self.con_name, "AddStudyEvent", role)
            self.get_study_event_by_study_id_access = self.data.get_access_status_of_controller(
                self.con_name, "GetStudyEventsById", role)
            self.delete_study_event_access = self.data.get_access_status_of_controller(
                self.con_name, "Delete", role)
            self.get_study_event_by_id_access = self.data.get_access_status_of_controller(
                self.con_name, "Get?id=", role)
            self.get_study_event_access = self.data.get_access_status_of_controller(
                self.con_name, "Get", role)
            self.patch_study_event_access = self.data.get_access_status_of_controller(
                self.con_name, "Patch", role)
            self.post_study_event_access = self.data.get_access_status_of_controller(
                self.con_name, "Post", role)
            self.put_study_event_access = self.data.get_access_status_of_controller(
                self.con_name, "Put", role)
            self.set_important_study_event_access = self.data.get_access_status_of_controller(
                self.con_name, "SetImportant", role)
        else:
            raise AssertionError("Please provide a valid role name")

    def set_token_study_events(self, token):
        """
        @desc - method to set token of the user
        :param - token: user's token
        """
        self.log.info("Current user's token : " + token)
        self.header = headers(token)

    def post_add_study_events_tc(self, study_id=None, recording_index=None,
                                 start_packet_index=None, sn=None):
        """
        @desc - call add_study_events and returns event id
        :param study_id: Study id
        :param recording_index: Study recording index
        :param start_packet_index: Starting packet index
        :param end_packet_index: Ending packet index
        :param comment: Comment to create study event
        :param sn: TC Serial Number
        :return:
        """
        self.log.info("TC method: post_add_study_events_tc has been called")
        self.study_id = study_id
        self.recording_index = recording_index
        self.start_packet_index = str(int(start_packet_index) + 100)
        self.end_packet_index = str(int(start_packet_index) + 200)
        self.comment = "API_Automation_Comment"

        response = self.post_add_study_events(self.study_id, self.recording_index,
                                              self.start_packet_index, self.end_packet_index, self.comment, sn)

        if str(response.status_code) == "201":
            self.response_body_of_created_study_event = response.text
            json_response = response.json()
            self.created_event_id = json_response['ID']
            print(self.created_event_id)
            return self.created_event_id
        elif str(response.status_code) == "403":
            print("Logged in user could not create study event")
        else:
            raise AssertionError("Something went wrong. Event could not be created.")

    def post_add_study_events(self, study_id, recording_index,
                              start_packet_index, end_packet_index, comment, sn=None):
        """
        @desc - Post method to add a study event
        :param study_id: Study id
        :param recording_index: Study recording index
        :param start_packet_index: Starting packet index
        :param end_packet_index: Ending packet index
        :param comment: Comment to create study event
        :param sn: TC Serial Number
        :return:
        """
        self.log.info("Endpoint: post_add_study_events has been called")
        self.study_id = study_id

        payload = {
            "StartPacketIndex": start_packet_index,
            "EndPacketIndex": end_packet_index,
            "RecordingIndex": recording_index,
            "EventTypeID": self.note_type_event_id,
            "StudyID": study_id,
            "Comment": comment}

        self.payload_of_adding_event = payload

        print(payload)

        response = request(method="POST", url=self.post_add_study_events_url,
                           data=dict_to_json(payload), headers=self.header)

        print("RESPONSE BODY -" + response.text)
        print("STATUS CODE -" + str(response.status_code))

        if sn is not None:
            code_match_update_checklist(str(response.status_code), self.post_add_study_events_access,
                                        self.sheet_name, sn)
        else:
            code_match(str(response.status_code), self.post_add_study_events_access)

        return response

    def get_study_events_by_study_id(self, study_id, sn=None):
        """
        @desc - Shows a list of Study Events by Study ID
        :param sn: TC Serial Number
        :param study_id:
        :return:
        """
        self.log.info("Endpoint: get_study_events_by_study_id has been called")
        querystring = {'id': study_id}

        print("Querystring: " + querystring['id'])

        response = request("GET", url=self.get_study_event_by_study_id_url, headers=self.header, params=querystring)

        print(response.text)

        if sn is not None:
            code_match_update_checklist(str(response.status_code), self.get_study_event_by_study_id_access,
                                        self.sheet_name, sn)
        else:
            code_match(str(response.status_code), self.get_study_event_by_study_id_access)

        return response

    def delete_study_event(self, event_id, sn=None):
        """
        @desc - To delete a study event by ID
        :param event_id: created study event id
        :param sn: TC Serial Number
        :return:
        """
        self.log.info("Endpoint: delete_study_event has been called")
        if event_id is None:
            event_id = self.created_event_id

        url = self.delete_study_event_url + event_id

        response = request(method="DELETE", url=url, headers=self.header)

        print(response.text)

        code_match_update_checklist(str(response.status_code), self.delete_study_event_access,
                                    self.sheet_name, sn)

    def get_study_events_by_event_id(self, event_id, sn=None):
        """
        @desc - Get study events by id
        :param event_id: created study event id
        :param sn: TC Serial Number
        :return:
        """
        self.log.info("Endpoint: get_study_events_by_event_id has been called")
        if event_id is None:
            event_id = self.created_event_id

        url = self.get_study_event_by_id_url + event_id

        response = request("GET", url=url, headers=self.header)

        print(response.text)

        code_match_update_checklist(str(response.status_code), self.get_study_event_by_id_access,
                                    self.sheet_name, sn)

    def get_study_events(self, sn=None):
        """
        @desc - Get all study events
        :return:
        """
        self.log.info("Endpoint: get_study_events has been called")
        response = request("GET", url=self.get_study_event_url, headers=self.header)

        print(response.text)

        code_match_update_checklist(str(response.status_code), self.get_study_event_access,
                                    self.sheet_name, sn)

    def patch_study_event(self, event_id, sn=None):
        """
        @desc - Patch a study event (not sure about the params)
        :param event_id: created study event id
        :param sn: TC Serial Number
        :return:
        """
        self.log.info("Endpoint: patch_study_event has been called")
        if event_id is None:
            event_id = self.created_event_id

        url = self.patch_study_event_url + event_id

        payload = {"StudyID": self.study_id}

        response = request(method="PATCH", url=url, data=dict_to_json(payload), headers=self.header)

        print(response.text)

        code_match_update_checklist(str(response.status_code), self.patch_study_event_access,
                                    self.sheet_name, sn)

    def post_study_event(self, sn=None):
        """
        @desc - Method to check post study event
        :param sn: TC Serial Number
        :return:
        """
        self.log.info("Endpoint: post_study_event has been called")
        payload = self.payload_of_adding_event

        response = request(method="POST", url=self.post_study_event_url,
                           data=dict_to_json(payload), headers=self.header)

        print(response.text)

        code_match_update_checklist(str(response.status_code), self.post_study_event_access,
                                    self.sheet_name, sn)

    def put_study_event(self, event_id, sn=None):
        """
        @desc - Put study event.
        :param event_id: created study event id
        :param sn: TC Serial Number
        :return:
        """
        self.log.info("Endpoint: put_study_event has been called")
        if event_id is None:
            event_id = self.created_event_id
        print("created id", self.created_event_id)

        url = self.put_study_event_url + event_id

        payload = self.payload_of_adding_event

        response = request(method="PUT", url=url, data=dict_to_json(payload), headers=self.header)

        print(response.text)

        code_match_update_checklist(str(response.status_code), self.put_study_event_access,
                                    self.sheet_name, sn)

    def set_important_study_events(self, event_id, sn=None):
        """
            @desc - Set the created event as important
            :param event_id: created study event id
            :param sn: TC Serial Number
            :return:
        """
        self.log.info("Endpoint: set_important_study_events has been called")
        if event_id is None:
            event_id = self.created_event_id

        querystring = {
            'id': event_id,
            'isImportant': self.is_set_important
        }
        response = request(method="POST", url=self.set_important_study_event_url,
                           headers=self.header, params=querystring)
        print(response)

        code_match_update_checklist(str(response.status_code), self.set_important_study_event_access,
                                    self.sheet_name, sn)

    class StudyEventsControllerTCs(Exception):
        pass
