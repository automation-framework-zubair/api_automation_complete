import os
import sys
import datetime
import time

lib_path2 = os.path.abspath(os.path.join('..', '..', 'libraries'))
sys.path.insert(0, lib_path2)
import conf as init

lib_path = os.path.abspath(os.path.join('..', '..', 'libraries', 'common'))
sys.path.insert(0, lib_path)
from logger_robot import Logger as logger_extended
from assertion import code_match_update_checklist, code_match
from request import request

lib_path1 = os.path.abspath(os.path.join('..', '..', 'libraries', 'Rendr'))
sys.path.insert(0, lib_path1)
from TestDataProvider import TestDataProvider
from application_util import headers, dict_to_json, get_formatted_date


class DevicesControllerTCs(object):
    ROBOT_LIBRARY_SCOPE = 'TEST SUITE'
    log = logger_extended().get_logger('API_Automation')

    data = TestDataProvider()

    device_name = ""
    device_sn = ""
    device_config = ""
    device_part_number = ""
    facility_id = ""
    device_id = ""

    device_date_created = ""

    user_token = ""
    header = {}

    sheet_name = "Devices"

    # URLS
    url = init.url
    post_create_device_url = ""
    get_device_by_sn_url = ""
    get_devices_url = ""
    post_devices_url = ""
    delete_devices_url = ""
    get_devices_by_id_url = ""
    patch_devices_url = ""
    put_devices_url = ""

    # Access Information
    post_create_device_access = ""
    get_device_by_sn_access = ""
    get_devices_access = ""
    post_devices_access = ""
    delete_devices_access = ""
    get_devices_by_id_access = ""
    patch_devices_access = ""
    put_devices_access = ""

    con_name = ""

    def __init__(self):
        self._expression = ''
        self.con_name = "DevicesController"
        self.log.info(self.con_name + " class has been initialized")
        self.post_create_device_url = self.url + self.data.get_url_of_controller(self.con_name, "CreateDevice")
        self.get_device_by_sn_url = self.url + self.data.get_url_of_controller(self.con_name, "GetBySn")
        self.get_devices_url = self.url + self.data.get_url_of_controller(self.con_name, "GetDevices")
        self.post_devices_url = self.url + self.data.get_url_of_controller(self.con_name, "Post")
        self.delete_devices_url = self.url + self.data.get_url_of_controller(self.con_name, "Delete")
        self.get_devices_by_id_url = self.url + self.data.get_url_of_controller(self.con_name, "Get?id=")
        self.patch_devices_url = self.url + self.data.get_url_of_controller(self.con_name, "Patch")
        self.put_devices_url = self.url + self.data.get_url_of_controller(self.con_name, "Put")

    def initialize_access_for_devices_controller(self, role):
        list_of_roles = ['super admin', 'support', 'production', 'facility admin', 'review doctor', 'lead tech',
                         'field tech', 'office personnel']

        if role in list_of_roles:
            self.log.info("Gathering access info for " + self.con_name + " role")
            self.post_create_device_access = self.data.get_access_status_of_controller(
                self.con_name, "CreateDevice", role)
            self.get_device_by_sn_access = self.data.get_access_status_of_controller(
                self.con_name, "GetBySn", role)
            self.get_devices_access = self.data.get_access_status_of_controller(
                self.con_name, "GetDevices", role)
            self.post_devices_access = self.data.get_access_status_of_controller(
                self.con_name, "Post", role)
            self.delete_devices_access = self.data.get_access_status_of_controller(
                self.con_name, "Delete", role)
            self.get_devices_by_id_access = self.data.get_access_status_of_controller(
                self.con_name, "Get?id=", role)
            self.patch_devices_access = self.data.get_access_status_of_controller(
                self.con_name, "Patch", role)
            self.put_devices_access = self.data.get_access_status_of_controller(
                self.con_name, "Put", role)

        else:
            raise AssertionError("Please provide a valid role name")

    def set_token_devices(self, token):
        """
        @desc - method to set token of the user
        :param - token: user's token
        """
        self.log.info("Current user's token : " + token)
        self.header = headers(token)

    def post_create_device_tc(self, device_name=None, device_sn=None, device_config=None,
                              device_part_number=None, facility_id=None, sn=None):
        """
        @desc - Call creates a new Device using Post method and returns id
        :param device_name: Name of the device
        :param device_sn: Serial Number of the device
        :param device_config: Configuration of the device
        :param device_part_number: Part Number of the device
        :param facility_id: ID of the facility to which the device will belong
        :param sn: TC Serial Number
        :return: Newly created device ID
        """
        self.log.info("TC method: post_create_device_tc has been called")
        response = self.post_create_device(device_name,
                                           device_sn,
                                           device_config,
                                           device_part_number,
                                           facility_id,
                                           sn)

        if str(response.status_code) == '201':
            json_response = response.json()
            self.device_id = json_response['ID']
            print("ID of the newly created Device: {}".format(self.device_id))
            self.device_name = json_response['Name']
            print("Name of the newly created Device: {}".format(self.device_name))
            self.device_sn = json_response['SerialNumber']
            print("Serial Number of the newly created Device: {}".format(self.device_sn))
            self.device_config = json_response['Configuration']
            print("Configuration of the newly created Device: {}".format(self.device_config))
            self.device_part_number = json_response['PartNumber']
            print("Part Number of the newly created Device: {}".format(self.device_part_number))
            return self.device_id

        elif str(response.status_code) == '403':
            print("Device could not be created with logged in user")

    def post_create_device(self, device_name=None, device_sn=None, device_config=None,
                           device_partnumber=None, facility_id=None, sn=None):
        """
        @desc - Creates a new Device using Post method
        :param device_name: Name of the device
        :param device_sn: Serial Number of the device
        :param device_config: Configuration of the device
        :param device_partnumber: Part Number of the device
        :param facility_id: ID of the facility to which the device will belong
        :param sn: TC Serial Number
        :return: Newly created device ID
        """

        self.log.info("Endpoint: post_create_device has been called")

        if device_name is None or device_sn is None or device_config is None or device_partnumber and None \
                or facility_id is None:
            self.device_name = "API_TEST_DEVICE_111"
            self.device_sn = get_formatted_date()
            self.device_config = "999"
            self.device_part_number = "999"
            self.facility_id = init.mobile_med_tek_facility_id
        else:
            self.device_name = device_name
            self.device_sn = device_sn
            self.device_config = device_config
            self.device_part_number = device_partnumber
            self.facility_id = facility_id

        querystring = {"facilityId": self.facility_id}

        payload = {
            "Name": self.device_name,
            "SerialNumber": self.device_sn,
            "Configuration": self.device_config,
            "PartNumber": self.device_part_number,
            "FacilityID": self.facility_id
        }
        response = request(method="POST", url=self.post_create_device_url,
                           data=dict_to_json(payload), headers=self.header, params=querystring)
        print(response.text)

        if sn is not None:
            code_match_update_checklist(str(response.status_code), self.post_create_device_access, self.sheet_name, sn)
        else:
            code_match(str(response.status_code), self.post_create_device_access)

        return response

    def get_devices_by_sn(self, sn_device=None, sn=None):
        """
        @desc - Get a device by serial number
        :param sn_device: Serial Number of the device. If set to None, the created device's sn will be used
        :param sn: TC Serial Number
        :return:
        """
        self.log.info("Endpoint: get_devices_by_sn has been called")

        if sn_device is None:
            sn_device = self.device_sn

        querystring = {'sn': sn_device}

        response = request("GET", url=self.get_device_by_sn_url,
                           headers=self.header, params=querystring)

        print(response.text)
        code_match_update_checklist(str(response.status_code), self.get_device_by_sn_access, self.sheet_name, sn)

    def get_devices(self, sn=None):
        """
        @desc - method to Get all Devices
        :param sn: TC Serial Number
        :return:
        """
        self.log.info("Endpoint: get_devices has been called")

        response = request("GET", url=self.get_devices_url, headers=self.header)

        print(response.text)
        code_match_update_checklist(str(response.status_code), self.get_devices_access, self.sheet_name, sn)

    def get_devices_by_id(self, id_devices=None, sn=None):
        """
        @desc - Get a Device's data using ID
        :param id_devices: ID of the device
        :param sn: TC Serial Number
        :return:
        """
        self.log.info("Endpoint: get_devices_by_id has been called")

        if id_devices is None:
            url = self.get_devices_by_id_url + self.device_id
        else:
            url = self.get_devices_by_id_url + id_devices

        print(self.url)
        response = request("GET", url=url, headers=self.header)

        print(response.text)
        if sn is not None:
            code_match_update_checklist(str(response.status_code), self.get_devices_by_id_access, self.sheet_name, sn)
        else:
            code_match(str(response.status_code), self.get_devices_by_id_access)
        return response

    def post_devices_tc(self, sn=None):
        """
        @desc - Calls Post method of Devices controller and returns device ID
        :param sn: TC Serial Number
        :return: new device id
        """
        self.log.info("TC method: post_devices_tc has been called")
        response = self.post_devices(sn)

        print(response)

        if str(response.status_code) == '201':
            print("Device has been created using POST method")
            json_response = response.json()
            device_id = json_response['ID']
            print("ID of the newly created Device: {}".format(device_id))
            return device_id
        else:
            print("Something went wrong!")

    def post_devices(self, sn=None):
        """
        @desc - Post method of Devices controller.
        :param sn: TC Serial Number
        :return: new device id
        """
        self.log.info("Endpoint: post_devices has been called")

        self.device_sn = get_formatted_date()
        device_name = self.device_name + "POST"

        payload = {
            "Name": device_name,
            "SerialNumber": self.device_sn,
            "Configuration": self.device_config,
            "PartNumber": self.device_part_number,
            "FacilityID": self.facility_id
        }

        response = request(method="POST", url=self.post_devices_url, data=dict_to_json(payload), headers=self.header)

        print(response.text)
        code_match_update_checklist(str(response.status_code), self.post_devices_access, self.sheet_name, sn)

        return response

    def patch_devices(self, device_id=None, sn=None):
        """
        @desc -  Updates a device by passing full entity
        :param device_id: device id
        :param sn: TC Serial Number
        :return: new device id
        """
        self.log.info("Endpoint: patch_device has been called")

        if device_id is not None:
            self.device_id = device_id

        url = self.patch_devices_url + self.device_id

        serial_no = self.device_sn + "EditedbyPatch"
        name = self.device_name + "EditedbyPatch"

        payload = {
            "Name": name,
            "SerialNumber": serial_no,
            "Configuration": self.device_config,
            "PartNumber": self.device_part_number,
            "FacilityID": self.facility_id
        }

        response = request("PATCH", url=url, data=dict_to_json(payload), headers=self.header)
        print(response.text)
        code_match_update_checklist(str(response.status_code), self.patch_devices_access, self.sheet_name, sn)

    def put_devices(self, device_id=None, sn=None):
        """
        @desc -  Updates a device by passing full entity
        :param device_id: device id
        :param sn: TC Serial Number
        :return: new device id
        """
        self.log.info("Endpoint: put_devices has been called")

        if device_id is not None:
            self.device_id = device_id

        url = self.put_devices_url + self.device_id

        device_name = self.device_name + "Editedbyput"

        payload = {
            "ID": self.device_id,
            "Name": device_name,
            "SerialNumber": self.device_sn,
            "Configuration": self.device_config,
            "PartNumber": self.device_part_number,
            "FacilityID": self.facility_id,
        }

        response = request("PUT", url=url, data=dict_to_json(payload), headers=self.header)

        print(response.text)
        code_match_update_checklist(str(response.status_code), self.put_devices_access, self.sheet_name, sn)

    def delete_delete_devices(self, device_id=None, sn=None):
        """
        @desc Deletes a device. If logged in user cannot delete, the device will be deleted using SuperAdmin token
        :param device_id: ID of the device to be deleted
        :param sn: TC Serial Number
        :return:
        """
        self.log.info("Endpoint: delete_devices has been called")
        if device_id is None:
            url = self.delete_devices_url + self.device_id
        else:
            url = self.delete_devices_url + device_id

        response = request("DELETE", url=url, headers=self.header)

        print(response.text)

        code_match_update_checklist(str(response.status_code), self.delete_devices_access, self.sheet_name, sn)

    class DevicesControllerTCs(Exception):
        pass
