import os
import sys
import datetime
import time

lib_path2 = os.path.abspath(os.path.join('..', '..', 'libraries'))
sys.path.insert(0, lib_path2)
import conf as init

lib_path = os.path.abspath(os.path.join('..', '..', 'libraries', 'common'))
sys.path.insert(0, lib_path)
from logger_robot import Logger as logger_extended
from assertion import code_match, code_match_update_checklist
from request import request

lib_path1 = os.path.abspath(os.path.join('..', '..', 'libraries', 'Rendr'))
sys.path.insert(0, lib_path1)
from TestDataProvider import TestDataProvider
from application_util import headers, dict_to_json, get_formatted_date


class AmplifierControllerTCs(object):
    ROBOT_LIBRARY_SCOPE = 'TEST SUITE'
    log = logger_extended().get_logger('API_Automation')

    data = TestDataProvider()

    user_token = ""
    header = {}

    sheet_name = "Amplifier"

    amp_id = ""

    # URLS
    url = init.url
    post_create_amplifier_url = ""
    get_by_sn_url = ""
    get_amplifier_type_url = ""
    get_modes_for_amplifier_url = ""
    get_sample_rate_url = ""
    delete_amp_url = ""
    get_amplifiers_url = ""
    get_amp_id_url = ""
    post_update_amp_name_url = ""
    patch_amplifiers_id_url = ""
    post_amplifiers_url = ""
    put_amplifiers_url = ""

    # Access information
    post_create_amplifier_access = ""
    get_by_sn_access = ""
    get_amplifier_type_access = ""
    get_modes_for_amplifier_access = ""
    get_sample_rate_access = ""
    get_delete_amp_access = ""
    get_amplifiers_access = ""
    get_amp_id_access = ""
    post_update_amp_name_access = ""
    patch_amplifiers_id_access = ""
    post_amplifiers_access = ""
    put_amplifiers_access = ""
    delete_amp_access = ""

    con_name = ""

    def __init__(self):
        self._expression = ''
        self.con_name = "AmplifiersController"
        self.log.info(self.con_name + " class has been initialized")
        self.post_create_amplifier_url = self.url + self.data.get_url_of_controller(self.con_name, "CreateAmplifier")
        self.get_by_sn_url = self.url + self.data.get_url_of_controller(self.con_name, "GetBySn")
        self.get_amplifier_type_url = self.url + self.data.get_url_of_controller(self.con_name, "GetAmplifierTypes")
        self.get_modes_for_amplifier_url = self.url + self.data.get_url_of_controller(self.con_name,
                                                                                      "GetModesForAmplifier")
        self.get_sample_rate_url = self.url + self.data.get_url_of_controller(self.con_name,
                                                                              "GetSampleRatesForAmplifier")
        self.delete_amp_url = self.url + self.data.get_url_of_controller(self.con_name, "Delete")
        self.get_amplifiers_url = self.url + self.data.get_url_of_controller(self.con_name, "GetAmplifiers")
        self.get_amp_id_url = self.url + self.data.get_url_of_controller(self.con_name, "Get?id=")
        self.post_update_amp_name_url = self.url + self.data.get_url_of_controller(self.con_name, "UpdateAmplifierName")
        self.patch_amplifiers_id_url = self.url + self.data.get_url_of_controller(self.con_name, "Patch")
        self.post_amplifiers_url = self.url + self.data.get_url_of_controller(self.con_name, "Post")
        self.put_amplifiers_url = self.url + self.data.get_url_of_controller(self.con_name, "Put")

    def initialize_access_for_amplifier_controller(self, role):
        if role in ('super admin', 'support', 'production', 'facility admin', 'review doctor', 'lead tech',
                    'field tech', 'office personnel'):
            self.log.info("Gathering access info for " + role + " role")
            self.post_create_amplifier_access = self.data.get_access_status_of_controller(
                self.con_name, "CreateAmplifier", role)
            self.get_by_sn_access = self.data.get_access_status_of_controller(
                self.con_name, "GetBySn", role)
            self.get_amplifier_type_access = self.data.get_access_status_of_controller(
                self.con_name, "GetAmplifierTypes", role)
            self.get_modes_for_amplifier_access = self.data.get_access_status_of_controller(
                self.con_name, "GetModesForAmplifier", role)
            self.get_sample_rate_access = self.data.get_access_status_of_controller(
                self.con_name, "GetSampleRatesForAmplifier", role)
            self.delete_amp_access = self.data.get_access_status_of_controller(
                self.con_name, "Delete", role)
            self.get_amplifiers_access = self.data.get_access_status_of_controller(
                self.con_name, "GetAmplifiers", role)
            self.get_amp_id_access = self.data.get_access_status_of_controller(
                self.con_name, "Get?id=", role)
            self.post_update_amp_name_access = self.data.get_access_status_of_controller(
                self.con_name, "UpdateAmplifierName", role)
            self.patch_amplifiers_id_access = self.data.get_access_status_of_controller(
                self.con_name, "Patch", role)
            self.post_amplifiers_access = self.data.get_access_status_of_controller(
                self.con_name, "Post", role)
            self.put_amplifiers_access = self.data.get_access_status_of_controller(
                self.con_name, "Put", role)

        else:
            raise AssertionError("Please provide a valid role name")

    def set_token_amplifiers(self, token):
        """
        @desc - method to set token of the user
        :param - token: user's token
        :return:
        """
        self.log.info("Current user's token : " + token)
        self.header = headers(token)

    def create_amplifier_tc(self, facility_id, serial_number, sn=None):
        """
        @desc - Call create an Amplifier using Post method and returms amp_id
        :param serial_number: amplifier serial number
        :param - facility_id: Id of the facility to which the Amp will belong
        :param sn: TC Serial Number
        :return:
        """
        self.log.info("TC method: create_amplifier_tc has been called")
        response = self.post_create_amplifier(amp_name="API_TEST",
                                              amp_sn=serial_number[0:10],
                                              facility_id=facility_id,
                                              amp_type_id=1,
                                              sn=sn)

        if str(response.status_code) != "403":
            json_response = response.json()
            return json_response['ID']

    def post_create_amplifier(self, amp_name, amp_sn, facility_id, amp_type_id, sn=None):
        """
        @desc - Create an Amplifier using Post method.
        :param - amp_name: Name of the Amp.
        :param - amp_sn: Serial number of the Amp
        :param - facility_id: Id of the facility to which the Amp will belong
        :param - amp_type_id: Amplifier type 1,7,8
        :param sn: TC Serial Number
        :return:
        """
        self.log.info("Endpoint: CreateAmplifier has been called")
        querystring = {'facilityId': facility_id}

        payload = {
            "Name": amp_name,
            "SerialNumber": amp_sn,
            "FacilityID": facility_id,
            "AmplifierTypeID": amp_type_id
        }
        print(payload["Name"])
        print(payload["SerialNumber"])
        print(payload["FacilityID"])

        response = request(method="POST", url=self.post_create_amplifier_url, data=dict_to_json(payload),
                           headers=self.header, params=querystring)

        print(response.text)
        if sn is not None:
            code_match_update_checklist(str(response.status_code), self.post_create_amplifier_access, self.sheet_name, sn)
        else:
            code_match(str(response.status_code), self.post_create_amplifier_access)

        return response

    def get_by_sn(self, sn_amp, sn=None):

        """
        @desc - Get an Amp by serial number
        :param - sn_amp: Serial number of amp. If set to None, the created amp's sn will be used
        :param sn: TC Serial Number
        :return:
        """
        self.log.info("Endpoint: get_by_sn been called")

        print(sn_amp)
        querystring = {'sn': sn_amp}

        response = request(method="GET", url=self.get_by_sn_url,
                           headers=self.header, params=querystring)
        print(response.text)

        if sn is not None:
            code_match_update_checklist(str(response.status_code), self.get_by_sn_access, self.sheet_name, sn)
        else:
            code_match(str(response.status_code), self.get_by_sn_access)

        return response

    def get_amplifier_types(self, sn=None):
        """
        @desc - Get all types of amplifiers
        :param sn: TC Serial Number
        :return:
        """
        self.log.info("Endpoint: get_amplifier_types has been called")
        response = request(method="GET", url=self.get_amplifier_type_url,
                           headers=self.header)
        print(response.text)

        code_match_update_checklist(str(response.status_code), self.get_amplifier_type_access, self.sheet_name, sn)

        return response

    def get_modes_for_amplifier(self, amplifier_type_id, sn=None):
        """
        @desc - Get an Amp's mode using ID
        :param - amplifier_type_id: ID of the amp type.
        :param sn: TC Serial Number
        :return:
        """
        self.log.info("Endpoint: get_modes_for_amplifier has been called")

        if amplifier_type_id is None:
            amplifier_type_id = "1"

        querystring = {'amplifierTypeId': amplifier_type_id}

        response = request(method="GET", url=self.get_modes_for_amplifier_url,
                           headers=self.header, params=querystring)
        print(response.text)

        code_match_update_checklist(str(response.status_code), self.get_modes_for_amplifier_access, self.sheet_name, sn)

        return response

    def get_sample_rates_for_amplifier(self, amplifier_type_id, sn=None):
        """
        @desc - Get an Amp's sample rate using ID
        :param - amplifier_type_id: ID of the amp type.
        :param sn: TC Serial Number
        :return:
        """
        self.log.info("Endpoint: get_sample_rates_for_amplifier has been called")
        if amplifier_type_id is None:
            amplifier_type_id = "1"

        querystring = {'amplifierTypeId': amplifier_type_id}

        response = request(method="GET", url=self.get_modes_for_amplifier_url,
                           headers=self.header, params=querystring)
        print(response.text)

        code_match_update_checklist(str(response.status_code), self.get_sample_rate_access, self.sheet_name, sn)

        return response

    def get_amplifiers(self, sn=None):
        """
        @desc - Get all Amps' data
        :param sn: TC Serial Number
        :return:
        """
        self.log.info("Endpoint: get_amplifiers has been called")
        response = request(method="GET", url=self.get_amplifiers_url, headers=self.header)
        print(response.text)

        code_match_update_checklist(str(response.status_code), self.get_amplifiers_access, self.sheet_name, sn)

        return response

    def get_amplifier_by_id(self, amp_id, sn=None):
        """
        @desc - Get an Amp's data using ID
        :param - amp_id: ID of the amp.
        :param sn: TC Serial Number
        :return:
        """
        self.log.info("Endpoint: get_amplifier_by_id has been called")
        if amp_id is None:
            url = self.get_amp_id_url + self.amp_id
        else:
            url = self.get_amp_id_url + amp_id

        print(url)
        response = request(method="GET", url=url, headers=self.header)
        print(response.text)
        if sn is not None:
            code_match_update_checklist(str(response.status_code), self.get_amp_id_access, self.sheet_name, sn)
        else:
            code_match(str(response.status_code), self.get_amp_id_access)
        return response

    def update_name_amplifier(self, amp_id=None, new_name=None, sn=None):
        """
        @desc - Get an Amp's data using ID
        :param new_name: New amplifier name
        :param - amp_id: ID of the amp.
        :param sn: TC Serial Number
        :return:
        """
        self.log.info("Endpoint: update_name_amplifier has been called")
        if amp_id is None:
            amp_id = self.amp_id
        if new_name is None:
            new_name = "API_Update_New"

        querystring1 = {'amplifierId': amp_id,
                        'newName': new_name}

        response = request(method="POST", url=self.post_update_amp_name_url, headers=self.header,
                           params=querystring1)
        print(response.text)
        code_match_update_checklist(str(response.status_code), self.post_update_amp_name_access, self.sheet_name, sn)

    def post_amplifier_tc(self, facility_id, sn=None):
        """
        @desc - Calls create an Amplifier using Post method and returns ID
        :param - facility_id: Id of the facility to which the Amp will belong
        :param sn: TC Serial Number
        :return:
        """
        self.log.info("TC method: post_amplifier_tc has been called")
        response = self.post_amplifiers(amp_name="API_TEST",
                                        amp_sn="API_",
                                        facility_id=facility_id,
                                        amp_type_id=1,
                                        sn=sn)

        if str(response.status_code) != "403":
            json_response = response.json()
            return json_response['ID']

    def post_amplifiers(self, amp_name, amp_sn, facility_id, amp_type_id, sn=None):
        """
        @desc - Create an Amplifier using Post method.
        :param - amp_name: Name of the Amp.
        :param - amp_sn: Serial number of the Amp
        :param - facility_id: Id of the facility to which the Amp will belong
        :param - amp_type_id: Amplifier type 1,7,8
        :param sn: TC Serial Number
        :return:
        """
        self.log.info("Endpoint: post_amplifiers has been called")

        payload = {
            "Name": amp_name,
            "SerialNumber": amp_sn,
            "FacilityID": facility_id,
            "AmplifierTypeID": amp_type_id
        }

        response = request(method="POST", url=self.post_amplifiers_url, data=dict_to_json(payload), headers=self.header)
        if sn is not None:
            code_match_update_checklist(str(response.status_code), self.post_amplifiers_access, self.sheet_name, sn)
        else:
            code_match(str(response.status_code), self.post_amplifiers_access)
        return response

    def patch_amplifiers_id(self, amp_id, sn=None):
        """
        @desc - Update an amp's values
        :param amp_id
        :param sn: TC Serial Number
        :return:
        """
        self.log.info("Endpoint: patch_amplifiers_id has been called")
        self.patch_amplifiers_id_url = self.patch_amplifiers_id_url + amp_id

        payload = {
            "Name": "EDITED"
        }

        response = request(method="PATCH", url=self.patch_amplifiers_id_url,
                           data=dict_to_json(payload), headers=self.header)
        print(response.text)

        code_match_update_checklist(str(response.status_code), self.patch_amplifiers_id_access, self.sheet_name, sn)
        return response

    def put_amplifiers_tc(self, amp_id, serial_number, fac_id, created_date, sn=None):
        """
        @desc - Calls put amplifier
        :serial_number:
        :amp_id:
        :facility_id:
        :created_date:
        :param sn: TC Serial Number
        :return:
        """
        self.log.info("TC method: put_amplifiers_tc has been called")
        self.put_amplifiers(amp_id, serial_number, fac_id, created_date, sn)

    def put_amplifiers(self, amp_id, serial_number, facility_id, created_date, sn=None):
        """
        @desc - Details unavailable
        :serial_number:
        :amp_id:
        :facility_id:
        :created_date:
        :param sn: TC Serial Number
        :return:
        """
        self.log.info("Endpoint: put_amplifiers has been called")

        url = self.put_amplifiers_url + amp_id

        payload = {
            "ID": amp_id,
            "SerialNumber": serial_number,
            "Name": "EDIT",
            "AmplifierTypeID": 1,
            "FacilityID": facility_id,
            "DateCreated": created_date
        }

        response = request(method="PUT", url=url,
                           data=dict_to_json(payload), headers=self.header)
        print(response.text)
        if sn is not None:
            code_match_update_checklist(str(response.status_code), self.put_amplifiers_access, self.sheet_name, sn)
        else:
            code_match(str(response.status_code), self.put_amplifiers_access)
        return response

    def delete_amplifier(self, amp_id, sn=None):
        """
        @desc - Deletes an amp. If logged in user cannot delete, the amp will be deleted using super admin's token
        :param - amp_id: ID of the amp to be deleted.
        :param sn: TC Serial Number
        :return:
        """
        self.log.info("Endpoint: delete_amplifier has been called")

        if amp_id is None:
            url = self.delete_amp_url + self.amp_id
        else:
            url = self.delete_amp_url + amp_id

        response = request(method="DELETE", url=url, headers=self.header)

        print(response.text)

        code_match_update_checklist(str(response.status_code), self.delete_amp_access, self.sheet_name, sn)

        return response

    class AmplifierControllerTCs(Exception):
        pass
