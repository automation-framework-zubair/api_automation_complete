import os
import sys

lib_path2 = os.path.abspath(os.path.join('..', '..', 'libraries'))
sys.path.insert(0, lib_path2)
import conf as init

lib_path = os.path.abspath(os.path.join('..', '..', 'libraries', 'common'))
sys.path.insert(0, lib_path)
from logger_robot import Logger as logger_extended
from assertion import code_match, code_match_update_checklist
from request import request

lib_path1 = os.path.abspath(os.path.join('..', '..', 'libraries', 'Rendr'))
sys.path.insert(0, lib_path1)
from TestDataProvider import TestDataProvider
from application_util import headers, dict_to_json


# from _AccountController import _AccountController
# from rendr_util import get_logged_in_user_facility_id, get_logged_in_user_id


class EegThemesControllerTCs(object):
    ROBOT_LIBRARY_SCOPE = 'TEST SUITE'
    log = logger_extended().get_logger('API_Automation')

    data = TestDataProvider()

    facility_id = ""
    logged_in_user_id = ""

    user_token = ""
    header = {}

    sheet_name = "EegThemes"

    # URLs
    url = init.url
    get_eeg_themes_url = ""
    delete_url = ""
    get_by_id_url = ""
    patch_url = ""
    put_url = ""
    post_url = ""

    # Access information
    get_eeg_themes_access = ""
    delete_access = ""
    get_by_id_access = ""
    patch_access = ""
    put_access = ""
    post_access = ""

    con_name = ""
    eeg_theme_id = ""

    def __init__(self):
        self._expression = ''
        self.con_name = "EegThemesController"
        self.log.info(self.con_name + " class has been initialized")
        self.get_eeg_themes_url = self.url + self.data.get_url_of_controller(self.con_name, "Get")
        self.get_by_id_url = self.url + self.data.get_url_of_controller(self.con_name, "GetByID")
        self.patch_url = self.url + self.data.get_url_of_controller(self.con_name, "Patch")
        self.put_url = self.url + self.data.get_url_of_controller(self.con_name, "Put")
        self.post_url = self.url + self.data.get_url_of_controller(self.con_name, "Post")
        self.delete_url = self.url + self.data.get_url_of_controller(self.con_name, "Delete")

    def initialize_access_for_eeg_themes_controller(self, role):
        list_of_roles = ['super admin', 'support', 'production', 'facility admin', 'review doctor', 'lead tech',
                         'field tech', 'office personnel']

        if role in list_of_roles:
            self.log.info("Gathering access info for " + role + " role")
            self.get_eeg_themes_access = self.data.get_access_status_of_controller(
                self.con_name, "Get", role)
            self.get_by_id_access = self.data.get_access_status_of_controller(
                self.con_name, "GetByID", role)
            self.delete_access = self.data.get_access_status_of_controller(
                self.con_name, "Delete", role)
            self.patch_access = self.data.get_access_status_of_controller(
                self.con_name, "Patch", role)
            self.put_access = self.data.get_access_status_of_controller(
                self.con_name, "Put", role)
            self.post_access = self.data.get_access_status_of_controller(
                self.con_name, "Post", role)

        else:
            raise AssertionError("Please provide a valid role name.")

    def set_token_eeg_themes(self, token):
        """
        @desc - method to set token of the user
        :param - token: user's token
        """
        self.log.info("Current user's token : " + token)
        self.header = headers(token)

    def delete_eeg_theme(self, eeg_themes_id, sn=None):
        """
        @desc - Delete eeg theme by id
        :param - eeg_themes_id: id of eeg theme
        :param sn: TC Serial Number

        """
        self.log.info("Endpoint: delete_eeg_theme has been called")
        url = self.delete_url + eeg_themes_id

        response = request(method="DELETE", url=url, headers=self.header)

        print(response.text)

        code_match_update_checklist(str(response.status_code), self.delete_access, 
                                    self.sheet_name, sn)

    def get_eeg_theme_by_id(self, eeg_themes_id, sn=None):
        """
        @desc - Get eeg theme by id
        :param eeg_themes_id: id of eeg theme
        :param sn: TC Serial Number
        """
        self.log.info("Endpoint: get_eeg_theme_by_id has been called")
        url = self.get_by_id_url + eeg_themes_id

        response = request(method="Get", url=url, headers=self.header)

        print(response.text)

        code_match_update_checklist(str(response.status_code), self.get_by_id_access, self.sheet_name, sn)

    def get_eeg_themes(self, sn=None):
        """
        @desc - Get eeg themes
        :param sn: TC Serial Number
        """
        self.log.info("Endpoint: get_eeg_themes has been called")
        response = request(method="Get", url=self.get_eeg_themes_url, headers=self.header)

        print(response.text)

        code_match_update_checklist(str(response.status_code), self.get_eeg_themes_access, self.sheet_name, sn)

    def patch_eeg_theme_tc(self, eeg_themes_id=None, eeg_theme_name=None,
                           background_color=None, label_color=None, facility_id=None, sn=None):
        """
        @desc - method to edit eeg theme by patch method
        :param eeg_themes_id: ID of the theme
        :param eeg_theme_name: Theme's name
        :param background_color: Theme's background color
        :param label_color: Theme's label color
        :param facility_id: Facility ID of the theme
        :param sn: TC Serial Number
        :return:

               """
        self.log.info("TC method: patch_eeg_theme_tc has been called")

        if eeg_theme_name is None and background_color is None and label_color is None and facility_id is None:
            eeg_theme_name = "EDIT_API_TEST_THEME"
            background_color = "black"
            label_color = "yellow"
            facility_id = self.facility_id

        self.patch_eeg_theme(eeg_themes_id, eeg_theme_name, background_color, label_color, facility_id, sn)

    def patch_eeg_theme(self, eeg_themes_id, eeg_theme_name, background_color, label_color, facility_id, sn):
        """
        @desc - method to edit eeg theme by patch method
        :param eeg_themes_id: ID of the theme
        :param eeg_theme_name: Theme's name
        :param background_color: Theme's background color
        :param label_color: Theme's label color
        :param facility_id: Facility ID of the theme
        :param sn: TC Serial Number
        :return:
        
        """
        self.log.info("Endpoint: get_eeg_themes has been called")
        url = self.patch_url + eeg_themes_id

        payload = {
            "ID": eeg_themes_id,
            "Name": eeg_theme_name,
            "BackgroundColor": background_color,
            "LabelColor": label_color,
            "FacilityID": facility_id
        }

        response = request(method="PATCH", url=url, data=dict_to_json(payload), headers=self.header)

        print(response.text)
        code_match_update_checklist(str(response.status_code), self.patch_access, self.sheet_name, sn)

    def put_eeg_theme_tc(self, eeg_themes_id=None, eeg_theme_name=None,
                         background_color=None, label_color=None, facility_id=None,sn=None):
        """
        @desc - Calls put eeh theme
        :param eeg_themes_id: ID of the theme
        :param eeg_theme_name: Theme's name
        :param background_color: Background color of the theme
        :param label_color: Label color of the theme
        :param facility_id: Facility ID of the theme to which it belongs
        :param sn: TC Serial Number
        :return:

        """
        self.log.info("TC method: put_eeg_theme_tc has been called")
        if eeg_theme_name is None and background_color is None and label_color is None and facility_id is None:
            eeg_theme_name = "EDIT_API_TEST_THEME"
            background_color = "black"
            label_color = "yellow"
            facility_id = self.facility_id

        self.put_eeg_theme(eeg_themes_id, eeg_theme_name, background_color, label_color, facility_id, sn)

    def put_eeg_theme(self, eeg_themes_id, eeg_theme_name, background_color, label_color, facility_id, sn):
        """
        @desc - Adds or updates an EEG Theme.
        :param eeg_themes_id: ID of the theme
        :param eeg_theme_name: Theme's name
        :param background_color: Background color of the theme
        :param label_color: Label color of the theme
        :param facility_id: Facility ID of the theme to which it belongs
        :param sn: TC Serial Number
        :return:
        
        """
        self.log.info("Endpoint: put_eeg_theme has been called")
        url = self.put_url + eeg_themes_id

        payload = {
            "ID": eeg_themes_id,
            "Name": eeg_theme_name,
            "BackgroundColor": background_color,
            "LabelColor": label_color,
            "FacilityID": facility_id
        }

        response = request(method="PUT", url=url, data=dict_to_json(payload), headers=self.header)

        print(response.text)
        code_match_update_checklist(str(response.status_code), self.put_access, self.sheet_name, sn)

    def post_eeg_theme_tc(self, facility_id=None, sn=None):
        """
        @desc - Call creates a new eeg theme and returns theme id
        :param facility_id: ID of the facility for which the theme will be saved
        :param sn: TC Serial Number
        """
        self.log.info("TC method: post_eeg_theme_tc has been called")

        if facility_id is None:
            facility_id = init.mobile_med_tek_facility_id
        else:
            self.facility_id = facility_id

        eeg_theme_name = "TEST_API"
        background_color = "black"
        label_color = "yellow"

        response = self.post_eeg_theme(eeg_theme_name, background_color, label_color, facility_id, sn)

        if str(response.status_code) == '201':
            print("EegTheme has been created using POST method")
            json_response = response.json()
            self.eeg_theme_id = json_response['ID']
            print("ID of the newly created EegTheme: {}".format(self.eeg_theme_id))

        return self.eeg_theme_id

    def post_eeg_theme(self, eeg_theme_name, background_color, label_color, facility_id, sn=None):
        """
        @desc - Creates a new eeg theme
        :param eeg_theme_name: name of the theme
        :param facility_id: ID of the facility for which the theme will be saved
        :param background_color: Background color of the new theme
        :param label_color: Label color of the new theme
        :param sn: TC Serial Number
        """
        self.log.info("Endpoint: post_eeg_theme has been called")
        payload = {
            "Name": eeg_theme_name,
            "BackgroundColor": background_color,
            "LabelColor": label_color,
            "FacilityID": facility_id
        }

        response = request(method="POST", url=self.post_url, data=dict_to_json(payload),
                           headers=self.header)

        print(response.text)

        if sn is not None:
            code_match_update_checklist(str(response.status_code), self.post_access, self.sheet_name, sn)
        else:
            code_match(str(response.status_code), self.post_access)

        return response

    class EegThemesControllerTCs(Exception):
        pass
