import os
import sys
import random

lib_path2 = os.path.abspath(os.path.join('..', '..', 'libraries'))
sys.path.insert(0, lib_path2)
import conf as init

lib_path = os.path.abspath(os.path.join('..', '..', 'libraries', 'Common'))
sys.path.insert(0, lib_path)
from logger_robot import Logger as logger_extended
from assertion import code_match, code_match_update_checklist
from request import request

lib_path1 = os.path.abspath(os.path.join('..', '..', 'libraries', 'Rendr'))
sys.path.insert(0, lib_path1)
from TestDataProvider import TestDataProvider
from application_util import headers, dict_to_json, get_formatted_date


class UsersControllerTCs(object):
    ROBOT_LIBRARY_SCOPE = 'TEST SUITE'
    log = logger_extended().get_logger('API_Automation')

    data = TestDataProvider()

    facility_id = ""
    theme_id = ""
    created_user_id = ""

    user_token = ""
    header = {}

    sheet_name = "Users"

    # URLs
    url = init.url
    create_user_url = ""
    delete_facility_user_url = ""
    change_user_role_url = ""
    get_my_roles_url = ""
    get_my_theme_url = ""
    set_my_theme_url = ""
    set_selected_facility_url = ""
    is_user_in_role_url = ""
    get_user_info_url = ""
    delete_url = ""
    get_users_url = ""
    get_users_by_id_url = ""
    get_by_id_url = ""
    patch_url = ""
    post_url = ""
    put_url = ""
    get_user_cpt_monitoring_url = ""

    # Access Information
    create_user_access = ""
    delete_facility_user_access = ""
    change_user_role_access = ""
    get_my_roles_access = ""
    get_my_theme_access = ""
    set_my_theme_access = ""
    set_selected_facility_access = ""
    is_user_in_role_access = ""
    get_user_info_access = ""
    delete_access = ""
    get_users_access = ""
    get_users_by_id_access = ""
    get_by_id_access = ""
    patch_access = ""
    post_access = ""
    put_access = ""
    get_user_cpt_monitoring_access = ""

    con_name = ""

    def __init__(self):
        self._expression = ''
        self.con_name = "UsersController"
        self.log.info(self.con_name + " class has been initialized")
        self.create_user_url = self.url + self.data.get_url_of_controller(self.con_name, "CreateUser")
        self.delete_facility_user_url = self.url + self.data.get_url_of_controller(self.con_name, "DeleteFacilityUser")
        self.change_user_role_url = self.url + self.data.get_url_of_controller(self.con_name, "ChangeUserRole")
        self.get_my_roles_url = self.url + self.data.get_url_of_controller(self.con_name, "GetMyRoles")
        self.get_my_theme_url = self.url + self.data.get_url_of_controller(self.con_name, "GetMyTheme")
        self.set_my_theme_url = self.url + self.data.get_url_of_controller(self.con_name, "SetMyTheme")
        self.set_selected_facility_url = self.url + self.data.get_url_of_controller(self.con_name,
                                                                                    "SetSelectedFacility")
        self.is_user_in_role_url = self.url + self.data.get_url_of_controller(self.con_name, "IsUserInRole")
        self.get_user_info_url = self.url + self.data.get_url_of_controller(self.con_name, "GetUserInfo")
        self.delete_url = self.url + self.data.get_url_of_controller(self.con_name, "Delete")
        self.get_users_url = self.url + self.data.get_url_of_controller(self.con_name, "GetUsers")
        self.get_users_by_id_url = self.url + self.data.get_url_of_controller(self.con_name, "GetUsersByIds")
        self.get_by_id_url = self.url + self.data.get_url_of_controller(self.con_name, "Get?id=")
        self.patch_url = self.url + self.data.get_url_of_controller(self.con_name, "Patch")
        self.post_url = self.url + self.data.get_url_of_controller(self.con_name, "Post")
        self.put_url = self.url + self.data.get_url_of_controller(self.con_name, "Put")
        self.get_user_cpt_monitoring_url = self.url + self.data.get_url_of_controller(self.con_name,
                                                                                      "GetUserForCPTMonitoring")

    def initialize_access_for_users_controller(self, role):
        list_of_roles = ['super admin', 'support', 'production', 'facility admin', 'review doctor', 'lead tech',
                         'field tech', 'office personnel']

        if role in list_of_roles:
            self.log.info("Gathering access info for " + role + " role")
            self.create_user_access = self.data.get_access_status_of_controller(
                self.con_name, "CreateUser", role)
            self.delete_facility_user_access = self.data.get_access_status_of_controller(
                self.con_name, "DeleteFacilityUser", role)
            self.change_user_role_access = self.data.get_access_status_of_controller(
                self.con_name, "ChangeUserRole", role)
            self.get_my_roles_access = self.data.get_access_status_of_controller(
                self.con_name, "GetMyRoles", role)
            self.get_my_theme_access = self.data.get_access_status_of_controller(
                self.con_name, "GetMyTheme", role)
            self.set_my_theme_access = self.data.get_access_status_of_controller(
                self.con_name, "SetMyTheme", role)
            self.set_selected_facility_access = self.data.get_access_status_of_controller(
                self.con_name, "SetSelectedFacility", role)
            self.is_user_in_role_access = self.data.get_access_status_of_controller(
                self.con_name, "IsUserInRole", role)
            self.get_user_info_access = self.data.get_access_status_of_controller(
                self.con_name, "GetUserInfo", role)
            self.delete_access = self.data.get_access_status_of_controller(
                self.con_name, "Delete", role)
            self.get_users_access = self.data.get_access_status_of_controller(
                self.con_name, "GetUsers", role)
            self.get_users_by_id_access = self.data.get_access_status_of_controller(
                self.con_name, "GetUsersByIds", role)
            self.get_by_id_access = self.data.get_access_status_of_controller(
                self.con_name, "Get?id=", role)
            self.patch_access = self.data.get_access_status_of_controller(
                self.con_name, "Patch", role)
            self.post_access = self.data.get_access_status_of_controller(
                self.con_name, "Post", role)
            self.put_access = self.data.get_access_status_of_controller(
                self.con_name, "Put", role)
            self.get_user_cpt_monitoring_access = self.data.get_access_status_of_controller(
                self.con_name, "GetUserForCPTMonitoring", role)

        else:
            raise AssertionError("Please provide a valid role name.")

    def set_token_users(self, token):
        """
        @desc - method to set token of the user
        :param - token: user's token
        :return created user's id
        """
        self.log.info("Current user's token : " + token)
        self.header = headers(token)

    def create_user_tc(self, role, sn=None):
        """
        @desc - test case method of create_user
        :param - role: user's role
        :param - sn: TC Serial Number
        """
        self.log.info("TC method: create_user_tc has been called")
        if role.lower() == "super admin":
            role_id = init.super_admin_role
        elif role.lower() == "support":
            role_id = init.support_role
        elif role.lower() == "production":
            role_id = init.production_role
        elif role.lower() == "facility admin":
            role_id = init.facility_admin_role
        elif role.lower() == "review doctor":
            role_id = init.review_doctor_role
        elif role.lower() == "lead tech":
            role_id = init.lead_tech_role
        elif role.lower() == "field tech":
            role_id = init.field_tech_role
        elif role.lower() == "office personnel":
            role_id = init.office_personnel_role
        else:
            raise AssertionError("Please select the correct user role to log in properly.")
        response = self.create_user(first_name="TestFname",
                                    last_name="TestLname",
                                    email="test" + str(random.random()) + "@testmail.com",
                                    password="Enosis123",
                                    user_role_id=role_id,
                                    sn=sn)
        if str(response.status_code) == "403":
            return
        json_response = response.json()
        return json_response["ID"]

    def create_user(self, first_name, last_name, email, password, user_role_id, sn=None):
        """
        @desc - Create user using  /api/Users/CreateUser endpoint
        :param - user_role_id: user's role
        :param - first_name:
        :param - last_name:
        :param - email:
        :param - password:
        :param sn: TC Serial Number
        :param sn: TC Serial Number
        :rtype :
        """
        self.log.info("Endpoint: create_user has been called")
        payload = {
            "FirstName": first_name,
            "LastName": last_name,
            "Email": email,
            "TwoFactorEnabled": "false",
            "Password": password,
            "ConfirmPassword": password,
            "RoleId": user_role_id,
            "Role": user_role_id
        }

        response = request(method="POST", url=self.create_user_url, headers=self.header, data=dict_to_json(payload))
        print(response.text)
        if sn is not None:
            code_match_update_checklist(str(response.status_code), self.create_user_access, self.sheet_name, sn)
        else:
            code_match(str(response.status_code), self.create_user_access)
        return response

    def change_user_role(self, user_id, role, sn=None):
        """
        @desc - Change user role /api/Users/ChangeUserRole endpoint
        :param - role: role which will be assigned
        :param - user_id:
        :param sn: TC Serial Number
        """
        self.log.info("Endpoint: change_user_role has been called")
        if role.lower() == "super admin":
            role_id = init.super_admin_role
        elif role.lower() == "support":
            role_id = init.support_role
        elif role.lower() == "production":
            role_id = init.production_role
        elif role.lower() == "facility admin":
            role_id = init.facility_admin_role
        elif role.lower() == "review doctor":
            role_id = init.review_doctor_role
        elif role.lower() == "lead tech":
            role_id = init.lead_tech_role
        elif role.lower() == "field tech":
            role_id = init.field_tech_role
        elif role.lower() == "office personnel":
            role_id = init.office_personnel_role
        else:
            raise AssertionError("Please select the correct user role to log in properly.")

        querystring = {'userId': user_id, 'newRoleId': role_id}
        response = request(method="POST", url=self.change_user_role_url, headers=self.header, params=querystring)
        print(response.text)
        code_match_update_checklist(str(response.status_code), self.change_user_role_access, self.sheet_name, sn)
        return response

    def get_my_roles(self, sn=None):
        """
        @desc - Get user role
        :param sn: TC Serial Number
        
        """
        self.log.info("Endpoint: get_my_roles has been called")
        response = request(method="GET", url=self.get_my_roles_url, headers=self.header)
        print(response.text)
        code_match_update_checklist(str(response.status_code), self.get_my_roles_access, self.sheet_name, sn)
        return response

    def get_my_theme(self, sn=None):
        """
        @desc - Get my theme. Set the value of theme_id
        :param sn: TC Serial Number
        
        """
        self.log.info("Endpoint: get_my_theme has been called")
        response = request(method="GET", url=self.get_my_theme_url, headers=self.header)
        print(response.text)
        code_match_update_checklist(str(response.status_code), self.get_my_theme_access, self.sheet_name, sn)
        return response

    def set_my_theme(self, theme_name, sn=None):
        """
        @desc - Set my theme. Theme id is taken from theme_id variable.
        :param theme_name
        :param sn: TC Serial Number
        """
        self.log.info("Endpoint: set_my_theme has been called")
        if theme_name.lower() == "dark":
            theme_id = init.dark_theme_id
        else:
            raise AssertionError("Theme could not be set. Provide Dark as param.")

        querystring = {'id': theme_id}
        response = request(method="POST", url=self.set_my_theme_url, headers=self.header, params=querystring)
        print(response.text)
        code_match_update_checklist(str(response.status_code), self.set_my_theme_access, self.sheet_name, sn)
        return response

    def set_selected_facility(self, facility_id, sn=None):
        """
        @desc - Set selected facility to MobileMedTek. Facility id is taken from init file.
        :param facility_id:
        :param sn: TC Serial Number
        
        """
        self.log.info("Endpoint: set_selected_facility has been called")
        querystring = {'id': facility_id}
        response = request(method="POST", url=self.set_selected_facility_url, headers=self.header, params=querystring)
        print(response.text)
        if sn is not None:
            code_match_update_checklist(str(response.status_code), self.set_selected_facility_access, self.sheet_name,
                                        sn)
        else:
            code_match(str(response.status_code), self.set_selected_facility_access)
        return response

    def is_user_in_role(self, role_name, sn=None):
        """
        @desc - Is user in role endpoint check. Task of this endpoint is unclear.
        ## "SuperAdmin"
        :param role_name:
        :param sn: TC Serial Number
        """
        self.log.info("Endpoint: is_user_in_role has been called")
        querystring = {'roleName': role_name}
        response = request(method="POST", url=self.is_user_in_role_url, headers=self.header, params=querystring)
        print(response.text)
        code_match_update_checklist(str(response.status_code), self.is_user_in_role_access, self.sheet_name, sn)
        return response

    def get_user_info(self, user_id, sn=None):
        """
        @desc - Check get user info with id
        :param user_id:
        :param sn: TC Serial Number
        
        """
        self.log.info("Endpoint: get_user_info has been called")
        querystring = {'id': user_id}
        response = request(method="GET", url=self.get_user_info_url, headers=self.header, params=querystring)
        print(response.text)
        if sn is not None:
            code_match_update_checklist(str(response.status_code), self.get_user_info_access, self.sheet_name, sn)
        else:
            code_match(str(response.status_code), self.get_user_info_access)
        return response

    def get_users(self, sn=None):
        """
        @desc - Get users endpoint check.
        :param sn: TC Serial Number
        """
        self.log.info("Endpoint: get_users has been called")
        response = request(method="GET", url=self.get_users_url, headers=self.header)
        print(response.text)
        code_match_update_checklist(str(response.status_code), self.get_users_access, self.sheet_name, sn)
        return response

    def get_users_by_ids(self, user_id, sn=None):
        """
        @desc - Get user with id endpoint check. Id is set to the id of created user.
        :param user_id: id of the user
        :param sn: TC Serial Number
        """
        self.log.info("Endpoint: get_users_by_ids has been called")
        querystring = {'userIds': user_id}
        response = request(method="GET", url=self.get_users_by_id_url, headers=self.header, params=querystring)
        print(response.text)
        code_match_update_checklist(str(response.status_code), self.get_users_by_id_access, self.sheet_name, sn)
        return response

    def get_user_by_id(self, user_id, sn=None):
        """
        @desc - Get user with id endpoint check. Id is set to the id of created user.
        :param user_id:
        :param sn: TC Serial Number
        """
        self.log.info("Endpoint: get_user_by_id has been called")
        response = request(method="GET", url=self.get_by_id_url + user_id, headers=self.header)
        print(response.text)
        code_match_update_checklist(str(response.status_code), self.get_by_id_access, self.sheet_name, sn)
        return response

    def patch_user(self, user_id, email, first_name, last_name, role, sn=None):
        """
        @desc - Patch user endpoint check. The values were initialized in UsersController class when user was created.
        :param user_id: id of the user
        :param first_name
        :param email
        :param last_name
        :param role
        :param sn: TC Serial Number
        """
        self.log.info("Endpoint: patch_user has been called")
        if role.lower() == "super admin":
            role_id = init.super_admin_role
        elif role.lower() == "support":
            role_id = init.support_role
        elif role.lower() == "production":
            role_id = init.production_role
        elif role.lower() == "facility admin":
            role_id = init.facility_admin_role
        elif role.lower() == "review doctor":
            role_id = init.review_doctor_role
        elif role.lower() == "lead tech":
            role_id = init.lead_tech_role
        elif role.lower() == "field tech":
            role_id = init.field_tech_role
        elif role.lower() == "office personnel":
            role_id = init.office_personnel_role
        else:
            raise AssertionError("Please select the correct user role to log in properly.")

        payload = {
            "ID": user_id,
            "FirstName": first_name,
            "LastName": last_name,
            "Email": email,
            "Role": role_id,
            "RoleId": role_id
        }

        response = request(method="PATCH", url=self.patch_url + user_id, headers=self.header,
                           data=dict_to_json(payload))
        print(response.text)
        code_match_update_checklist(str(response.status_code), self.patch_access, self.sheet_name, sn)
        return response

    def put_user(self, user_id, email, first_name, last_name, role, sn=None):
        """
        @desc - Put user endpoint check. The values were initialized in UsersController class when user was created.
        :param user_id: id of the user
        :param first_name
        :param email
        :param last_name
        :param role
        :param sn: TC Serial Number
        """
        self.log.info("Endpoint: put_user has been called")
        if role.lower() == "super admin":
            role_id = init.super_admin_role
        elif role.lower() == "support":
            role_id = init.support_role
        elif role.lower() == "production":
            role_id = init.production_role
        elif role.lower() == "facility admin":
            role_id = init.facility_admin_role
        elif role.lower() == "review doctor":
            role_id = init.review_doctor_role
        elif role.lower() == "lead tech":
            role_id = init.lead_tech_role
        elif role.lower() == "field tech":
            role_id = init.field_tech_role
        elif role.lower() == "office personnel":
            role_id = init.office_personnel_role
        else:
            raise AssertionError("Please select the correct user role to log in properly.")

        payload = {
            "ID": user_id,
            "FirstName": first_name,
            "LastName": last_name,
            "Email": email,
            "Role": role_id,
            "RoleId": role_id
        }

        response = request(method="PUT", url=self.put_url + user_id, headers=self.header, data=dict_to_json(payload))
        print(response.text)
        code_match_update_checklist(str(response.status_code), self.put_access, self.sheet_name, sn)
        return response

    def post_user_tc(self, role, sn=None):
        """
        @desc - test case method of create_user
        :param - role: user's role
        :param sn: TC Serial Number
        """
        self.log.info("TC method: post_user_tc has been called")
        if role.lower() == "super admin":
            role_id = init.super_admin_role
        elif role.lower() == "support":
            role_id = init.support_role
        elif role.lower() == "production":
            role_id = init.production_role
        elif role.lower() == "facility admin":
            role_id = init.facility_admin_role
        elif role.lower() == "review doctor":
            role_id = init.review_doctor_role
        elif role.lower() == "lead tech":
            role_id = init.lead_tech_role
        elif role.lower() == "field tech":
            role_id = init.field_tech_role
        elif role.lower() == "office personnel":
            role_id = init.office_personnel_role
        else:
            raise AssertionError("Please select the correct user role to log in properly.")
        response = self.post_user(first_name="TestFname",
                                  last_name="TestLname",
                                  email="test" + str(random.random()) + "@testmail.com",
                                  password="Enosis123",
                                  user_role_id=role_id,
                                  sn=sn)
        if str(response.status_code) == "403":
            return
        json_response = response.json()
        return json_response["ID"]

    def post_user(self, password, first_name, last_name, user_role_id, email, sn=None):
        """
        @desc - Post user endpoint check.
        :param password:
        :param first_name:
        :param last_name:
        :param user_role_id:
        :param email:
        :param sn: TC Serial Number
        """
        self.log.info("Endpoint: post_user has been called")
        payload = {
            "Password": password,
            "ConfirmPassword": password,
            "RoleId": user_role_id,
            "FirstName": first_name,
            "LastName": last_name,
            "Email": email
        }

        response = request(method="POST", url=self.post_url, headers=self.header, data=dict_to_json(payload))
        print(response.text)
        code_match_update_checklist(str(response.status_code), self.post_access, self.sheet_name, sn)
        return response

    def delete_facility_user_with_id(self, user_id, sn=None):
        """
        @desc - Delete user from facility check. ID is the id of created user.
        :param - user_id: user's id
        :param sn: TC Serial Number
        
        """
        self.log.info("Endpoint: delete_facility_user_with_id has been called")
        querystring = {'id': user_id}
        response = request(method="DELETE", url=self.delete_facility_user_url, headers=self.header, params=querystring)
        print(response.text)
        code_match_update_checklist(str(response.status_code), self.delete_facility_user_access, self.sheet_name, sn)
        return response

    def delete_user(self, user_id, sn=None):
        """
        @desc - Delete user from RAS check. ID is the id of created user.
        :param sn: TC Serial Number
        :param user_id:
        
        """
        self.log.info("Endpoint: delete_user has been called")
        response = request(method="DELETE", url=self.delete_url + user_id, headers=self.header)
        code_match_update_checklist(str(response.status_code), self.delete_access, self.sheet_name, sn)
        print(response.text)
        return response

    def get_users_cpt_monitoring(self, user_id, sn=None):
        """
        @desc - get user information for CPT monitoring
        :param sn: TC Serial Number
        :param user_id:

        """
        self.log.info("Endpoint: get_users_cpt_monitoring has been called")

        querystring = {'id': user_id}

        response = request(method="GET", url=self.get_user_cpt_monitoring_url, headers=self.header, params=querystring)

        print(response.text)

        code_match_update_checklist(str(response.status_code), self.get_by_id_access, self.sheet_name, sn)

    class UsersControllerTCs(Exception):
        pass
