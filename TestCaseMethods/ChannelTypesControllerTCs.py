import os
import sys

lib_path2 = os.path.abspath(os.path.join('..', '..', 'libraries'))
sys.path.insert(0, lib_path2)
import conf as init

lib_path = os.path.abspath(os.path.join('..', '..', 'libraries', 'common'))
sys.path.insert(0, lib_path)
from logger_robot import Logger as logger_extended
from assertion import code_match, code_match_update_checklist
from request import request

lib_path1 = os.path.abspath(os.path.join('..', '..', 'libraries', 'Rendr'))
sys.path.insert(0, lib_path1)
from TestDataProvider import TestDataProvider
from application_util import headers, dict_to_json
# from _AccountController import _AccountController
# from rendr_util import get_logged_in_user_facility_id, get_logged_in_user_id


class ChannelTypesControllerTCs(object):
    ROBOT_LIBRARY_SCOPE = 'TEST SUITE'
    log = logger_extended().get_logger('API_Automation')

    data = TestDataProvider()

    facility_id = ""
    logged_in_user_id = ""

    user_token = ""
    header = {}

    sheet_name = "ChannelTypes"

    # URLs
    url = init.url
    get_supported_channel_types_url = ""

    # Access information
    get_supported_channel_types_access = ""

    con_name = ""

    def __init__(self):
        self._expression = ''
        self.con_name = "ChannelTypesController"
        self.log.info(self.con_name + " class has been initialized")
        self.get_supported_channel_types_url = self.url + self.data.get_url_of_controller(self.con_name,
                                                                                          "GetSupportedChannelTypes")

    def initialize_access_for_channel_types_controller(self, role):
        list_of_roles = ['super admin', 'support', 'production', 'facility admin', 'review doctor', 'lead tech',
                         'field tech', 'office personnel']

        if role in list_of_roles:
            self.log.info("Gathering access info for " + role + " role")
            self.get_supported_channel_types_access = self.data.get_access_status_of_controller(
                self.con_name, "GetSupportedChannelTypes", role)

        else:
            raise AssertionError("Please provide a valid role name.")

    def set_token_channel_types(self, token):
        """
        @desc - method to set token of the user
        :param - token: user's token
        """
        self.log.info("Current user's token : " + token)
        self.header = headers(token)

    def get_supported_channel_types(self, sn=None):
        """
        @desc - Get the supported channel types
        :param sn: TC Serial Number
        """
        self.log.info("Endpoint: get_supported_channel_types has been called")

        response = request(method="GET", url=self.get_supported_channel_types_url, headers=self.header)

        print(response.text)

        code_match_update_checklist(str(response.status_code), self.get_supported_channel_types_access, \
                                    self.sheet_name, sn)

    class ChannelTypesControllerTCs(Exception):
        pass
