import os
import sys

lib_path = os.path.abspath(os.path.join('..', '..', 'TestCaseMethods'))
sys.path.insert(0, lib_path)
from UsersControllerTCs import UsersControllerTCs
from AccountControllerTCs import AccountControllerTCs
from DevicesControllerTCs import DevicesControllerTCs
from AmplifierControllerTCs import AmplifierControllerTCs
from FacilitiesControllerTCs import FacilitiesControllerTCs
from PatientsControllerTCs import PatientsControllerTCs
from StudiesControllerTCs import StudiesControllerTCs
from FacilityPreferencesControllerTCs import FacilityPreferencesControllerTCs
from FacilityUsersControllerTCs import FacilityUsersControllerTCs
from NotificationsControllerTCs import NotificationsControllerTCs
from LogsControllerTCs import LogsControllerTCs
from EegThemesControllerTCs import EegThemesControllerTCs

lib_path1 = os.path.abspath(os.path.join('..', '..', 'libraries', 'Common'))
sys.path.insert(0, lib_path1)
from logger_robot import Logger as logger_extended
from request import request

lib_path2 = os.path.abspath(os.path.join('..', '..', 'libraries', 'Rendr'))
sys.path.insert(0, lib_path2)
from TestDataProvider import TestDataProvider
from spreadsheet import set_version_number
from application_util import headers, get_formatted_date
from database_utils import connect_to_db, delete_user_by_email, delete_device_by_id, delete_amplifier_by_id, \
    delete_facility_by_id, delete_user_by_id, delete_patient_by_id, get_share_study_id, delete_theme_by_id

lib_path3 = os.path.abspath(os.path.join('..', '..', 'libraries'))
sys.path.insert(0, lib_path3)
import conf as init


class _CommonTCs(object):
    ROBOT_LIBRARY_SCOPE = 'TEST SUITE'
    log = logger_extended().get_logger('API_Automation')
    data = TestDataProvider()

    account = AccountControllerTCs()
    user = UsersControllerTCs()
    device = DevicesControllerTCs()
    amplifier = AmplifierControllerTCs()
    facility = FacilitiesControllerTCs()
    fac_pref = FacilityPreferencesControllerTCs()
    fac_user = FacilityUsersControllerTCs()
    patient = PatientsControllerTCs()
    studies = StudiesControllerTCs()
    notification = NotificationsControllerTCs()
    logs = LogsControllerTCs()
    eegthmes = EegThemesControllerTCs()

    # variables for newly created test users
    id_super_admin = ""
    id_support = ""
    id_production = ""
    id_facility_admin = ""
    id_review_doctor = ""
    id_lead_tech = ""
    id_field_tech = ""
    id_office_personnel = ""

    token_super_admin = ""
    token_support = ""
    token_production = ""
    token_facility_admin = ""
    token_review_doctor = ""
    token_lead_tech = ""
    token_field_tech = ""
    token_office_personal = ""

    facility_id = ""
    theme_id = ""

    super_admin_email = ""
    support_email = ""
    production_email = ""
    facility_admin_email = ""
    review_doctor_email = ""
    lead_tech_email = ""
    field_tech_email = ""
    office_personnel_email = ""
    password = ""

    test_super_admin_email = ""
    test_support_email = ""
    test_production_email = ""
    test_facility_admin_email = ""
    test_review_doctor_email = ""
    test_lead_tech_email = ""
    test_field_tech_email = ""
    test_office_personnel_email = ""

    # Variables for Studies user
    test_share_study_id = ""
    test_study_id = ""
    test_upload_study_id = ""
    test_recording_index = ""
    test_camera_index = ""
    test_video_index = ""
    test_ext = ""
    test_created_study_date = ""
    test_study_state = ""
    test_amplifier_id = ""
    test_sync_state = ""
    test_eeg_theme_id = ""

    def __init__(self):
        self._expression = ''

        self.super_admin_email = self.data.get_rendr_user_email("Super Admin")
        self.support_email = self.data.get_rendr_user_email("Support")
        self.production_email = self.data.get_rendr_user_email("Production")
        self.facility_admin_email = self.data.get_rendr_user_email("Facility Admin")
        self.review_doctor_email = self.data.get_rendr_user_email("Review Doctor")
        self.lead_tech_email = self.data.get_rendr_user_email("Lead Tech")
        self.field_tech_email = self.data.get_rendr_user_email("Field Tech")
        self.office_personnel_email = self.data.get_rendr_user_email("Office Personnel")

        self.password = self.data.get_common_password()

        self.test_super_admin_email = self.data.get_test_user_email("Super Admin")
        self.test_support_email = self.data.get_test_user_email("Support")
        self.test_production_email = self.data.get_test_user_email("Production")
        self.test_facility_admin_email = self.data.get_test_user_email("Facility Admin")
        self.test_review_doctor_email = self.data.get_test_user_email("Review Doctor")
        self.test_lead_tech_email = self.data.get_test_user_email("Lead Tech")
        self.test_field_tech_email = self.data.get_test_user_email("Field Tech")
        self.test_office_personnel_email = self.data.get_test_user_email("Office Personnel")

    def login_to_portal(self, email, password):
        """
        @desc - Login to Portal with email, password. Returns the Access token
        :param email:
        :param password:
        :return:
        """
        response = self.account.login(email=email, password=password)
        if response.status_code == 200:
            self.log.info("User (" + email + ") logged in successfully")
            json_response = response.json()
            access_token = json_response['access_token']
            return access_token
        else:
            raise AssertionError("Could not login using Email:" + email + " and Password: " + password)

    def user_login_by_role(self, role):
        """
        @desc - To login into the system
        :param - role: user's role
        :rtype : token of the logged in user
        """
        if role.lower() == "super admin":
            self.token_super_admin = self.login_to_portal(self.super_admin_email, self.password)
            return self.token_super_admin
        elif role.lower() == "support":
            self.token_support = self.login_to_portal(self.support_email, self.password)
            return self.token_support
        elif role.lower() == "production":
            self.token_production = self.login_to_portal(self.production_email, self.password)
            return self.token_production
        elif role.lower() == "facility admin":
            self.token_facility_admin = self.login_to_portal(self.facility_admin_email, self.password)
            return self.token_facility_admin
        elif role.lower() == "review doctor":
            self.token_review_doctor = self.login_to_portal(self.review_doctor_email, self.password)
            return self.token_review_doctor
        elif role.lower() == "lead tech":
            self.token_lead_tech = self.login_to_portal(self.lead_tech_email, self.password)
            return self.token_lead_tech
        elif role.lower() == "field tech":
            self.token_field_tech = self.login_to_portal(self.field_tech_email, self.password)
            return self.token_field_tech
        elif role.lower() == "office personnel":
            self.token_office_personal = self.login_to_portal(self.office_personnel_email, self.password)
            return self.token_office_personal
        else:
            raise AssertionError("Please select the correct user role to log in properly.")

    def test_user_login_by_role(self, role, password):
        """
        @desc - To login into the system with test users. [Create the user before login]
        :param - role: user's role
        :param - password: user's password. If not provided, program will look for default pw
        :rtype : token of the logged in user
        """
        if password is None:
            password = self.password

        if role.lower() == "super admin":
            token = self.login_to_portal(self.test_super_admin_email, password)
        elif role.lower() == "support":
            token = self.login_to_portal(self.test_support_email, password)
        elif role.lower() == "production":
            token = self.login_to_portal(self.test_production_email, password)
        elif role.lower() == "facility admin":
            token = self.login_to_portal(self.test_facility_admin_email, password)
        elif role.lower() == "review doctor":
            token = self.login_to_portal(self.test_review_doctor_email, password)
        elif role.lower() == "lead tech":
            token = self.login_to_portal(self.test_lead_tech_email, password)
        elif role.lower() == "field tech":
            token = self.login_to_portal(self.test_field_tech_email, password)
        elif role.lower() == "office personnel":
            token = self.login_to_portal(self.test_office_personnel_email, password)
        else:
            raise AssertionError("Please select the correct user role to log in properly.")
        return token

    def get_logged_in_user_id(self, token):
        """
        @desc - This method returns ID of the logged in user
        :param token:
        :return:
        """
        self.account.initialize_access_for_account_controller("super admin")
        self.account.set_token_accounts(token=token)
        response = self.account.user_info()

        json_response = response.json()

        self.log.info("Logged in user's ID: " + json_response['ID'])
        return json_response['ID']

    def get_email_of_user_id(self, user_id):
        """
        @desc - This method returns email of given user ID
        :param user_id:
        :return:
        """
        self.user.initialize_access_for_users_controller("super admin")
        self.user.set_token_users(token=self.token_super_admin)

        response = self.user.get_user_info(user_id=user_id)
        json_response = response.json()

        self.log.info("User's EMAIL: " + json_response['Email'])
        return json_response['Email']

    def get_logged_in_user_email(self, token):
        """
        @desc - This method returns email of the logged in user
        :param token:
        :return:
        """
        self.account.initialize_access_for_account_controller("super admin")
        self.account.set_token_accounts(token=token)
        response = self.account.user_info()

        json_response = response.json()

        self.log.info("Logged in user's Email: " + json_response['Email'])
        return json_response['Email']

    def get_logged_in_user_selected_facility_id(self, token):
        """
        @desc - This method returns email of the logged in user
        :param token:
        :return:
        """
        self.account.initialize_access_for_account_controller("super admin")
        self.account.set_token_accounts(token=token)
        response = self.account.user_info()
        json_response = response.json()

        self.log.info("Logged in user's Facility ID: " + json_response['FacilityID'])
        return json_response['FacilityID']

    def get_version_number(self, token):
        """
        @desc - To get the API version number
        :param token: user token
        :return: API version
        """
        url_to_meta_ep = init.url + "/api/Meta/ApiVersion"
        header = headers(token)

        response = request(method="GET", url=url_to_meta_ep, headers=header)
        if str(response.status_code) == "200":
            print("API Build : " + response.text)
            self.log.info("API Build : " + response.text)
            set_version_number(build=response.text)
            return response.text

        else:
            raise AssertionError("API Build could not be found. "+str(response.status_code)+ " was returned!")

    def get_test_user_email(self, role):
        """
        @desc - this method returns email address of test user based on its role
        :param role: role of the user
        """
        if role.lower() == 'super admin':
            return self.test_super_admin_email
        elif role.lower() == 'support':
            return self.test_support_email
        elif role.lower() == 'production':
            return self.test_production_email
        elif role.lower() == 'facility admin':
            return self.test_facility_admin_email
        elif role.lower() == 'review doctor':
            return self.test_review_doctor_email
        elif role.lower() == 'lead tech':
            return self.test_lead_tech_email
        elif role.lower() == 'field tech':
            return self.test_field_tech_email
        elif role.lower() == 'office personnel':
            return self.test_office_personnel_email
        else:
            raise AssertionError("Please select the correct user role to log in properly.")

    def create_test_user(self, role):
        """
        @desc - Create a test user with specific role
        :param - role: user's role
        :return - Created user's ID
        """
        if self.token_super_admin is None:
            raise AssertionError("Login with Super Admin first to set the token.")
        else:
            token = self.token_super_admin

        if role.lower() == 'super admin':
            role_id = init.super_admin_role
            email = self.test_super_admin_email
        elif role.lower() == 'support':
            role_id = init.support_role
            email = self.test_support_email
        elif role.lower() == 'production':
            role_id = init.production_role
            email = self.test_production_email
        elif role.lower() == 'facility admin':
            role_id = init.facility_admin_role
            email = self.test_facility_admin_email
        elif role.lower() == 'review doctor':
            role_id = init.review_doctor_role
            email = self.test_review_doctor_email
        elif role.lower() == 'lead tech':
            role_id = init.lead_tech_role
            email = self.test_lead_tech_email
        elif role.lower() == 'field tech':
            role_id = init.field_tech_role
            email = self.test_field_tech_email
        elif role.lower() == 'office personnel':
            role_id = init.office_personnel_role
            email = self.test_office_personnel_email
        else:
            raise AssertionError("Please select the correct user role to log in properly.")

        self.user.initialize_access_for_users_controller(role='super admin')
        self.user.set_token_users(token=token)
        response = self.user.create_user(first_name="API_TEST_F_NAME",
                                         last_name="API_TEST_L_NAME",
                                         email=email,
                                         password="Enosis123",
                                         user_role_id=role_id
                                         )
        print(response.text)
        json_response = response.json()
        created_user_id = json_response['ID']
        print("Created user's ID = " + created_user_id)
        return created_user_id

    def delete_user_by_id(self, user_id):
        """
        @desc - this method deletes a user with user_id
        :param user_id
        """
        con = connect_to_db()
        delete_user_by_id(connection=con, id=user_id)

    def delete_test_user(self, role):
        """
        @desc - Delete test user of the given role.
        :param role:
        """
        if role.lower() == "super admin":
            email = self.test_super_admin_email
        elif role.lower() == "support":
            email = self.test_support_email
        elif role.lower() == "production":
            email = self.test_production_email
        elif role.lower() == "facility admin":
            email = self.test_facility_admin_email
        elif role.lower() == "review doctor":
            email = self.test_review_doctor_email
        elif role.lower() == "lead tech":
            email = self.test_lead_tech_email
        elif role.lower() == "field tech":
            email = self.test_field_tech_email
        elif role.lower() == "office personnel":
            email = self.test_office_personnel_email
        else:
            raise AssertionError("Please select the correct user role to log in properly.")

        con = connect_to_db()
        delete_user_by_email(connection=con, email=email)

    def create_test_device(self, fac_id):
        """
        @desc - Create a test device
        :param fac_id: Id of the facility where you want to create the device
        :return - Created Device's ID
        """
        if self.token_super_admin is None:
            raise AssertionError("Login with Super Admin first to set the token.")
        else:
            token = self.token_super_admin

        self.device.initialize_access_for_devices_controller(role='super admin')
        self.device.set_token_devices(token=token)
        response = self.device.post_create_device(device_name="API_TEST_DEVICE", device_sn=get_formatted_date(),
                                                  device_config="999", device_partnumber="999", facility_id=fac_id)
        json_response = response.json()
        device_id = json_response['ID']
        print("Created amplifier's ID = " + device_id)
        return device_id

    def get_test_device_by_sn(self, device_id):
        """
        @desc - get a the created devices's sn
        :param device_id : created device ID
        :return device serial number
        """
        if self.token_super_admin is None:
            raise AssertionError("Login with Super Admin first to set the token.")
        else:
            token = self.token_super_admin

        self.device.initialize_access_for_devices_controller(role='super admin')
        self.device.set_token_devices(token=token)
        response = self.device.get_devices_by_id(device_id)

        json_response = response.json()
        device_sn = json_response['SerialNumber']

        return device_sn

    def delete_test_device(self, device_id):
        """
        @desc - Delete a device with its ID
        :param device_id: test device id
        """
        con = connect_to_db()
        delete_device_by_id(connection=con, device_id=device_id)

    def create_test_amplifier(self, fac_id):
        """
        @desc - Create a test amplifier
        :param fac_id: Id of the facility where you want to create the amplifier
        :return - Created amplifier's ID
        """
        if self.token_super_admin is None:
            raise AssertionError("Login with Super Admin first to set the token.")
        else:
            token = self.token_super_admin

        self.amplifier.initialize_access_for_amplifier_controller(role='super admin')
        self.amplifier.set_token_amplifiers(token=token)

        response = self.amplifier.post_create_amplifier(amp_name="API_TEST_AMP",
                                                        amp_sn=token[0:10],
                                                        facility_id=fac_id,
                                                        amp_type_id=1)

        json_response = response.json()
        amplifier_id = json_response['ID']
        print("Created amplifier's ID = " + amplifier_id)
        return amplifier_id

    def get_amplifier_sn_by_id(self, amp_id):
        """
        @desc - This method returns an amp's sn by its ID
        :return amplifier serial number
        :param amp_id-
        """
        if self.token_super_admin is None:
            raise AssertionError("Login with Super Admin first to set the token.")
        else:
            token = self.token_super_admin

        self.amplifier.initialize_access_for_amplifier_controller(role='super admin')
        self.amplifier.set_token_amplifiers(token=token)

        response = self.amplifier.get_amplifier_by_id(amp_id=amp_id)

        json_response = response.json()
        amplifier_sn = json_response['SerialNumber']
        print("Created amplifier's SerialNumber = " + amplifier_sn)
        return amplifier_sn

    def get_amplifier_created_date_by_id(self, amp_id):
        """
        @desc - This method returns an amp's created date by its ID
        :param amp_id-
        """
        if self.token_super_admin is None:
            raise AssertionError("Login with Super Admin first to set the token.")
        else:
            token = self.token_super_admin

        self.amplifier.initialize_access_for_amplifier_controller(role='super admin')
        self.amplifier.set_token_amplifiers(token=token)

        response = self.amplifier.get_amplifier_by_id(amp_id=amp_id)

        json_response = response.json()
        amplifier_created_date = json_response['DateCreated']
        print("Created amplifier's DateCreated = " + amplifier_created_date)
        return amplifier_created_date

    def delete_test_amplifier(self, amplifier_id):
        """
        @desc - Delete a amplifier with its ID
        :param amplifier_id: amplifier id
        """
        con = connect_to_db()
        delete_amplifier_by_id(connection=con, amplifier_id=amplifier_id)

    def create_test_facility(self, fac_name):
        """
        @desc - Create a test facility
        :param fac_name: name of the facility
        :return - Created facility's ID
        """
        if self.token_super_admin is None:
            raise AssertionError("Login with Super Admin first to set the token.")
        else:
            token = self.token_super_admin

        self.facility.initialize_access_for_facilities_controller(role='super admin')
        self.facility.set_token_facilities(token=token)
        response = self.facility.post_create_facility(fac_name=fac_name + "_" + get_formatted_date(),
                                                      fac_domain=fac_name + "_dom_" + get_formatted_date())

        if str(response.status_code) == "200":
            json_response = response.json()
            facility_id = json_response['ID']
            print("Created facility's ID = " + facility_id)
            return facility_id

    def create_a_device_log(self, dev_id, fac_id):
        """
        @desc-  This method creates a Device log and returns its ID
        :param dev_id:
        :param fac_id:
        """
        if self.token_super_admin is None:
            raise AssertionError("Login with Super Admin first to set the token.")
        else:
            token = self.token_super_admin

        self.logs.initialize_access_for_logs_controller(role='super admin')
        self.logs.set_token_logs(token=token)

        response = self.logs.post_log(dev_id, fac_id)

        if str(response.status_code) == "201":
            json_response = response.json()
            created_log_id = json_response['ID']
            print("CREATED LOG ID = " + created_log_id)
            return created_log_id

    def get_random_facility_log_id(self, fac_id):
        """
        @desc - This method returns random log ID of given Facility
        :param fac_id-
        """
        if self.token_super_admin is None:
            raise AssertionError("Login with Super Admin first to set the token.")
        else:
            token = self.token_super_admin

        self.logs.initialize_access_for_logs_controller(role='super admin')
        self.logs.set_token_logs(token=token)

        response = self.logs.get_facility_logs(fac_id)

        if str(response.status_code) == "200":
            json_response = response.json()
            for each in range(len(json_response)):
                facility_log_id = json_response[each]['ID']
                print("Facility LOG ID = " + facility_log_id)
                return facility_log_id

    def get_random_invitation_id(self, active_only):
        """
        @desc - Test case for Get Invitations. Returns a random Invitation ID
        :param active_only- set to TRUE to get an Active invitation, FALSE for otherwise.
        """
        if self.token_super_admin is None:
            raise AssertionError("Login with Super Admin first to set the token.")
        else:
            token = self.token_super_admin

        self.facility.initialize_access_for_facilities_controller(role='super admin')
        self.facility.set_token_facilities(token=token)

        response = self.facility.get_invitations(active_only)

        if str(response.status_code) == "200":
            json_response = response.json()
            for each in range(len(json_response)):
                invitation_id = json_response[each]['ID']
                print("INVITATION ID = " + invitation_id)
                return invitation_id

    def get_random_supported_facility_camera(self):
        """
        @desc - Returns a random supported facility camera ID
        """
        if self.token_super_admin is None:
            raise AssertionError("Login with Super Admin first to set the token.")
        else:
            token = self.token_super_admin

        self.facility.initialize_access_for_facilities_controller(role='super admin')
        self.facility.set_token_facilities(token=token)

        response = self.facility.get_supported_network_cameras()

        if str(response.status_code) == "200":
            json_response = response.json()
            for each in range(len(json_response)):
                camera_id = json_response[each]['ID']
                print("SELECTED CAMERA ID = " + camera_id)
                return camera_id

    def add_facility_camera_for_deletion_test(self, camera_id, device_id):
        """
        @desc - To add a facility camera to the for provided Device
        :param camera_id: ID of the camera
        :param device_id: ID of the Device
        """
        if self.token_super_admin is None:
            raise AssertionError("Login with Super Admin first to set the token.")
        else:
            token = self.token_super_admin

        self.facility.initialize_access_for_facilities_controller(role='super admin')
        self.facility.set_token_facilities(token=token)

        response = self.facility.add_facility_network_camera(camera_id, device_id)

        if str(response.status_code) == "200":
            print("The camera has been added to ["+device_id+"] Device")

    def get_facility_network_camera_based_on_device_id(self, device_id):
        """
        @desc - Returns a facility camera ID that belongs to the provided device ID
        :param device_id: ID of the device the Facility Camera was created for
        """
        if self.token_super_admin is None:
            raise AssertionError("Login with Super Admin first to set the token.")
        else:
            token = self.token_super_admin

        self.facility.initialize_access_for_facilities_controller(role='super admin')
        self.facility.set_token_facilities(token=token)

        response = self.facility.get_facility_network_cameras()

        if str(response.status_code) == "200":
            json_response = response.json()
            for each in range(len(json_response)):
                if json_response[each]['DeviceId'] == device_id:
                    camera_id = json_response[each]['ID']
                    print("CAMERA ID of the provided device is " + camera_id)
                    return camera_id

    def get_random_facility_preference_id(self, fac_id, preference_type):
        """
        @desc - Test case of Get Facility Preferences
        :param - fac_id: ID of the facility
        :param - preference_type: Type of the Preference (MaxDocumentsPerPatient/MaxPatientDocumentSizeBytes/
        MaxVideoDurationMinutes)
        :return: preference id
        """
        if self.token_super_admin is None:
            raise AssertionError("Login with Super Admin first to set the token.")
        else:
            token = self.token_super_admin

        self.fac_pref.initialize_access_for_facility_preferences_controller(role='super admin')
        self.fac_pref.set_token_facility_preferences(token=token)

        response = self.fac_pref.get_facility_preferences(fac_id, preference_type)
        if str(response.status_code) == "200":
            json_response = response.json()
            for each in range(len(json_response)):
                print(json_response[each]["ID"])
                preference_id = json_response[each]["ID"]
                return preference_id

    def get_random_facility_user_id(self):
        """
        @desc - Executes /api/FacilityUsers/GetUsers endpoint and returns a random facilityUserId
        :return: facility user id
        """
        if self.token_super_admin is None:
            raise AssertionError("Login with Super Admin first to set the token.")
        else:
            token = self.token_super_admin

        self.fac_user.initialize_access_for_facility_users_controller(role='super admin')
        self.fac_user.set_token_facility_users(token=token)

        response = self.fac_user.get_users()
        if str(response.status_code) == "200":
            json_response = response.json()
            print(json_response[0]['ID'] + " is the random FacUserId")
            return json_response[0]['ID']

    def create_a_facility_user(self, user_email):
        """
        @desc - Calls Create Facility User and returns created facility user's user id
        :param user_email:
        """
        if self.token_super_admin is None:
            raise AssertionError("Login with Super Admin first to set the token.")
        else:
            token = self.token_super_admin

        self.fac_user.initialize_access_for_facility_users_controller(role='super admin')
        self.fac_user.set_token_facility_users(token=token)

        response = self.fac_user.post_create_facility_user(user_email)

        if str(response.status_code) == "201":
            print("Facility user with " + user_email + " email was created")
            json_response = response.json()
            return json_response['ID']

    def delete_test_facility(self, facility_id):
        """
        @desc - Delete a facility with its ID
        :param facility_id : facility id
        """
        con = connect_to_db()
        delete_facility_by_id(connection=con, facility_id=facility_id)

    def change_selected_facility(self, facility_id):
        """
        @desc - Change user's current facility to given facility id
        :param facility_id: facility id
        """
        if self.token_super_admin is None:
            raise AssertionError("Login with Super Admin first to set the token.")
        else:
            token = self.token_super_admin

        self.user.initialize_access_for_users_controller(role='super admin')
        self.user.set_token_users(token=token)
        response = self.user.set_selected_facility(facility_id=facility_id)

        json_response = response.json()
        user_name = json_response['Username']
        facility_name = json_response['SelectedFacilityName']
        print(user_name + "'s current facility is now " + facility_name)
        return facility_id

    def create_test_patient(self):
        """
        @desc - Create a test patient
        :return - Created Patient's ID
        """
        if self.token_super_admin is None:
            raise AssertionError("Login with Super Admin first to set the token.")
        else:
            token = self.token_super_admin

        self.patient.initialize_access_for_patients_controller(role='super admin')
        self.patient.set_token_patients(token=token)

        pat_id = "PAT_" + get_formatted_date()

        self.patient.create_patient(f_name="API_Patient", l_name="Controllers", pat_id=pat_id)
        print("pat_id", pat_id)
        response = self.patient.search_patient(search_string=pat_id)
        json_response = response.json()
        print(json_response)
        created_patient_id = json_response[0]['ID']
        print("Patient ID of the newly created Patient : " + created_patient_id)

        return created_patient_id

    def get_patient_custom_id_by_guid(self, pat_id):
        """
        @desc - Create a test patient's custom ID by its GUID
        :return - Created Patient's GUID
        """
        if self.token_super_admin is None:
            raise AssertionError("Login with Super Admin first to set the token.")
        else:
            token = self.token_super_admin

        self.patient.initialize_access_for_patients_controller(role='super admin')
        self.patient.set_token_patients(token=token)

        response = self.patient.get_patient_by_id(pat_id=pat_id)
        json_response = response.json()
        custom_id = json_response['PatientID']
        print("Custom ID :" + custom_id)
        return custom_id

    def get_created_patient_id(self, pat_id):
        """
        @desc - Get created patient's id
        :return - Created Patient's id
        """
        created_patient_id= ""
        if self.token_super_admin is None:
            raise AssertionError("Login with Super Admin first to set the token.")
        else:
            token = self.token_super_admin

        self.patient.initialize_access_for_patients_controller(role='super admin')
        self.patient.set_token_patients(token=token)

        response = self.patient.search_patient(pat_id)
        json_response = response.json()
        print(json_response)
        if len(json_response) == 1:
            created_patient_id = json_response[0]['ID']
            print("Patient ID of the newly created Patient : " + created_patient_id)
        return created_patient_id

    def delete_test_patient(self, pat_id):
        """
        @desc - Delete test patient
        :param pat_id : patient's id
        """
        con = connect_to_db()
        delete_patient_by_id(connection=con, patient_id=pat_id)

    def get_share_study_id(self, study_id, user_id):
        """
        @desc - Gets the shareStudyId from ShareStudies table of database
        :param study_id : test study id
        :param user_id : created test patient id
        """
        con = connect_to_db()
        share_id = get_share_study_id(connection=con, study_id=study_id, user_id=user_id)

        return share_id

    def create_test_theme(self, fac_id):
        """
        @desc - Create a test theme
        :param fac_id: Id of the facility where you want to create the theme will be created
        :return - Created theme's ID
        """
        if self.token_super_admin is None:
            raise AssertionError("Login with Super Admin first to set the token.")
        else:
            token = self.token_super_admin

        self.eegthmes.initialize_access_for_eeg_themes_controller(role='super admin')
        self.eegthmes.set_token_eeg_themes(token)
        response = self.eegthmes.post_eeg_theme(eeg_theme_name="API_TEST_AMP", facility_id=fac_id,
                                                background_color="black", label_color="yellow",)
        if str(response.status_code) == '201':
            print("EegTheme has been created using POST method")
            json_response = response.json()
            self.test_eeg_theme_id = json_response['ID']

        return self.test_eeg_theme_id

    def delete_theme_by_id(self, theme_id):
        """
        @desc - Delete test Theme
        :param theme_id: Theme ID
        """
        con = connect_to_db()

        if theme_id is not init.dark_theme_id or theme_id is not init.light_theme_id:
            delete_theme_by_id(connection=con, theme_id=theme_id)

    class _CommonTCs(Exception):
        pass
