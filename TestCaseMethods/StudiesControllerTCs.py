import os
import sys
import datetime
import time

lib_path2 = os.path.abspath(os.path.join('..', '..', 'libraries'))
sys.path.insert(0, lib_path2)
import conf as init

lib_path = os.path.abspath(os.path.join('..', '..', 'libraries', 'common'))
sys.path.insert(0, lib_path)
from logger_robot import Logger as logger_extended
from assertion import code_match, code_match_update_checklist
from request import request

lib_path1 = os.path.abspath(os.path.join('..', '..', 'libraries', 'Rendr'))
sys.path.insert(0, lib_path1)
from TestDataProvider import TestDataProvider
from application_util import headers, dict_to_json, get_formatted_date


class StudiesControllerTCs(object):
    ROBOT_LIBRARY_SCOPE = 'TEST SUITE'
    log = logger_extended().get_logger('API_Automation')

    data = TestDataProvider()

    study_id = ""

    created_study_date = ""
    facility_id = init.mobile_med_tek_facility_id
    uploaded_study_id = ""
    include_zero_studies = True
    include_deleted_users = True
    study_report_id = ""
    amplifier_id = ""
    date_completed = ""
    study_state = ""
    sync_state = ""
    recording_count = ""
    recording_index = ""
    video_index = ""
    camera_index = ""

    user_token = ""
    header = {}

    sheet_name = "Studies"

    # URLs
    url = init.url
    put_assign_patient_url = ""
    post_study_upload_setup_url = ""
    post_aws_bucket_for_study_url = ""
    get_list_view_url = ""
    get_study_view_url = ""
    get_sync_info_url = ""
    get_serialize_study_url = ""
    post_deserialize_study_url = ""
    post_generate_study_dat_file_url = ""
    get_id_recordingindex_url = ""
    post_update_study_notes_url = ""
    get_study_report_url = ""
    post_update_study_report_url = ""
    post_restore_study_url = ""
    post_create_study_url = ""
    post_change_study_state_url = ""
    get_shared_users_url = ""
    post_remove_study_share_url = ""
    delete_studies_by_id_url = ""
    get_studies_by_id_url = ""
    patch_studies_url = ""
    put_studies_url = ""
    get_studies_url = ""
    post_delete_studies_url = ""

    # Access
    put_assign_patient_access = ""
    post_study_upload_setup_access = ""
    post_aws_bucket_for_study_access = ""
    get_list_view_access = ""
    get_study_view_access = ""
    get_sync_info_access = ""
    get_serialize_study_access = ""
    post_deserialize_study_access = ""
    post_generate_study_dat_file_access = ""
    get_id_recordingindex_access = ""
    post_update_study_notes_access = ""
    get_study_report_access = ""
    post_update_study_report_access = ""
    post_restore_study_access = ""
    post_create_study_access = ""
    post_change_study_state_access = ""
    get_shared_users_access = ""
    post_remove_study_share_access = ""
    delete_studies_by_id_access = ""
    get_studies_by_id_access = ""
    patch_studies_access = ""
    put_studies_access = ""
    get_studies_access = ""
    post_delete_studies_access = ""

    con_name = ""

    def __init__(self):
        self._expression = ''
        self.con_name = "StudiesController"
        self.log.info(self.con_name + " class has been initialized")
        self.put_assign_patient_url = self.url + self.data.get_url_of_controller(
            self.con_name, "AssignPatient")
        self.post_study_upload_setup_url = self.url + self.data.get_url_of_controller(
            self.con_name, "StudyUploadSetup")
        self.post_aws_bucket_for_study_url = self.url + self.data.get_url_of_controller(
            self.con_name, "AwsBucketForStudy")
        self.get_list_view_url = self.url + self.data.get_url_of_controller(
            self.con_name, "ListView")
        self.get_study_view_url = self.url + self.data.get_url_of_controller(
            self.con_name, "StudyView")
        self.get_sync_info_url = self.url + self.data.get_url_of_controller(
            self.con_name, "SyncInfo")
        self.get_serialize_study_url = self.url + self.data.get_url_of_controller(
            self.con_name, "SerializeStudy")
        self.post_deserialize_study_url = self.url + self.data.get_url_of_controller(
            self.con_name, "DeserializeStudy")
        self.post_generate_study_dat_file_url = self.url + self.data.get_url_of_controller(
            self.con_name, "GenerateStudyDatFile")
        self.get_id_recordingindex_url = self.url + self.data.get_url_of_controller(
            self.con_name, "id / recordingIndex / cameraIndex / videoIndex.ext")
        self.post_update_study_notes_url = self.url + self.data.get_url_of_controller(
            self.con_name, "UpdateStudyNotes")
        self.get_study_report_url = self.url + self.data.get_url_of_controller(
            self.con_name, "GetStudyReport")
        self.post_update_study_report_url = self.url + self.data.get_url_of_controller(
            self.con_name, "UpdateStudyReport")
        self.post_restore_study_url = self.url + self.data.get_url_of_controller(
            self.con_name, "RestoreStudy")
        self.post_create_study_url = self.url + self.data.get_url_of_controller(
            self.con_name, "CreateStudy")
        self.post_change_study_state_url = self.url + self.data.get_url_of_controller(
            self.con_name, "ChangeStudyState")
        self.get_shared_users_url = self.url + self.data.get_url_of_controller(
            self.con_name, "GetSharedUsers")
        self.post_remove_study_share_url = self.url + self.data.get_url_of_controller(
            self.con_name, "RemoveStudyShare")
        self.delete_studies_by_id_url = self.url + self.data.get_url_of_controller(
            self.con_name, "Delete")
        self.get_studies_by_id_url = self.url + self.data.get_url_of_controller(
            self.con_name, "Get?id=")
        self.patch_studies_url = self.url + self.data.get_url_of_controller(
            self.con_name, "Patch")
        self.put_studies_url = self.url + self.data.get_url_of_controller(
            self.con_name, "Put")
        self.get_studies_url = self.url + self.data.get_url_of_controller(
            self.con_name, "Get")
        self.post_delete_studies_url = self.url + self.data.get_url_of_controller(
            self.con_name, "DeleteStudy")

    def initialize_access_for_studies_controller(self, role):
        list_of_roles = ['super admin', 'support', 'production', 'facility admin', 'review doctor', 'lead tech',
                         'field tech', 'office personnel']

        if role in list_of_roles:
            self.log.info("Gathering access info for " + role + " role")
            self.put_assign_patient_access = self.data.get_access_status_of_controller(
                self.con_name, "AssignPatient", role)
            self.post_study_upload_setup_access = self.data.get_access_status_of_controller(
                self.con_name, "StudyUploadSetup", role)
            self.post_aws_bucket_for_study_access = self.data.get_access_status_of_controller(
                self.con_name, "AwsBucketForStudy", role)
            self.get_list_view_access = self.data.get_access_status_of_controller(
                self.con_name, "ListView", role)
            self.get_study_view_access = self.data.get_access_status_of_controller(
                self.con_name, "StudyView", role)
            self.get_sync_info_access = self.data.get_access_status_of_controller(
                self.con_name, "SyncInfo", role)
            self.get_serialize_study_access = self.data.get_access_status_of_controller(
                self.con_name, "SerializeStudy", role)
            self.post_deserialize_study_access = self.data.get_access_status_of_controller(
                self.con_name, "DeserializeStudy", role)
            self.post_generate_study_dat_file_access = self.data.get_access_status_of_controller(
                self.con_name, "GenerateStudyDatFile", role)
            self.get_id_recordingindex_access = self.data.get_access_status_of_controller(
                self.con_name, "id / recordingIndex / cameraIndex / videoIndex.ext", role)
            self.post_update_study_notes_access = self.data.get_access_status_of_controller(
                self.con_name, "UpdateStudyNotes", role)
            self.get_study_report_access = self.data.get_access_status_of_controller(
                self.con_name, "GetStudyReport", role)
            self.post_update_study_report_access = self.data.get_access_status_of_controller(
                self.con_name, "UpdateStudyReport", role)
            self.post_restore_study_access = self.data.get_access_status_of_controller(
                self.con_name, "RestoreStudy", role)
            self.post_create_study_access = self.data.get_access_status_of_controller(
                self.con_name, "CreateStudy", role)
            self.post_change_study_state_access = self.data.get_access_status_of_controller(
                self.con_name, "ChangeStudyState", role)
            self.get_shared_users_access = self.data.get_access_status_of_controller(
                self.con_name, "GetSharedUsers", role)
            self.post_remove_study_share_access = self.data.get_access_status_of_controller(
                self.con_name, "RemoveStudyShare", role)
            self.delete_studies_by_id_access = self.data.get_access_status_of_controller(
                self.con_name, "Delete", role)
            self.get_studies_by_id_access = self.data.get_access_status_of_controller(
                self.con_name, "Get?id=", role)
            self.patch_studies_access = self.data.get_access_status_of_controller(
                self.con_name, "Patch", role)
            self.put_studies_access = self.data.get_access_status_of_controller(
                self.con_name, "Put", role)
            self.get_studies_access = self.data.get_access_status_of_controller(
                self.con_name, "Get", role)
            self.post_delete_studies_access = self.data.get_access_status_of_controller(
                self.con_name, "DeleteStudy", role)
        else:
            raise AssertionError("Please provide a valid role name")

    def set_token_studies(self, token):
        """
        @desc - method to set token of the user
        :param - token: user's token
        :return:
        """
        self.log.info("Current user's token : " + token)
        self.header = headers(token)

    def put_assign_patient(self, patient_id, study_id, sn=None):
        """
            @desc - Method to associate a study with a specified patient
            :param patient_id: ID of the patient
            :param study_id: ID of the study
            :param sn: TC Serial Number
            :return:
        """
        self.log.info("Endpoint: put_assign_patient has been called")
        querystring = {
            "patientId": patient_id,
            "studyId": study_id
        }

        response = request(method="PUT", url=self.put_assign_patient_url,
                           headers=self.header, params=querystring)
        print(response.text)

        code_match_update_checklist(str(response.status_code), self.put_assign_patient_access,
                                    self.sheet_name, sn)

        if str(response.status_code) == '200':
            print("Patient has been associated with the given study")
        else:
            print("Logged in user could not access AssignPatient endpoint")

    def post_study_upload_study_setup_tc(self, patient_id=None, study_sync_state=None,
                                         scale=None, use_video=None, sample_rate=None, harness_id=None, sn=None):

        """
        @desc - Method study upload and returns study_id
        :param sample_rate: amplifier sample rate
        :param harness_id: harness id
        :param scale: amplifier scale
        :param use_video: study uses video or not
        :param study_sync_state: sync state id
        :param patient_id: ID of the patient
        :param sn: TC Serial Number
        :return:
        """

        self.log.info("TC method: post_study_upload_study_setup_tc has been called")

        response = self.post_study_upload_study_setup(patient_id, study_sync_state, scale, use_video, sample_rate,
                                                      harness_id, sn)

        if str(response.status_code) == '200':
            json_response = response.json()
            self.uploaded_study_id = json_response['study']['ID']
            return self.uploaded_study_id

        elif str(response.status_code) == '403':
            print('Logged in user could not access StudyUploadSetup endpoint')

    def post_study_upload_study_setup(self, patient_id=None, study_sync_state=None,
                                      scale=None, use_video=None, sample_rate=None, harness_id=None, sn=None):
        """
        @desc - Method to create a study and an associated Amazon s3 bucket for uploading study data
        :param patient_id: ID of the patient
        :param study_sync_state: Study sync state
        :param scale: scale
        :param use_video: Boolean value to determine if video to be used or not
        :param sample_rate: Sample rate of the amplifier
        :param harness_id: Harness ID
        :param sn: TC Serial Number
        :return: STUDY ID
        """
        self.log.info("Endpoint: post_study_upload_study_setup has been called")

        if study_sync_state is None or scale is None \
                or use_video is None or sample_rate is None or harness_id is None:
            study_sync_state = "0"
            scale = "0"
            use_video = "false"
            sample_rate = "256"
            harness_id = "2"

        payload = {
            "PatientID": patient_id,
            "SyncState": study_sync_state,
            "Scale": scale,
            "UseVideo": use_video,
            "SampleRate": sample_rate,
            "CustomElectrodes": [],
            "HarnessID": harness_id,
        }

        response = request(method="POST", url=self.post_study_upload_setup_url,
                           data=dict_to_json(payload), headers=self.header)
        print(response.text)

        code_match_update_checklist(str(response.status_code), self.post_study_upload_setup_access,
                                    self.sheet_name, sn)

        return response

    def post_aws_bucket_for_study(self, study_id, sn=None):
        """
        @desc - method to create an amazon s3 bucket for uploading study data
        :param study_id:
        :param sn: TC Serial Number
        :return:
        """
        self.log.info("Endpoint: post_aws_bucket_for_study has been called")
        if study_id is None:
            study_id = self.uploaded_study_id

        querystring = {"studyId": study_id}

        response = request(method="POST", url=self.post_aws_bucket_for_study_url,
                           headers=self.header, params=querystring)
        print(response.text)

        code_match_update_checklist(str(response.status_code), self.post_aws_bucket_for_study_access,
                                    self.sheet_name, sn)

    def get_search_list_view(self, search_string=None, include_zero_duration=None, include_delete=None, sn=None):
        """
        @desc - Method to search for studies in list view
        :param search_string: search string for given patient or id
        :param include_zero_duration: include zero duration true/false
        :param include_delete: include deleted user true/false
        :param sn: TC Serial Number
        :return:
        """
        self.log.info("Endpoint: get_search_list_view has been called")
        if search_string is None:
            search_string = 'API_Patient'
        if include_zero_duration is None:
            include_zero_duration = self.include_zero_studies
        if include_delete is None:
            include_delete = self.include_deleted_users

        querystring = {
            "searchString": search_string,
            "includeZeroDuration": include_zero_duration,
            "includeDeleted": include_delete
        }

        response = request(method="GET", url=self.get_list_view_url,
                           headers=self.header, params=querystring)

        if sn is not None:
            code_match_update_checklist(str(response.status_code), self.get_list_view_access,
                                        self.sheet_name, sn)
        else:
            code_match(str(response.status_code), self.get_list_view_access)

        return response

    def get_study_view(self, study_id, sn=None):
        """
        @desc - Method to get study view
         :param study_id: ID of the study
         :param sn: TC Serial Number
        :return
        """
        self.log.info("Endpoint: get_study_view has been called")
        if study_id is None:
            study_id = self.study_id

        querystring = {
            "id": study_id
        }

        response = request(method="GET", url=self.get_study_view_url, headers=self.header, params=querystring)
        if sn is not None:
            code_match_update_checklist(str(response.status_code), self.get_study_view_access,
                                        self.sheet_name, sn)
        else:
            code_match(str(response.status_code), self.get_study_view_access)

        return response

    def get_study_sync_info(self, study_id, sn=None):
        """
        @desc - Method to get study view
         :param study_id: ID of the study
         :param sn: TC Serial Number
        :return
        """
        self.log.info("Endpoint: get_study_sync_info has been called")
        if study_id is None:
            study_id = self.study_id

        querystring = {
            "id": study_id
        }

        response = request(method="GET", url=self.get_sync_info_url, headers=self.header, params=querystring)

        print(response.text)

        code_match_update_checklist(str(response.status_code), self.get_sync_info_access,
                                    self.sheet_name, sn)

    def get_serialize_study(self, study_id, sn=None):
        """
         @desc - Method to get study view
        :param study_id: ID of the study
        :param sn: TC Serial Number
        :return
        """
        self.log.info("Endpoint: get_serialize_study has been called")
        if study_id is None:
            study_id = self.study_id

        querystring = {
            "studyId": study_id
        }

        response = request(method="GET", url=self.get_serialize_study_url, headers=self.header, params=querystring)

        print(response.text)

        code_match_update_checklist(str(response.status_code), self.get_serialize_study_access,
                                    self.sheet_name, sn)

    # DO NOT CALL THIS ONE
    def post_deserialize_study(self, serialized_study, sn=None):
        """
        @desc - Method to get deserialize study
        :param serialized_study:
        :param sn: TC Serial Number
        """
        self.log.info("Endpoint: post_deserialize_study has been called")
        payload = {
            "serializedStudy": serialized_study
        }
        response = request(method="POST", url=self.post_deserialize_study_url, headers=self.header,
                           data=dict_to_json(payload))
        print(response.text)

        code_match_update_checklist(str(response.status_code), self.post_deserialize_study_access, self.sheet_name, sn)

        return response

    def post_generate_study_dat_file(self, study_id, sn=None):
        """
        @desc - Method to generate study dat file
        :param study_id: ID of the study
        :param sn: TC Serial Number
        :return
        """
        self.log.info("Endpoint: post_generate_study_dat_file has been called")
        if study_id is None:
            study_id = self.study_id

        querystring = {
            "studyId": study_id
        }

        response = request(method="POST", url=self.post_generate_study_dat_file_url,
                           headers=self.header, params=querystring)

        print(response.text)

        code_match_update_checklist(str(response.status_code), self.post_generate_study_dat_file_access,
                                    self.sheet_name, sn)

    def get_videorecording_index_camera_index(self, study_id, recording_index=None,
                                              camera_index=None, video_index=None, ext=None, sn=None):
        """
        @desc - method to get calculate server video checksum
            :param study_id: ID of the study
            :param recording_index: Recording index
            :param camera_index: Camera index
            :param video_index: Video index
            :param ext: extension
            :param sn: TC Serial Number
            :return:
        """
        self.log.info("Endpoint: get_videorecording_index_camera_index has been called")
        if study_id is None:
            study_id = self.study_id

        if recording_index is None and camera_index is None and video_index is None and ext is None:
            recording_index = self.recording_index
            camera_index = self.camera_index
            video_index = self.video_index

        ext = 'mp4'

        url_format = study_id + "/" + recording_index + "/" + camera_index + "/" + video_index + "." + ext

        url = self.get_id_recordingindex_url + url_format

        print("url ", url)

        response = request(method="GET", url=url, headers=self.header)

        print(response.status_code)

        code_match_update_checklist(str(response.status_code), self.get_id_recordingindex_access,
                                    self.sheet_name, sn)

    def post_update_study_notes(self, study_id=None, sn=None):
        """
        @desc - Method to post update study notes
        :param study_id: ID of the study
        :param sn: TC Serial Number
        :return
        """
        self.log.info("Endpoint: post_update_study_notes has been called")
        if study_id is None:
            study_id = self.study_id

        payload = {
            "StudyID": study_id,
            "ExamId": "Auto ExamID",
            "CptCode": "Auto CPT Codes",
            "TechInitials": "Auto TechInitials",
            "Notes": "Auto Notes"
        }

        response = request(method="POST", url=self.post_update_study_notes_url,
                           data=dict_to_json(payload), headers=self.header)

        print(response.text)

        code_match_update_checklist(str(response.status_code), self.post_update_study_notes_access,
                                    self.sheet_name, sn)

    def get_study_report_tc(self, study_id=None, sn=None):
        """
        @desc - Method call study report and return report id
        :param study_id: ID of the study
        :param sn: TC Serial Number
        :return
        """
        self.log.info("TC method: get_study_report_tc has been called")
        response = self.get_study_report(study_id, sn)

        if str(response.status_code) == '200':
            json_response = response.json()
            self.study_report_id = json_response['ID']
            return self.study_report_id
        else:
            print("No report has been found for this study")

    def get_study_report(self, study_id=None, sn=None):
        """
        @desc - Method to get study report
        :param study_id: ID of the study
        :param sn: TC Serial Number
        :return
        """
        self.log.info("Endpoint: get_study_report has been called")
        if study_id is None:
            study_id = self.study_id

        querystring = {
            "id": study_id
        }

        response = request(method="GET", url=self.get_study_report_url,
                           headers=self.header, params=querystring)
        print(response.text)

        code_match_update_checklist(str(response.status_code), self.get_study_report_access,
                                    self.sheet_name, sn)

        return response

    def post_update_study_report(self, study_id=None, report_id=None, sn=None):
        """
        @desc - Method to post update study report
        :param study_id: ID of the study
        :param report_id: study report id
        :param sn: TC Serial Number
        :return
        """
        self.log.info("Endpoint: post_update_study_report has been called")
        if study_id is None:
            study_id = self.study_id
        if report_id is None:
            report_id = self.study_report_id

        payload = {
            "ID": report_id,
            "Doctor": "Auto Doctor",
            "Notes": "Auto doctor notes",
            "StudyID": study_id
        }

        response = request(method="POST", url=self.post_update_study_report_url,
                           data=dict_to_json(payload), headers=self.header)

        print(response.text)

        code_match_update_checklist(str(response.status_code), self.post_update_study_report_access,
                                    self.sheet_name, sn)

    def post_restore_study(self, study_id=None, facility_id=None, sn=None):
        """
        @desc - Method to restore a study
        :param study_id: ID of the study
        :param facility_id: ID of the facility
        :param sn: TC Serial Number
        :return
        """
        self.log.info("Endpoint: post_restore_study has been called")

        if study_id is None:
            study_id = self.study_id
        if facility_id is None:
            facility_id = self.facility_id

        querystring = {
            "studyId": study_id,
            "facilityId": facility_id
        }

        response = request(method="POST", url=self.post_restore_study_url,
                           headers=self.header, params=querystring)
        print(response.text)

        code_match_update_checklist(str(response.status_code), self.post_restore_study_access,
                                    self.sheet_name, sn)

    # DO NOT CALL THIS ONE
    def post_create_study(self, sn=None):
        """
        @desc - Method to restore a study
        :param sn: TC Serial Number
        :return
        """
        self.log.info("Endpoint: post_create_study has been called")
        created_date = get_formatted_date()

        payload = {
            "RecordingCount": 1,
            "Notes": "string",
            "TechInitials": "string",
            "CptCode": "string",
            "SyncState": 0,
            "TimeZone": "string",
            "LocalUserName": "string",
            "ScaleDecimal": 0,
            "SampleRate": 0,
            "ExamID": "string",
            "StudyTypeID": 0,
            "StudyStateID": 0,
            "HarnessID": 0,
            "DateCreated": created_date
        }

        response = request(method="POST", url=self.post_create_study_url,
                           data=dict_to_json(payload), headers=self.header)
        print(response.text)

        code_match_update_checklist(str(response.status_code), self.post_create_study_access,
                                    self.sheet_name, sn)

    def post_change_study_state(self, study_id=None, study_state=None, sn=None):
        """
        @desc - Method to change study state
        :param study_id: ID of the study
        :param study_state: state of the study
        :param sn: TC Serial Number
        :return
        """
        self.log.info("Endpoint: post_change_study_state has been called")
        if study_id is None:
            study_id = self.study_id

        if study_state is None:
            study_state = "5"

        querystring = {
            "studyId": study_id,
            "studyState": study_state
        }

        response = request(method="POST", url=self.post_change_study_state_url,
                           headers=self.header, params=querystring)
        print(response.text)

        if sn is not None:
            code_match_update_checklist(str(response.status_code), self.post_change_study_state_access,
                                        self.sheet_name, sn)
        else:
            code_match(str(response.status_code), self.post_change_study_state_access)

    def get_shared_studies(self, study_id=None, sn=None):
        """
        @desc - Method to change study state
        :param study_id: ID of the study
        :param sn: TC Serial Number
        :return
        """
        self.log.info("Endpoint: get_shared_studies has been called")
        if study_id is None:
            study_id = self.study_id

        querystring = {
            "studyId": study_id
        }
        response = request(method="GET", url=self.get_shared_users_url,
                           headers=self.header, params=querystring)
        print(response.text)

        code_match_update_checklist(str(response.status_code), self.get_shared_users_access,
                                    self.sheet_name, sn)

    def post_remove_shared_study(self, share_study_id=None, sn=None):
        """
        @desc - Method to change study state
        :param share_study_id: ID of the study
        :param sn: TC Serial Number
        :return
        """
        self.log.info("Endpoint: post_remove_shared_study has been called")
        querystring = {
            "studyShareId": share_study_id
        }
        response = request(method="POST", url=self.post_remove_study_share_url,
                           headers=self.header, params=querystring)
        print(response.text)

        code_match_update_checklist(str(response.status_code), self.post_remove_study_share_access,
                                    self.sheet_name, sn)

    def get_studies_by_id(self, study_id, sn=None):
        """
        @desc - method to get a study by ID
        :param study_id: ID of the study
        :param sn: TC Serial Number
        :return: Response
        """
        self.log.info("Endpoint: get_studies_by_id has been called")
        if study_id is None:
            url = self.get_studies_by_id_url + self.study_id
        else:
            url = self.get_studies_by_id_url + study_id

        self.log.info('METHOD: get_studies_id; url : ' + url)

        response = request(method="GET", url=url, headers=self.header)

        if sn is not None:
            code_match_update_checklist(str(response.status_code), self.get_studies_by_id_access,
                                        self.sheet_name, sn)
        else:
            code_match(str(response.status_code), self.get_studies_by_id_access)

        return response

    def get_studies(self, sn=None):
        """
        @desc - method to get all studies in the current facility
        :param sn: TC Serial Number
        :return: response
        """
        self.log.info("Endpoint: get_studies has been called")
        response = request(method="GET", url=self.get_studies_url, headers=self.header)
        if sn is not None:
            code_match_update_checklist(str(response.status_code), self.get_studies_access,
                                        self.sheet_name, sn)
        else:
            code_match(str(response.status_code), self.get_studies_access)

        return response

    def put_studies_tc(self, study_id, sync_state, study_state, amp_id, fac_id, create_date, sn=None):

        """
        @desc - method to call put studies
        :param amp_id: amplifier id
        :param fac_id: facility id
        :param create_date: created date
        :param study_state: study state
        :param sync_state: sync state
        :param study_id study id
        :param sn: TC Serial Number
        :return: response
        """
        self.log.info("TC methods: authorized has been called")
        self.sync_state = sync_state
        self.study_state = study_state
        self.amplifier_id = amp_id
        self.facility_id = fac_id
        self.created_study_date = create_date
        self.put_studies(study_id, sn)

    def put_studies(self, study_id=None, sn=None):
        """
        @desc - Update study data by Patch
        :param sn: TC Serial Number
        :param study_id
        """
        self.log.info("Endpoint: put_studies has been called")
        if study_id is None:
            url = self.put_studies_url + self.study_id
        else:
            url = self.put_studies_url + study_id

        payload = {
            "ID": study_id,
            "SyncState": self.sync_state,
            "TimeZone": "America/Chicago",
            "ExamID": "null",
            "StudyStateID": self.study_state,
            "AmplifierID": self.amplifier_id,
            "FacilityID": self.facility_id,
            "DateCreated": self.created_study_date
        }

        response = request(method="PUT", url=url, data=dict_to_json(payload),
                           headers=self.header)

        print(response.text)

        code_match_update_checklist(str(response.status_code), self.put_studies_access,
                                    self.sheet_name, sn)

    def patch_studies(self, study_id=None, sn=None):
        """
        @desc - Update study data by Patch
        :param sn: TC Serial Number
        :param study_id
        """

        self.log.info("Endpoint: patch_studies has been called")
        if study_id is None:
            url = self.patch_studies_url + self.study_id
        else:
            url = self.patch_studies_url + study_id

        payload = {
            "ID": study_id,
            "SyncState": self.sync_state,
            "TimeZone": "America/Chicago",
            "ExamID": "null",
            "StudyStateID": self.study_state,
            "AmplifierID": self.amplifier_id,
            "FacilityID": self.facility_id,
            "DateCreated": self.created_study_date
        }

        print("payload ", payload)

        response = request(method="PATCH", url=url, data=dict_to_json(payload),
                           headers=self.header)

        print(response.text)

        code_match_update_checklist(str(response.status_code), self.patch_studies_access,
                                    self.sheet_name, sn)

    def post_delete_study(self, study_id=None, facility_id=None, sn=None):
        """
        @desc - Post deletes a study by ID
        :param study_id: ID of the study
        :param facility_id: faciltiy Id of the study
        :param sn: TC Serial Number
        :return:
        """
        self.log.info("Endpoint: post_delete_study has been called")
        if study_id is None:
            study_id = self.study_id

        if facility_id is None:
            facility_id = self.facility_id

        querystring = {
            "studyId": study_id,
            "facilityID": facility_id
        }

        response = request(method="POST", url=self.post_delete_studies_url,
                           headers=self.header, params=querystring)
        print(response.text)

        code_match_update_checklist(str(response.status_code), self.post_delete_studies_access,
                                    self.sheet_name, sn)

    def delete_study_id(self, study_id=None, sn=None):
        """
        @desc - Deletes a study by ID
        :param study_id: ID of the study
        :param sn: TC Serial Number
        :return:
        """
        self.log.info("Endpoint: delete_study_id has been called")
        if study_id is None:
            study_id = self.study_id

        querystring = {
            "studyId": study_id
        }
        response = request(method="DELETE", url=self.delete_studies_by_id_url,
                           headers=self.header, params=querystring)

        code_match_update_checklist(str(response.status_code), self.delete_studies_by_id_access,
                                    self.sheet_name, sn)

    class StudiesControllerTCs(Exception):
        pass
