import os
import sys

lib_path2 = os.path.abspath(os.path.join('..', '..', 'libraries'))
sys.path.insert(0, lib_path2)
import conf as init

lib_path = os.path.abspath(os.path.join('..', '..', 'libraries', 'common'))
sys.path.insert(0, lib_path)
from logger_robot import Logger as logger_extended
from assertion import code_match, code_match_update_checklist
from request import request

lib_path1 = os.path.abspath(os.path.join('..', '..', 'libraries', 'Rendr'))
sys.path.insert(0, lib_path1)
from TestDataProvider import TestDataProvider
from application_util import headers, dict_to_json
# from _AccountController import _AccountController
# from rendr_util import get_logged_in_user_facility_id, get_logged_in_user_id


class LogsControllerTCs(object):
    ROBOT_LIBRARY_SCOPE = 'TEST SUITE'
    log = logger_extended().get_logger('API_Automation')

    data = TestDataProvider()

    facility_id = ""
    logged_in_user_id = ""

    user_token = ""
    header = {}

    sheet_name = "Logs"

    # URLs
    url = init.url
    add_device_log_item_url = ""
    get_facility_logs_url = ""
    get_device_logs_url = ""
    delete_url = ""
    get_url = ""
    get_by_id_url = ""
    patch_url = ""
    post_url = ""
    put_url = ""

    # Access information
    add_device_log_item_access = ""
    get_facility_logs_access = ""
    get_device_logs_access = ""
    delete_access = ""
    get_access = ""
    get_by_id_access = ""
    patch_access = ""
    post_access = ""
    put_access = ""

    con_name = ""
    log_id = ""

    def __init__(self):
        self._expression = ''
        self.con_name = "LogsController"
        self.log.info(self.con_name + " class has been initialized")
        self.add_device_log_item_url = self.url + self.data.get_url_of_controller(self.con_name, "AddDeviceLogItem")
        self.get_facility_logs_url = self.url + self.data.get_url_of_controller(self.con_name, "GetFacilityLogs")
        self.get_device_logs_url = self.url + self.data.get_url_of_controller(self.con_name, "GetDeviceLogs")
        self.delete_url = self.url + self.data.get_url_of_controller(self.con_name, "Delete")
        self.get_url = self.url + self.data.get_url_of_controller(self.con_name, "Get")
        self.get_by_id_url = self.url + self.data.get_url_of_controller(self.con_name, "Get?id=")
        self.patch_url = self.url + self.data.get_url_of_controller(self.con_name, "Patch")
        self.post_url = self.url + self.data.get_url_of_controller(self.con_name, "Post")
        self.put_url = self.url + self.data.get_url_of_controller(self.con_name, "Put")

    def initialize_access_for_logs_controller(self, role):
        list_of_roles = ['super admin', 'support', 'production', 'facility admin', 'review doctor', 'lead tech',
                         'field tech', 'office personnel']

        if role in list_of_roles:
            self.log.info("Gathering access info for " + role + " role")
            self.add_device_log_item_access = self.data.get_access_status_of_controller(self.con_name,
                                                                                        "AddDeviceLogItem", role)
            self.get_facility_logs_access = self.data.get_access_status_of_controller(self.con_name,
                                                                                      "GetFacilityLogs", role)
            self.get_device_logs_access = self.data.get_access_status_of_controller(self.con_name,
                                                                                    "GetDeviceLogs", role)
            self.delete_access = self.data.get_access_status_of_controller(self.con_name, "Delete", role)
            self.get_access = self.data.get_access_status_of_controller(self.con_name, "Get", role)
            self.get_by_id_access = self.data.get_access_status_of_controller(self.con_name, "Get?id=", role)
            self.patch_access = self.data.get_access_status_of_controller(self.con_name, "Patch", role)
            self.post_access = self.data.get_access_status_of_controller(self.con_name, "Post", role)
            self.put_access = self.data.get_access_status_of_controller(self.con_name, "Put", role)

        else:
            raise AssertionError("Please provide a valid role name.")

    def set_token_logs(self, token):
        """
        @desc - method to set token of the user
        :param - token: user's token
        """
        self.log.info("Current user's token : " + token)
        self.header = headers(token)

    def post_add_device_log_item(self, device_id, facility_id, sn=None):
        """
        @desc - method to create a new  log
        :param device_id: ID of the device for which log files are created
        :param facility_id:
        :param sn: TC Serial Number
        :return:
        """
        self.log.info("Endpoint: post_add_device_log_item has been called")

        payload = {
            "DeviceID": device_id,
            "FacilityID": facility_id
        }

        response = request(method="POST", url=self.add_device_log_item_url, headers=self.header,
                           data=dict_to_json(payload))

        code_match_update_checklist(str(response.status_code), self.add_device_log_item_access,
                                    self.sheet_name, sn)

        return response

    def get_facility_logs(self, facility_id, sn=None):
        """
        @desc - method to get all logs of a facility
        :param facility_id:
        :param sn: TC Serial Number
        """
        self.log.info("Endpoint: get_facility_logs has been called")
        querystring = {'facilityId': facility_id}

        response = request(method="GET", url=self.get_facility_logs_url, headers=self.header,
                           params=querystring)
        print(response.text)
        if sn is not None:
            code_match_update_checklist(str(response.status_code), self.get_facility_logs_access, self.sheet_name, sn)
        else:
            code_match(str(response.status_code), self.get_facility_logs_access)

        return response

    def get_device_logs(self, device_id, max_result, sn=None):
        """
        @desc - method to get all logs of a device
        :param device_id:
        :param max_result:
        :param sn: TC Serial Number
        """
        self.log.info("Endpoint: get_device_logs has been called")
        querystring = {
            'deviceId': device_id,
            'maxSearchResults': max_result
        }

        response = request(method="GET", url=self.get_device_logs_url, headers=self.header,
                           params=querystring)
        print(response.text)

        code_match_update_checklist(str(response.status_code), self.get_device_logs_access,
                                    self.sheet_name, sn)

    def delete_log(self, log_id, sn=None):
        """
        @desc - method to delete log by id
        :param log_id: ID of the log
        :param sn: TC Serial Number
        """
        self.log.info("Endpoint: delete_log has been called")
        url = self.delete_url + log_id

        response = request(method="DELETE", url=url, headers=self.header)
        print(response.text)
        code_match_update_checklist(str(response.status_code), self.delete_access,
                                    self.sheet_name, sn)

    def get_log_by_id(self, log_id, sn=None):
        """
        @desc - method to get log by id
        :param log_id: ID of the log
        :param sn: TC Serial Number
        """
        self.log.info("Endpoint: get_log_by_id has been called")
        url = self.get_by_id_url + log_id

        response = request(method="GET", url=url, headers=self.header)
        print(response.text)

        code_match_update_checklist(str(response.status_code), self.get_by_id_access,
                                    self.sheet_name, sn)

    def get_logs(self, sn=None):
        """
        @desc - method to get logs
        :param sn: TC Serial Number
        """
        self.log.info("Endpoint: get_logs has been called")
        response = request(method="GET", url=self.get_url, headers=self.header)
        print(response.text)

        code_match_update_checklist(str(response.status_code), self.get_access,
                                    self.sheet_name, sn)

    def patch_log(self, log_id, device_id, facility_id, sn=None):
        """
        @desc - method to edit control software log using PATCH method
        :param log_id: ID of the control software log
        :param device_id: ID of the device
        :param facility_id: ID of the facility
        :param sn: TC Serial Number
        """
        self.log.info("Endpoint: patch_log has been called")

        url = self.patch_url + log_id

        payload = {
            "ID": log_id,
            "DeviceID": device_id,
            "UserName": "EDIT",
            "FacilityID": facility_id
        }

        response = request(method="PATCH", url=url, headers=self.header, data=dict_to_json(payload))
        print(response.text)

        code_match_update_checklist(str(response.status_code), self.patch_access,
                                    self.sheet_name, sn)

    def put_log(self, log_id, device_id, facility_id, sn=None):
        """
        @desc - method to edit control software log using PUT method
        :param log_id: ID of the control software log
        :param device_id: ID of the device
        :param facility_id: ID of the facility
        :param sn: TC Serial Number
        """
        self.log.info("Endpoint: put_log has been called")
        url = self.put_url + log_id

        payload = {
            "ID": log_id,
            "DeviceID": device_id,
            "UserName": "EDIT",
            "FacilityID": facility_id
        }

        response = request(method="PUT", url=url, headers=self.header, data=dict_to_json(payload))
        print(response.text)
        code_match_update_checklist(str(response.status_code), self.put_access,
                                    self.sheet_name, sn)

    def post_log(self, device_id, facility_id, sn=None):
        """
        @desc - method to create log using POST method
        :param device_id: ID of the device
        :param facility_id: ID of the facility
        :param sn: TC Serial Number
        """
        self.log.info("Endpoint: post_log has been called")
        payload = {
            "DeviceID": device_id,
            "FacilityID": facility_id
        }

        response = request(method="POST", url=self.post_url, headers=self.header, data=dict_to_json(payload))
        print(response.text)

        if sn is not None:
            code_match_update_checklist(str(response.status_code), self.post_access,
                                        self.sheet_name, sn)
        else:
            code_match(str(response.status_code), self.post_access)

        return response

    class LogsControllerTCs(Exception):
        pass
