import os
import sys


lib_path2 = os.path.abspath(os.path.join('..', '..', 'libraries'))
sys.path.insert(0, lib_path2)
import conf as init

lib_path = os.path.abspath(os.path.join('..', '..', 'libraries', 'common'))
sys.path.insert(0, lib_path)
from logger_robot import Logger as logger_extended
from assertion import code_match, code_match_update_checklist
from request import request

lib_path1 = os.path.abspath(os.path.join('..', '..', 'libraries', 'Rendr'))
sys.path.insert(0, lib_path1)
from TestDataProvider import TestDataProvider
from application_util import headers, dict_to_json


class FacilityUsersControllerTCs(object):
    ROBOT_LIBRARY_SCOPE = 'TEST SUITE'
    log = logger_extended().get_logger('API_Automation')

    data = TestDataProvider()

    facility_id = ""
    logged_in_user_id = ""

    user_token = ""
    header = {}

    sheet_name = "FacilityUsers"

    mmt_facility_id = init.mobile_med_tek_facility_id

    random_user_id_taken_from_users_list = ""
    default_email_address = init.email_address_for_invitation
    facility_user_id = ""
    user_created_date = ""

    # URLs
    url = init.url
    get_users_url = ""
    get_by_fac_user_id_url = ""
    post_create_facility_users_url = ""
    delete_facility_users_url = ""
    get_users_by_id_url = ""
    get_facility_users_url = ""
    patch_facility_users_url = ""
    put_facility_users_url = ""
    post_facility_users_url = ""

    # Access Information
    get_users_access = ""
    get_by_fac_user_id_access = ""
    post_create_facility_user_access = ""
    delete_facility_users_access = ""
    get_user_by_id_access = ""
    get_facility_users_access = ""
    patch_facility_users_access = ""
    put_facility_users_access = ""
    post_facility_users_access = ""

    con_name = ""

    def __init__(self):
        self._expression = ''
        self.con_name = "FacilityUsersController"
        self.log.info(self.con_name + " class has been initialized")
        self.get_users_url = self.url + self.data.get_url_of_controller(self.con_name, "GetUsers")
        self.get_by_fac_user_id_url = self.url + self.data.get_url_of_controller(self.con_name, "GetUser")
        self.post_create_facility_users_url = self.url + self.data.get_url_of_controller(self.con_name,
                                                                                         "CreateFacilityUser")
        self.delete_facility_users_url = self.url + self.data.get_url_of_controller(self.con_name, "Delete")
        self.get_users_by_id_url = self.url + self.data.get_url_of_controller(self.con_name, "Get?id=")
        self.get_facility_users_url = self.url + self.data.get_url_of_controller(self.con_name, "Get")
        self.patch_facility_users_url = self.url + self.data.get_url_of_controller(self.con_name, "Patch")
        self.put_facility_users_url = self.url + self.data.get_url_of_controller(self.con_name, "Put")
        self.post_facility_users_url = self.url + self.data.get_url_of_controller(self.con_name, "Post")

    def initialize_access_for_facility_users_controller(self, role):
        list_of_roles = ['super admin', 'support', 'production', 'facility admin', 'review doctor', 'lead tech',
                         'field tech', 'office personnel']

        if role in list_of_roles:
            self.log.info("Gathering access info for " + role + " role")
            self.get_users_access = self.data.get_access_status_of_controller(
                self.con_name, "GetUsers", role)
            self.get_by_fac_user_id_access = self.data.get_access_status_of_controller(
                self.con_name, "GetUser", role)
            self.post_create_facility_user_access = self.data.get_access_status_of_controller(
                self.con_name, "CreateFacilityUser", role)
            self.delete_facility_users_access = self.data.get_access_status_of_controller(
                self.con_name, "Delete", role)
            self.get_user_by_id_access = self.data.get_access_status_of_controller(
                self.con_name, "Get?id=", role)
            self.get_facility_users_access = self.data.get_access_status_of_controller(
                self.con_name, "Get", role)
            self.patch_facility_users_access = self.data.get_access_status_of_controller(
                self.con_name, "Patch", role)
            self.put_facility_users_access = self.data.get_access_status_of_controller(
                self.con_name, "Put", role)
            self.post_facility_users_access = self.data.get_access_status_of_controller(
                self.con_name, "Post", role)

        else:
            raise AssertionError("Please provide a valid role name.")

    def set_token_facility_users(self, token):
        """
        @desc - method to set token of the user
        :param - token: user's token
        """
        self.log.info("Current user's token : " + token)
        self.header = headers(token)

    def get_users(self, sn=None):
        """
        @desc - Gets all the facility users from the facility that the app user has selected
        :param sn: TC Serial Number
        :return:
        """
        self.log.info("Endpoint: get_users has been called")
        response = request(method="GET", url=self.get_users_url, headers=self.header)

        print(response.text)
        if sn is not None:
            code_match_update_checklist(str(response.status_code), self.get_users_access, self.sheet_name, sn)
        else:
            code_match(str(response.status_code), self.get_users_access)
        return response

    def get_user(self, fac_user_id, sn=None):
        """
        @desc - Gets a particular facility user by ID
        :param fac_user_id:
        :param sn: TC Serial Number
        """
        self.log.info("Endpoint: get_user_by_facility_user_id has been called")
        querystring = {'facilityUserId': fac_user_id}
        response = request(method="GET", url=self.get_by_fac_user_id_url,
                           headers=self.header, params=querystring)
        print(response.text)
        code_match_update_checklist(str(response.status_code), self.get_by_fac_user_id_access, self.sheet_name, sn)
        return response

    def create_facility_user_tc(self, user_email, sn=None):
        """
        @desc - Calls Create Facility User and returns created facility user's user id
        :param user_email:
        :param sn: TC Serial Number
        """
        self.log.info("TC method: create_facility_user_tc has been called")
        response = self.post_create_facility_user(user_email, sn)

        if str(response.status_code) == "201" or str(response.status_code) == "409":
            print("Facility user with " + user_email + " email was created")
            json_response = response.json()
            self.facility_user_id = json_response['ID']

    def post_create_facility_user(self, user_email, sn=None):
        """
        @desc - Creates the facility user as long as an MMT user exists with the given email address
        :param user_email:
        :param sn: TC Serial Number
        :return:
        """
        self.log.info("Endpoint: post_create_facility_user has been called")
        querystring = {'emailAddress': user_email}
        response = request(method="POST", url=self.post_create_facility_users_url,
                           headers=self.header, params=querystring)

        print(response.text)

        if sn is not None:
            code_match_update_checklist(str(response.status_code), self.post_create_facility_user_access, self.sheet_name, sn)
        else:
            code_match(str(response.status_code), self.post_create_facility_user_access)
        return response

    def get_user_by_fac_user_id(self, fac_user_id, sn=None):
        """
        @desc - Gets a particular facility user by ID
        :param fac_user_id:
        :param sn: TC Serial Number
        :return:
        """
        self.log.info("Endpoint: get_user_by_fac_user_id has been called")
        querystring = {'facilityUserId': fac_user_id}

        response = request(method="GET", url=self.get_users_by_id_url,
                           headers=self.header, params=querystring)

        print(response.text)
        code_match_update_checklist(str(response.status_code), self.get_user_by_id_access, self.sheet_name, sn)

    def delete_facility_user(self, fac_user_id, sn=None):
        """
        @desc - Deletes the Facility User, but not the MMT user account
        :param fac_user_id:
        :param sn: TC Serial Number
        :return:
        """
        self.log.info("Endpoint: delete_facility_user has been called")
        url = self.delete_facility_users_url + fac_user_id

        response = request(method="DELETE", url=url, headers=self.header)

        print(response.text)
        code_match_update_checklist(str(response.status_code), self.delete_facility_users_access, self.sheet_name, sn)

    def get_facility_users(self, sn=None):
        """
        @desc- Gets a queryable collection
        :param sn: TC Serial Number
        :return:
        """
        self.log.info("Endpoint: get_facility_users has been called")
        response = request(method="GET", url=self.get_facility_users_url, headers=self.header)

        print(response.text)
        code_match_update_checklist(str(response.status_code), self.get_facility_users_access, self.sheet_name, sn)

    def put_facility_user(self, fac_id, fac_user_id, user_id, sn=None):
        """
        @desc - Update facility user info
        :param fac_id:
        :param fac_user_id:
        :param user_id
        :param sn: TC Serial Number
        :return:
        """
        self.log.info("Endpoint: put_facility_user has been called")
        if fac_id is None:
            fac_id = self.mmt_facility_id

        url = self.put_facility_users_url + fac_user_id

        payload = {

            "ID": fac_user_id,
            "FacilityID": fac_id,
            "UserID": "" + user_id
        }

        response = request(method="PUT", url=url,
                           data=dict_to_json(payload), headers=self.header)

        print(response.text)
        code_match_update_checklist(str(response.status_code), self.put_facility_users_access, self.sheet_name, sn)

    def post_facility_user(self, fac_id, user_id, sn=None):
        """
        @des- Create a facility user
        :param fac_id:
        :param user_id:
        :param sn: TC Serial Number
        :return:
        """
        self.log.info("Endpoint: post_facility_user has been called")

        if fac_id is None:
            fac_id = self.mmt_facility_id

        payload = {
            "FacilityID": fac_id,
            "UserID": user_id
        }

        response = request(method="POST", url=self.post_facility_users_url, data=dict_to_json(payload),
                           headers=self.header)

        print(response.text)

        code_match_update_checklist(str(response.status_code), self.post_facility_users_access, self.sheet_name, sn)
        return response

    def patch_facility_user(self, fac_id, fac_user_id, user_id, sn=None):
        """
        @desc - Update facility user info
        :param fac_id:
        :param user_id:
        :param fac_user_id:
        :param sn: TC Serial Number
        :return:
        """
        self.log.info("Endpoint: patch_facility_user has been called")

        if fac_id is None:
            fac_id = self.mmt_facility_id

        url = self.patch_facility_users_url + fac_user_id
        payload = {
            "ID": fac_user_id,
            "FacilityID": fac_id,
            "UserID": "" + user_id
        }

        response = request(method="PATCH", url=url, data=dict_to_json(payload), headers=self.header)

        print(response.text)
        code_match_update_checklist(str(response.status_code), self.patch_facility_users_access, self.sheet_name, sn)

    class FacilityUsersControllerTCs(Exception):
        pass
