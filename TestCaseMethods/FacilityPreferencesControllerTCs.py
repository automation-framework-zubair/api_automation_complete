import os
import sys

lib_path2 = os.path.abspath(os.path.join('..', '..', 'libraries'))
sys.path.insert(0, lib_path2)
import conf as init

lib_path = os.path.abspath(os.path.join('..', '..', 'libraries', 'common'))
sys.path.insert(0, lib_path)
from logger_robot import Logger as logger_extended
from assertion import code_match, code_match_update_checklist
from request import request

lib_path1 = os.path.abspath(os.path.join('..', '..', 'libraries', 'Rendr'))
sys.path.insert(0, lib_path1)
from TestDataProvider import TestDataProvider
from application_util import headers, dict_to_json


class FacilityPreferencesControllerTCs(object):
    ROBOT_LIBRARY_SCOPE = 'TEST SUITE'
    log = logger_extended().get_logger('API_Automation')

    data = TestDataProvider()

    facility_id = ""
    logged_in_user_id = ""

    user_token = ""
    header = {}

    sheet_name = "FacilityPreferences"

    # URLs
    url = init.url
    get_preferences_url = ""
    get_url = ""
    delete_url = ""
    get_by_id_url = ""
    patch_url = ""
    put_url = ""
    post_url = ""

    # Access information
    get_preferences_access = ""
    get_access = ""
    delete_access = ""
    get_by_id_access = ""
    patch_access = ""
    put_access = ""
    post_access = ""

    con_name = ""

    mmt_facility_id = "e3dd1742-335e-438e-8d8d-65df10447ea3"

    def __init__(self):
        self._expression = ''
        self.con_name = "FacilitiesPreferencesController"
        self.log.info(self.con_name + " class has been initialized")
        self.get_preferences_url = self.url + self.data.get_url_of_controller(self.con_name, "GetPreferences")
        self.get_url = self.url + self.data.get_url_of_controller(self.con_name, "Get")
        self.delete_url = self.url + self.data.get_url_of_controller(self.con_name, "Delete")
        self.get_by_id_url = self.url + self.data.get_url_of_controller(self.con_name, "GetByID")
        self.patch_url = self.url + self.data.get_url_of_controller(self.con_name, "Patch")
        self.put_url = self.url + self.data.get_url_of_controller(self.con_name, "Put")
        self.post_url = self.url + self.data.get_url_of_controller(self.con_name, "Post")

    def initialize_access_for_facility_preferences_controller(self, role):
        list_of_roles = ['super admin', 'support', 'production', 'facility admin', 'review doctor', 'lead tech',
                         'field tech', 'office personnel']

        if role in list_of_roles:
            self.log.info("Gathering access info for " + role + " role")
            self.get_preferences_access = self.data.get_access_status_of_controller(
                self.con_name, "GetPreferences", role)
            self.get_access = self.data.get_access_status_of_controller(
                self.con_name, "Get", role)
            self.delete_access = self.data.get_access_status_of_controller(
                self.con_name, "Delete", role)
            self.get_by_id_access = self.data.get_access_status_of_controller(
                self.con_name, "GetByID", role)
            self.patch_access = self.data.get_access_status_of_controller(
                self.con_name, "Patch", role)
            self.put_access = self.data.get_access_status_of_controller(
                self.con_name, "Put", role)
            self.post_access = self.data.get_access_status_of_controller(
                self.con_name, "Post", role)

        else:
            raise AssertionError("Please provide a valid role name.")

    def set_token_facility_preferences(self, token):
        """
        @desc - method to set token of the user
        :param - token: user's token
        """
        self.log.info("Current user's token : " + token)
        self.header = headers(token)

    def get_facility_preferences_tc(self, fac_id, preference_type, sn=None):
        """
        @desc - Test case of Get Facility Preferences
        :param - fac_id: ID of the facility
        :param - preference_type: Type of the Preference (MaxDocumentsPerPatient/MaxPatientDocumentSizeBytes/
        MaxVideoDurationMinutes)
        :param sn: TC Serial Number
        :return: preference id
        """
        self.log.info("TC method: get_facility_preferences_tc has been called")
        response = self.get_facility_preferences(fac_id, preference_type, sn)
        if str(response.status_code) == "200":
            json_response = response.json()
            for each in range(len(json_response)):
                print(json_response[each]["ID"])
                preference_id = json_response[each]["ID"]
                return preference_id

    def get_facility_preferences(self, fac_id, preference_type, sn=None):
        """
        @desc - Returns requested facility preferences
        :param - fac_id: ID of the facility
        :param - preference_type: Type of the Preference (MaxDocumentsPerPatient/MaxPatientDocumentSizeBytes/
        MaxVideoDurationMinutes)
        :param sn: TC Serial Number
        """
        self.log.info("Endpoint: get_facility_preferences has been called")
        if fac_id is None:
            fac_id = self.mmt_facility_id

        if preference_type not in ("MaxDocumentsPerPatient", "MaxPatientDocumentSizeBytes",
                                   "MaxVideoDurationMinutes"):
            raise AssertionError("Please select the proper preference type")

        querystring = {
            'id': fac_id,
            'typesCsv': preference_type
        }

        response = request(method="GET", url=self.get_preferences_url, headers=self.header, params=querystring)

        print(response.text)
        if sn is not None:
            code_match_update_checklist(str(response.status_code), self.get_preferences_access, self.sheet_name, sn)
        else:
            code_match(str(response.status_code), self.get_preferences_access)
        return response

    def delete_preference(self, pref_id, sn=None):
        """
        @desc - Deletes a facility preference
        :param - pref_id: ID of the preference
        :param sn: TC Serial Number
        """
        self.log.info("Endpoint: delete_preference has been called")
        url = self.delete_url + pref_id

        response = request(method="GET", url=url, headers=self.header)

        print(response.text)
        code_match_update_checklist(str(response.status_code), self.delete_access, self.sheet_name, sn)

    def get_preference_by_id(self, pref_id, sn=None):
        """
        @desc - Get a preference by id
        :param - pref_id: ID of the preference
        :param sn: TC Serial Number
        """
        self.log.info("Endpoint: get_preference_by_id has been called")
        url = self.get_by_id_url + pref_id

        response = request(method="GET", url=url, headers=self.header)

        print(response.text)
        code_match_update_checklist(str(response.status_code), self.get_by_id_access, self.sheet_name, sn)
        return response

    def get_preferences(self, sn=None):
        """
        @desc - Returns list of all preferences
        :param sn: TC Serial Number
        """
        self.log.info("Endpoint: get_preferences has been called")
        response = request(method="GET", url=self.get_url, headers=self.header)

        print(response.text)
        code_match_update_checklist(str(response.status_code), self.get_access, self.sheet_name, sn)

    def patch_preference(self, pref_id, value, sn=None):
        """
        @desc - Edits a preference's value by id
        :param pref_id: id of the preference
        :param value: value that is to modify (70/80)
        :param sn: TC Serial Number
        """
        self.log.info("Endpoint: patch_preference has been called")
        url = self.patch_url + pref_id

        payload = {
            "Value": value
        }

        response = request(method="PATCH", url=url, headers=self.header,
                           data=dict_to_json(payload))
        code_match_update_checklist(str(response.status_code), self.patch_access, self.sheet_name, sn)

    def put_preference(self, pref_id, value, fac_id, pref_type_id, sn=None):
        """
        @desc - Edits a preference's value by id
        :param pref_id: id of the preference
        :param fac_id: id of the facility
        :param pref_type_id: id of the preference type ("1"/"2"/"3")
        :param value: value that is to modify (70/80)
        :param sn: TC Serial Number
        """
        self.log.info("Endpoint: put_preference has been called")
        if fac_id is None:
            fac_id = self.mmt_facility_id

        url = self.put_url + pref_id

        payload = {
            "ID": pref_id,
            "Value": value,
            "FacilityID": fac_id,
            "FacilityPreferenceTypeID": pref_type_id
        }

        response = request(method="PUT", url=url, headers=self.header,
                           data=dict_to_json(payload))
        code_match_update_checklist(str(response.status_code), self.put_access, self.sheet_name, sn)

    def post_preference(self, value, fac_id, pref_type_id, sn=None):
        """
        @desc - Create a preference's value by id
        :param fac_id: id of the facility
        :param pref_type_id: id of the preference type ("1"/"2"/"3")
        :param value: value that is to modify (70/80)
        :param sn: TC Serial Number
        :return: created preference's ID
        """
        self.log.info("Endpoint: post_preference has been called")
        if fac_id is None:
            fac_id = self.mmt_facility_id

        if pref_type_id is None:
            pref_type_id = 1

        payload = {
            "Value": value,
            "FacilityID": fac_id,
            "FacilityPreferenceTypeID": pref_type_id
        }

        response = request(method="POST", url=self.post_url, headers=self.header,
                           data=dict_to_json(payload))
        code_match_update_checklist(str(response.status_code), self.post_access, self.sheet_name, sn)

        return response

    class FacilityPreferencesControllerTCs(Exception):
        pass
