import os
import sys

lib_path2 = os.path.abspath(os.path.join('..', '..', 'libraries'))
sys.path.insert(0, lib_path2)
import conf as init

lib_path = os.path.abspath(os.path.join('..', '..', 'libraries', 'common'))
sys.path.insert(0, lib_path)
from logger_robot import Logger as logger_extended
from assertion import code_match, code_match_update_checklist
from request import request

lib_path1 = os.path.abspath(os.path.join('..', '..', 'libraries', 'Rendr'))
sys.path.insert(0, lib_path1)
from TestDataProvider import TestDataProvider
from application_util import headers, dict_to_json


# from _AccountController import _AccountController
# from rendr_util import get_logged_in_user_facility_id, get_logged_in_user_id


class AuditLogsControllerTCs(object):
    ROBOT_LIBRARY_SCOPE = 'TEST SUITE'
    log = logger_extended().get_logger('API_Automation')

    data = TestDataProvider()

    facility_id = ""
    logged_in_user_id = ""

    user_token = ""
    header = {}

    sheet_name = "AuditLogs"

    # URLs
    url = init.url
    filtered_audit_logs_url = ""
    log_audit_information_url = ""
    add_audit_item_url = ""
    get_audit_logs_url = ""
    delete_url = ""
    get_by_id_url = ""
    patch_url = ""
    put_url = ""
    post_url = ""

    # Access information
    filtered_audit_logs_access = ""
    log_audit_information_access = ""
    add_audit_item_access = ""
    get_audit_logs_access = ""
    delete_access = ""
    get_by_id_access = ""
    patch_access = ""
    put_access = ""
    post_access = ""

    con_name = ""

    def __init__(self):
        self._expression = ''
        self.con_name = "AuditLogsController"
        self.log.info(self.con_name + " class has been initialized")
        self.filtered_audit_logs_url = self.url + self.data.get_url_of_controller(self.con_name, "FilteredAuditLogs")
        self.log_audit_information_url = self.url + self.data.get_url_of_controller(self.con_name,
                                                                                    "LogAuditInformation")
        self.add_audit_item_url = self.url + self.data.get_url_of_controller(self.con_name, "AddAuditLogItem")
        self.get_audit_logs_url = self.url + self.data.get_url_of_controller(self.con_name, "Get")
        self.get_by_id_url = self.url + self.data.get_url_of_controller(self.con_name, "GetByID")
        self.patch_url = self.url + self.data.get_url_of_controller(self.con_name, "Patch")
        self.put_url = self.url + self.data.get_url_of_controller(self.con_name, "Put")
        self.post_url = self.url + self.data.get_url_of_controller(self.con_name, "Post")
        self.delete_url = self.url + self.data.get_url_of_controller(self.con_name, "Delete")

    def initialize_access_for_audit_logs_controller(self, role):
        list_of_roles = ['super admin', 'support', 'production', 'facility admin', 'review doctor', 'lead tech',
                         'field tech', 'office personnel']

        if role in list_of_roles:
            self.log.info("Gathering access info for " + role + " role")
            self.filtered_audit_logs_access = self.data.get_access_status_of_controller(
                self.con_name, "FilteredAuditLogs", role)
            self.log_audit_information_access = self.data.get_access_status_of_controller(
                self.con_name, "LogAuditInformation", role)
            self.add_audit_item_access = self.data.get_access_status_of_controller(
                self.con_name, "AddAuditLogItem", role)
            self.get_audit_logs_access = self.data.get_access_status_of_controller(
                self.con_name, "Get", role)
            self.get_by_id_access = self.data.get_access_status_of_controller(
                self.con_name, "GetByID", role)
            self.delete_access = self.data.get_access_status_of_controller(
                self.con_name, "Delete", role)
            self.patch_access = self.data.get_access_status_of_controller(
                self.con_name, "Patch", role)
            self.put_access = self.data.get_access_status_of_controller(
                self.con_name, "Put", role)
            self.post_access = self.data.get_access_status_of_controller(
                self.con_name, "Post", role)

        else:
            raise AssertionError("Please provide a valid role name.")

    def set_token_audit_logs(self, token):
        """
        @desc - method to set token of the user
        :param - token: user's token
        """
        self.log.info("Current user's token : " + token)
        self.header = headers(token)

    def post_filtered_audit_logs(self, item_type=None, id=None, start_date=None, end_date=None, sn=None):
        """
        @desc - Gets the audit logs for a given ID and set of conditions. The id is a Guid but could be for a patient,
        study, or facility, depending on what is being tracked
        :param - id: ID of the log
        :param - start_date: start date
        :param - end_date: end date
        :param - item_type: type of the Item (Study/PatientDocument)
        :param sn: TC Serial Number
        """
        self.log.info("Endpoint: post_filtered_audit_logs has been called")
        if id is None:
            id = "cpt-monitoring"

        if start_date is None:
            start_date = "2020-03-01T07:08:00.000"

        if end_date is None:
            end_date = "2020-03-30T07:08:00.000"

        if item_type is None:
            item_type = "Study"
            # item_type = "PatientDocument"
            # raise AssertionError("Select a proper Item Type (Study/PatientDocument)!")

        querystring = {
            "id": id,
            "startDateTime": start_date,
            "endDateTime": end_date
        }

        payload = {
            "ItemType": item_type
        }

        response = request("POST", url=self.filtered_audit_logs_url, data=dict_to_json(payload), headers=self.header,
                           params=querystring)

        print(response.text)

        code_match_update_checklist(str(response.status_code), self.filtered_audit_logs_access, self.sheet_name, sn)

    def post_log_audit_information(self, id, sn=None):
        """
        @desc - Create an audit log item. For use when creating an auditing item is required in Phoenix but no other
        API endpoint is used in the change.
        :param sn: TC Serial Number
        :param - id: Study, patient, or facility ID. [Pref: Facility ID]
        
        """
        self.log.info("Endpoint: post_log_audit_information has been called")

        querystring = {'id': id}

        payload = {
            "UserName": "TEST DATA"
        }

        response = request(method="POST", url=self.log_audit_information_url, data=dict_to_json(payload),
                           headers=self.header, params=querystring)

        print(response.text)

        code_match_update_checklist(str(response.status_code), self.log_audit_information_access, self.sheet_name, sn)

    def post_add_audit_log_items(self, group_id, sn=None):
        """
        @desc - Adds the given audit log item from Acquisition to the audit logs on the server
        :param sn: TC Serial Number
        :param - group_id: The group identifier
        """
        self.log.info("Endpoint: post_add_audit_log_items has been called")

        querystring = {'groupId': group_id}

        payload = {
            "UserName": "TEST DATA"
        }

        response = request(method="POST", url=self.add_audit_item_url, data=dict_to_json(payload),
                           headers=self.header, params=querystring)

        print(response.text)
        code_match_update_checklist(str(response.status_code), self.add_audit_item_access, self.sheet_name, sn)

    def delete_audit_log(self, id, sn=None):
        """
        @desc - Delete audit log by id
        :param - id: id of U N K N O W N
        :param sn: TC Serial Number
        """
        self.log.info("Endpoint: delete_audit_log has been called")

        url = self.delete_url + id

        response = request(method="DELETE", url=url, headers=self.header)

        print(response.text)

        code_match_update_checklist(str(response.status_code), self.delete_access, self.sheet_name, sn)

    def get_audit_log_by_id(self, id, sn=None):
        """
        @desc - Get audit log by id
        :param - id: id of Audit Log
        :param sn: TC Serial Number
        """
        self.log.info("Endpoint: get_audit_log_by_id has been called")

        url = self.get_by_id_url + id

        response = request(method="Get", url=url, headers=self.header)

        print(response.text)

        code_match_update_checklist(str(response.status_code), self.get_by_id_access, self.sheet_name, sn)

    def get_audit_logs(self, sn=None):
        """
        @desc - Get audit logs
        :param sn: TC Serial Number
        """
        self.log.info("Endpoint: get_audit_logs has been called")
        response = request(method="Get", url=self.get_audit_logs_url, headers=self.header)

        print(response.text)

        code_match_update_checklist(str(response.status_code), self.get_audit_logs_access, self.sheet_name, sn)

    def patch_audit(self, id, sn=None):
        """
        @desc - Updates Audit info
        :param - id: id of the Audit Log
        :param sn: TC Serial Number
        """
        self.log.info("Endpoint: patch_audit has been called")

        url = self.patch_url + id

        payload = {
            "UserName": "TEST DATA"
        }

        response = request(method="PATCH", url=url, data=dict_to_json(payload), headers=self.header)

        code_match_update_checklist(str(response.status_code), self.patch_access, self.sheet_name, sn)

        print(response.text)

    def put_audit(self, id, sn=None):
        """
        @desc - Adds or updates an Audit info.
        :param - id: id of the Audit Log
        :param sn: TC Serial Number
        
        """
        self.log.info("Endpoint: put_audit has been called")

        url = self.put_url + id

        payload = {
            "UserName": "TEST DATA"
        }

        response = request(method="PUT", url=url, data=dict_to_json(payload), headers=self.header)

        print(response.text)
        code_match_update_checklist(str(response.status_code), self.put_access, self.sheet_name, sn)

    def post_audit_log(self, sn=None):
        """
        @desc - Adds an Audit info.
        :param sn: TC Serial Number
        """
        self.log.info("Endpoint: post_audit_log has been called")

        payload = {
            "UserName": "TEST DATA"
        }

        response = request(method="POST", url=self.post_url, data=dict_to_json(payload),
                           headers=self.header)

        print(response.text)

        code_match_update_checklist(str(response.status_code), self.post_access, self.sheet_name, sn)

    class AuditLogsControllerTCs(Exception):
        pass
