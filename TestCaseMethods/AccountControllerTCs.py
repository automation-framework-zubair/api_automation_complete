import os
import sys
import time

lib_path2 = os.path.abspath(os.path.join('..', '..', 'libraries'))
sys.path.insert(0, lib_path2)
import conf as init

lib_path = os.path.abspath(os.path.join('..', '..', 'libraries', 'common'))
sys.path.insert(0, lib_path)
from logger_robot import Logger as logger_extended
from assertion import code_match_update_checklist, code_match
from request import request

lib_path1 = os.path.abspath(os.path.join('..', '..', 'libraries', 'Rendr'))
sys.path.insert(0, lib_path1)
from TestDataProvider import TestDataProvider
from application_util import headers, dict_to_json


class AccountControllerTCs(object):
    ROBOT_LIBRARY_SCOPE = 'TEST SUITE'
    log = logger_extended().get_logger('API_Automation')

    data = TestDataProvider()

    facility_id = ""
    logged_in_user_id = ""

    user_token = ""
    header = {}

    sheet_name = "Account"

    # URLs
    url = init.url
    authorized_url = ""
    reset_user_password_url = ""
    forget_password_url = ""
    account_recovery_url = ""
    user_info_url = ""
    is_authorized_for_device_use_url = ""
    logout_url = ""
    get_manage_info_url = ""
    change_password_url = ""
    set_password_url = ""
    add_external_login_url = ""
    remove_login_url = ""
    get_external_login_url = ""
    get_external_logins_url = ""
    register_url = ""
    login_url = ""

    # Access information
    authorized_access = ""
    reset_user_password_access = ""
    forgot_password_access = ""
    account_recovery_access = ""
    user_info_access = ""
    is_authorized_for_device_use_access = ""
    logout_access = ""
    get_manage_info_access = ""
    change_password_access = ""
    set_password_access = ""
    add_external_login_access = ""
    remove_login_access = ""
    get_external_login_access = ""
    get_external_logins_access = ""
    register_access = ""
    login_access = ""

    con_name = ""

    def __init__(self):
        self._expression = ''
        self.con_name = "AccountController"
        self.log.info(self.con_name + " class has been initialized")
        self.authorized_url = self.url + self.data.get_url_of_controller(self.con_name, "Authorized")
        self.reset_user_password_url = self.url + self.data.get_url_of_controller(self.con_name, "ResetUserPassword")
        self.forget_password_url = self.url + self.data.get_url_of_controller(self.con_name, "ForgotPassword")
        self.account_recovery_url = self.url + self.data.get_url_of_controller(self.con_name, "AccountRecovery")
        self.user_info_url = self.url + self.data.get_url_of_controller(self.con_name, "UserInfo")
        self.is_authorized_for_device_use_url = self.url + self.data.get_url_of_controller(self.con_name, "IsAuthorizedForDeviceUse")
        self.logout_url = self.url + self.data.get_url_of_controller(self.con_name, "Logout")
        self.get_manage_info_url = self.url + self.data.get_url_of_controller(self.con_name, "GetManageInfo")
        self.change_password_url = self.url + self.data.get_url_of_controller(self.con_name, "ChangePassword")
        self.set_password_url = self.url + self.data.get_url_of_controller(self.con_name, "SetPassword")
        self.add_external_login_url = self.url + self.data.get_url_of_controller(self.con_name, "AddExternalLogin")
        self.remove_login_url = self.url + self.data.get_url_of_controller(self.con_name, "RemoveLogin")
        self.get_external_login_url = self.url + self.data.get_url_of_controller(self.con_name, "GetExternalLogin")
        self.get_external_logins_url = self.url + self.data.get_url_of_controller(self.con_name, "GetExternalLogins")
        self.register_url = self.url + self.data.get_url_of_controller(self.con_name, "Register")
        self.login_url = self.url + self.data.get_url_of_controller(self.con_name, "Login")

    def initialize_access_for_account_controller(self, role):
        list_of_roles = ['super admin', 'support', 'production', 'facility admin', 'review doctor', 'lead tech',
                         'field tech', 'office personnel']

        if role in list_of_roles:
            self.log.info("Gathering access info for " + role + " role")
            self.authorized_access = self.data.get_access_status_of_controller(
                self.con_name, "Authorized", role)
            self.reset_user_password_access = self.data.get_access_status_of_controller(
                self.con_name, "ResetUserPassword", role)
            self.forgot_password_access = self.data.get_access_status_of_controller(
                self.con_name, "ForgotPassword", role)
            self.account_recovery_access = self.data.get_access_status_of_controller(
                self.con_name, "AccountRecovery", role)
            self.user_info_access = self.data.get_access_status_of_controller(
                self.con_name, "UserInfo", role)
            self.is_authorized_for_device_use_access = self.data.get_access_status_of_controller(
                self.con_name, "IsAuthorizedForDeviceUse", role)
            self.logout_access = self.data.get_access_status_of_controller(
                self.con_name, "Logout", role)
            self.get_manage_info_access = self.data.get_access_status_of_controller(
                self.con_name, "GetManageInfo", role)
            self.change_password_access = self.data.get_access_status_of_controller(
                self.con_name, "ChangePassword", role)
            self.set_password_access = self.data.get_access_status_of_controller(
                self.con_name, "SetPassword", role)
            self.add_external_login_access = self.data.get_access_status_of_controller(
                self.con_name, "AddExternalLogin", role)
            self.remove_login_access = self.data.get_access_status_of_controller(
                self.con_name, "RemoveLogin", role)
            self.get_external_login_access = self.data.get_access_status_of_controller(
                self.con_name, "GetExternalLogin", role)
            self.get_external_logins_access = self.data.get_access_status_of_controller(
                self.con_name, "GetExternalLogins", role)
            self.register_access = self.data.get_access_status_of_controller(
                self.con_name, "Register", role)
            self.login_access = self.data.get_access_status_of_controller(
                self.con_name, "Login", role)
        else:
            raise AssertionError("Please provide a valid role name.")

    def set_token_accounts(self, token):
        """
        @desc - method to set token of the user
        :param token: user's token
        """
        self.log.info("Current user's token : " + token)
        self.header = headers(token)

    def login(self, email, password):
        """
        @desc - Method to login to API server.
        :param email
        :param password
        :return
        """
        self.log.info("Endpoint: login has been called")
        payload = "username=" + email + "&password=" + password + "&grant_type=password"

        response = request(method="POST", url=self.login_url, data=payload, headers=self.header)
        print(response.text)
        return response

    def authorized(self, sn=None):
        """
        @desc - Authorized check
        :param sn: TC Serial Number
        :return:
        """
        self.log.info("Endpoint: authorized has been called")

        response = request(method="GET", url=self.authorized_url, headers=self.header)
        print(response.text)
        code_match_update_checklist(str(response.status_code), self.authorized_access, self.sheet_name, sn)
        return response

    def reset_user_password(self, user_id, sn=None):
        """
        @desc - resetting a user password
        :param user_id:
        :param sn: TC Serial Number
        :return:
        """
        self.log.info("Endpoint: reset_user_password has been called")

        payload = ""
        querystring = {'ID': user_id}
        response = request(method="POST", url=self.reset_user_password_url, data=payload, headers=self.header,
                           params=querystring)

        print(response.text)

        code_match_update_checklist(str(response.status_code), self.reset_user_password_access, self.sheet_name, sn)

        if str(response.status_code) == "200":
            changed_password = response.text
            return changed_password.replace('\"', '')

    def forget_password(self, email, sn=None):
        """
        @desc - method to send password reset link
        :param email:
        :param sn: TC Serial Number
        :return:
        """
        self.log.info("Endpoint: forget_password has been called")

        querystring = {'email': email}
        response = request(method="POST", url=self.forget_password_url, headers=self.header,
                           params=querystring)

        print(response.text)

        code_match_update_checklist(str(response.status_code), self.forgot_password_access, self.sheet_name, sn)
        return response

    # DO NOT CALL THIS ONE
    def account_recovery(self, user_id, password, code, sn=None):
        """
        @desc - method to recover account. Not performing now as API cannot prove the param "CODE"
        :param user_id:
        :param password:
        :param code:
        :param sn: TC Serial Number
        :return:
        """
        self.log.info("Endpoint: account_recovery has been called")

        payload = {
            "userId": user_id,
            "password": password,
            "confirmPassword": password,
            "code": code
        }

        response = request(method="POST", url=self.account_recovery_url, headers=self.header,
                           data=dict_to_json(payload))

        print(response.text)
        code_match_update_checklist(str(response.status_code), self.account_recovery_access, self.sheet_name, sn)
        return response

    def user_info(self, sn=None):
        """
        @desc - get user info
        :param sn: TC Serial Number
        :return:
        """
        self.log.info("Endpoint: user_info has been called")

        response = request(method="GET", url=self.user_info_url, headers=self.header)

        print(response.text)
        code_match_update_checklist(str(response.status_code), self.user_info_access, self.sheet_name, sn)
        return response

    def is_authorized_for_device_use(self, device_id, sn=None):
        """
        @desc - checking if device is authorized for device
        :param device_id:
        :param sn: TC Serial Number
        :return:
        """
        self.log.info("Endpoint: is_authorized_for_device_use has been called")

        querystring = {'deviceId': device_id}
        response = request(method="GET", url=self.is_authorized_for_device_use_url, headers=self.header,
                           params=querystring)

        print(response.text)
        code_match_update_checklist(str(response.status_code), self.is_authorized_for_device_use_access, self.sheet_name, sn)
        return response

    # DO NOT CALL THIS ONE
    def manage_info(self, return_url, general_state, sn=None):
        """
        @desc - Not clear
        :param return_url:
        :param general_state:
        :param sn: TC Serial Number
        :return:
        """
        self.log.info("Endpoint: manage_info has been called")

        querystring = {'returnUrl': return_url, 'generateState': general_state}
        response = request(method="GET", url=self.get_manage_info_url, headers=self.header,
                           params=querystring)

        print(response.text)
        code_match_update_checklist(str(response.status_code), self.get_manage_info_access, self.sheet_name, sn)
        return response

    def change_password(self, old_password, new_password, sn=None):
        """
        @desc - To change logged in user password
        :param old_password:
        :param new_password:
        :param sn: TC Serial Number
        :return:
        """
        self.log.info("Endpoint: change_password has been called")

        payload = {
            "OldPassword": old_password,
            "NewPassword": new_password,
            "ConfirmPassword": new_password
        }

        response = request(method="POST", url=self.change_password_url, headers=self.header,
                           data=dict_to_json(payload))

        print(response.text)
        code_match_update_checklist(str(response.status_code), self.change_password_access, self.sheet_name, sn)
        return response

    def set_password(self, new_password, sn=None):
        """
        @desc - To set password
        :param new_password:
        :param sn: TC Serial Number
        :return:
        """
        self.log.info("Endpoint: set_password has been called")

        payload = {
            "NewPassword": new_password,
            "ConfirmPassword": new_password
        }

        response = request(method="POST", url=self.set_password_url, headers=self.header,
                           data=dict_to_json(payload))

        print(response.text)
        code_match_update_checklist(str(response.status_code), self.set_password_access, self.sheet_name, sn)
        return response

    # DO NOT CALL THIS ONE
    def add_external_login(self, external_token, sn=None):
        """
        :return:
        :param external_token:
        :param sn: TC Serial Number
        """
        self.log.info("Endpoint: add_external_login has been called")

        payload = {
            "ExternalAccessToken": external_token
        }

        response = request(method="POST", url=self.add_external_login_url, headers=self.header,
                           data=dict_to_json(payload))

        print(response.text)
        code_match_update_checklist(str(response.status_code), self.add_external_login_access, self.sheet_name, sn)
        return response

    # DO NOT CALL THIS ONE
    def remove_login(self, login_provider, provider_key, sn=None):
        """
        :return:
        :param login_provider:
        :param provider_key:
        :param sn: TC Serial Number
        """
        self.log.info("Endpoint: remove_login has been called")

        payload = {
            "LoginProvider": login_provider,
            "ProviderKey": provider_key
        }

        response = request(method="POST", url=self.remove_login_url, headers=self.header,
                           data=dict_to_json(payload))

        print(response.text)
        code_match_update_checklist(str(response.status_code), self.remove_login_access, self.sheet_name, sn)
        return response

    # DO NOT CALL THIS ONE
    def external_login(self, provider, error, sn=None):
        """
        @desc - Not clear
        :param provider:
        :param error:
        :param sn: TC Serial Number
        :return:
        """
        self.log.info("Endpoint: external_login has been called")

        querystring = {'provider': provider, 'error': error}

        response = request(method="GET", url=self.get_external_login_url, headers=self.header,
                           prarams=querystring)

        print(response.text)
        code_match_update_checklist(str(response.status_code), self.get_external_login_access, self.sheet_name, sn)
        return response

    # DO NOT CALL THIS ONE
    def external_logins(self, return_url, generate_state, sn=None):
        """
        @desc - Not clear
        :param return_url:
        :param generate_state:
        :param sn: TC Serial Number
        :return:
        """
        self.log.info("Endpoint: external_logins has been called")

        querystring = {'returnUrl': return_url, 'generateState': generate_state}

        response = request(method="GET", url=self.get_external_login_url, headers=self.header,
                           prarams=querystring)

        print(response.text)
        code_match_update_checklist(str(response.status_code), self.get_external_logins_access, self.sheet_name, sn)
        return response

    def log_out(self, sn=None):
        """
        @desc - method to log out
        :param sn: TC Serial Number
        :return:
        """
        self.log.info("Endpoint: log_out has been called")

        response = request(method="POST", url=self.logout_url, headers=self.header)

        print(response.text)
        code_match_update_checklist(str(response.status_code), self.logout_access, self.sheet_name, sn)
        return response

    class AccountTCs(Exception):
        pass
