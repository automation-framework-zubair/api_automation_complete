import os
import sys

lib_path2 = os.path.abspath(os.path.join('..', '..', 'libraries'))
sys.path.insert(0, lib_path2)
import conf as init

lib_path = os.path.abspath(os.path.join('..', '..', 'libraries', 'common'))
sys.path.insert(0, lib_path)
from logger_robot import Logger as logger_extended
from assertion import code_match, code_match_update_checklist
from request import request

lib_path1 = os.path.abspath(os.path.join('..', '..', 'libraries', 'Rendr'))
sys.path.insert(0, lib_path1)
from TestDataProvider import TestDataProvider
from application_util import headers, dict_to_json, get_formatted_date


class PatientsControllerTCs(object):
    ROBOT_LIBRARY_SCOPE = 'TEST SUITE'
    log = logger_extended().get_logger('API_Automation')

    data = TestDataProvider()

    facility_id = ""
    logged_in_user_id = ""

    user_token = ""
    header = {}

    sheet_name = "Patients"

    # URLs
    url = init.url
    search_url = ""
    get_documents_url = ""
    set_document_lock_url = ""
    delete_document_url = ""
    get_document_upload_creds_url = ""
    get_document_download_url_url = ""
    log_document_creation_url = ""
    get_patient_for_editing_url = ""
    get_patient_by_patient_id_url = ""
    get_patient_by_study_ids_url = ""
    create_patient_url = ""
    get_patients_url = ""
    get_by_id_url = ""
    delete_patient_url = ""
    patch_url = ""
    put_url = ""
    post_url = ""

    # Access information
    search_access = ""
    get_documents_access = ""
    set_document_lock_access = ""
    delete_document_access = ""
    get_document_upload_creds_access = ""
    get_document_download_url_access = ""
    log_document_creation_access = ""
    get_patient_for_editing_access = ""
    get_patient_by_patient_id_access = ""
    get_patient_by_study_ids_access = ""
    create_patient_access = ""
    get_patients_access = ""
    get_by_id_access = ""
    delete_patient_access = ""
    patch_access = ""
    put_access = ""
    post_access = ""

    con_name = ""
    patient_id = ""

    mmt_facility_id = "e3dd1742-335e-438e-8d8d-65df10447ea3"

    def __init__(self):
        self._expression = ''
        self.con_name = "PatientsController"
        self.log.info(self.con_name + " class has been initialized")
        self.search_url = self.url + self.data.get_url_of_controller(self.con_name, "Search")
        self.get_documents_url = self.url + self.data.get_url_of_controller(self.con_name, "GetDocuments")
        self.set_document_lock_url = self.url + self.data.get_url_of_controller(self.con_name, "SetDocumentLock")
        self.delete_document_url = self.url + self.data.get_url_of_controller(self.con_name, "DeleteDocument")
        self.get_document_upload_creds_url = self.url + self.data.get_url_of_controller(self.con_name,
                                                                                        "GetDocumentUploadCreds")
        self.get_document_download_url_url = self.url + self.data.get_url_of_controller(self.con_name,
                                                                                        "GetDocumentDownloadURL")
        self.log_document_creation_url = self.url + self.data.get_url_of_controller(self.con_name,
                                                                                    "LogDocumentCreation")
        self.get_patient_for_editing_url = self.url + self.data.get_url_of_controller(self.con_name,
                                                                                      "GetPatientForEditing")
        self.get_patient_by_patient_id_url = self.url + self.data.get_url_of_controller(self.con_name,
                                                                                        "GetPatientByPatientId")
        self.get_patient_by_study_ids_url = self.url + self.data.get_url_of_controller(self.con_name,
                                                                                       "GetPatientByStudyIds")
        self.create_patient_url = self.url + self.data.get_url_of_controller(self.con_name, "CreatePatient")
        self.get_patients_url = self.url + self.data.get_url_of_controller(self.con_name, "GetPatients")
        self.get_by_id_url = self.url + self.data.get_url_of_controller(self.con_name, "GetById")
        self.delete_patient_url = self.url + self.data.get_url_of_controller(self.con_name, "Delete")
        self.patch_url = self.url + self.data.get_url_of_controller(self.con_name, "Patch")
        self.put_url = self.url + self.data.get_url_of_controller(self.con_name, "Put")
        self.post_url = self.url + self.data.get_url_of_controller(self.con_name, "Post")

    def initialize_access_for_patients_controller(self, role):
        list_of_roles = ['super admin', 'support', 'production', 'facility admin', 'review doctor', 'lead tech',
                         'field tech', 'office personnel']

        if role in list_of_roles:
            self.log.info("Gathering access info for " + role + " role")
            self.search_access = self.data.get_access_status_of_controller(
                self.con_name, "Search", role)
            self.get_documents_access = self.data.get_access_status_of_controller(
                self.con_name, "GetDocuments", role)
            self.set_document_lock_access = self.data.get_access_status_of_controller(
                self.con_name, "SetDocumentLock", role)
            self.delete_document_access = self.data.get_access_status_of_controller(
                self.con_name, "DeleteDocument", role)
            self.get_document_upload_creds_access = self.data.get_access_status_of_controller(
                self.con_name, "GetDocumentUploadCreds", role)
            self.get_document_download_url_access = self.data.get_access_status_of_controller(
                self.con_name, "GetDocumentDownloadURL", role)
            self.log_document_creation_access = self.data.get_access_status_of_controller(
                self.con_name, "LogDocumentCreation", role)
            self.get_patient_for_editing_access = self.data.get_access_status_of_controller(
                self.con_name, "GetPatientForEditing", role)
            self.get_patient_by_patient_id_access = self.data.get_access_status_of_controller(
                self.con_name, "GetPatientByPatientId", role)
            self.get_patient_by_study_ids_access = self.data.get_access_status_of_controller(
                self.con_name, "GetPatientByStudyIds", role)
            self.create_patient_access = self.data.get_access_status_of_controller(
                self.con_name, "CreatePatient", role)
            self.get_patients_access = self.data.get_access_status_of_controller(
                self.con_name, "GetPatients", role)
            self.get_by_id_access = self.data.get_access_status_of_controller(
                self.con_name, "GetById", role)
            self.delete_patient_access = self.data.get_access_status_of_controller(
                self.con_name, "Delete", role)
            self.patch_access = self.data.get_access_status_of_controller(
                self.con_name, "Patch", role)
            self.put_access = self.data.get_access_status_of_controller(
                self.con_name, "Put", role)
            self.post_access = self.data.get_access_status_of_controller(
                self.con_name, "Post", role)
        else:
            raise AssertionError("Please provide a valid role name.")

    def set_token_patients(self, token):
        """
        @desc - method to set token of the user
        :param token: user's token
        """
        self.log.info("Current user's token : " + token)
        self.header = headers(token)

    def search_patient(self, search_string=None, sn=None):
        """
        @desc - Gets a List of Patients by the search criteria
        :param search_string:
        :param sn: TC Serial Number
        """
        if search_string is None:
            search_string = self.patient_id

        print("search string ", search_string)

        self.log.info("Endpoint: search_patient has been called")
        querystring = {'searchString': search_string}

        response = request(method="GET", url=self.search_url, headers=self.header, params=querystring)

        print(response.text)
        if sn is not None:
            code_match_update_checklist(str(response.status_code), self.search_access, self.sheet_name, sn)
        else:
            code_match(str(response.status_code), self.search_access)
        return response

    def get_documents(self, pat_id, sn=None):
        """
        @desc - Gets a List of PatientDocuments
        :param pat_id:
        :param sn: TC Serial Number
        """
        self.log.info("Endpoint: get_documents has been called")

        querystring = {'patientId': pat_id}

        response = request(method="GET", url=self.get_documents_url, headers=self.header, params=querystring)

        print(response.text)
        if sn is not None:
            code_match_update_checklist(str(response.status_code), self.get_documents_access, self.sheet_name, sn)
        else:
            code_match(str(response.status_code), self.get_documents_access)

    def set_document_lock(self, pat_id, document_name, sn=None):
        """
        @desc - Sets the lock on a PatientDocument.
        :param pat_id
        :param document_name
        :param sn: TC Serial Number
        """
        self.log.info("Endpoint: set_document_lock has been called")

        querystring = {
            'patientId': pat_id,
            'documentName': document_name,
            'isLocked': 'true'
        }
        response = request(method="PUT", url=self.set_document_lock_url, headers=self.header, params=querystring)

        print(response.text)
        code_match_update_checklist(str(response.status_code), self.set_document_lock_access, self.sheet_name, sn)

    def delete_document(self, pat_id, document_name, sn=None):
        """
        @desc - Deletes a PatientDocument
        :param pat_id
        :param document_name
        :param sn: TC Serial Number
        """
        self.log.info("Endpoint: delete_document has been called")
        querystring = {
            'patientId': pat_id,
            'documentName': document_name
        }
        response = request(method="DELETE", url=self.delete_document_url, headers=self.header, params=querystring)

        print(response.text)
        code_match_update_checklist(str(response.status_code), self.delete_document_access, self.sheet_name, sn)

    def get_document_upload_creds(self, pat_id, sn=None):
        """
        @desc - Gets credentials to upload files to patient documents bucket in s3
        :param pat_id
        :param sn: TC Serial Number
        """
        self.log.info("Endpoint: get_document_upload_creds has been called")
        querystring = {
            'patientId': pat_id
        }
        response = request(method="GET", url=self.get_document_upload_creds_url, headers=self.header,
                           params=querystring)

        print(response.text)
        code_match_update_checklist(str(response.status_code), self.get_document_upload_creds_access, self.sheet_name, sn)

    def get_document_download_url(self, pat_id, document_name, sn=None):
        """
        @desc - Gets url to download patient document
        :param pat_id
        :param document_name
        :param sn: TC Serial Number
        """
        self.log.info("Endpoint: get_document_download_url has been called")
        querystring = {
            'patientId': pat_id,
            'documentName': document_name
        }
        response = request(method="GET", url=self.get_document_download_url_url, headers=self.header,
                           params=querystring)

        print(response.text)
        code_match_update_checklist(str(response.status_code), self.get_document_download_url_access, self.sheet_name, sn)

    def log_document_creation(self, pat_id, document_name, sn=None):
        """
        @desc - Logs creation of patient document
        :param pat_id
        :param document_name
        :param sn: TC Serial Number
        """
        self.log.info("Endpoint: log_document_creation has been called")
        querystring = {
            'patientId': pat_id,
            'documentName': document_name
        }
        response = request(method="POST", url=self.log_document_creation_url, headers=self.header,
                           params=querystring)

        print(response.text)
        code_match_update_checklist(str(response.status_code), self.log_document_creation_access, self.sheet_name, sn)

    def get_patient_for_editing(self, pat_id, sn=None):
        """
        @desc - Gets a specific patient for editing
        :param pat_id
        :param sn: TC Serial Number
        """
        self.log.info("Endpoint: get_patient_for_editing has been called")
        querystring = {
            'patientId': pat_id
        }
        response = request(method="GET", url=self.get_patient_for_editing_url, headers=self.header,
                           params=querystring)

        print(response.text)
        code_match_update_checklist(str(response.status_code), self.get_patient_for_editing_access, self.sheet_name, sn)

    def get_patient_by_patient_id(self, pat_id, sn=None):
        """
        @desc - Gets a patient by facility set patient ID
        :param pat_id
        :param sn: TC Serial Number
        """
        self.log.info("Endpoint: get_patient_by_patient_id has been called")
        querystring = {
            'patientId': pat_id
        }
        response = request(method="GET", url=self.get_patient_by_patient_id_url, headers=self.header,
                           params=querystring)

        print(response.text)
        code_match_update_checklist(str(response.status_code), self.get_patient_by_patient_id_access, self.sheet_name, sn)

    def get_patient_by_study_ids(self, study_id, sn=None):
        """
        @desc - Gets patient information associated with a list of study ids
        :param study_id
        :param sn: TC Serial Number
        """
        self.log.info("Endpoint: get_patient_by_study_ids has been called")
        payload = {
            'studyGuids': [study_id]
        }

        response = request(method="POST", url=self.get_patient_by_study_ids_url, headers=self.header,
                           data=dict_to_json(payload))

        print(response.text)
        code_match_update_checklist(str(response.status_code), self.get_patient_by_study_ids_access, self.sheet_name, sn)

    def set_pat_id(self):
        pat_id = "PAT_" + get_formatted_date()
        self.patient_id = pat_id
        return pat_id

    def create_patient_tc(self, f_name, l_name, sn=None):
        """
        @desc - Creates a new patient test case method. Returns created patient's ID
        :param f_name
        :param l_name
        :param sn: TC Serial Number
        :return
        """
        created_patient_id = ""
        self.log.info("Endpoint: create_patient_tc has been called")

        response = self.create_patient(f_name=f_name, l_name=l_name, pat_id=self.patient_id, sn=sn)

        if str(response.status_code) == "200":

            response1 = self.search_patient(search_string=self.patient_id)

            if str(response1.status_code) == "200" or str(response1.status_code) == "201":
                json_response = response1.json()
                print(json_response)
                created_patient_id = json_response[0]['ID']
                print("Patient ID of the newly created Patient : " + created_patient_id)
        return created_patient_id

    def create_patient(self, f_name, l_name, pat_id, sn=None):
        """
        @desc - Creates a new patient
        :param f_name
        :param l_name
        :param pat_id
        :param sn: TC Serial Number
        """
        self.log.info("Endpoint: create_patient has been called")
        date_of_birth = get_formatted_date()
        payload = {
            "FirstName": f_name,
            "LastName": l_name,
            "DateOfBirth": date_of_birth,
            "PatientID": pat_id
        }

        response = request(method="POST", url=self.create_patient_url, headers=self.header,
                           data=dict_to_json(payload))

        if sn is not None:
            code_match_update_checklist(str(response.status_code), self.create_patient_access, self.sheet_name, sn)
        else:
            code_match(str(response.status_code), self.create_patient_access)
        print(response.text)

        return response

    def delete_patient(self, pat_id, sn=None):
        """
        @desc - Deletes a patient by ID
        :param pat_id
        :param sn: TC Serial Number
        """
        self.log.info("Endpoint: delete_patient has been called")
        url = self.delete_patient_url + pat_id

        response = request(method="DELETE", url=url, headers=self.header)

        print(response.text)
        code_match_update_checklist(str(response.status_code), self.delete_patient_access, self.sheet_name, sn)

    def get_patient_by_id(self, pat_id, sn=None):
        """
        @desc - Get a patient by ID
        :param pat_id
        :param sn: TC Serial Number
        """
        self.log.info("Endpoint: get_patient_by_id has been called")
        url = self.get_by_id_url + pat_id

        response = request(method="GET", url=url, headers=self.header)

        print(response.text)

        if sn is not None:
            code_match_update_checklist(str(response.status_code), self.get_by_id_access, self.sheet_name, sn)
        else:
            code_match(str(response.status_code), self.get_by_id_access)
        return response

    def patch_patient(self, pat_id, f_name, l_name, sn=None):
        """
        @desc - Update patient data by Patch
        :param pat_id
        :param f_name
        :param l_name
        :param sn: TC Serial Number
        """
        self.log.info("Endpoint: patch_patient has been called")
        url = self.patch_url + pat_id

        payload = {
            "FirstName": f_name + "_EDIT",
            "LastName": l_name + "_EDIT"
        }

        response = request(method="PATCH", url=url, headers=self.header,
                           data=dict_to_json(payload))

        print(response.text)
        code_match_update_checklist(str(response.status_code), self.patch_access, self.sheet_name, sn)

    def put_patient(self, pat_id, f_name, l_name, sn=None):
        """
        @desc - Update patient data by Put
        :param pat_id
        :param f_name
        :param l_name
        :param sn: TC Serial Number
        """
        self.log.info("Endpoint: put_patient has been called")
        url = self.put_url + pat_id

        payload = {
            "FirstName": f_name + "_EDIT",
            "LastName": l_name + "_EDIT"
        }

        response = request(method="PUT", url=url, headers=self.header,
                           data=dict_to_json(payload))

        print(response.text)
        code_match_update_checklist(str(response.status_code), self.put_access, self.sheet_name, sn)

    def get_patients(self, sn=None):
        """
        @desc - Get list of patients
        :param sn: TC Serial Number
        """
        self.log.info("Endpoint: get_patients has been called")
        response = request(method="GET", url=self.get_patients_url, headers=self.header)

        print(response.text)
        code_match_update_checklist(str(response.status_code), self.get_patients_access, self.sheet_name, sn)

    def post_patient_tc(self, f_name, l_name, sn=None):
        """
        @desc - Post method's test case. Returns created pat's ID
        :param f_name
        :param l_name
        :param sn: TC Serial Number
        :return
        """
        self.log.info("TC method: post_patient_tc has been called")
        response = self.post_patient(f_name, l_name, sn)
        if str(response.status_code) == "201":
            json_response = response.json()
            return json_response['ID']

    def post_patient(self, f_name, l_name, sn=None):
        """
        @desc - Creates a new patient
        :param f_name
        :param l_name
        :param sn: TC Serial Number
        """
        self.log.info("Endpoint: post_patient has been called")
        date_of_birth = get_formatted_date()
        pat_id = "PAT_" + get_formatted_date()

        print("pat_id", pat_id)

        payload = {
            "FirstName": f_name,
            "LastName": l_name,
            "DateOfBirth": date_of_birth,
            "PatientID": pat_id
        }

        response = request(method="POST", url=self.post_url, headers=self.header,
                           data=dict_to_json(payload))

        print(response.text)
        code_match_update_checklist(str(response.status_code), self.post_access, self.sheet_name, sn)

        return response

    class PatientsControllerTCs(Exception):
        pass
