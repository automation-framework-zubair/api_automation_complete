import os
import sys
import datetime
import time

lib_path2 = os.path.abspath(os.path.join('..', '..', 'libraries'))
sys.path.insert(0, lib_path2)
import conf as init

lib_path = os.path.abspath(os.path.join('..', '..', 'libraries', 'common'))
sys.path.insert(0, lib_path)
from logger_robot import Logger as logger_extended
from assertion import code_match, code_match_update_checklist
from request import request

lib_path1 = os.path.abspath(os.path.join('..', '..', 'libraries', 'Rendr'))
sys.path.insert(0, lib_path1)
from TestDataProvider import TestDataProvider
from application_util import headers,dict_to_json


class StudyRecordingsControllerTCs(object) :
    ROBOT_LIBRARY_SCOPE = 'TEST SUITE'
    log = logger_extended().get_logger('API_Automation')

    data = TestDataProvider()

    user_token = ""
    header = {}

    sheet_name = "StudyRecordings"

    # URLS
    url = init.url
    get_last_sync_packet_index_url = ""
    get_mappings_url = ""
    get_server_video_size_url = ""
    update_study_recording_notes_url = ""
    get_study_recording_url = ""

    # Access information
    get_last_sync_packet_index_access = ""
    get_mappings_access = ""
    get_server_video_size_access = ""
    update_study_recording_notes_access = ""
    get_study_recording_access = ""

    con_name = ""
    mmt_facility_id = "e3dd1742-335e-438e-8d8d-65df10447ea3"

    def __init__(self):
        self._expression = ''
        self.con_name = "StudyRecordingsController"
        self.log.info(self.con_name + " class has been initialized")
        self.get_last_sync_packet_index_url = self.url + self.data.get_url_of_controller(
            self.con_name, "GetLastSyncPacketIndex")
        self.get_server_video_size_url = self.url + self.data.get_url_of_controller(
            self.con_name, "GetServerVideoSize")
        self.get_mappings_url = self.url + self.data.get_url_of_controller(
            self.con_name, "GetMappings")
        self.update_study_recording_notes_url = self.url + self.data.get_url_of_controller(
            self.con_name, "UpdateStudyRecordingNotes")
        self.get_study_recording_url = self.url + self.data.get_url_of_controller(
            self.con_name, "GetStudyRecording")

    def initialize_access_for_study_recordings_controller(self, role):
        list_of_roles = ['super admin', 'support', 'production', 'facility admin', 'review doctor', 'lead tech',
                         'field tech', 'office personnel']

        if role in list_of_roles:
            self.log.info("Gathering access info for " + role + " role")
            self.get_last_sync_packet_index_access = self.data.get_access_status_of_controller (
                self.con_name, "GetLastSyncPacketIndex", role)
            self.get_server_video_size_access = self.data.get_access_status_of_controller (
                self.con_name, "GetServerVideoSize", role)
            self.get_mappings_access = self.data.get_access_status_of_controller (
                self.con_name, "GetMappings", role)
            self.update_study_recording_notes_access = self.data.get_access_status_of_controller (
                self.con_name, "UpdateStudyRecordingNotes", role)
            self.get_study_recording_access = self.data.get_access_status_of_controller (
                self.con_name, "GetStudyRecording", role)
        else:
            raise AssertionError("Please provide a valid role name")

    def set_token_study_recordings(self, token):

        """
        @desc - method to set token of the user
        :param - token: user's token
        :return:
        """
        self.log.info("Current user's token : " + token)
        self.header = headers(token)

    def get_last_sync_packet_index(self, study_id, recording_index, sn=None):
        """
        @desc - Gets the index of the last study packet that was synchronized.
        :param study_id:
        :param recording_index:
        :param sn: TC Serial Number
        """
        self.log.info("Endpoint: get_last_sync_packet_index has been called")
        querystring = {
            'id': study_id,
            'recordingIndex': recording_index
        }
        response = request(method="GET", url=self.get_last_sync_packet_index_url, headers=self.header,
                           params=querystring)
        print(response.text)
        code_match_update_checklist(str(response.status_code), self.get_last_sync_packet_index_access, self.sheet_name, sn)

    def get_server_video_size(self, study_id, recording_index, camera_index, video_index, sn=None):
        """
        @desc - Gets the size of the video on the server for the given study recording.
        :param study_id:
        :param recording_index:
        :param camera_index:
        :param video_index:
        :param sn: TC Serial Number
        """
        self.log.info("Endpoint: get_server_video_size has been called")
        querystring = {
            'id': study_id,
            'recordingIndex': recording_index,
            'cameraIndex': camera_index,
            'videoIndex': video_index
        }
        response = request(method="GET", url=self.get_server_video_size_url, headers=self.header,
                           params=querystring)
        print(response.text)
        code_match_update_checklist(str(response.status_code), self.get_server_video_size_access, self.sheet_name, sn)

    def get_mappings(self, study_id, recording_index, sn=None):
        """
        @desc - Gets the mappings associated with a study recording.
        :param study_id:
        :param recording_index:
        :param sn: TC Serial Number
        """
        self.log.info("Endpoint: get_mappings has been called")
        querystring = {
            'id': study_id,
            'recordingIndex': recording_index
        }
        response = request(method="GET", url=self.get_mappings_url, headers=self.header,
                           params=querystring)
        print(response.text)
        code_match_update_checklist(str(response.status_code), self.get_mappings_access, self.sheet_name, sn)

    def update_study_recording_notes(self, study_id, recording_index, sn=None):
        """
        @desc - Updates study notes of a study recording.
        :param study_id:
        :param recording_index:
        :param sn: TC Serial Number
        """
        self.log.info("Endpoint: update_study_recording_notes has been called")
        payload = {
            "StudyID": study_id,
            "Index": recording_index,
            "Notes": "notes provided via automation"
        }

        response = request(method="POST", url=self.update_study_recording_notes_url, headers=self.header,
                           data=dict_to_json(payload))
        print(response.text)
        code_match_update_checklist(str(response.status_code), self.update_study_recording_notes_access, self.sheet_name, sn)
        return response

    def get_study_recordings(self, study_id, recording_index, sn=None):
        """
        @desc - Gets a StudyRecording by id and index since it's a composite key
        :param study_id: ID of the Study
        :param recording_index: Index of the StudyRecording.
        :param sn: TC Serial Number
        """
        self.log.info("Endpoint: get_study_recordings has been called")
        querystring = {
            'id': study_id,
            'index': recording_index
        }
        response = request(method="GET", url=self.get_study_recording_url, headers=self.header,
                           params=querystring)
        print(response.text)
        code_match_update_checklist(str(response.status_code), self.get_study_recording_access, self.sheet_name, sn)

    class StudyRecordingsControllerTCs(Exception):
        pass
