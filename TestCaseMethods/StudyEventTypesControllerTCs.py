import os
import sys
import datetime
import time

lib_path2 = os.path.abspath(os.path.join('..', '..', 'libraries'))
sys.path.insert(0, lib_path2)
import conf as init

lib_path = os.path.abspath(os.path.join('..', '..', 'libraries', 'common'))
sys.path.insert(0, lib_path)
from logger_robot import Logger as logger_extended
from assertion import code_match, code_match_update_checklist
from request import request

lib_path1 = os.path.abspath(os.path.join('..', '..', 'libraries', 'Rendr'))
sys.path.insert(0, lib_path1)
from TestDataProvider import TestDataProvider
from application_util import headers, dict_to_json, get_formatted_date


class StudyEventTypesControllerTCs(object):
    ROBOT_LIBRARY_SCOPE = 'TEST SUITE'
    log = logger_extended().get_logger('API_Automation')

    data = TestDataProvider()

    study_event_type_id = ""
    study_event_type_name = ""
    study_event_type_description = ""

    user_token = ""
    header = {}

    sheet_name = "StudyEventTypes"

    # URLs
    url = init.url
    delete_study_event_types_url = ""
    get_study_event_types_by_id_url = ""
    get_study_event_types_url = ""
    patch_study_event_types_url = ""
    post_study_event_types_url = ""
    put_study_event_types_url = ""

    # Access Information
    delete_study_event_types_access = ""
    get_study_event_types_by_id_access = ""
    get_study_event_types_access = ""
    patch_study_event_types_access = ""
    post_study_event_types_access = ""
    put_study_event_types_access = ""

    con_name = ""

    def __init__(self):
        self._expression = ''
        self.con_name = "StudyEventTypesController"
        self.log.info(self.con_name + " class has been initialized")
        self.delete_study_event_types_url = self.url + self.data.get_url_of_controller(
            self.con_name, "Delete")
        self.get_study_event_types_by_id_url = self.url + self.data.get_url_of_controller(
            self.con_name, "Get?id=")
        self.get_study_types_url = self.url + self.data.get_url_of_controller(
            self.con_name, "Get")
        self.patch_study_event_types_url = self.url + self.data.get_url_of_controller(
            self.con_name, "Patch")
        self.post_study_event_types_url = self.url + self.data.get_url_of_controller(
            self.con_name, "Post")
        self.put_study_event_types_url = self.url + self.data.get_url_of_controller(
            self.con_name, "Put")

    def initialize_access_for_study_event_type_controller(self, role):
        list_of_roles = ['super admin', 'support', 'production', 'facility admin', 'review doctor', 'lead tech',
                         'field tech', 'office personnel']

        if role in list_of_roles:
            self.log.info("Gathering access info for " + role + " role")
            self.delete_study_event_types_access = self.data.get_access_status_of_controller(
                self.con_name, "Delete", role)
            self.get_study_event_types_by_id_access = self.data.get_access_status_of_controller(
                self.con_name, "Get?id=", role)
            self.get_study_event_types_access = self.data.get_access_status_of_controller(
                self.con_name, "Get", role)
            self.patch_study_event_types_access = self.data.get_access_status_of_controller(
                self.con_name, "Patch", role)
            self.post_study_event_types_access = self.data.get_access_status_of_controller(
                self.con_name, "Post", role)
            self.put_study_event_types_access = self.data.get_access_status_of_controller(
                self.con_name, "Put", role)
        else:
            raise AssertionError("Please provide a valid role name")

    def set_token_study_event_types(self, token):
        """
        @desc - method to set token of the user
        :param - token: user's token
        """
        self.log.info("Current user's token : " + token)
        self.header = headers(token)

    def post_study_event_types_tc(self, study_event_type_name=None, description=None, sn=None):
        """
        @desc - calls study_event_types method and returns study_event_type_id
        :param study_event_type_name: name of the study event
        :param description: Description of the study event type
        :param sn: TC Serial Number
        :return:
        """
        self.log.info("TC method: post_study_event_types_tc has been called")
        response = self.post_study_event_types(study_event_type_name, description, sn)

        if str(response.status_code) != "403":
            json_response = response.json()
            self.study_event_type_id = str(json_response['ID'])
            print("ID of the newly created study event type: {}".format(self.study_event_type_id))
            self.study_event_type_name = json_response['Name']
            print("Name of the newly created study event type: {}".format(self.study_event_type_name))
            self.study_event_type_description = json_response['Description']
            print("Description of the newly created study event type: {}".format(self.study_event_type_description))

            return self.study_event_type_id

    def post_study_event_types(self, study_event_type_name=None, description=None, sn=None):
        """
        @desc - method to create a new study event type
        :param study_event_type_name: name of the study event
        :param description: Description of the study event type
        :param sn: TC Serial Number
        :return:
        """
        self.log.info("Endpoint: post_study_event_types has been called")
        if study_event_type_name is None and description is None:
            study_event_type_name = "TEST_API"
            description = "TEST_API"

        st1 = get_formatted_date()

        self.study_event_type_name = study_event_type_name + st1
        self.study_event_type_description = description + st1

        payload = {
            "Name": self.study_event_type_name,
            "Description": self.study_event_type_description,
        }

        print(payload)
        response = request(method="POST", url=self.post_study_event_types_url,
                           data=dict_to_json(payload), headers=self.header)
        print(response.text)

        if sn is not None:
            code_match_update_checklist(str(response.status_code), self.post_study_event_types_access, self.sheet_name, sn)
        else:
            code_match(str(response.status_code), self.post_study_event_types_access)
        return response

    def get_study_event_types_by_id(self, study_event_type_id=None, sn=None):
        """
        @desc - method to get study event types by id
        :param study_event_type_id: ID of the study event type
        :param sn: TC Serial Number
        :return:
        """
        self.log.info("Endpoint: get_study_event_types_by_id has been called")
        if study_event_type_id is None:
            url = self.get_study_event_types_by_id_url + self.study_event_type_id
        else:
            url = self.get_study_event_types_by_id_url + study_event_type_id

        response = request(method="GET", url=url, headers=self.header)
        print(response.text)
        code_match_update_checklist(str(response.status_code), self.get_study_event_types_by_id_access, self.sheet_name, sn)

    def get_study_event_types(self, sn=None):
        """
        @desc - method to get a list of study event types
        :param sn: TC Serial Number
        :return:
        """
        self.log.info("Endpoint: get_study_event_types has been called")
        response = request(method="GET", url=self.get_study_types_url, headers=self.header)
        print(response.text)
        code_match_update_checklist(str(response.status_code), self.get_study_event_types_access, self.sheet_name, sn)

    def patch_study_event_types(self, type_id=None, sn=None):
        """
        @desc - method to edit study event type using patch endpoint
        :param type_id: ID of the study event type
        :param sn: TC Serial Number
        :return:
        """
        self.log.info("Endpoint: patch_study_event_types has been called")
        print("type", type_id)

        if type_id is None:
            url = self.patch_study_event_types_url + self.study_event_type_id

        else:
            url = self.patch_study_event_types_url + type_id

        name = "TEST_API"
        description = "TEST_API"
        st1 = get_formatted_date()
        print("URL", url)
        self.study_event_type_name = name + st1
        self.study_event_type_description = description + st1

        payload = {
            "Name": self.study_event_type_name,
            "Description": self.study_event_type_description,
        }
        response = request(method="PATCH", url=url, data=dict_to_json(payload), headers=self.header)
        print(response.text)
        code_match_update_checklist(str(response.status_code), self.patch_study_event_types_access, self.sheet_name, sn)

    def put_study_event_types(self, type_id=None, sn=None):
        """
        @desc - method to edit study event type using put endpoint
        :param type_id: ID of the study event type
        :param sn: TC Serial Number
        :return:
        """
        self.log.info("Endpoint: put_study_event_types has been called")
        print("type", type_id)

        if type_id is None:
            url = self.put_study_event_types_url + self.study_event_type_id

        else:
            url = self.put_study_event_types_url + type_id
            self.study_event_type_id = type_id

        st1 = get_formatted_date()
        name = "TEST_API"
        description = "TEST_API"
        self.study_event_type_name = name + st1
        self.study_event_type_description = description + st1

        payload = {
            "ID": self.study_event_type_id,
            "Name": self.study_event_type_name,
            "Description": self.study_event_type_description,
        }
        response = request(method="PUT", url=url, data=dict_to_json(payload), headers=self.header)
        print(response.text)
        code_match_update_checklist(str(response.status_code), self.put_study_event_types_access, self.sheet_name, sn)

    def delete_study_event_types(self, type_id=None, sn=None):
        """
        @desc - method to delete a study event types
        :param type_id: ID of the study event type
        :param sn: TC Serial Number
        """
        self.log.info("Endpoint: delete_study_event_types has been called")
        if type_id is None:
            url = self.delete_study_event_types_url + self.study_event_type_id
        else:
            url = self.delete_study_event_types_url + type_id

        response = request(method="DELETE", url=url, headers=self.header)
        print(response.text)
        if sn is not None:
            code_match_update_checklist(str(response.status_code), self.delete_study_event_types_access, self.sheet_name, sn)
        else:
            code_match(str(response.status_code), self.delete_study_event_types_access)

    class StudyEventTypesControllerTCs(Exception):
        pass
