import os
import sys

lib_path2 = os.path.abspath(os.path.join('..', '..', 'libraries'))
sys.path.insert(0, lib_path2)
import conf as init

lib_path = os.path.abspath(os.path.join('..', '..', 'libraries', 'common'))
sys.path.insert(0, lib_path)
from logger_robot import Logger as logger_extended
from assertion import code_match_update_checklist, code_match
from request import request

lib_path1 = os.path.abspath(os.path.join('..', '..', 'libraries', 'Rendr'))
sys.path.insert(0, lib_path1)
from TestDataProvider import TestDataProvider
from application_util import headers, dict_to_json, get_formatted_date
# from _AccountController import _AccountController
# from rendr_util import get_logged_in_user_facility_id, get_logged_in_user_id


class FacilitiesControllerTCs(object):
    ROBOT_LIBRARY_SCOPE = 'TEST SUITE'
    log = logger_extended().get_logger('API_Automation')

    data = TestDataProvider()

    facility_id = ""
    logged_in_user_id = ""

    user_token = ""
    header = {}

    sheet_name = "Facilities"

    # URLs
    url = init.url
    rotate_token_url = ""
    metrics_url = ""
    get_facilities_url = ""
    create_facility_url = ""
    send_invitation_url = ""
    accept_invitation_url = ""
    get_invitation_url = ""
    get_invitations_url = ""
    does_invited_user_exist_url = ""
    is_invitation_expired_url = ""
    delete__url = ""
    get_by_id_url = ""
    patch_url = ""
    put_url = ""
    get_supported_network_cameras_url = ""
    get_facility_network_cameras_url = ""
    add_facility_network_camera_url = ""
    remove_facility_network_camera_url = ""
    retrieve_token_url = ""

    # Access information
    rotate_token_access = ""
    metrics_access = ""
    get_facilities_access = ""
    create_facility_access = ""
    send_invitation_access = ""
    accept_invitation_access = ""
    get_invitation_access = ""
    get_invitations_access = ""
    does_invited_user_exist_access = ""
    is_invitation_expired_access = ""
    delete__access = ""
    get_by_id_access = ""
    patch_access = ""
    put_access = ""
    get_supported_network_cameras_access = ""
    get_facility_network_cameras_access = ""
    add_facility_network_camera_access = ""
    remove_facility_network_camera_access = ""
    retrieve_token_access = ""

    con_name = ""

    mmt_facility_name = ""

    mmt_facility_domain = ""
    mmt_local_storage_days = ""
    mmt_date_created = ""
    mmt_facility_id = init.mobile_med_tek_facility_id

    def __init__(self):
        self._expression = ''
        self.con_name = "FacilitiesController"
        self.log.info(self.con_name + " class has been initialized")
        self.rotate_token_url = self.url + self.data.get_url_of_controller(self.con_name, "RotateToken")
        self.metrics_url = self.url + self.data.get_url_of_controller(self.con_name, "Metrics")
        self.get_facilities_url = self.url + self.data.get_url_of_controller(self.con_name, "GetFacilities")
        self.create_facility_url = self.url + self.data.get_url_of_controller(self.con_name, "Post")
        self.send_invitation_url = self.url + self.data.get_url_of_controller(self.con_name, "SendInvitation")
        self.accept_invitation_url = self.url + self.data.get_url_of_controller(self.con_name, "AcceptInvitation")
        self.get_invitation_url = self.url + self.data.get_url_of_controller(self.con_name, "GetInvitation")
        self.get_invitations_url = self.url + self.data.get_url_of_controller(self.con_name, "GetInvitations")
        self.does_invited_user_exist_url = self.url + self.data.get_url_of_controller(self.con_name, "DoesInvitedUserExist")
        self.is_invitation_expired_url = self.url + self.data.get_url_of_controller(self.con_name, "IsInvitationExpired")
        self.delete__url = self.url + self.data.get_url_of_controller(self.con_name, "Delete")
        self.get_by_id_url = self.url + self.data.get_url_of_controller(self.con_name, "Get?id=")
        self.patch_url = self.url + self.data.get_url_of_controller(self.con_name, "Patch")
        self.put_url = self.url + self.data.get_url_of_controller(self.con_name, "Put")
        self.get_supported_network_cameras_url = self.url + self.data.get_url_of_controller(self.con_name, "GetSupportedNetworkCameras")
        self.get_facility_network_cameras_url = self.url + self.data.get_url_of_controller(self.con_name, "GetFacilityNetworkCameras")
        self.add_facility_network_camera_url = self.url + self.data.get_url_of_controller(self.con_name, "AddFacilityNetworkCamera")
        self.remove_facility_network_camera_url = self.url + self.data.get_url_of_controller(self.con_name, "RemoveFacilityNetworkCamera")
        self.retrieve_token_url = self.url + self.data.get_url_of_controller(self.con_name, "RetrieveToken")

    def initialize_access_for_facilities_controller(self, role):
        list_of_roles = ['super admin', 'support', 'production', 'facility admin', 'review doctor', 'lead tech',
                         'field tech', 'office personnel']

        if role in list_of_roles:
            self.log.info("Gathering access info for " + role + " role")
            self.rotate_token_access = self.data.get_access_status_of_controller(
                self.con_name, "RotateToken", role)
            self.metrics_access = self.data.get_access_status_of_controller(
                self.con_name, "Metrics", role)
            self.get_facilities_access = self.data.get_access_status_of_controller(
                self.con_name, "GetFacilities", role)
            self.create_facility_access = self.data.get_access_status_of_controller(
                self.con_name, "Post", role)
            self.send_invitation_access = self.data.get_access_status_of_controller(
                self.con_name, "SendInvitation", role)
            self.accept_invitation_access = self.data.get_access_status_of_controller(
                self.con_name, "AcceptInvitation", role)
            self.get_invitation_access = self.data.get_access_status_of_controller(
                self.con_name, "GetInvitation", role)
            self.get_invitations_access = self.data.get_access_status_of_controller(
                self.con_name, "GetInvitations", role)
            self.does_invited_user_exist_access = self.data.get_access_status_of_controller(
                self.con_name, "DoesInvitedUserExist", role)
            self.is_invitation_expired_access = self.data.get_access_status_of_controller(
                self.con_name, "IsInvitationExpired", role)
            self.delete__access = self.data.get_access_status_of_controller(
                self.con_name, "Delete", role)
            self.get_by_id_access = self.data.get_access_status_of_controller(
                self.con_name, "Get?id=", role)
            self.patch_access = self.data.get_access_status_of_controller(
                self.con_name, "Patch", role)
            self.put_access = self.data.get_access_status_of_controller(
                self.con_name, "Put", role)
            self.get_supported_network_cameras_access = self.data.get_access_status_of_controller(
                self.con_name, "GetSupportedNetworkCameras", role)
            self.get_facility_network_cameras_access = self.data.get_access_status_of_controller(
                self.con_name, "GetFacilityNetworkCameras", role)
            self.add_facility_network_camera_access = self.data.get_access_status_of_controller(
                self.con_name, "AddFacilityNetworkCamera", role)
            self.remove_facility_network_camera_access = self.data.get_access_status_of_controller(
                self.con_name, "RemoveFacilityNetworkCamera", role)
            self.retrieve_token_access = self.data.get_access_status_of_controller(
                self.con_name, "RetrieveToken", role)

        else:
            raise AssertionError("Please provide a valid role name.")

    def set_token_facilities(self, token):
        """
        @desc - method to set token of the user
        :param - token: user's token
        """
        self.log.info("Current user's token : " + token)
        self.header = headers(token)

    def post_rotate_token(self, fac_id, sn=None):
        """
        @desc - Applies a new token for a facility
        :param - fac_id: ID of the facility
        :param sn: TC Serial Number
        """
        self.log.info("Endpoint: post_rotate_token has been called")
        if fac_id is None:
            fac_id = self.mmt_facility_id

        querystring = {'id': fac_id}

        response = request(method="POST", url=self.rotate_token_url, headers=self.header, params=querystring)

        print(response.text)
        if sn is not None:
            code_match_update_checklist(str(response.status_code), self.rotate_token_access, self.sheet_name, sn)
        else:
            code_match(str(response.status_code), self.rotate_token_access)

    def get_retrieve_token(self, sn=None):
        """
        @desc - Retrieves a token for a facilityID
        :param sn: TC Serial Number
        """
        self.log.info("Endpoint: get_retrieve_token has been called")
        response = request(method="GET", url=self.retrieve_token_url, headers=self.header)

        print(response.text)
        code_match_update_checklist(str(response.status_code), self.retrieve_token_access, self.sheet_name, sn)

    def get_supported_network_cameras(self, sn=None):
        """
        @def - Returns a list of supported LNC network cameras types
        :param sn: TC Serial Number
        """
        self.log.info("Endpoint: get_supported_network_cameras has been called")
        response = request(method="GET", url=self.get_supported_network_cameras_url, headers=self.header)

        print(response.text)
        if sn is not None:
            code_match_update_checklist(str(response.status_code), self.get_supported_network_cameras_access, self.sheet_name, sn)
        else:
            code_match(str(response.status_code), self.get_supported_network_cameras_access)
        return response

    def get_facility_network_cameras(self, sn=None):
        """
        @def - Returns a list of network cameras associated with the users currently selected facility
        :param sn: TC Serial Number
        """
        self.log.info("Endpoint: get_facility_network_cameras has been called")
        response = request(method="GET", url=self.get_facility_network_cameras_url, headers=self.header)

        print(response.text)
        if sn is not None:
            code_match_update_checklist(str(response.status_code), self.get_facility_network_cameras_access, self.sheet_name, sn)
        else:
            code_match(str(response.status_code), self.get_facility_network_cameras_access)
        return response

    def add_facility_network_camera(self, network_camera_id, device_id, sn=None):
        """
        @def - Adds existing network camera to facility
        :param - network_camera_id: ID of the network camera
        :param - device_id: ID of the Device for which the camera would be created
        :param sn: TC Serial Number
        """
        self.log.info("Endpoint: add_facility_network_camera has been called")
        querystring = {'networkCameraId': network_camera_id, 'deviceId': device_id}

        response = request(method="GET", url=self.add_facility_network_camera_url, headers=self.header, params=querystring)

        print(response.text)
        if sn is not None:
            code_match_update_checklist(str(response.status_code), self.add_facility_network_camera_access, self.sheet_name, sn)
        else:
            code_match(str(response.status_code), self.add_facility_network_camera_access)
        return response

    def remove_facility_network_camera(self, facility_network_camera_id, sn=None):
        """
        @def - Delete network camera from facility
        :param - facility_network_camera_id: ID of the network camera
        :param sn: TC Serial Number
        """
        self.log.info("Endpoint: remove_facility_network_camera has been called")
        querystring = {'facilityNetworkCameraId': facility_network_camera_id}

        response = request(method="GET", url=self.remove_facility_network_camera_url, headers=self.header, params=querystring)

        print(response.text)
        code_match_update_checklist(str(response.status_code), self.remove_facility_network_camera_access, self.sheet_name, sn)
        return response

    def get_metrics(self, fac_id, sn=None):
        """
        @desc - Returns dashboard metrics for facility
        :param - fac_id: ID of the facility
        :param sn: TC Serial Number
        """
        self.log.info("Endpoint: get_metrics has been called")
        if fac_id is None:
            fac_id = self.mmt_facility_id

        querystring = {'id': fac_id}

        response = request(method="GET", url=self.metrics_url, headers=self.header, params=querystring)

        print(response.text)
        code_match_update_checklist(str(response.status_code), self.metrics_access, self.sheet_name, sn)

    def get_facilities(self, sn=None):
        """
        @desc - Gets a list of all facilities
        :param sn: TC Serial Number
        
        """
        self.log.info("Endpoint: get_facilities has been called")
        response = request(method="GET", url=self.get_facilities_url, headers=self.header)

        print(response.text)
        code_match_update_checklist(str(response.status_code), self.get_facilities_access, self.sheet_name, sn)

    def post_create_facility_tc(self, fac_name, sn=None):
        """
        @desc - This method Creates Facility and returns Created Facility ID
        :param - fac_name: Name of the facility
        :param sn: TC Serial Number
        :returns
        """
        self.log.info("TC method: post_create_facility_tc has been called")

        response = self.post_create_facility(fac_name=fac_name + "_" + get_formatted_date(),
                                             fac_domain=fac_name + "_dom_" + get_formatted_date(),
                                             sn=sn)
        if str(response.status_code) == "200":
            json_response = response.json()
            facility_id = json_response['ID']
            print("Created facility's ID = "+facility_id)
            return facility_id

    def post_create_facility(self, fac_name, fac_domain, sn=None):
        """
        @desc - Creates a new facility
        :param - fac_name: Name of the facility
        :param - fac_domain: domain name of the facility
        :param sn: TC Serial Number
        
        """
        self.log.info("Endpoint: post_create_facility has been called")
        payload = {
            "Name": fac_name,
            "Domain": fac_domain
        }

        response = request(method="POST", url=self.create_facility_url, data=dict_to_json(payload), headers=self.header)

        print(response.text)
        if sn is not None:
            code_match_update_checklist(str(response.status_code), self.create_facility_access, self.sheet_name, sn)
        else:
            code_match(str(response.status_code), self.create_facility_access)
        return response

    def get_send_invitation(self, role, email, sn=None):
        """
        @desc - Creates and sends an invite to an email with the intent of adding a user (new or existing) to a facility
        :param - role: invitation role of the user
        :param - email: Email of the user you are sending invitation. (MUST BE VALID EMAIL)
        :param sn: TC Serial Number

        """
        self.log.info("Endpoint: get_send_invitation has been called")

        if role.lower() == 'facility admin':
            role_id = init.facility_admin_role
        elif role.lower() == 'review doctor':
            role_id = init.review_doctor_role
        elif role.lower() == 'lead tech':
            role_id = init.lead_tech_role
        elif role.lower() == 'field tech':
            role_id = init.field_tech_role
        elif role.lower() == 'office personnel':
            role_id = init.office_personnel_role
        else:
            raise AssertionError("Please select the correct user role to log in properly.")

        if email is None:
            email = init.email_address_for_invitation

        if role_id is None:
            role_id = init.facility_admin_role

        querystring = {'email': email, 'roleid': role_id}

        response = request(method="GET", url=self.send_invitation_url, headers=self.header, params=querystring)

        print(response.text)

        code_match_update_checklist(str(response.status_code), self.send_invitation_access, self.sheet_name, sn)

    def post_accept_invitation(self, invitation_id, sn=None):
        """
        @desc - Allows user to accept a facility invitation
        :param - invitation_id: Id of the invitation
        :param sn: TC Serial Number
        
        """
        self.log.info("Endpoint: post_accept_invitation has been called")

        querystring = {'invitationId': invitation_id}

        payload = {
            "FirstName": "Invited",
            "LastName": "User",
            "Password": "Enosis123",
            "ConfirmPassword": "Enosis123",
            "RoleId": "0"
        }

        print("querystring :" + querystring['invitationId'])

        response = request(method="POST", url=self.accept_invitation_url, data=payload, headers=self.header, params=querystring)

        print(response.text)

        return code_match_update_checklist(str(response.status_code), self.accept_invitation_access, self.sheet_name, sn)

    def get_invitation(self, invitation_id, sn=None):
        """
        @desc - Get invitation by id
        :param - invitation_id: Id of the invitation
        :param sn: TC Serial Number
        
        """
        self.log.info("Endpoint: get_invitation by invitation id has been called")

        querystring = {'id': invitation_id}

        response = request(method="GET", url=self.get_invitation_url, headers=self.header, params=querystring)

        print(response.text)
        code_match_update_checklist(str(response.status_code), self.get_invitation_access, self.sheet_name, sn)

    def get_invitations(self, active_only, sn=None):
        """
        @desc - Get list of invitations. Returns all invitations for a facility
        :param - active_only: Optional. True if only active invitation are needed false otherwise
        :param sn: TC Serial Number
        """
        self.log.info("Endpoint: get_invitations has been called")

        querystring = ""
        if active_only is not None:
            if active_only == "True":
                querystring = {'activeOnly': "true"}
            elif active_only == "False":
                querystring = {'activeOnly': "false"}
            else:
                raise AssertionError("activeOnly value has to be either true or false")

        response = request(method="GET", url=self.get_invitations_url, headers=self.header, params=querystring)

        print(response.text)
        if sn is not None:
            code_match_update_checklist(str(response.status_code), self.get_invitations_access, self.sheet_name, sn)
        else:
            code_match(str(response.status_code), self.get_invitations_access)
        return response

    def get_does_invited_user_exist(self, inv_id, sn=None):
        """
        @desc - Indicates if the invitation is for an existing user
        :param - inv_id: id of the invitation
        :param sn: TC Serial Number
        
        """
        self.log.info("Endpoint: get_does_invited_user_exist has been called")

        querystring = {'id': inv_id}

        response = request(method="GET", url=self.does_invited_user_exist_url, headers=self.header, params=querystring)

        print(response.text)
        code_match_update_checklist(str(response.status_code), self.does_invited_user_exist_access, self.sheet_name, sn)

    def get_is_invitation_expired(self, invitation_id, sn=None):
        """
        @desc - Indicates if the invitation is expired or not
        :param - invitation_id: id of the invitation
        :param sn: TC Serial Number
        
        """
        self.log.info("Endpoint: get_is_invitation_expired has been called")

        querystring = {'id': invitation_id}

        response = request(method="GET", url=self.is_invitation_expired_url, headers=self.header, params=querystring)

        print(response.text)
        code_match_update_checklist(str(response.status_code), self.is_invitation_expired_access, self.sheet_name, sn)

    def delete_facility(self, fac_id, sn=None):
        """
        @desc - Delete facility by id
        :param - fac_id: id of facility
        :param sn: TC Serial Number
        
        """
        self.log.info("Endpoint: delete_facility has been called")

        if fac_id.lower() == init.mobile_med_tek_facility_id.lower():
            raise AssertionError("Cannot delete MobileMedTek facility")

        if self.facility_id.lower() == init.mobile_med_tek_facility_id.lower():
            raise AssertionError("Facility ID is set to MMT fac ID. Cannot delete this facility")

        if fac_id is None:
            fac_id = self.facility_id

        url = self.delete__url + fac_id

        response = request(method="DELETE", url=url, headers=self.header)

        print(response.text)

        code_match_update_checklist(str(response.status_code), self.delete__access, self.sheet_name, sn)

    def get_facility_by_id_tc(self,fac_id, sn=None):
        """
        @desc - This method gets a facility with facility id
        :param fac_id: fac_id
        :param sn: TC Serial Number
        :returns
        """
        response = self.get_facility_by_id(fac_id, sn)

        if str(response.status_code) == "200" and fac_id == init.mobile_med_tek_facility_id:
            json_response = response.json()
            self.mmt_facility_name = json_response['Name']
            self.mmt_facility_domain = json_response['Domain']
            self.mmt_local_storage_days = json_response['LocalStorageDays']
            self.mmt_date_created = json_response['DateCreated']

    def get_facility_by_id(self, fac_id, sn=None):
        """
        @desc - Gets a facility by ID. Facility admins are only allowed to get facilities that they are an admin of
        :param - fac_id: id of facility
        :param sn: TC Serial Number
        
        """
        self.log.info("Endpoint: get_facility_by_id has been called")

        if fac_id is None:
            fac_id = self.facility_id

        url = self.get_by_id_url + fac_id

        response = request(method="GET", url=url, headers=self.header)

        print(response.text)
        if sn is not None:
            code_match_update_checklist(str(response.status_code), self.get_by_id_access, self.sheet_name, sn)
        else:
            code_match(str(response.status_code), self.get_by_id_access)
        return response

    def patch_facility_id(self, fac_id, sn=None):
        """
        @desc - Updates facility info
        :param - fac_id: id of the facility
        :param sn: TC Serial Number
        
        """
        self.log.info("Endpoint: patch_facility has been called")

        url = self.patch_url + fac_id

        payload = {
            "ID": fac_id,
            "Name": "FAC_edit",
            "Domain": "Domain_edit_"+get_formatted_date(),
            "ArchiveLocation": "",
            "LocalStorageDays": 30,
            "UsesCloudStorage": "true",
            "DisableAutoArchive": "false",
            "StudyNotesTemplate": "",
            "VideoFileLengthOptionID": 1
        }

        response = request(method="PATCH", url=url, data=dict_to_json(payload), headers=self.header)

        print(response.text)
        code_match_update_checklist(str(response.status_code), self.patch_access, self.sheet_name, sn)

    def put_facility_id(self, fac_id, sn=None):
        """
        @desc - Adds or updates a facility. The facility admin can only update their own facility
        :param - fac_id: id of the facility
        :param sn: TC Serial Number
        
        """
        self.log.info("Endpoint: put_facility has been called")

        url = self.put_url + fac_id

        payload = {
            "ID": fac_id,
            "Name": "Fac_name_edit",
            "Domain": "Fac_dom_"+get_formatted_date(),
            "ArchiveLocation": "",
            "LocalStorageDays": 30,
            "UsesCloudStorage": "true",
            "DisableAutoArchive": "false",
            "StudyNotesTemplate": "",
            "VideoFileLengthOptionID": 1
        }

        response = request(method="PUT", url=url, data=payload, headers=self.header)

        print(response.text)
        code_match_update_checklist(str(response.status_code), self.put_access, self.sheet_name, sn)

    class FacilitiesControllerTCs(Exception):
        pass
