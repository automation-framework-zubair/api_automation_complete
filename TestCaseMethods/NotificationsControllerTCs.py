import os
import sys
import datetime
import time

lib_path2 = os.path.abspath(os.path.join('..', '..', 'libraries'))
sys.path.insert(0, lib_path2)
import conf as init

lib_path = os.path.abspath(os.path.join('..', '..', 'libraries', 'common'))
sys.path.insert(0, lib_path)
from logger_robot import Logger as logger_extended
from assertion import code_match, code_match_update_checklist
from request import request

lib_path1 = os.path.abspath(os.path.join('..', '..', 'libraries', 'Rendr'))
sys.path.insert(0, lib_path1)
from TestDataProvider import TestDataProvider
from application_util import headers, dict_to_json


class NotificationsControllerTCs(object):
    ROBOT_LIBRARY_SCOPE = 'TEST SUITE'
    log = logger_extended().get_logger('API_Automation')

    data = TestDataProvider()

    user_token = ""
    header = {}

    sheet_name = "Notifications"

    # URLS
    url = init.url
    share_study_url = ""
    share_study_event_url = ""
    notify_study_submitted_url = ""
    get_shareable_users_and_groups_url = ""
    get_submittable_users_and_groups_url = ""

    # Access information
    share_study_access = ""
    share_study_event_access = ""
    notify_study_submitted_access = ""
    get_shareable_users_and_groups_access = ""
    get_submittable_users_and_groups_access = ""

    con_name = ""
    mmt_facility_id = init.mobile_med_tek_facility_id

    def __init__(self):
        self._expression = ''
        self.con_name = "NotificationsController"
        self.log.info(self.con_name + " class has been initialized")
        self.share_study_url = self.url + self.data.get_url_of_controller(self.con_name, "ShareStudy")
        self.share_study_event_url = self.url + self.data.get_url_of_controller(self.con_name, "ShareStudyEvent")
        self.notify_study_submitted_url = self.url + self.data.get_url_of_controller(self.con_name,
                                                                                     "NotifyStudySubmitted")
        self.get_shareable_users_and_groups_url = self.url + self.data.get_url_of_controller(
            self.con_name, "GetShareableUsersAndGroups")
        self.get_submittable_users_and_groups_url = self.url + self.data.get_url_of_controller(
            self.con_name, "GetSubmittableUsersAndGroups")

    def initialize_access_for_notifications_controller(self, role):
        list_of_roles = ['super admin', 'support', 'production', 'facility admin', 'review doctor', 'lead tech',
                         'field tech', 'office personnel']

        if role in list_of_roles:
            self.log.info("Gathering access info for " + role + " role")
            self.share_study_access = self.data.get_access_status_of_controller(
                self.con_name, "ShareStudy", role)
            self.share_study_event_access = self.data.get_access_status_of_controller(
                self.con_name, "ShareStudyEvent", role)
            self.notify_study_submitted_access = self.data.get_access_status_of_controller(
                self.con_name, "NotifyStudySubmitted", role)
            self.get_shareable_users_and_groups_access = self.data.get_access_status_of_controller(
                self.con_name, "GetShareableUsersAndGroups", role)
            self.get_submittable_users_and_groups_access = self.data.get_access_status_of_controller(
                self.con_name, "GetSubmittableUsersAndGroups", role)
        else:
            raise AssertionError("Please provide a valid role name")

    def set_token_notifications(self, token):

        """
        @desc - method to set token of the user
        :param - token: user's token
        :return:
        """
        self.log.info("Current user's token : " + token)
        self.header = headers(token)

    def share_study(self, study_id, user_id, sn=None):
        """
        @desc - Post share study endpoint
        :param study_id: id of the study to share
        :param user_id: receiver user id
        :param sn: TC Serial Number
        """
        self.log.info("Endpoint: share_study has been called")

        payload = {
            "StudyId": study_id,
            "UserIds": [user_id]
        }
        response = request(method="POST", url=self.share_study_url, headers=self.header,
                           data=dict_to_json(payload))
        print(response.text)
        if sn is not None:
            code_match_update_checklist(str(response.status_code), self.share_study_access,
                                        self.sheet_name, sn)
        else:
            code_match(str(response.status_code), self.share_study_access)

    def share_study_event(self, study_id, study_event_id, user_id, sn=None):
        """
        @desc - Post share study event endpoint
        :param study_id: id of the study to share
        :param study_event_id: id of an event of the study to share
        :param user_id: receiver user id
        :param sn: TC Serial Number
        """
        self.log.info("Endpoint: share_study_event has been called")
        payload = {
            "StudyId": study_id,
            "StudyEventId": study_event_id,
            "UserIds": [user_id]
        }
        response = request(method="POST", url=self.share_study_event_url, headers=self.header,
                           data=dict_to_json(payload))
        print(response.text)
        code_match_update_checklist(str(response.status_code), self.share_study_event_access,
                                    self.sheet_name, sn)

    def notify_study_submitted(self, study_id, user_id, user_email, sn=None):
        """
        @desc - Post notify study submitted endpoint
        :param study_id: id of the study to share
        :param user_id: receiver user id
        :param user_email: receiver user email
        :param sn: TC Serial Number
        """
        self.log.info("Endpoint: notify_study_submitted has been called")

        payload = {
            "StudyId": study_id,
            "UserIds": [user_id],
            "Emails": [user_email]
        }
        response = request(method="POST", url=self.notify_study_submitted_url, headers=self.header,
                           data=dict_to_json(payload))
        print(response.text)
        code_match_update_checklist(str(response.status_code), self.notify_study_submitted_access,
                                    self.sheet_name, sn)

    def get_shareable_users_and_groups(self, facility_id, sn=None):
        """
        @desc - Get sharable users and groups endpoint
        :param facility_id:
        :param sn: TC Serial Number
        """
        self.log.info("Endpoint: get_shareable_users_and_groups has been called")
        if facility_id is None:
            facility_id = self.mmt_facility_id

        querystring = {'facilityId': facility_id}

        response = request(method="GET", url=self.get_shareable_users_and_groups_url, headers=self.header,
                           params=querystring)
        print(response.text)

        code_match_update_checklist(str(response.status_code), self.get_shareable_users_and_groups_access,
                                    self.sheet_name, sn)

    def get_submittable_users_and_groups(self, facility_id, sn=None):
        """
        @desc - get submittable user and groups endpoint
        :param facility_id:
        :param sn: TC Serial Number
        """
        self.log.info("Endpoint: get_submittable_users_and_groups has been called")
        if facility_id is None:
            facility_id = self.mmt_facility_id

        querystring = {'facilityId': facility_id}

        response = request(method="GET", url=self.get_submittable_users_and_groups_url, headers=self.header,
                           params=querystring)
        print(response.text)

        code_match_update_checklist(str(response.status_code), self.get_submittable_users_and_groups_access,
                                    self.sheet_name, sn)

    class NotificationsControllerTCs(Exception):
        pass
