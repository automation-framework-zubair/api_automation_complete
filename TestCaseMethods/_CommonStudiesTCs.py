import os
import sys

lib_path = os.path.abspath(os.path.join('..', '..', 'TestCaseMethods'))
sys.path.insert(0, lib_path)
from PatientsControllerTCs import PatientsControllerTCs
from StudiesControllerTCs import StudiesControllerTCs
from NotificationsControllerTCs import NotificationsControllerTCs
from StudyEventTypesControllerTCs import StudyEventTypesControllerTCs
from StudyEventsControllerTCs import StudyEventsControllerTCs

lib_path1 = os.path.abspath(os.path.join('..', '..', 'libraries', 'Common'))
sys.path.insert(0, lib_path1)
from logger_robot import Logger as logger_extended

lib_path2 = os.path.abspath(os.path.join('..', '..', 'libraries', 'Rendr'))
sys.path.insert(0, lib_path2)
from TestDataProvider import TestDataProvider

lib_path3 = os.path.abspath(os.path.join('..', '..', 'libraries'))
sys.path.insert(0, lib_path3)
import conf as init


class _CommonStudiesTCs(object):
    ROBOT_LIBRARY_SCOPE = 'TEST SUITE'
    log = logger_extended().get_logger('API_Automation')
    data = TestDataProvider()

    patient = PatientsControllerTCs()
    studies = StudiesControllerTCs()
    notification = NotificationsControllerTCs()
    study_event_types = StudyEventTypesControllerTCs()
    study_events = StudyEventsControllerTCs()

    token_super_admin = ""
    token_support = ""
    token_production = ""
    token_facility_admin = ""
    token_review_doctor = ""
    token_lead_tech = ""
    token_field_tech = ""
    token_office_personal = ""

    super_admin_email = ""
    support_email = ""
    production_email = ""
    facility_admin_email = ""
    review_doctor_email = ""
    lead_tech_email = ""
    field_tech_email = ""
    office_personnel_email = ""
    password = ""

    # Variables for Studies user
    test_share_study_id = ""
    test_study_id = ""
    test_upload_study_id = ""
    test_recording_index = ""
    test_camera_index = ""
    test_video_index = ""
    test_ext = ""
    test_created_study_date = ""
    test_study_state = ""
    test_amplifier_id = ""
    test_sync_state = ""
    test_packet_count = ""
    test_study_event_type_id = ""
    test_event_id = ""

    def __init__(self):
        self._expression = ''

        self.super_admin_email = self.data.get_rendr_user_email("Super Admin")
        self.support_email = self.data.get_rendr_user_email("Support")
        self.production_email = self.data.get_rendr_user_email("Production")
        self.facility_admin_email = self.data.get_rendr_user_email("Facility Admin")
        self.review_doctor_email = self.data.get_rendr_user_email("Review Doctor")
        self.lead_tech_email = self.data.get_rendr_user_email("Lead Tech")
        self.field_tech_email = self.data.get_rendr_user_email("Field Tech")
        self.office_personnel_email = self.data.get_rendr_user_email("Office Personnel")

        self.password = self.data.get_common_password()

        self.test_super_admin_email = self.data.get_test_user_email("Super Admin")
        self.test_support_email = self.data.get_test_user_email("Support")
        self.test_production_email = self.data.get_test_user_email("Production")
        self.test_facility_admin_email = self.data.get_test_user_email("Facility Admin")
        self.test_review_doctor_email = self.data.get_test_user_email("Review Doctor")
        self.test_lead_tech_email = self.data.get_test_user_email("Lead Tech")
        self.test_field_tech_email = self.data.get_test_user_email("Field Tech")
        self.test_office_personnel_email = self.data.get_test_user_email("Office Personnel")

    def set_common_studies_token(self, token):
        """
        @desc - method to set token of the user
        :param - token: user's token
        """
        self.token_super_admin = token

    def get_random_test_study_id(self):
        """
        @desc - get study random study id and set recording index and packet count for that study
        """
        if self.token_super_admin is None:
            raise AssertionError("Login with Super Admin first to set the token.")
        else:
            token = self.token_super_admin

        self.studies.initialize_access_for_studies_controller(role='super admin')
        self.studies.set_token_studies(token=token)
        response = self.studies.get_studies()
        json = response.json()
        is_found = 0

        for each in range(len(json)):
            if is_found == 1:
                break
            if json[each]['SyncInfo']['SyncState'] == 1 and len(json[each]['SyncInfo']['StudyRecordings']) > 0:
                for data in range(len(json[each]['SyncInfo']['StudyRecordings'])):
                    if (json[each]['SyncInfo']['StudyRecordings'][data]['SyncState'] == 1) and (
                            10000 < json[each]['SyncInfo']['StudyRecordings'][data]['PacketCount'] < 20000):
                        print("INDEX -> " + str(json[each]['SyncInfo']['StudyRecordings'][data]['Index']))
                        self.test_recording_index = str(json[each]['SyncInfo']['StudyRecordings'][data]['Index'])
                        print("STUDY ID -> " + json[each]['SyncInfo']['StudyRecordings'][data]['StudyId'])
                        self.test_study_id = json[each]['SyncInfo']['StudyRecordings'][data]['StudyId']
                        print("PACKET COUNT -> " + str(
                            json[each]['SyncInfo']['StudyRecordings'][data]['PacketCount']))
                        self.test_packet_count = str(
                            json[each]['SyncInfo']['StudyRecordings'][data]['PacketCount'])
                        is_found = 1
                        break
        if is_found == 1:
            return self.test_study_id

    def get_video_study_id(self):
        """
        @desc - get study with video recording
        :return returns random study with video
        """
        if self.token_super_admin is None:
            raise AssertionError("Login with Super Admin first to set the token.")
        else:
            token = self.token_super_admin

        self.studies.initialize_access_for_studies_controller(role='super admin')
        self.studies.set_token_studies(token=token)

        response = self.studies.get_search_list_view("", False, False)
        test_study_id = ''
        json_response = response.json()
        length = len(json_response)
        for i in range(length):
            if str(json_response[i]['SyncInfo']['UseVideo'] == 'True') \
                    and str(json_response[i]['SyncInfo']['SyncState'] == '1'
                            and str(json_response[i]['DateDeleted']) == "null"):
                test_study_id = json_response[i]['ID']
                response2 = self.studies.get_study_view(test_study_id)
                json_response2 = response2.json()
                if len(json_response2['Study']['SyncInfo']['StudyRecordingVideos']) != 0 and \
                        str(json_response2['Study']['SyncInfo']['StudyRecordingVideos'][0]['SyncState']) == '1'\
                        and str(json_response2['Study']['StudyState']['ID']) == '2':
                    print(json_response2['Study']['StudyState']['ID'])
                    self.test_study_id = test_study_id
                    break
        print(self.test_study_id)
        return self.test_study_id

    def change_test_study_state(self, study_id):
        """
        @desc - Change test study state to pending review (study_state =3)
        :param study_id: ID of the study
        :return
        """
        if self.token_super_admin is None:
            raise AssertionError("Login with Super Admin first to set the token.")
        else:
            token = self.token_super_admin

        self.studies.initialize_access_for_studies_controller(role='super admin')
        self.studies.set_token_studies(token=token)
        self.studies.post_change_study_state(study_id=study_id, study_state=3)

    def get_test_study_information(self, study_id):
        """
        @desc - Method get the study's recording index, created date,
         amplifier id, sync state, study state, video index, camera index
        :param study_id: ID of the study
        :return
        """
        if self.token_super_admin is None:
            raise AssertionError("Login with Super Admin first to set the token.")
        else:
            token = self.token_super_admin

        self.studies.initialize_access_for_studies_controller(role='super admin')
        self.studies.set_token_studies(token=token)

        response = self.studies.get_studies_by_id(study_id)
        print(response)
        print(str(response.status_code))
        flag = 0
        if str(response.status_code) == '200':
            json_response = response.json()
            self.test_created_study_date = json_response['DateCreated']
            self.test_study_state = json_response['StudyState']['ID']
            self.test_amplifier_id = json_response['AmplifierID']
            self.test_sync_state = json_response['SyncState']
            for each in range(len(json_response['StudyRecordings'])):
                if json_response['StudyRecordings'][each]['Index']:
                    for data in range(len(json_response['StudyRecordings'][each]['Videos'])):
                        if json_response['StudyRecordings'][each]['Videos'][data]['VideoIndex'] and \
                                json_response['StudyRecordings'][each]['Videos'][data]['CameraIndex']:
                            self.test_recording_index = str(json_response['StudyRecordings'][each]['Index'])
                            print(self.test_recording_index)
                            self.test_video_index = \
                                str(json_response['StudyRecordings'][each]['Videos'][data]['VideoIndex'])
                            print(self.test_video_index)
                            self.test_camera_index = \
                                str(json_response['StudyRecordings'][each]['Videos'][data]['CameraIndex'])
                            print(self.test_camera_index)
                            flag = 1
                            break
                    if flag == 1:
                        break
            print("ID of the study selected: {}".format(study_id))
        else:
            print("Logged in user could not access the endpoint")

        if flag == 0:
            print("Could not find a study with video, camera index & video index! Something went wrong!")

    def create_test_study_event_type(self):
        """
        @desc - Method creates a study event type
        """
        if self.token_super_admin is None:
            raise AssertionError("Login with Super Admin first to set the token.")
        else:
            token = self.token_super_admin

        self.study_event_types.initialize_access_for_study_event_type_controller(role='super admin')
        self.study_event_types.set_token_study_event_types(token=token)

        response = self.study_event_types.post_study_event_types()

        if str(response.status_code) != "403":
            json_response = response.json()
            self.test_study_event_type_id = str(json_response['ID'])

        return self.test_study_event_type_id

    def delete_study_event_type(self, study_event_type_id):
        """
        @desc - Method deletes study event type
        @:param - study_event_type_id - created study event type id
        """
        if self.token_super_admin is None:
            raise AssertionError("Login with Super Admin first to set the token.")
        else:
            token = self.token_super_admin

        self.study_event_types.initialize_access_for_study_event_type_controller(role='super admin')
        self.study_event_types.set_token_study_event_types(token=token)

        self.study_event_types.delete_study_event_types(study_event_type_id)

    def get_study_event_id(self, study_id):
        """
        @desc - Method gets study event id
        @:param - study_id - study id
        """
        if self.token_super_admin is None:
            raise AssertionError("Login with Super Admin first to set the token.")
        else:
            token = self.token_super_admin

        self.study_events.initialize_access_for_study_events_controller(role='super admin')
        self.study_events.set_token_study_events(token=token)
        response = self.study_events.get_study_events_by_study_id(study_id)

        json = response.json()
        self.test_event_id = json[0]['ID']
        print("Event ID has been set to :" + self.test_event_id)
        return self.test_event_id

    def get_created_study_date(self):
        """
        @desc - Get test study's created date
        :return
        """
        return self.test_created_study_date

    def get_study_state(self):
        """
        @desc - Get test study dtate
        :return
        """
        return self.test_study_state

    def get_amplifier_id(self):
        """
        @desc - Get test study's amplifier ID
        :return
        """
        return self.test_amplifier_id

    def get_sync_state(self):
        """
        @desc - Get study sync state
        :return
        """
        return self.test_sync_state

    def get_recording_index(self):
        """
        @desc - Get test study's recording index
        :return
        """
        return self.test_recording_index

    def get_camera_index(self):
        """
         @desc - Get test study's camera index
        :return
        """
        return self.test_camera_index

    def get_video_index(self):
        """
        @desc - Get test study's video index
        :return
        """
        return self.test_video_index

    def get_packet_index(self):
        """
        @desc - Get test study's packet index
        :return
        """
        return self.test_packet_count

    def get_review_doctor_mail(self):

        return self.review_doctor_email

    def get_field_tech_mail(self):

        return self.field_tech_email

    def create_study_event(self, study_id, recording_index, start_packet_index):
        """
        @desc - create a study event
        :param start_packet_index:
        :param recording_index:
        :param study_id : test study id
        :return newly created study event id
        """
        if self.token_super_admin is None:
            raise AssertionError("Login with Super Admin first to set the token.")
        else:
            token = self.token_super_admin

        self.study_events.initialize_access_for_study_events_controller(role='super admin')
        self.study_events.set_token_study_events(token=token)
        print(int(start_packet_index))
        start_packet_index = str(int(start_packet_index) + 100)
        end_packet_index = str(int(start_packet_index) + 200)
        comment = "API_Automation_Comment"
        response = self.study_events.post_add_study_events(study_id, recording_index,
                                                           start_packet_index, end_packet_index, comment)

        if str(response.status_code) == "201":
            response_body_of_created_study_event = response.text
            json_response = response.json()
            created_event_id = json_response['ID']
            print(created_event_id)
            return created_event_id
        elif str(response.status_code) == "403":
            print("Logged in user could not create study event")
        else:
            raise AssertionError("Something went wrong. Event could not be created.")

    def share_test_study(self, study_id, user_id):
        """
        @desc - share the selected test study with patient
        :param study_id : test study id
        :param user_id : created test patient id
        """
        if self.token_super_admin is None:
            raise AssertionError("Login with Super Admin first to set the token.")
        else:
            token = self.token_super_admin

        self.notification.initialize_access_for_notifications_controller(role='super admin')
        self.notification.set_token_notifications(token=token)
        self.notification.share_study(study_id, user_id)

    class _CommonStudiesTCs(Exception):
        pass
