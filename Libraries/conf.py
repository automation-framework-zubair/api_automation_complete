# API SERVER URL
url = "https://napi-stagingn.rendrneuro.com"

# COMMON PASSWORD
password = "Enosis123"

# ID OF RENDR USER ROLES
super_admin_role = "907198ED-EFBD-4B48-ABE9-1C62C129D947"
support_role = "1139367A-07B8-46D3-A949-20DFE2B7E9AC"
production_role = "A247B200-ED53-4F9A-8449-4E56043A00C1"
facility_admin_role = "C94FD9B6-0C93-4901-91F7-764FEEE1FA21"
review_doctor_role = "289C85AC-F6CB-4DA5-9BA1-06A8E467821E"
lead_tech_role = "17950AA1-181D-4BA2-8FA5-CB8A91283B4B"
field_tech_role = "F6A4A81B-4BB0-452D-908C-5563D4A77CCE"
office_personnel_role = "EECD0CA1-5EAF-46CC-A3F1-7B90248F6CEF"

# IDs OF MOBILEMEDTEK SUPER ADMIN USER
default_super_admin_id = "2E449249-26B7-4DDB-8ED2-92E1655EA217"

# MOBILEMEDTEK FACILITY ID
mobile_med_tek_facility_id = "e3dd1742-335e-438e-8d8d-65df10447ea3"

# ENOSIS FACILITY ID
enosis_facility_id = "8f587102-afcd-4c26-850c-a9225948647a"

# DARK THEME ID
dark_theme_id = "595ee95c-815d-4d13-bc3b-8d255373a6cc"
# LIGHT THEME ID
light_theme_id = "64CCB76D-F480-4010-8E53-BAFD5A5FAF95"

# EMAIL BEING USED TO SEND FACILITY INVITATION
email_address_for_invitation = "shovan.ahmedzia@ymail.com"

# EMAIL AND PASSWORD FOR SENDING EMAIL WITH TEST REPORTS ZIPPED
user_email = 'enosis.mmt@gmail.com'
user_password = 'Admin1@3'

# REPORT EMAIL WILL BE SENT WITH FOLLOWING SUBJECT
subject_for_email = 'Test Reports of All the Controllers'

# LIST OF THE RECIPIENTS WHO WILL GET REPORT EMAIL
recipients = [
                'islamuddin.ahmed@enosisbd.com',
                'naeer.amin@enosisbd.com',
                'maliha.aurini@enosisbd.com',
                'zubair.alam@enosisbd.com'
            ]

# INFO RELATED TO CSVs
api_access_csv_source = "../../DataSource/AccessToAPIs/"
user_credentials = "../../DataSource/UserCredentials/"

# Staging DB Credentials
server_name = "db-StagingN.rendrneuro.com,61433"
db_name = "ETstagingn"
db_user_name = "etstagingnuser"
db_password = "tMbG#|hSxR!J6hL^"

# Google Sheet Access
sheet_name = "API AUTOMATION CHECKLIST"
