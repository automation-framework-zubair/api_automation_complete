import os
import sys
import datetime
import gspread
from oauth2client.service_account import ServiceAccountCredentials

lib_path = os.path.abspath(os.path.join('..', 'Libraries'))
sys.path.insert(0, lib_path)
import conf as init

# define the scope
scope = ['https://spreadsheets.google.com/feeds','https://www.googleapis.com/auth/drive']

# add credentials to the account
creds = ServiceAccountCredentials.from_json_keyfile_name('../../client_id.json', scope)

# authorize the clientsheet
client = gspread.authorize(creds)

# get the instance of the Spreadsheet
sheet = client.open(init.sheet_name)
# sheet = client.open("API AUTOMATION CHECKLIST")


def set_version_number(build):
    sh = sheet.worksheet("API_AUTOMATION_Cover")
    cl = sh.find("API Version:")
    print("Found something at Row %s Column %s" % (cl.row, cl.col))

    t = int(cl.col) + 3
    sh.update_cell(col=t, row=cl.row, value=build)

    d = int(cl.col) + 5
    x = datetime.datetime.now()

    sh.update_cell(col=d, row=cl.row, value=str(x.year) + "-" + str(x.month) + "-" + str(x.day))


def update_status(sheet_name, sl_no, result, code, access):

    sheet_instance = sheet.worksheet(sheet_name)
    cell = sheet_instance.find(sl_no)

    if cell is not None:
        print("Found something at Row %s Column %s" % (cell.row, cell.col))

        # access_col = sheet_instance.find("Result Summary")
        # col = int(access_col.col)

        col_result = int(cell.col) + 7
        col_code = int(cell.col) + 6
        col_access = int(cell.col) + 4

        sheet_instance.update_cell(col=col_result, row=cell.row, value=result)
        sheet_instance.update_cell(col=col_code, row=cell.row, value=code)
        sheet_instance.update_cell(col=col_access, row=cell.row, value=access)

    else:
        raise AssertionError("Serial Number not found")


def update_code(sheet_name, sl_no, code):

    sheet_instance = sheet.worksheet(sheet_name)
    cell = sheet_instance.find(sl_no)

    if cell is not None:
        print("Found something at Row %s Column %s" % (cell.row, cell.col))

        # access_col = sheet_instance.find("Found Code")
        # col = int(access_col.col)
        col = int(cell.col) + 6

        sheet_instance.update_cell(col=col, row=cell.row, value=code)

        print("For SN# "+sl_no+" the observed code has been set to "+sheet_instance.cell(col=col, row=cell.row).value)
    else:
        raise AssertionError("Serial Number not found")


def update_access(sheet_name, sl_no, access):

    sheet_instance = sheet.worksheet(sheet_name)
    cell = sheet_instance.find(sl_no)

    if cell is not None:
        print("Found something at Row %s Column %s" % (cell.row, cell.col))

        # access_col = sheet_instance.find("Has Access")
        # col = int(access_col.col)
        col = int(cell.col) + 4

        sheet_instance.update_cell(col=col, row=cell.row, value=access)

        print("For SN# "+sl_no+" the HAS ACCESS has been set to "+sheet_instance.cell(col=col, row=cell.row).value)
    else:
        raise AssertionError("Serial Number not found")
