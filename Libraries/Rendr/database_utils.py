import os
import sys
import pyodbc

lib_path2 = os.path.abspath(os.path.join('../..', '..', 'libraries'))
sys.path.insert(0, lib_path2)
import conf as init

lib_path1 = os.path.abspath(os.path.join('../..', '..', 'libraries', 'common'))
sys.path.insert(0, lib_path1)
from logger_robot import Logger as logger_extended
from request import my_timer

log = logger_extended().get_logger('API_Automation_DB_Access_Log')


@my_timer
def connect_to_db():
    try:
        conn = pyodbc.connect('Driver={SQL Server};'
                              'Server=' + init.server_name + ';'
                              'Database=' + init.db_name + ';'
                              'UID=' + init.db_user_name + ';'
                              'PWD=' + init.db_password + ';'
                              'Trusted_Connection=no;'
                              )

        log.info("Connected to " + init.db_name + " Database")
        return conn

    except (pyodbc.Error, pyodbc.OperationalError) as ex:
        log.warn("Could not connect to DB!")
        sqlstate = ex.args[1]
        log.warn(sqlstate)
        sqlstate = ex.args[0]
        if sqlstate == '28000':
            log.warn("DB Connection failed: check password")


@my_timer
def delete_user_by_email(connection, email):
    """
    @desc - Creates a new Device using Post method
    :param connection: Database connection object
    :param email: User email address
    """
    list_of_undeletable_emails = ['rendrsuper@yahoo.com',
                                  'rendrsupp.enosis@yahoo.com',
                                  'rendrprouser@yahoo.com',
                                  'rendr_facilityadm@yahoo.com',
                                  'rendrreviewdoctor@yahoo.com',
                                  'rendrleadtech@yahoo.com',
                                  'rendrfieldtech@yahoo.com',
                                  'rendrofficepersonnel@yahoo.com']

    if email in list_of_undeletable_emails:
        raise AssertionError(email + " was attempted to be deleted")

    user_id = ""

    if email is None or email == '':
        log.info("Email address is empty")
    else:
        cursor = connection.cursor()

        cursor.execute('Select ID from dbo.AspNetUsers where Email=?', email)

        if cursor.rowcount == 0:
            print("No user found with given email (" + email + ")")
            log.info("No user found with given email (" + email + ")")
        else:
            for row in cursor:
                print(row[0])
                user_id = row[0]

            cursor.execute('DELETE FROM dbo.FacilityInvitations WHERE invitedID = ?', user_id)
            cursor.execute('DELETE FROM dbo.FacilityUsers WHERE userid = ?', user_id)
            cursor.execute('DELETE FROM dbo.SharedStudies WHERE userid = ?', user_id)
            cursor.execute('DELETE FROM dbo.UserProfiles WHERE id = ?', user_id)
            cursor.execute('DELETE FROM dbo.AspNetUserRoles WHERE userid = ?', user_id)
            cursor.execute('DELETE FROM dbo.AspNetUsers WHERE id = ?', user_id)

            if cursor.rowcount == 1:
                print("User having email " + email + " has been deleted")
                log.info("User having email " + email + " has been deleted")
                connection.commit()
            else:
                log.warn("Something went wrong. User (ID: " + user_id + ") could not be deleted.")

    connection.close()


@my_timer
def delete_user_by_id(connection, id):
    """
    @desc - Creates a new Device using Post method
    :param connection: Database connection object
    :param id: ID of user
    """
    list_of_undeletable_emails = ['rendrsuper@yahoo.com',
                                  'rendrsupp.enosis@yahoo.com',
                                  'rendrprouser@yahoo.com',
                                  'rendr_facilityadm@yahoo.com',
                                  'rendrreviewdoctor@yahoo.com',
                                  'rendrleadtech@yahoo.com',
                                  'rendrfieldtech@yahoo.com',
                                  'rendrofficepersonnel@yahoo.com']

    user_id = ""
    if id is None or id == '':
        log.info("User id is empty")

    else:
        cursor = connection.cursor()

        cursor.execute('Select Email from dbo.AspNetUsers where ID=?', id)

        if cursor.rowcount != 0:
            for row in cursor:
                print(row[0])
                email_of_user = row[0]
                log.info("User's email is : " + email_of_user)
                if email_of_user in list_of_undeletable_emails:
                    raise AssertionError(email_of_user + " was attempted to be deleted")

        cursor.execute('Select ID from dbo.AspNetUsers where ID=?', id)

        if cursor.rowcount == 0:
            print("No user found with given id (" + id + ")")
            log.info("No user found with given id (" + id + ")")
        else:
            for row in cursor:
                print(row[0])
                user_id = row[0]

            cursor.execute('DELETE FROM dbo.FacilityInvitations WHERE invitedID = ?', user_id)
            cursor.execute('DELETE FROM dbo.FacilityUsers WHERE userid = ?', user_id)
            cursor.execute('DELETE FROM dbo.SharedStudies WHERE userid = ?', user_id)
            cursor.execute('DELETE FROM dbo.UserProfiles WHERE id = ?', user_id)
            cursor.execute('DELETE FROM dbo.AspNetUserRoles WHERE userid = ?', user_id)
            cursor.execute('DELETE FROM dbo.AspNetUsers WHERE id = ?', user_id)

            if cursor.rowcount == 1:
                print("User having ID " + id + " has been deleted")
                log.info("User having ID " + id + " has been deleted")
                connection.commit()
            else:
                log.warn("Something went wrong. User (ID: " + user_id + ") could not be deleted.")

    connection.close()


@my_timer
def delete_device_by_id(connection, device_id):
    """
    @desc - Deletes device by id from database
    :param connection: Database connection object
    :param device_id: ID of the device
    """
    cursor = connection.cursor()

    if device_id is None or device_id == '':
        log.info("Device ID is empty")
    else:
        cursor.execute('Select ID from dbo.Devices where ID=?', device_id)

        if cursor.rowcount == 0:
            print("No device found with given id (" + device_id + ")")
            log.info("No device found with given id (" + device_id + ")")
        else:
            cursor.execute('DELETE FROM dbo.FacilityNetworkCameras WHERE deviceid = ?', device_id)
            cursor.execute('DELETE FROM dbo.Devices WHERE id = ?', device_id)

            if cursor.rowcount == 1:
                print("Device having id " + device_id + " has been deleted")
                log.info("Device having id " + device_id + " has been deleted")
                connection.commit()
            else:
                log.warn("Something went wrong. Device (ID: " + device_id + ") could not be deleted.")

    connection.close()


@my_timer
def delete_facility_by_id(connection, facility_id):
    """
    @desc - Deletes facility using database
    :param connection: Database connection object
    :param facility_id: Facility ID
    """

    lnc_fac_id = "e3dd1742-335e-438e-8d8d-65df10447ea3"

    if facility_id is None or facility_id == '':
        log.info("Facility ID is empty")
    else:

        if facility_id == lnc_fac_id:
            log.warn("LNC Facility was tried to delete")
            raise AssertionError("LNC Facility cannot be deleted. Please check the provided Facility ID.")

        else:

            cursor = connection.cursor()
            cursor.execute('Select ID from dbo.Facilities where ID=?', facility_id)

            if cursor.rowcount == 0:
                print("No facility found with given id (" + facility_id + ")")
                log.info("No facility found with given id (" + facility_id + ")")
            else:
                cursor.execute('DELETE FROM dbo.FacilityUsers WHERE facilityid = ?', facility_id)
                cursor.execute('DELETE FROM dbo.aspnetusers WHERE selectedfacilityid = ?', facility_id)
                cursor.execute('DELETE FROM dbo.ControlSoftwareLogs WHERE facilityid = ?', facility_id)
                cursor.execute('DELETE FROM dbo.CalibrationPrograms WHERE facilityid = ?', facility_id)
                cursor.execute('DELETE FROM dbo.FacilityGroups WHERE facilityid = ?', facility_id)
                cursor.execute('DELETE FROM dbo.PhoticPrograms WHERE facilityid = ?', facility_id)
                cursor.execute('DELETE FROM dbo.Patients WHERE facilityid = ?', facility_id)

                cursor.execute('DELETE FROM dbo.StudyRecordingVideos WHERE facilityid = ?', facility_id)
                cursor.execute('DELETE FROM dbo.StudyRecordings WHERE facilityid = ?', facility_id)
                cursor.execute('DELETE FROM dbo.studies WHERE facilityid = ?', facility_id)

                cursor.execute('Select id from dbo.StudyRecordingMappings where facilityid=?', facility_id)
                rows = cursor.fetchall()
                for row in rows:
                    cursor.execute('DELETE FROM dbo.StudyRecordingMappingItems WHERE studyrecordingmappingid = ?',
                                   row.id)
                    cursor.execute('DELETE FROM dbo.StudyRecordingMappings WHERE id = ?', row.id)

                cursor.execute('Select id from dbo.Mappings where facilityid=?', facility_id)
                row = cursor.fetchone()
                if row:
                    cursor.execute('DELETE FROM dbo.MappingItems WHERE MappingID = ?', row.id)
                    cursor.execute('DELETE FROM dbo.Mappings WHERE id = ?', row.id)

                cursor.execute('Delete from dbo.EegThresholdSettings where facilityid=?', facility_id)

                cursor.execute('DELETE FROM dbo.FacilityNetworkCameras WHERE facilityid = ?', facility_id)
                cursor.execute('DELETE FROM dbo.facilities WHERE ID = ?', facility_id)

                if cursor.rowcount > 0:
                    print("Facility having id " + facility_id + " has been deleted")
                    log.info("Facility having id " + facility_id + " has been deleted")
                    connection.commit()
                else:
                    log.warn("Something went wrong. Facility (ID: " + facility_id + ") could not be deleted.")
    connection.close()


@my_timer
def delete_amplifier_by_id(connection, amplifier_id):
    """
    @desc - Deletes amplifier using database
    :param connection: Database connection object
    :param amplifier_id: Amplifier ID
    """

    if amplifier_id is None or amplifier_id == '':
        log.info("Amplifier ID is empty")
    else:
        cursor = connection.cursor()

        cursor.execute('Select ID from dbo.Amplifiers where ID=?', amplifier_id)

        if cursor.rowcount == 0:
            print("No Amplifier found with given id (" + amplifier_id + ")")
            log.info("No Amplifier found with given id (" + amplifier_id + ")")
        else:
            cursor.execute('DELETE FROM dbo.Amplifiers WHERE id = ?', amplifier_id)

            if cursor.rowcount == 1:
                print("Amplifier having id " + amplifier_id + " has been deleted")
                log.info("Amplifier having id " + amplifier_id + " has been deleted")
                connection.commit()
            else:
                log.warn("Something went wrong. Amplifier (ID: " + amplifier_id + ") could not be deleted.")

    connection.close()


@my_timer
def delete_patient_by_id(connection, patient_id):
    """
    @desc - Deletes a new Device using database
    :param connection: Database connection object
    :param patient_id: ID of the patient
    """

    if patient_id is None or patient_id == '':
        log.info("Patient ID is empty")
    else:
        cursor = connection.cursor()
        cursor.execute('Select ID from dbo.Patients where ID=?', patient_id)

        if cursor.rowcount == 0:
            print("No patient found with given id (" + patient_id + ")")
            log.info("No patient found with given id (" + patient_id + ")")
        else:
            cursor.execute('Select id from dbo.studies where PatientID=?', patient_id)
            rows = cursor.fetchall()
            for row in rows:
                print(row.id)
                cursor.execute('DELETE FROM dbo.StudyRecordingVideos WHERE StudyRecording_StudyID = ?', row.id)
                print("StudyRecordingVideos ? " + str(cursor.rowcount))
                cursor.execute('DELETE FROM dbo.StudyRecordings WHERE studyid = ?', row.id)
                print("StudyRecordings ? " + str(cursor.rowcount))
            cursor.execute('DELETE FROM dbo.Studies WHERE PatientID = ?', patient_id)
            print("Studies ? " + str(cursor.rowcount))

            cursor.execute('DELETE FROM dbo.Patients WHERE id = ?', patient_id)

            if cursor.rowcount == 1:
                print("Patient having id " + patient_id + " has been deleted")
                log.info("Patient having id " + patient_id + " has been deleted")
                connection.commit()
            else:
                log.warn("Something went wrong. Patient (ID: " + patient_id + ") could not be deleted.")

    connection.close()


@my_timer
def delete_theme_by_id(connection, theme_id):
    """
       @desc - Deletes theme by id
       :param theme_id: theme id
       :param connection: Database connection object
       """

    if theme_id is None or theme_id == '':
        log.info("Theme ID is empty")
    else:
        cursor = connection.cursor()
        cursor.execute('Select ID from dbo.EegThemes where ID=?', theme_id)

        if cursor.rowcount == 0:
            print("No Theme found with given id (" + theme_id + ")")
            log.info("No Theme found with given id (" + theme_id + ")")
        else:
            cursor.execute('DELETE FROM dbo.EegThemes WHERE ID=?', theme_id)

            if cursor.rowcount == 1:
                print("Theme having id " + theme_id + " has been deleted")
                log.info("Theme having id " + theme_id + " has been deleted")
                connection.commit()

            else:
                log.warn("Something went wrong. Theme (ID: " + theme_id + ") could not be deleted.")

    connection.close()


@my_timer
def get_share_study_id(connection, study_id, user_id):
    """
    @desc - get the newly shared study's shareStudyID from databse
    :param connection: Database connection object
    :param study_id: ID of the patient
    :param user_id: shared user's id
    """
    share_study_id = ''
    if study_id is None:
        log.info("Study ID is empty")
    else:
        cursor = connection.cursor()
        cursor.execute('Select ID from dbo.SharedStudies where StudyID=? and UserID=?', study_id, user_id)
        if cursor.rowcount == 0:
            print("No study found with given id (" + study_id + ")")
            log.info("No study found with given id (" + study_id + ")")
        else:
            cursor.execute('Select ID from dbo.SharedStudies where StudyID=? and UserID=?', study_id, user_id)
            rows = cursor.fetchall()
            for row in rows:
                share_study_id = row[0]
                print("share studyID ", row[0])

    connection.close()

    return share_study_id
