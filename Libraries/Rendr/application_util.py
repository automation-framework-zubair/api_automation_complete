import os, sys, json, datetime, time


def headers(token):
    """
    @desc - Embeds token in header and returns header as python dict.
    :param token
    """
    if len(token) < 10:
        raise AssertionError("Proper token was not found.")
    header = {
        'Authorization': "Bearer " + token,
        'Content-Type': "application/json",
        'Accept': "application/json, text/json",
        'cache-control': "no-cache"
    }
    return header


def dict_to_json(dictionary):
    """
    @desc - Converts python dictionary to JSON and returns as JSON
    :param dictionary
    """
    return json.dumps(dictionary)


def get_formatted_date():
    """
    @desc - To get the format-> 2019-05-26T05:58:24.566Z
    """
    ts = time.time()
    st1 = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d')
    st3 = datetime.datetime.fromtimestamp(ts).strftime('%H:%M:%S.%f')[:-3]

    formatted_date = st1 + "T" + st3 + "Z"
    return formatted_date
