import os
import sys

lib_path = os.path.abspath(os.path.join('../..', 'libraries'))
sys.path.insert(0, lib_path)

import requests
from functools import wraps
from logger_robot import Logger as logger_extended
import time

status = logger_extended().get_logger('API_Automation')


def my_timer(orig_func):
    """
    @desc - This is a python decorator to calculate the time taken to perform a method
    :param - orig_func: Name of the method
    :rtype : wrapper
    """

    @wraps(orig_func)
    def wrapper(*args, **kwargs):
        t1 = time.time()
        result = orig_func(*args, **kwargs)
        t2 = time.time() - t1
        print('{} ran in: {} sec'.format(orig_func.__name__, t2))
        status.info('{} executed in: {} sec'.format(orig_func.__name__, t2))
        return result

    return wrapper


@my_timer
def request(method, url, data=None, headers=None, params=None):
    """
    @desc - This is a custom request field to track the time required to perform the request.
    :param - method: Type of Request
    :param - url: URL to the Request
    :param - data: Payload
    :param - headers: Header elements
    :param - params: Query string (Optional)
    :rtype : response
    """
    status.info('METHOD: {}; URL : {}'.format(method, url))

    response = requests.request(method, url, data=data, headers=headers, params=params)
    print(response.status_code)
    return response


@my_timer
def request_session(method, url, data=None, headers=None, params=None):
    """
    @desc - This is a custom request field to track the time required to perform the request.
    :param - method: Type of Request
    :param - url: URL to the Request
    :param - data: Payload
    :param - headers: Header elements
    :param - params: Query string (Optional)
    :rtype : response
    """

    if method.lower() == "post":
        session = requests.Session()
        response = session.post(url, data=data, headers=headers, params=params)
        return response, session
    elif method.lower() == "get":
        session = requests.Session()
        response = session.get(url, data=data, headers=headers, params=params)
        return response, session
    else:
        raise AssertionError("Failed executing request_session with method=" + method + ", url=" + url
                             + ", data=" + str(data) + ", headers=" + str(headers) + ", params=" + str(params))
