from logger_robot import Logger as logger_extended

import os
import sys
import time

lib_path = os.path.abspath(os.path.join('../..', 'libraries/Rendr/'))
sys.path.insert(0, lib_path)
from spreadsheet import update_status, update_code, update_access

status = logger_extended().get_logger('API_Automation')


def code_match_update_checklist(found_code, target_code, sheet_name, serial_no):
    """
    @desc - The method verifies if the found code matches with the target code.
    :param found_code: Code that was returned from server
    :param target_code: Yes/No or pass the explicit code
    :param sheet_name: name of the sheet in the checklist where the results would display
    :param serial_no: SN of the Test Case
    :return:
    """
    if target_code in ('Yes', 'No', 'YES', 'NO', 'UNRESTRICTED', 'Unrestricted'):
        if target_code in ('Yes', 'YES', 'UNRESTRICTED', 'Unrestricted'):
            if found_code in ('200', '201', '204'):
                print("Returned status code: " + found_code + ". And the user has access to the Endpoint.")
                status.info('Returned Code:{} - PASSED'.format(found_code))
                update_status(sheet_name, serial_no, "Pass", found_code, "YES")
                return "success"
            elif found_code == "500":
                print('500: Returned.')
                status.warn('Returned Code:{} - Internal Server Error'.format(found_code))
                update_status(sheet_name, serial_no, "Pass", found_code, "YES")
                return "success"
            elif found_code == "404":
                print('404: Not Found.')
                status.warn('Returned Code:{} - NOT FOUND'.format(found_code))
                update_status(sheet_name, serial_no, "Pass", found_code, "YES")
                return "success"
            elif found_code == "400":
                print('400: Returned.')
                status.warn('Returned Code:{} '.format(found_code))
                update_status(sheet_name, serial_no, "Pass", found_code, "YES")
                return "success"
            elif found_code == "409":
                print('409: Returned. Conflict Occurred')
                status.warn('Returned Code:{} '.format(found_code))
                update_status(sheet_name, serial_no, "Pass", found_code, "YES")
                return "success"
            else:
                status.error('Returned Code:{} - FAILED'.format(found_code))
                if found_code == "401":
                    status.warn("401 found! Please check if the token passing was correct.")
                    update_status(sheet_name, serial_no, "Fail", found_code, "YES")
                update_status(sheet_name, serial_no, "Fail", found_code, "YES")
                raise AssertionError(
                    "Status Code : " + found_code + ". User's access to this Endpoint is: " + target_code)
        elif target_code in ('No', 'NO'):
            if found_code == "403":
                status.info('Returned Code:{} - PASSED and User does DOES NOT have access to the Endpoint'.format(found_code))
                update_status(sheet_name, serial_no, "Pass", found_code, "NO")
                return "success"
            elif found_code == "404":
                print('404: Not Found.')
                status.warn('Returned Code:{} - Not Found . And the user has NO access to the Endpoint. '
                            '403 should be returned.'.format(found_code))
                update_status(sheet_name, serial_no, "Pass", found_code, "NO")
                return "success"
            else:
                print("Returned status code: " + found_code + ". And the user has NO access to the Endpoint.")
                update_status(sheet_name, serial_no, "Fail", found_code, "NO")
                raise AssertionError("User does not have access to the Endpoint. But " + found_code + " was returned.")
        # time.sleep(10)
    else:
        raise AssertionError("PLEASE PROVIDE A VALID TARGET CODE.")


def code_match(found_code, target_code):
    """
    @desc - The method verifies if the found code matches with the target code.
    :param found_code: Code that was returned from server
    :param target_code: Yes/No or pass the explicit code
    :return:
    """
    if target_code in ('Yes', 'No', 'YES', 'NO', 'UNRESTRICTED', 'Unrestricted'):
        if target_code in ('Yes', 'YES', 'UNRESTRICTED', 'Unrestricted'):
            if found_code in ('200', '201', '204'):
                print("Returned status code: " + found_code + ". And the user has access to the Endpoint.")
                status.info('Returned Code:{} - PASSED'.format(found_code))
                return "success"
            elif found_code == "500":
                print('500: Returned.')
                status.warn('Returned Code:{} - Internal Server Error'.format(found_code))
                return "success"
            elif found_code == "404":
                print('404: Not Found.')
                status.warn('Returned Code:{} - NOT FOUND'.format(found_code))
                return "success"
            elif found_code == "400":
                print('400: Returned.')
                status.warn('Returned Code:{} '.format(found_code))
                return "success"
            elif found_code == "409":
                print('409: Returned. Conflict Occurred')
                status.warn('Returned Code:{} '.format(found_code))
                return "success"
            else:
                status.error('Returned Code:{} - FAILED'.format(found_code))
                if found_code == "401":
                    status.warn("401 found! Please check if the token passing was correct.")
                raise AssertionError(
                    "Status Code : " + found_code + ". User's access to this Endpoint is: " + target_code)
        elif target_code in ('No', 'NO'):
            if found_code == "403":
                status.info('Returned Code:{} - FAILED and User does DOES NOT have access to the Endpoint'.format(found_code))
                return "success"
            elif found_code == "404":
                print('404: Not Found.')
                status.warn('Returned Code:{} - Not Found . And the user has NO access to the Endpoint. '
                            '403 should be returned.'.format(found_code))
                return "success"
            else:
                print("Returned status code: " + found_code + ". And the user has NO access to the Endpoint.")
                raise AssertionError("User does not have access to the Endpoint. But " + found_code + " was returned.")

    else:
        raise AssertionError("PLEASE PROVIDE A VALID TARGET CODE.")


def is_equal(actual, expected):
    """
    @desc - This method is for checking equality of 'actual' value and 'expected' value
    :param - actual: actual value
    :param - expected: expected value
    :rtype : pass/fail
    """
    if actual == expected:
        print("'" + str(actual) + "' and '" + str(expected) + "' are equal")
        status.info('Equal Data:{} - PASSED'.format(actual))
        return "success"
    else:
        status.error('Unequal Data:{} - FAILED'.format(actual))
        raise AssertionError("'" + str(actual) + "' and '" + str(expected) + "' are not equal")


def is_attribute_present(attribute, data_content):
    """
    @desc - This method is for detecting whether the attribute is present in data content
    :param - attribute: Attribute that needs to be checked in data content
    :param - data_content: Data content in which attribute will be checked
    :rtype : True/False
    """
    if attribute in data_content:
        print("'" + attribute + "' attribute is present in data content")
        status.info('Attribute Present:{} - PASSED'.format(attribute))
        return True
    else:
        status.error('Attribute Absent:{} - FAILED'.format(attribute))
        raise AssertionError("'" + attribute + "' attribute is ABSENT in data content: \n" + str(data_content) + "\n")
