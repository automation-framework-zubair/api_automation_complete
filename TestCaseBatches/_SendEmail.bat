@echo off

set curTimestamp=%date:~-2,2%_%date:~-6,3%_%date:~-9,2%_%time:~0,2%_%time:~3,2%
echo %curTimestamp%

: Variables initialized to contain the path of the all merged reports of all the controllers
SET structuremod=..\TestReports\Reports_UsersController\_All_Merged
SET studyRecordingsmod=..\TestReports\Reports_StudyRecordingsController\_All_Merged
SET studyeventtypesmod=..\TestReports\Reports_StudyEventTypesController\_All_Merged
SET studyeventmod=..\TestReports\Reports_StudyEventsController\_All_Merged
SET studiesmod=..\TestReports\Reports_StudiesController\_All_Merged
SET patientmod=..\TestReports\Reports_PatientsController\_All_Merged
SET logscontrollermod=..\TestReports\Reports_LogsController\_All_Merged
SET notificationsmod=..\TestReports\Reports_NotificationsController\_All_Merged
SET facilityusersmod=..\TestReports\Reports_FacilityUsersController\_All_Merged
SET facilitypreferencesmod=..\TestReports\Reports_FacilityPreferencesController\_All_Merged
SET facilitiesmod=..\TestReports\Reports_FacilitiesController\_All_Merged
SET EegThemesmod=..\TestReports\Reports_EegThemesController\_All_Merged
SET Devicesmod=..\TestReports\Reports_DevicesController\_All_Merged
SET channeltypesmod=..\TestReports\Reports_ChannelTypesController\_All_Merged
SET Amplifiersmod=..\TestReports\Reports_AmplifiersController\_All_Merged
SET Auditlogsmod=..\TestReports\Reports_AuditLogsController\_All_Merged
SET Accountmod=..\TestReports\Reports_AccountController\_All_Merged

: Folder name that will contain the report of all the controllers
SET FILENAME=TestReportsOfAllControllers

: Making a direcotory to keep all the copied files
mkdir ..\%FILENAME%

:Setting the target location
SET targetLocation=..\%FILENAME%

: Initializing variables to hold the name of different folders which will contain the copied files
SET Usersfilename=_UsersControllerReport
SET StudyRecordingsfilename=_StudyRecordingsControllerReport
SET StudyEventTypesfilename=_StudyEventTypesControllerReport
SET StudyEventsfilename=_StudyEventsControllerReport
SET Studiesfilename=_StudiesControllerReport
SET Patientsfilename=_PatientsControllerReport
SET Notificationsfilename=_NotificationsControllerReport
SET Logscontrollerfilename=_LogsControllerReport
SET FacilityUsersfilename=_FacilityUsersControllerReport
SET FacilityPreferencesfilename=_FacilityPreferencesControllerReport
SET Facilitiesfilename=_FacilitiesControllerReport
SET EegThemesfilename=_EegThemesControllerReport
SET Devicesfilename=_DevicesControllerReport
SET ChannelTypesfilename=_ChannelTypesControllerReport
SET Amplifiersfilename=_AmplifiersControllerReport
SET Auditlogsfilename=_AuditlogsControllerReport
SET Accountfilename=_AccountControllerReport

: Making directories with the above initialized folder names
mkdir ..\%FILENAME%\%Usersfilename%
mkdir ..\%FILENAME%\%StudyRecordingsfilename%
mkdir ..\%FILENAME%\%StudyEventTypesfilename%
mkdir ..\%FILENAME%\%StudyEventsfilename%
mkdir ..\%FILENAME%\%Studiesfilename%
mkdir ..\%FILENAME%\%Patientsfilename%
mkdir ..\%FILENAME%\%Notificationsfilename%
mkdir ..\%FILENAME%\%FacilityUsersfilename%
mkdir ..\%FILENAME%\%Logscontrollerfilename%
mkdir ..\%FILENAME%\%FacilityPreferencesfilename%
mkdir ..\%FILENAME%\%Facilitiesfilename%
mkdir ..\%FILENAME%\%EegThemesfilename%
mkdir ..\%FILENAME%\%Devicesfilename%
mkdir ..\%FILENAME%\%ChannelTypesfilename%
mkdir ..\%FILENAME%\%Amplifiersfilename%
mkdir ..\%FILENAME%\%Auditlogsfilename%
mkdir ..\%FILENAME%\%Accountfilename%

:Looking for the most recent files in the directory
FOR /F "tokens=*" %%A IN ('DIR "%structuremod%\"  /B /AD-H /T:C /O:D') DO SET recentStructuremod=%%A
FOR /F "tokens=*" %%A IN ('DIR "%studyRecordingsmod%\"  /B /AD-H /T:C /O:D') DO SET recentstudyRecordingsmod=%%A
FOR /F "tokens=*" %%A IN ('DIR "%studyeventtypesmod%\"  /B /AD-H /T:C /O:D') DO SET recentStudyeventtypemod=%%A
FOR /F "tokens=*" %%A IN ('DIR "%studyeventmod%\"  /B /AD-H /T:C /O:D') DO SET recentStudyeventmod=%%A
FOR /F "tokens=*" %%A IN ('DIR "%studiesmod%\"  /B /AD-H /T:C /O:D') DO SET recentStudiesmod=%%A
FOR /F "tokens=*" %%A IN ('DIR "%patientmod%\"  /B /AD-H /T:C /O:D') DO SET recentpatientmod=%%A
FOR /F "tokens=*" %%A IN ('DIR "%notificationsmod%\"  /B /AD-H /T:C /O:D') DO SET recentnotificationsmod=%%A
FOR /F "tokens=*" %%A IN ('DIR "%logscontrollermod%\"  /B /AD-H /T:C /O:D') DO SET recentlogscontrollermod=%%A
FOR /F "tokens=*" %%A IN ('DIR "%facilityusersmod%\"  /B /AD-H /T:C /O:D') DO SET recentfacilityusersmod=%%A
FOR /F "tokens=*" %%A IN ('DIR "%facilitypreferencesmod%\"  /B /AD-H /T:C /O:D') DO SET recentfacilitypreferencesmod=%%A
FOR /F "tokens=*" %%A IN ('DIR "%facilitiesmod%\"  /B /AD-H /T:C /O:D') DO SET recentfacilitiesmod=%%A
FOR /F "tokens=*" %%A IN ('DIR "%EegThemesmod%\"  /B /AD-H /T:C /O:D') DO SET recentEegThemesmod=%%A
FOR /F "tokens=*" %%A IN ('DIR "%Devicesmod%\"  /B /AD-H /T:C /O:D') DO SET recentDevicesmod=%%A
FOR /F "tokens=*" %%A IN ('DIR "%channeltypesmod%\"  /B /AD-H /T:C /O:D') DO SET recentchanneltypesmod=%%A
FOR /F "tokens=*" %%A IN ('DIR "%Amplifiersmod%\"  /B /AD-H /T:C /O:D') DO SET recentAmplifiersmod=%%A
FOR /F "tokens=*" %%A IN ('DIR "%Auditlogsmod%\"  /B /AD-H /T:C /O:D') DO SET recentAuditlogsmod=%%A
FOR /F "tokens=*" %%A IN ('DIR "%Accountmod%\"  /B /AD-H /T:C /O:D') DO SET recentAccountmod=%%A
: FOR /F "delims=|" %%I IN ('DIR "%structuremod%\%recentStructuremod%\*.xml" /B /O:D') DO SET xmlStructuremod=%%I

: Copying the most recent file into the target location
copy "%structuremod%\%recentStructuremod%" "%targetLocation%\%Usersfilename%"
copy "%studyRecordingsmod%\%recentstudyRecordingsmod%" "%targetLocation%\%StudyRecordingsfilename%"
copy "%studyeventtypesmod%\%recentStudyeventtypemod%" "%targetLocation%\%StudyEventTypesfilename%"
copy "%studyeventmod%\%recentStudyeventmod%" "%targetLocation%\%StudyEventsfilename%"
copy "%studiesmod%\%recentStudiesmod%" "%targetLocation%\%Studiesfilename%"
copy "%patientmod%\%recentpatientmod%" "%targetLocation%\%Patientsfilename%"
copy "%logscontrollermod%\%recentlogscontrollermod%" "%targetLocation%\%Logscontrollerfilename%"
copy "%notificationsmod%\%recentnotificationsmod%" "%targetLocation%\%Notificationsfilename%"
copy "%facilityusersmod%\%recentfacilityusersmod%" "%targetLocation%\%FacilityUsersfilename%"
copy "%facilitypreferencesmod%\%recentfacilitypreferencesmod%" "%targetLocation%\%FacilityPreferencesfilename%"
copy "%facilitiesmod%\%recentfacilitiesmod%" "%targetLocation%\%Facilitiesfilename%"
copy "%EegThemesmod%\%recentEegThemesmod%" "%targetLocation%\%EegThemesfilename%"
copy "%Devicesmod%\%recentDevicesmod%" "%targetLocation%\%Devicesfilename%"
copy "%channeltypesmod%\%recentchanneltypesmod%" "%targetLocation%\%ChannelTypesfilename%"
copy "%Amplifiersmod%\%recentAmplifiersmod%" "%targetLocation%\%Amplifiersfilename%"
copy "%Auditlogsmod%\%recentAuditlogsmod%" "%targetLocation%\%Auditlogsfilename%"
copy "%Accountmod%\%recentAccountmod%" "%targetLocation%\%Accountfilename%"

PowerShell.exe -NoProfile -ExecutionPolicy Bypass -Command "& '%~dpn0.ps1'"

call python ..\Libraries\Rendr\send_email.py

echo deleting the generated files

del "..\%FILENAME%.zip" /s /f /q
rmdir "..\%FILENAME%" /q /s

pause